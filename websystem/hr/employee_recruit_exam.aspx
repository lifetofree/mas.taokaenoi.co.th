﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage_recurit.master" AutoEventWireup="true" CodeFile="employee_recruit_exam.aspx.cs" Inherits="websystem_employee_recruit_exam" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
    <title>แบบทดสอบ</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <script>
        $(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });
        //});

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepicker').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
            });
        });

    </script>
    <style type="text/css">
        .bg-logo {
            background-image: url('../../masterpage/images/hr/background_register_2.png');
            background-size: 100% 100%;
            width: 100%;
            height: 100%;
            text-align: center;
        }

        .imghead {
            margin: 0 auto;
            max-height: 500px;
            width: auto;
        }

        .btn-backtomain {
            padding: 5px 40px;
            display: table;
            background: #FCB326;
            margin: 0 auto;
            color: #fff;
            border-radius: 8px;
            font-size: 20px;
            cursor: pointer;
            box-shadow: 0px 4px #F99B27;
        }

        .img-h500 {
            max-height: 500px;
            width: auto;
            max-width: 100%;
        }

        .p-0 {
            padding: 0;
        }

        .rule-exam {
            text-indent: 1.5em;
            margin-top: 0;
            color: #808080;
        }

        .box-type-topic {
            background-color: #5CA2D0;
            border-radius: 3px;
            padding: 7px;
            border: 1px solid #DDDDDD;
            color: #fff;
        }

        .answer_choice input[type="radio"] {
            display: none;
        }

        .answer_choice label {
            cursor: pointer;
            width: 100%;
            border: 1px solid #ddd;
            padding: 3px;
            line-height: 1rem;
            border-radius: 4px;
            height: 40px;
            overflow: hidden;
            font-weight: 100;
            font-size: 0.8rem;
        }

            .answer_choice label:hover {
                border: 1px solid #ffa06a;
                background-color: #ffa06a0d;
            }

        .answer_choice input[type="radio"]:checked + label {
            border: 1px solid #35c695;
            background-color: #35c6950d;
        }

        .box-choice:nth-child(odd) {
            padding-right: 7px;
        }

        .box-choice:nth-child(even) {
            padding-left: 7px;
        }

        .panel-exam-main {
            border-color: #EC2839;
        }

            .panel-exam-main > .panel-heading {
                background-color: #EC2839;
                color: #fff;
            }

        textarea {
            width: 100%;
            margin-top: 7px;
            resize: none;
        }

        :focus {
            outline: unset;
        }

        html {
            scroll-behavior: smooth;
        }

        #mtoh {
            display: none;
            position: fixed;
            bottom: 20px;
            right: 10px;
            z-index: 99;
        }

        @media (max-width: 772px) {
            .box-choice {
                padding: 0 15px !important;
            }

            .sm-row {
                margin: 0 -15px;
            }

            .xs-mt-7 {
                margin-top: 7px;
            }
        }
    </style>


    <asp:Literal ID="txt" runat="server"></asp:Literal>
    <asp:Literal ID="litDebug" runat="server"></asp:Literal>
    <asp:HyperLink ID="setOntop" runat="server"></asp:HyperLink>

    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

        <asp:View ID="ViewIndex" runat="server">
            <div class="col-lg-12 bg-logo">

                <div class="col-sm-12">
                    <asp:Image ID="imghead" runat="server" class="img-responsive img-fluid" ImageUrl="~/images/recruit/Cover_Head_แบบทดสอบ_02-03.png" />
                </div>

                <div class="col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="background-color: burlywood; text-align: left">
                            <h2 class="panel-title"><i class="fa fa-question-circle"></i><strong>&nbsp; แบบทดสอบ</strong></h2>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">

                                <asp:GridView ID="GvTopic" runat="server"
                                    AutoGenerateColumns="false"
                                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                    HeaderStyle-CssClass="default small"
                                    HeaderStyle-Height="40px"
                                    AllowPaging="false"
                                    DataKeyNames="m0_toidx">

                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                    <EmptyDataTemplate>
                                        <div style="text-align: center">Data Cannot Be Found..</div>
                                    </EmptyDataTemplate>
                                    <Columns>

                                        <asp:TemplateField HeaderText="ฝ่าย" ItemStyle-Width="20%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <asp:Label ID="lbleveldevice" runat="server" Text='<%# Eval("DeptNameTH") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="แผนก" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <asp:Label ID="lbname" runat="server" Text='<%# Eval("SecNameTH") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ตำแหน่ง" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <asp:Label ID="lbuse_year" runat="server" Text='<%# Eval("PosNameTH") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ชุดคำถาม" ItemStyle-Width="20%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <asp:Label ID="Label1" runat="server" Visible="false" Text='<%# Eval("m0_toidx") %>'></asp:Label>
                                                <b>
                                                    <asp:Label ID="Label3" runat="server" Text="Link : "></asp:Label></b>
                                                <%--<asp:Label ID="lbpricenotshow" runat="server" Text='<%# Eval("topic_name") %>'></asp:Label>--%>
                                                <asp:LinkButton ID="btnlinkquest" runat="server" Font-Bold="true" OnCommand="btnCommand" CommandName="CmdDetail" ForeColor="#0066ff" Text='<%# Eval("topic_name") %>' CommandArgument='<%#  Eval("m0_toidx")+ ";" + "0"%>'></asp:LinkButton>

                                            </ItemTemplate>
                                        </asp:TemplateField>



                                    </Columns>
                                </asp:GridView>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:View>

        <asp:View ID="ViewDetailAdd" runat="server">

            <div class="col-lg-12 bg-logo">
                <div class="col-sm-12">
                    <asp:Image ID="Image1" runat="server" class="img-responsive img-fluid imghead" ImageUrl="~/images/recruit/Cover_Head_แบบทดสอบ_02-03.png" />
                </div>

                <div class="col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-heading" style="background-color: darksalmon; text-align: left">
                            <h2 class="panel-title"><i class="fa fa-question-circle"></i><strong>&nbsp; แบบทดสอบ</strong>
                                <asp:Label ID="lbltopic_name" Visible="false" runat="server" Font-Bold="true" Text='<%# Eval("topic_name") %>'></asp:Label>
                            </h2>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">


                                <asp:GridView ID="GvTypeAns" runat="server"
                                    AutoGenerateColumns="false"
                                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                    HeaderStyle-Height="40px"
                                    AllowPaging="false"
                                    DataKeyNames="m0_taidx"
                                    OnRowDataBound="Master_RowDataBound">

                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                    <EmptyDataTemplate>
                                        <div style="text-align: center">Data Cannot Be Found</div>
                                    </EmptyDataTemplate>
                                    <Columns>


                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="" ItemStyle-CssClass="text-left">
                                            <ItemTemplate>

                                                <div class="alert-message alert-message-success">
                                                    <blockquote class="danger" style="background-color: #cce6ff;">
                                                        <h4><b>
                                                            <i class="fa fa-edit"></i>&nbsp;
                                            <asp:Label ID="Label1" runat="server" Font-Bold="true" Text='<%# Eval("type_asn") %>'></asp:Label>
                                                            <asp:Label ID="lblm0_toidx_type" Visible="false" runat="server" Font-Bold="true" Text='<%# Eval("m0_toidx") %>'></asp:Label>
                                                            <asp:Label ID="lblm0_taidx_type" Visible="false" runat="server" Font-Bold="true" Text='<%# Eval("m0_taidx") %>'></asp:Label>
                                                        </b></h4>
                                                    </blockquote>
                                                </div>

                                                <asp:Label ID="lblm0_taidx" runat="server" CssClass="col-sm-10" Visible="false" Text='<%# Eval("m0_taidx") %>'></asp:Label>

                                                <asp:GridView ID="GvQuestion" runat="server"
                                                    AutoGenerateColumns="false"
                                                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                                    HeaderStyle-CssClass="danger small"
                                                    HeaderStyle-Height="40px"
                                                    AllowPaging="false"
                                                    DataKeyNames="m0_quidx"
                                                    OnRowDataBound="Master_RowDataBound">

                                                    <PagerStyle CssClass="pageCustom" />
                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center">Data Cannot Be Found</div>
                                                    </EmptyDataTemplate>
                                                    <Columns>

                                                        <asp:TemplateField ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" ItemStyle-Width="5%" Visible="true" ItemStyle-Font-Size="Small">
                                                            <ItemTemplate>
                                                                <asp:HiddenField ID="hfM0NodeIDX" runat="server" Value='<%# Eval("unidx") %>' />
                                                                <asp:HiddenField ID="hfM0ActoreIDX" runat="server" Value='<%# Eval("acidx") %>' />
                                                                <asp:HiddenField ID="hfM0StatusIDX" runat="server" Value='<%# Eval("staidx") %>' />
                                                                <asp:HiddenField ID="hfM0Node_decision" runat="server" Value='<%# Eval("node_decision") %>' />

                                                                <asp:Label ID="lblm0_tqidx" Visible="false" runat="server" Text='<%# Eval("m0_tqidx") %>' />
                                                                <asp:Label ID="lblm0_toidx" Visible="false" runat="server" Text='<%# Eval("m0_toidx") %>' />
                                                                <asp:Label ID="lblm0_taidx" Visible="false" runat="server" Text='<%# Eval("m0_taidx") %>' />
                                                                <asp:Label ID="lblm0_quidx" Visible="false" runat="server" Text='<%# Eval("m0_quidx") %>' />
                                                                <asp:Label ID="lblu1_anidx" Visible="false" runat="server" Text='<%# Eval("u1_anidx") %>' />
                                                                <asp:Label ID="lblu0_anidx" Visible="false" runat="server" Text='<%# Eval("u0_anidx") %>' />

                                                                <asp:Label ID="lblno_choice" runat="server" Text='<%# Eval("no_choice") %>' />
                                                                &nbsp;
                                                                 <asp:Label ID="Label2" runat="server" Text='<%# Eval("quest_name") %>' />
                                                                <asp:Label ID="lblchoice1" Visible="false" runat="server" Text='<%# Eval("choice_a") %>'></asp:Label>
                                                                <asp:Label ID="lblchoice2" Visible="false" runat="server" Text='<%# Eval("choice_b") %>'></asp:Label>
                                                                <asp:Label ID="lblchoice3" Visible="false" runat="server" Text='<%# Eval("choice_c") %>'></asp:Label>
                                                                <asp:Label ID="lblchoice4" Visible="false" runat="server" Text='<%# Eval("choice_d") %>'></asp:Label>
                                                                <asp:Label ID="lblanswer" Visible="false" runat="server" Text='<%# Eval("answer") %>'></asp:Label>
                                                                <asp:Label ID="lbldetermine" Visible="false" runat="server" Text='<%# Eval("determine") %>'></asp:Label>
                                                                <br />
                                                                <br />
                                                                <asp:HyperLink runat="server" ID="images_q" CssClass="pull-letf" data-toggle="tooltip" title="View" data-original-title="" Target="_blank"></asp:HyperLink>

                                                                <br />
                                                                <asp:Panel runat="server" ID="panel_choice" Visible="false">
                                                                    <br />
                                                                    <asp:RadioButtonList ID="rdochoice" runat="server" CssClass="radio-list-inline-emps" RepeatDirection="Vertical">
                                                                    </asp:RadioButtonList>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6"
                                                                        ValidationGroup="saveanswer" runat="server" Font-Size="13px" ForeColor="Red" ErrorMessage="กรุณาเลือกคำตอบให้ครบถ้วน"
                                                                        Display="None" SetFocusOnError="true" ControlToValidate="rdochoice" />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server"
                                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator6" Width="160"
                                                                        PopupPosition="BottomLeft" />

                                                                </asp:Panel>

                                                                <br />
                                                                <asp:Panel runat="server" ID="panel_comment" Visible="false">
                                                                    <div class="form-group">
                                                                        <div class="col-sm-10">
                                                                            <asp:TextBox ID="txtquest" ValidationGroup="saveanswer" row="20" TextMode="MultiLine" runat="server" CssClass="form-control"></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="saveanswer" runat="server" Display="None" ControlToValidate="txtquest" Font-Size="11"
                                                                                ErrorMessage="กรุณากรอกคำตอบ"
                                                                                ValidationExpression="กรุณากรอกคำตอบ"
                                                                                SetFocusOnError="true" />
                                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator4" Width="160" />
                                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                                                ValidationGroup="saveanswer" Display="None"
                                                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                                                ControlToValidate="txtquest"
                                                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                                                SetFocusOnError="true" />

                                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />
                                                                        </div>

                                                                        <div id="div_score" visible=" false" runat="server">
                                                                            <div class="col-sm-1">
                                                                                <asp:TextBox ID="txtscore" ValidationGroup="AddScore" runat="server" Text='<%# Eval("score") %>' CssClass="form-control"></asp:TextBox>

                                                                                <asp:RequiredFieldValidator ID="RqRetxtprdddice22" ValidationGroup="AddScore" runat="server" Display="None"
                                                                                    ControlToValidate="txtscore" Font-Size="11"
                                                                                    ErrorMessage="กรุณากรอกจำนวน" />
                                                                                <asp:RegularExpressionValidator ID="Retsssxtprice22" runat="server" ValidationGroup="AddScore" Display="None"
                                                                                    ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                                                    ControlToValidate="txtscore"
                                                                                    ValidationExpression="^[0-9,.]{1,10}$" />
                                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqRetxtprdddice22" Width="160" />
                                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender223" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Retsssxtprice22" Width="160" />

                                                                            </div>
                                                                            <div class="col-sm-1">
                                                                                <asp:Label ID="lblkanan" Text="คะแนน" CssClass="col-sm-1 control-label" runat="server"></asp:Label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </asp:Panel>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                    </Columns>
                                                </asp:GridView>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                    </Columns>
                                </asp:GridView>

                                <div class="form-group" id="div_selecrpass" runat="server" visible="false">

                                    <div class="form-group">
                                        <asp:Label ID="Label67" CssClass="col-sm-5 control-label" runat="server" Text="สถานะการทดสอบ :" />

                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlpass" runat="server" CssClass="form-control" ValidationGroup="AddScore">
                                                <asp:ListItem Value="0" Text="กรุณาเลือกสถานะการทดสอบ"></asp:ListItem>
                                                <asp:ListItem Value="3" Text="ผ่านแบบทดสอบ"></asp:ListItem>
                                                <asp:ListItem Value="4" Text="ไม่ผ่านแบบทดสอบ"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="AddScore" runat="server" Display="None"
                                                ControlToValidate="ddlpass" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกสถานะการทดสอบ"
                                                ValidationExpression="กรุณาเลือกสถานะการทดสอบ" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="160" />

                                        </div>
                                    </div>


                                    <div class="form-group" id="div_call" runat="server">
                                        <asp:Label ID="Label9" CssClass="col-sm-5 control-label" runat="server" Text="สถานะอนุมัติ :" />

                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlconfirm" runat="server" CssClass="form-control" ValidationGroup="AddScore">
                                                <asp:ListItem Value="0" Text="กรุณาเลือกสถานะอนุมัติ"></asp:ListItem>
                                                <asp:ListItem Value="3" Text="นัดสัมภาษณ์"></asp:ListItem>
                                                <asp:ListItem Value="4" Text="ไม่นัดสัมภาษณ์"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ValidationGroup="AddScore" runat="server" Display="None"
                                                ControlToValidate="ddlconfirm" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกสถานะอนุมัติ"
                                                ValidationExpression="กรุณาเลือกสถานะอนุมัติ" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator5" Width="160" />

                                        </div>
                                    </div>
                                </div>


                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
                                    <ContentTemplate>
                                        <div class="form-group">
                                            <div class="col-sm-2 col-sm-offset-10">
                                                <asp:LinkButton ID="lbladd" ValidationGroup="saveanswer" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="btnsaveanswer" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                <asp:LinkButton ID="lbladdscore" CssClass="btn btn-success btn-sm" ValidationGroup="AddScore" Visible="false" data-toggle="tooltip" title="Save" runat="server" CommandName="btnsavescore" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check "></i> เสร็จสิ้น</asp:LinkButton>

                                                <asp:LinkButton ID="lblcancel" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancel" data-toggle="tooltip" title="Close" OnClientClick="return confirm('คุณต้องการยกเลิกการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-times"></i></asp:LinkButton>

                                            </div>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="lbladdscore" />
                                    </Triggers>
                                </asp:UpdatePanel>

                                <asp:Panel runat="server" ID="panel_log" Visible="false">
                                    <div class="col-lg-12">
                                        <div class="panel panel-info">
                                            <div class="panel-heading " style="text-align: left">
                                                <h3 class="panel-title"><i class="glyphicon glyphicon-eye-open"></i><strong>&nbsp; ประวัติดำเนินการ</strong></h3>
                                            </div>
                                            <div class="panel-body">

                                                <asp:Repeater ID="rptLog" runat="server">
                                                    <HeaderTemplate>
                                                        <div class="row">
                                                            <label class="col-sm-2 control-label"><small>วัน / เวลา</small></label>
                                                            <label class="col-sm-2 control-label"><small>ชื่อผู้ดำเนินการ</small></label>
                                                            <label class="col-sm-2 control-label"><small>สถานะผู้ดำเนินการ</small></label>
                                                            <label class="col-sm-2 control-label"><small>ดำเนินการ</small></label>
                                                            <label class="col-sm-2 control-label"><small>ผลการดำเนินการ</small></label>
                                                        </div>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <div class="row">
                                                            <span><small>
                                                                <asp:Label ID="Label2" CssClass="col-sm-2 control-label" runat="server" Text='<%# Eval("CreateDate") %>' /></small></span>

                                                            <span><small>
                                                                <asp:Label ID="Label5" CssClass="col-sm-2 control-label" runat="server" Text='<%# Eval("FullNameTH") %>' />
                                                            </small></span>
                                                            <span><small>
                                                                <asp:Label ID="Label6" CssClass="col-sm-2 control-label" runat="server" Text='<%# Eval("actor_name") %>' />
                                                            </small></span>
                                                            <span><small>
                                                                <asp:Label ID="Label7" CssClass="col-sm-2 control-label" runat="server" Text='<%# Eval("node_name") %>' /></small></span>
                                                            <span><small>
                                                                <asp:Label ID="Label8" CssClass="col-sm-2 control-label" runat="server" Text='<%# Eval("status_name") %>' /></small></span>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </div>
                                        </div>
                                    </div>

                                </asp:Panel>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </asp:View>

        <asp:View ID="ViewComplete" runat="server">
            <asp:UpdatePanel ID="update_complete" runat="server" UpdateMode="Always">
                <ContentTemplate>
                    <div class="col-lg-12">
                        <asp:Image ID="image" runat="server" Width="100%" Height="100%" ImageUrl="~/images/recruit/page_finished.jpg"></asp:Image>
                    </div>
                    <div class="col-lg-12">
                        <div class="col-lg-3">
                        </div>
                        <div class="col-lg-6" style="text-align: center">
                            <asp:ImageButton runat="server" ID="imgbtnhome" CommandName="btnhome" OnCommand="btnCommand" Width="30%" Height="30%" ImageUrl="~/images/recruit/btngohome.jpg" />

                        </div>
                        <div class="col-lg-3">
                        </div>
                    </div>


                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="imgbtnhome" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:View>

        <asp:View ID="ViewCheckResultHR" runat="server">
            <div class="row">
                <div class="col-lg-12 bg-logo">

                    <div class="col-sm-12">
                        <asp:Image ID="Image2" runat="server" class="img-responsive img-fluid imghead" ImageUrl="~/images/recruit/Cover_Head_ประเมินคะแนน_OK-02.png" />
                    </div>


                    <div class="col-sm-12" id="boxsearch" runat="server">
                        <div class="panel panel-default">
                            <div class="panel-heading" style="background-color: coral; text-align: left">
                                <h2 class="panel-title"><i class="fa fa-folder-open"></i><strong>&nbsp; ค้นหาผลการทำแบบทดสอบ</strong></h2>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">

                                        <%------------ Search Start----------------%>
                                        <div class="form-group">
                                            <asp:Label ID="Label20" runat="server" Text="ประเภทชุดคำถาม" CssClass="col-sm-2 control-label"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddltypequest" ValidationGroup="SaveTopic" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                    <asp:ListItem Value="0" Text="กรุณาเลือกประเภทคำถาม"></asp:ListItem>
                                                </asp:DropDownList>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator10" ValidationGroup="SaveDataSet" runat="server" Display="None"
                                                    ControlToValidate="ddltypequest" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกประเภทคำถาม"
                                                    ValidationExpression="กรุณาเลือกประเภทคำถาม" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender20" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator10" Width="160" />

                                            </div>
                                        </div>

                                        <div id="divchoosetype" runat="server" visible="false">
                                            <div class="form-group">
                                                <asp:Label ID="Label59" runat="server" Text="องค์กร" CssClass="col-sm-2 control-label"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddlOrg" ValidationGroup="SaveTopic" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                    </asp:DropDownList>
                                                    <%--                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" ValidationGroup="SaveDataSet" runat="server" Display="None"
                                                    ControlToValidate="ddlOrg" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกองค์กร"
                                                    ValidationExpression="กรุณาเลือกองค์กร" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender21" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator11" Width="160" />--%>
                                                </div>
                                                <asp:Label ID="Label4" runat="server" Text="ฝ่าย" CssClass="col-sm-3 control-label"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddlDep" ValidationGroup="SaveTopic" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                        <asp:ListItem Value="0">กรุณาเลือกฝ่าย...</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <%--     <asp:RequiredFieldValidator ID="RequiredFieldValidator12" ValidationGroup="SaveDataSet" runat="server" Display="None"
                                                    ControlToValidate="ddlDep" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกฝ่าย"
                                                    ValidationExpression="กรุณาเลือกฝ่าย" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender22" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator12" Width="160" />--%>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label19" runat="server" Text="แผนก" CssClass="col-sm-2 control-label"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddlSec" ValidationGroup="SaveTopic" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" runat="server" CssClass="form-control">
                                                        <asp:ListItem Value="0">กรุณาเลือกแผนก...</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <%--   <asp:RequiredFieldValidator ID="RequiredFieldValidator13" ValidationGroup="SaveDataSet" runat="server" Display="None"
                                                    ControlToValidate="ddlSec" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกแผนก"
                                                    ValidationExpression="กรุณาเลือกแผนก" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender23" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator13" Width="160" />--%>
                                                </div>
                                                <asp:Label ID="Label1" runat="server" Text="ตำแหน่ง" CssClass="col-sm-3 control-label"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddlPos" ValidationGroup="SaveTopic" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="0">กรุณาเลือกตำแหน่ง...</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <%--   <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="SaveDataSet" runat="server" Display="None"
                                                    ControlToValidate="ddlPos" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกตำแหน่ง"
                                                    ValidationExpression="กรุณาเลือกตำแหน่ง" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />--%>
                                                </div>

                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label10" runat="server" Text="ชุดคำถาม" CssClass="col-sm-2 control-label"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddltopic" ValidationGroup="SaveDataSet" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="0" Text="กรุณาเลือกชุดคำถาม"></asp:ListItem>
                                                </asp:DropDownList>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="SaveDataSet" runat="server" Display="None"
                                                    ControlToValidate="ddltopic" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกชุดคำถาม"
                                                    ValidationExpression="กรุณาเลือกชุดคำถาม" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator3" Width="160" />

                                            </div>
                                        </div>



                                        <div class="form-group">
                                            <asp:Label ID="AddStart" CssClass="col-sm-2 control-label" runat="server" Text="ระบุวันที่ค้นหา" />
                                            <div class="col-sm-2">
                                                <asp:DropDownList ID="ddlSearchDate" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                    <asp:ListItem Text="เลือกเงื่อนไข ...." Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="มากกว่า >" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="น้อยกว่า <" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="ระหว่าง <>" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-3 ">

                                                <div class='input-group date'>
                                                    <asp:TextBox ID="AddStartdate" runat="server" CssClass="form-control from-date-datepicker" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                    <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class='input-group date'>
                                                    <asp:TextBox ID="AddEndDate" runat="server" CssClass="form-control from-date-datepicker" Enabled="false" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                    <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <div class="col-sm-3 col-sm-offset-1">
                                                <asp:LinkButton ID="btnsearch" CssClass="btn btn-primary btn-sm" data-toggle="tooltip" title="Search" runat="server" CommandName="btnsearch" ValidationGroup="btnsearch" OnCommand="btnCommand"><i class="fa fa-search"></i> </asp:LinkButton>
                                                <asp:LinkButton ID="btnRefresh" CssClass="btn btn-primary btn-sm" data-toggle="tooltip" title="Refresh" runat="server" CommandName="cmdback" OnCommand="btnCommand"><i class="fa fa-refresh"></i></asp:LinkButton>
                                            </div>

                                        </div>


                                        <%------------- Search End-----------------%>

                                        <%------------ ViewIndex Start----------------%>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-heading" style="background-color: burlywood; text-align: left">
                                <h2 class="panel-title"><i class="fa fa-question-circle"></i><strong>&nbsp; ผลการทำแบบทดสอบ</strong></h2>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <asp:GridView ID="GvSumExam" runat="server"
                                        AutoGenerateColumns="false"
                                        CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                        HeaderStyle-CssClass="info small"
                                        HeaderStyle-Height="40px"
                                        AllowPaging="false"
                                        DataKeyNames="m0_toidx"
                                        OnRowDataBound="Master_RowDataBound">

                                        <PagerStyle CssClass="pageCustom" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                        <EmptyDataTemplate>
                                            <div style="text-align: center">Data Cannot Be Found</div>
                                        </EmptyDataTemplate>
                                        <Columns>

                                            <asp:TemplateField HeaderText="ข้อมูลเปิดรับสมัคร" ItemStyle-Width="25%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblunidx" Visible="false" runat="server" Text='<%# Eval("unidx") %>' />
                                                    <strong>
                                                        <asp:Label ID="Label60" runat="server"> ตำแหน่ง: </asp:Label></strong>
                                                    <asp:Literal ID="lbPosName" runat="server" Text='<%# Eval("P_Name") %>' />
                                                    <asp:Literal ID="lblPIDX" Visible="false" runat="server" Text='<%# Eval("PIDX") %>' />
                                                    <br />
                                                    <strong>
                                                        <asp:Label ID="Label61" runat="server"> รายละเอียด: </asp:Label></strong>
                                                    <asp:Label ID="lbdes" runat="server" Text='<%# Eval("P_Description") %>'></asp:Label>
                                                    <br />
                                                    <strong>
                                                        <asp:Label ID="lbdat" runat="server"> สถานที่ปฏิบัติงาน: </asp:Label></strong>
                                                    <asp:Label ID="lblocate" runat="server" Text='<%# Eval("P_Location") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ข้อมูลผู้ทำรายการ" ItemStyle-Width="20%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblnode_decision" Visible="false" runat="server" Text='<%# Eval("node_decision") %>' />

                                                    <strong>
                                                        <asp:Label ID="Labelna" runat="server"> ชื่อผู้ทำรายการ: </asp:Label></strong>
                                                    <asp:Literal ID="lbemp_name_th" runat="server" Text='<%# Eval("emp_name_th") %>' />
                                                    <br />
                                                    <strong>
                                                        <asp:Label ID="Labeldate" runat="server"> วันที่ทำรายการ: </asp:Label></strong>
                                                    <asp:Label ID="lbldatedoing" runat="server" Text='<%# Eval("datedoing") %>'></asp:Label>



                                                    <%--<asp:Label ID="lbemp_name_th" runat="server" Text='<%# Eval("emp_name_th") %>'></asp:Label>--%>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ข้อมูลชุดคำถามสำหรับสมัครงาน" ItemStyle-Width="20%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <%-- <asp:Label ID="lbtotal_score" runat="server" Text='<%# Eval("total_score") %>'></asp:Label>--%>
                                                    <strong>
                                                        <asp:Label ID="Label60de" runat="server"> ฝ่าย: </asp:Label></strong>
                                                    <asp:Literal ID="lbDeptNameTH" runat="server" Text='<%# Eval("DeptNameTH") %>' />
                                                    <br />
                                                    <strong>
                                                        <asp:Label ID="Label61se" runat="server"> แผนก: </asp:Label></strong>
                                                    <asp:Label ID="lbSecNameTH" runat="server" Text='<%# Eval("SecNameTH") %>'></asp:Label>
                                                    <br />
                                                    <strong>
                                                        <asp:Label ID="lbdatpo" runat="server"> ตำแหน่ง: </asp:Label></strong>
                                                    <asp:Label ID="lbPosNameTH" runat="server" Text='<%# Eval("PosNameTH") %>'></asp:Label>
                                                    <br />
                                                    <strong>
                                                        <asp:Label ID="Label4" runat="server"> ชื่อชุดคำถาม: </asp:Label></strong>
                                                    <asp:Label ID="lbtopic_name" runat="server" Text='<%# Eval("topic_name") %>'></asp:Label>
                                                    <asp:Label ID="lblm0_tqidx" runat="server" Visible="false" Text='<%# Eval("m0_tqidx") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="คะแนน" ItemStyle-Width="20%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblu0_anidx" Visible="false" runat="server" Text='<%# Eval("u0_anidx") %>'></asp:Label>
                                                    <strong>
                                                        <asp:Label ID="lbltot" runat="server"> คะแนนรวม: </asp:Label></strong>
                                                    <asp:Literal ID="lbtotal_score" runat="server" Text='<%# Eval("total_score") %>' />
                                                    <br />
                                                    <strong>
                                                        <asp:Label ID="lbltot1" runat="server"> คะแนนอัตนัย: </asp:Label></strong>
                                                    <asp:Literal ID="Literal8" runat="server" Text='<%# Eval("score_choice") %>' />
                                                    <br />
                                                    <strong>
                                                        <asp:Label ID="lbltot2" runat="server"> คะแนนปรนัย: </asp:Label></strong>
                                                    <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("score_comment") %>' />
                                                    <br />
                                                    <strong>
                                                        <asp:Label ID="Label11" runat="server"> สถานะแบบทดสอบ: </asp:Label></strong>
                                                    <b>
                                                        <asp:Label ID="lbldetermine" runat="server" Text='<%# Eval("determine") %>'></asp:Label></b>
                                                    <br />
                                                    <div id="div_showstatusinterview" runat="server">
                                                        <strong>
                                                            <asp:Label ID="Label12" runat="server"> สถานะตัดสินใจ: </asp:Label></strong>
                                                        <b>
                                                            <asp:Label ID="lblstatusinterview" runat="server" Text='<%# Eval("statusinterview") %>'></asp:Label></b>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="สถานะรายการ" ItemStyle-Width="20%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbStatusDoc" runat="server" Text='<%# Eval("StatusDoc") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-Width="5%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="btnlinkquest" runat="server" ToolTip="view" OnCommand="btnCommand" CommandName="CmdDetail_ViewScore" CssClass="btn btn-sm btn-info" CommandArgument='<%#  Eval("m0_toidx")+ ";" +  Eval("CEmpIDX")+ ";" +  Eval("u0_anidx")+ ";" +  Eval("unidx")+ ";" +  Eval("m0_tqidx")+ ";" +  Eval("acidx")%>'><i class="fa fa-book"></i></asp:LinkButton>

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>
                                    </asp:GridView>


                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </asp:View>

        <asp:View ID="ViewShowCompleteAgain" runat="server">
            <div class="col-lg-12">
                <div class="col-lg-12">
                    <asp:Image ID="image3" runat="server" Width="100%" Height="100%" ImageUrl="~/images/recruit/Cover_Head_ทำแบบสอบถามครบแล้ว_OK-04-1.jpg"></asp:Image>
                </div>
            </div>
            <br />
            <div class="col-lg-12">
                <div class="col-lg-3">
                </div>
                <div class="col-lg-6" style="text-align: center">
                    <asp:ImageButton runat="server" ID="imghomeagain" CommandName="btnhome" OnCommand="btnCommand" Width="30%" Height="30%" ImageUrl="~/images/recruit/btngohome.jpg" />
                    <%--<div class="btn-backtomain">กลับไปหน้าหลัก</div>--%>
                </div>
                <div class="col-lg-3">
                </div>
            </div>
        </asp:View>
        <asp:View ID="ViewExam" runat="server">

            <asp:LinkButton ID="backTomain"
                CssClass="btn btn-default"
                runat="server"
                CommandName="backTomain"
                OnCommand="btnCommand">
                                BackToHome
            </asp:LinkButton>
            <div class="col-sm-12 text-center p-0 ">
                <asp:Image ID="Image5" runat="server" class=" img-h500" ImageUrl="~/images/recruit/Cover_Head_แบบทดสอบ_02-03.png" />
            </div>

            <div class="col-sm-12 p-0 ">
                <div class="container p-0 sm-row ">
                    <div class="panel panel-default panel-exam-main">
                        <div class="panel-heading" id="homepage">
                            <div style="font-size: 1.3rem; line-height: initial;">แบบทดสอบสมัครงาน Examination</div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-12" style="font-size: 18px;">สวัสดี คุณ&nbsp;<span runat="server" id="t_name_user">.</span></div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <p class="rule-exam">กฎ ข้อบังคับในการทำข้อสอบ</p>
                                </div>

                            </div>
                            <div class="row">

                                <div class="col-sm-12">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">

                                        <asp:Repeater ID="Reptertopic" runat="server">
                                            <ItemTemplate>
                                                <li role="presentation" class=" <%# (Container.ItemIndex == 0) ? " active " : "" %>"><a href="#topic_<%#Eval("m0_toidx") %>" aria-controls="settings" role="tab" data-index=" <%# Container.ItemIndex %>" data-toggle="tab"><%#Eval("topic_name") %></a></li>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <asp:Repeater ID="ReptertopicTap" runat="server" OnItemDataBound="Boundtopicdata">
                                            <ItemTemplate>

                                                <div role="tabpanel" class="tab-pane <%# (Container.ItemIndex == 0) ? " active " : "" %>" id="topic_<%#Eval("m0_toidx") %>">
                                                    <div class="col-sm-12" style="padding-top: 7px;">
                                                        <asp:TextBox runat="server" ID="m0_toidx" Visible="false" Text='<%# Eval("m0_toidx") %>'></asp:TextBox>
                                                        <asp:Repeater ID="ChildRepeater" runat="server" OnItemDataBound="Boundtopictype">

                                                            <ItemTemplate>
                                                                <div class="row m-b-10">
                                                                    <asp:TextBox runat="server" ID="m0_toidx" Visible="false" Text='<%# Eval("m0_toidx") %>'></asp:TextBox>
                                                                    <asp:TextBox runat="server" ID="m0_taidx" Visible="false" Text='<%# Eval("m0_taidx") %>'></asp:TextBox>

                                                                    <div class="col-sm-12 box-type-topic">
                                                                        <div class=""><%#Eval("type_asn") %></div>
                                                                    </div>
                                                                    <div class="col-sm-12 m-t-10">
                                                                        <asp:Repeater ID="Repquestion" runat="server">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox runat="server" ID="m0_toidx" Visible="false" Text='<%# Eval("m0_toidx") %>'></asp:TextBox>
                                                                                <asp:TextBox runat="server" ID="m0_taidx" Visible="false" Text='<%# Eval("m0_taidx") %>'></asp:TextBox>
                                                                                <asp:TextBox runat="server" ID="m0_quidx" Visible="false" Text='<%# Eval("m0_quidx") %>'></asp:TextBox>

                                                                                <div class="row">
                                                                                    <div class="col-sm-12"><%# Container.ItemIndex+1 +". "+  Eval("quest_name") %></div>
                                                                                </div>
                                                                                <div class="row">

                                                                                    <div class="col-sm-12" runat="server" id="box_quest_radio" visible='<%# (Eval("m0_taidx").ToString() == "1" )?true:false %>'>
                                                                                        <div class="row">
                                                                                            <div class="col-xs-12 col-sm-6 box-choice">
                                                                                                <asp:RadioButton ID="ra_choice_a" runat="server" Text='<%# "a. "+ Eval("choice_a") %>' CssClass="answer_choice" GroupName="choice" />

                                                                                            </div>
                                                                                            <div class="col-xs-12 col-sm-6 box-choice">
                                                                                                <asp:RadioButton ID="ra_choice_b" runat="server" Text='<%# "b. "+  Eval("choice_b") %>' CssClass="answer_choice" GroupName="choice" />

                                                                                            </div>
                                                                                            <div class="col-xs-12 col-sm-6 box-choice">
                                                                                                <asp:RadioButton ID="ra_choice_c" runat="server" Text='<%# "c. "+ Eval("choice_c") %>' CssClass="answer_choice" GroupName="choice" />

                                                                                            </div>
                                                                                            <div class="col-xs-12 col-sm-6 box-choice">
                                                                                                <asp:RadioButton ID="ra_choice_d" runat="server" Text='<%# "d. "+ Eval("choice_d") %>' CssClass="answer_choice" GroupName="choice" />

                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-sm-12" runat="server" id="box_quest_text" visible='<%# (Eval("m0_taidx").ToString() == "2" )?true:false %>'>
                                                                                        <asp:TextBox ID="answer_txt" TextMode="multiline" CssClass="form-control" Columns="50" Rows="5" runat="server" />
                                                                                    </div>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:Repeater>
                                                                    </div>

                                                                </div>

                                                                <!-- Nested repeated data -->
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="panel-footer text-center">
                            <asp:LinkButton ID="btn_add_answer"
                                CssClass="btn btn-success"
                                Visible="true"
                                data-original-title="บันทึก"
                                data-toggle="tooltip" title="บันทึก"
                                runat="server"
                                CommandName="add_answer"
                                OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ บันทึกแล้วไม่สามารถแก้ไขได้?')">
                                <i class="fa fa-save" aria-hidden="true"></i>&nbsp;บันทัก

                            </asp:LinkButton>
                            <a href="#homepage" id="mtoh">
                                <div class="btn btn-primary"><i class="fa fa-home"></i></div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <script>
                var mybutton = document.getElementById("mtoh");
                window.onscroll = function () { scrollFunction() };

                function scrollFunction() {
                    if (document.body.scrollTop > 610 || document.documentElement.scrollTop > 610) {
                        mybutton.style.display = "block";
                    } else {
                        mybutton.style.display = "none";
                    }
                }
            </script>
        </asp:View>
        <asp:View ID="viewerror" runat="server">
            <div class="col-xs-12 text-center">
                <div class="h1 text-danger" style="min-height: 75vh;">Page Not Found</div>
            </div>
        </asp:View>
        <asp:View ID="viewreadonly" runat="server">
            <div class="col-xs-12 text-center">
                <asp:Image ID="image4" runat="server" Width="100%" Height="100%" ImageUrl="~/images/recruit/Cover_Head_ทำแบบสอบถามครบแล้ว_OK-04-1.jpg"></asp:Image>
                <asp:LinkButton ID="LinkButton1"
                    CssClass="btn btn-default"
                    runat="server"
                    CommandName="backTomain"
                    OnCommand="btnCommand">
                                BackToHome
                </asp:LinkButton>
            </div>
        </asp:View>

    </asp:MultiView>



</asp:Content>

