﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="qa_lab_create_form.aspx.cs" Inherits="websystem_MasterData_qa_lab_create_form" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:Literal ID="litDebug" runat="server"></asp:Literal>
    <asp:HyperLink ID="SETFOCUS" runat="server"></asp:HyperLink>
    <!--multiview-->
    <asp:MultiView ID="mvMaster" runat="server">
        <asp:View ID="pageGenaral" runat="server">
            <div class="form-group">
                <asp:LinkButton ID="btnCreateForm" CssClass="btn btn-primary" runat="server" data-original-title="สร้างฟอร์ม" data-toggle="tooltip"
                    OnCommand="btnCommand" CommandName="cmdCreateForm" CommandArgument="0" title="สร้างฟอร์ม"><i class="fa fa-plus-square" aria-hidden="true"></i> สร้างฟอร์ม</asp:LinkButton>

            </div>
            <!--formview insert-->
            <asp:FormView ID="fvformInsert" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>

                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i>&nbsp;เพิ่มฟอร์ม</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <asp:RequiredFieldValidator ID="ReqtbFormName" runat="server"
                                        ControlToValidate="tbFormName" Display="None" SetFocusOnError="true"
                                        ErrorMessage="*กรุณากรอกชื่อหัวข้อ" ValidationGroup="saveFormName" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValtbFormName" runat="Server" PopupPosition="TopLeft"
                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqtbFormName" Width="180" />

                                    <label class="col-sm-2 control-label">ชื่อฟอร์ม</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="tbFormName" runat="server" CssClass="form-control" placeholder="กรอกชื่อฟอร์ม ..." Enabled="true" />
                                    </div>


                                    <div class="col-sm-1"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ประเภทรายการที่ตรวจ</label>
                                    <div class="col-sm-9">
                                        <asp:DropDownList ID="ddlTestType" runat="server" CssClass="form-control" Enabled="true"></asp:DropDownList>
                                    </div>
                                    <div class="col-sm-1"></div>
                                </div>
                                <%-- <div class="form-group">
                                    <label class="col-sm-2 control-label">ฟอร์มรายการที่ตรวจ</label>
                                    <div class="col-sm-9">
                                        <asp:DropDownList ID="ddlTopicForm" runat="server" CssClass="form-control" Enabled="true"></asp:DropDownList>
                                    </div>
                                    <div class="col-sm-1"></div>
                                </div>--%>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">สถานะ</label>
                                    <div class="col-sm-9">
                                        <asp:DropDownList ID="ddlFormStatus" runat="server" CssClass="form-control" Enabled="true">
                                            <asp:ListItem Text=" Online " Value="1"></asp:ListItem>
                                            <asp:ListItem Text=" Offline " Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-sm-1"></div>
                                </div>

                                <div class="form-group">

                                    <div class="col-sm-11">
                                        <asp:LinkButton ID="btnAddForm" CssClass="btn btn-default pull-right" Visible="false" runat="server" data-original-title="เพิ่ม" data-toggle="tooltip"
                                            OnCommand="btnCommand" CommandName="cmdAddForm" title="เพิ่ม"><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่ม</asp:LinkButton>

                                    </div>
                                    <div class="col-sm-1"></div>
                                </div>
                                <asp:Panel ID="panelFormButton" runat="server" Visible="true">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"></label>
                                        <div class="col-sm-10">
                                            <asp:LinkButton ID="lnkbtnSave" CssClass="btn btn-success" ValidationGroup="saveFormName" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                Text="Save" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="0"></asp:LinkButton>
                                            <asp:LinkButton ID="lnkbtnCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                Text="Cancel" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="0"></asp:LinkButton>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <div class="form-group">
                                    <%--   <label class="col-sm-2 control-label">หัวข้อรายการที่เพิ่ม</label>--%>
                                    <div class="col-sm-9">

                                        <asp:GridView ID="gvCreateFormList"
                                            runat="server" Visible="false"
                                            AutoGenerateColumns="false"
                                            CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                            HeaderStyle-CssClass="success"
                                            HeaderStyle-Height="30px"
                                            OnRowDeleting="gvRowDeleted"
                                            OnPageIndexChanging="gvPageIndexChanging"
                                            OnRowEditing="gvRowEditing"
                                            OnRowUpdating="gvRowUpdating"
                                            OnRowCancelingEdit="gvRowCancelingEdit"
                                            OnRowDataBound="gvRowDataBound">
                                            <EmptyDataTemplate>
                                                <div style="text-align: center">Data Cannot Be Found</div>
                                            </EmptyDataTemplate>
                                            <Columns>
                                                <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                                    ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbnumber" runat="server" Visible="false" />
                                                        <%# (Container.DataItemIndex +1) %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="ประเภทรายการการที่ตรวจ" HeaderStyle-CssClass="text-center"
                                                    ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                                    <ItemTemplate>
                                                        <small>
                                                            <asp:Label ID="lblNameTestDetail" runat="server" CssClass="col-sm-12"
                                                                Text='<%# Eval("nameTestDetail") %>'></asp:Label>
                                                        </small>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="ฟอร์มรายการที่ตรวจ" HeaderStyle-CssClass="text-center"
                                                    ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                                    <ItemTemplate>
                                                        <small>
                                                            <asp:Label ID="lblNameTopic" runat="server" CssClass="col-sm-12"
                                                                Text='<%# Eval("nameTopic") %>'></asp:Label>
                                                        </small>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center"
                                                    ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                                    <ItemTemplate>
                                                        <small>
                                                            <asp:LinkButton ID="btndelete" CssClass="text-trash small" runat="server" CommandName="Delete"
                                                                data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('Do you want delete this item?')" title="delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                                                        </small>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                    <div class="col-sm-1"></div>
                                </div>


                            </div>
                        </div>
                </InsertItemTemplate>
            </asp:FormView>

            <asp:GridView ID="gvMaster"
                runat="server"
                AutoGenerateColumns="false"
                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                HeaderStyle-CssClass="success"
                HeaderStyle-Height="30px"
                AllowPaging="true"
                PageSize="10"
                OnPageIndexChanging="gvPageIndexChanging"
                OnRowEditing="gvRowEditing"
                OnRowUpdating="gvRowUpdating"
                OnRowCancelingEdit="gvRowCancelingEdit"
                OnRowDataBound="gvRowDataBound">
                <PagerStyle CssClass="pageCustom" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                    FirstPageText="First" LastPageText="Last" />
                <EmptyDataTemplate>
                    <div style="text-align: center">Data Cannot Be Found</div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="left" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <div style="text-align: center;">
                                <asp:Label ID="lbFormidx" runat="server" CssClass=" font_text text_center" Visible="false" Text='<%# Eval("r0_form_create_idx")%>'></asp:Label>
                                <%# (Container.DataItemIndex +1) %>
                            </div>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="_Update_R0FormIDX" runat="server" CssClass="form-control" Visible="False" Text='<%# Eval("r0_form_create_idx")%>' />

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label1" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อฟอร์ม" />
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="tbupdate_formname" runat="server" CssClass="form-control" Text='<%# Eval("r0_form_create_name")%>' />

                                        </div>
                                        <asp:RequiredFieldValidator ID="Reqtbupdate_formname" runat="server"
                                            ControlToValidate="tbupdate_formname" Display="None" SetFocusOnError="true"
                                            ErrorMessage="*กรุณากรอกชื่อฟอร์ม" ValidationGroup="saveFormNameUpdate" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="Valtbupdate_formname" runat="Server" PopupPosition="TopLeft"
                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Reqtbupdate_formname" Width="180" />
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label2" CssClass="col-sm-3 control-label" runat="server" Text="ประเภทรายการที่ตรวจ" />
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="tbtestIDXUpdate" runat="server" Visible="false" CssClass="form-control" Text='<%# Eval("test_detail_idx")%>' />
                                            <asp:DropDownList ID="ddlTestTypeUpdate" runat="server" CssClass="form-control" Enabled="false"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="ReqddlTestTypeUpdate" runat="server" InitialValue="0"
                                                ControlToValidate="ddlTestTypeUpdate" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกประเภทรายการที่ตรวจ" ValidationGroup="saveFormNameUpdate" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValddlTestTypeUpdate" runat="Server" PopupPosition="TopLeft"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqddlTestTypeUpdate" Width="180" />
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="lbupdate_formname_status" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                        <div class="col-sm-7">
                                            <asp:DropDownList ID="ddlupdater0status" Text='<%# Eval("r0_form_create_status") %>'
                                                CssClass="form-control fa-align-left" runat="server">
                                                <asp:ListItem Value="1">online</asp:ListItem>
                                                <asp:ListItem Value="0">offline</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-9">
                                            <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" ValidationGroup="saveFormNameUpdate" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                Text="Save" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"></asp:LinkButton>
                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ชื่อฟอร์ม" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="lblFormName" runat="server" Text='<%# Eval("r0_form_create_name")%>'></asp:Label>
                                <%--      <asp:TextBox ID="tbFormIDXLevel2" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("form_detail_root_idx")%>' />--%>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ประเภทการตรวจ" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="lbTestType" runat="server" Text='<%# Eval("test_detail_name")%>'></asp:Label>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>


                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>

                                <asp:Label ID="lbstatus" Visible="false" runat="server"
                                    CssClass="col-sm-12" Text='<%# Eval("r0_form_create_status") %>'></asp:Label>
                                <asp:Label ID="status_offline" runat="server" Visible="false"
                                    data-toggle="tooltip" data-placement="top" title="offline"
                                    CssClass="col-sm-12">
                                                <div style="text-align: center; color: red;">
                                                    <span><b>offline</b></span>
                                </asp:Label>
                                <asp:Label ID="status_online" runat="server" Visible="false"
                                    data-toggle="tooltip" data-placement="top" title="online" CssClass="col-sm-12">
                                                <div style="text-align: center; color: green;">
                                                <span><b>online</b></span>
                                </asp:Label>

                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <asp:LinkButton ID="btnView" CssClass="text-read" runat="server" CommandName="cmdView"
                                data-toggle="tooltip" OnCommand="btnCommand"
                                CommandArgument='<%#Eval("r0_form_create_idx") + "," + "0" %>' title="view">
                                        <i class="glyphicon glyphicon-list-alt"></i></asp:LinkButton>
                            <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                data-toggle="tooltip" OnCommand="btnCommand"
                                CommandArgument='<%#Eval("r0_form_create_idx") %>' title="edit">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                            <asp:LinkButton ID="btndelettypeNews" CssClass="text-trash" runat="server" CommandName="cmdDelete"
                                data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('Do you want delete this item?')"
                                CommandArgument='<%#Eval("r0_form_create_idx") %>' title="delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </asp:View>

        <asp:View ID="pageCreateRoot" runat="server">
            <div class="form-group">
                <asp:LinkButton ID="btnBack" CssClass="btn btn-default" runat="server" data-original-title="Back" data-toggle="tooltip"
                    OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="0" title=""><i class="fa fa-reply" aria-hidden="true"></i> กลับ</asp:LinkButton>
                <asp:LinkButton ID="btnAddRoot" CssClass="btn btn-primary" runat="server" data-original-title="เพิ่มฟอร์มรายการที่ตรวจ" data-toggle="tooltip"
                    OnCommand="btnCommand" CommandName="cmdCreateForm" CommandArgument="1" title=""><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่มฟอร์มรายการที่ตรวจ</asp:LinkButton>
            </div>
            <!--formview insert Level 2-->
            <asp:FormView ID="fvformInsertRoot" DefaultMode="Insert" runat="server" Width="100%">
                <InsertItemTemplate>

                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i>&nbsp;เพิ่มชื่อฟอร์มรายการที่ตรวจ</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">

                                    <label class="col-sm-2 control-label">ชื่อฟอร์ม</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="tbFormNameLevel1" runat="server" CssClass="form-control" placeholder="กรอกชื่อฟอร์มรายการที่ตรวจ ..." Enabled="false" />
                                    </div>
                                    <asp:RequiredFieldValidator ID="ReqtbFormNameLevel1" runat="server"
                                        ControlToValidate="tbFormNameLevel1" Display="None" SetFocusOnError="true"
                                        ErrorMessage="*กรุณากรอกชื่อฟอร์มรายการที่ตรวจ" ValidationGroup="saveFormNameLV1" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValtbFormNameLevel1" runat="Server" PopupPosition="TopLeft"
                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqtbFormNameLevel1" Width="180" />
                                    <div class="col-sm-1"></div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ฟอร์มรายการที่ตรวจ</label>
                                    <div class="col-sm-9">
                                        <asp:DropDownList ID="ddlTopicForm" runat="server" CssClass="form-control" Enabled="true"></asp:DropDownList>
                                    </div>
                                    <asp:RequiredFieldValidator ID="ReqddlTopicForm" runat="server" InitialValue="0"
                                        ControlToValidate="ddlTopicForm" Display="None" SetFocusOnError="true"
                                        ErrorMessage="*กรุณาเลือกฟอร์มรายการที่ตรวจ" ValidationGroup="saveFormNameLV1" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValddlTopicForm" runat="Server" PopupPosition="TopLeft"
                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqddlTopicForm" Width="180" />
                                    <div class="col-sm-1"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">สถานะ</label>
                                    <div class="col-sm-9">
                                        <asp:DropDownList ID="ddlFormStatusLevel2" runat="server" CssClass="form-control" Enabled="true">
                                            <asp:ListItem Text=" Online " Value="1"></asp:ListItem>
                                            <asp:ListItem Text=" Offline " Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-sm-1"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <asp:LinkButton ID="lnkbtnSave" CssClass="btn btn-success" ValidationGroup="saveFormNameLV1" runat="server" data-original-title="Save" data-toggle="tooltip"
                                            Text="Save" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="1"></asp:LinkButton>
                                        <asp:LinkButton ID="lnkbtnCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                            Text="Cancel" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="1"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                </InsertItemTemplate>
            </asp:FormView>

            <asp:GridView ID="gvR1FormCreate"
                runat="server"
                AutoGenerateColumns="false"
                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                HeaderStyle-CssClass="success"
                HeaderStyle-Height="30px"
                AllowPaging="true"
                PageSize="10"
                OnPageIndexChanging="gvPageIndexChanging"
                OnRowEditing="gvRowEditing"
                OnRowUpdating="gvRowUpdating"
                OnRowCancelingEdit="gvRowCancelingEdit"
                OnRowDataBound="gvRowDataBound">
                <PagerStyle CssClass="pageCustom" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                    FirstPageText="First" LastPageText="Last" />
                <EmptyDataTemplate>
                    <div style="text-align: center">Data Cannot Be Found</div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="left" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <div style="text-align: center;">
                                <asp:Label ID="lbFormR1idx" runat="server" CssClass=" font_text text_center" Visible="false" Text='<%# Eval("r1_form_create_idx")%>'></asp:Label>
                                <%# (Container.DataItemIndex +1) %>
                            </div>
                        </ItemTemplate>

                        <EditItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="_Update_R1FormName" runat="server" CssClass="form-control" Visible="False" Text='<%# Eval("r1_form_create_idx")%>' />

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label1" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อฟอร์ม" />
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="tbupdate_R0formname" runat="server" Enabled="false" CssClass="form-control" Text='<%# Eval("r0_form_create_name")%>' />

                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label2" CssClass="col-sm-3 control-label" runat="server" Text="ฟอร์มรายการที่ตรวจ" />
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="tbFormDetailIDXUpdate" runat="server" Visible="false" CssClass="form-control" Text='<%# Eval("form_detail_idx")%>' />
                                            <asp:DropDownList ID="ddlFormDetailUpdate" runat="server" CssClass="form-control" Enabled="true"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="ReqddlFormDetailUpdate" runat="server" InitialValue="0"
                                                ControlToValidate="ddlFormDetailUpdate" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกฟอร์มรายการที่ตรวจ" ValidationGroup="saveFormNameLV1Update" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValddlFormDetailUpdate" runat="Server" PopupPosition="TopLeft"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqddlFormDetailUpdate" Width="180" />
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="lbupdate_formname_status" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                        <div class="col-sm-7">
                                            <asp:DropDownList ID="ddlupdater1status" Text='<%# Eval("r1_form_create_status") %>'
                                                CssClass="form-control fa-align-left" runat="server">
                                                <asp:ListItem Value="1">online</asp:ListItem>
                                                <asp:ListItem Value="0">offline</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-9">
                                            <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                Text="Save" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"></asp:LinkButton>
                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ชื่อฟอร์ม" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="lblR0FormName" runat="server" Text='<%# Eval("r0_form_create_name")%>'></asp:Label>
                                <%--      <asp:TextBox ID="tbFormIDXLevel2" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("form_detail_root_idx")%>' />--%>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ฟอร์มรายการที่ตรวจ" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="lblFormName" runat="server" Text='<%# Eval("form_detail_name")%>'></asp:Label>
                                <%--      <asp:TextBox ID="tbFormIDXLevel2" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("form_detail_root_idx")%>' />--%>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>

                                <asp:Label ID="lbR1status" Visible="false" runat="server"
                                    CssClass="col-sm-12" Text='<%# Eval("r1_form_create_status") %>'></asp:Label>
                                <asp:Label ID="lbstatus_offline" runat="server" Visible="false"
                                    data-toggle="tooltip" data-placement="top" title="offline"
                                    CssClass="col-sm-12">
                                                <div style="text-align: center; color: red;">
                                                    <span><b>offline</b></span>
                                </asp:Label>
                                <asp:Label ID="lbstatus_online" runat="server" Visible="false"
                                    data-toggle="tooltip" data-placement="top" title="online" CssClass="col-sm-12">
                                                <div style="text-align: center; color: green;">
                                                <span><b>online</b></span>
                                </asp:Label>

                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <asp:LinkButton ID="btnView" CssClass="text-read" runat="server" CommandName="cmdViewRoot"
                                data-toggle="tooltip" OnCommand="btnCommand"
                                CommandArgument='<%#Eval("r1_form_create_idx") + "," + "0" + "," + Eval("form_detail_idx") + "," + Eval("r1_form_detail_root_idx") %>' title="view">
                                <i class="glyphicon glyphicon-list-alt"></i></asp:LinkButton>
                            <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                data-toggle="tooltip" OnCommand="btnCommand"
                                CommandArgument='<%#Eval("r1_form_create_idx") %>' title="edit">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                            <asp:LinkButton ID="btndelettypeNews" CssClass="text-trash" runat="server" CommandName="cmdDeleteRoot"
                                data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('Do you want delete this item?')"
                                CommandArgument='<%#Eval("r1_form_create_idx") + "," + "0" %>' title="delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>

        </asp:View>
        <asp:View ID="pageCreateRootSub" runat="server">
            <div class="form-group">
                <asp:LinkButton ID="btnBackLv2" CssClass="btn btn-default" runat="server" data-original-title="Back" data-toggle="tooltip"
                    OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="1" title=""><i class="fa fa-reply" aria-hidden="true"></i> กลับ</asp:LinkButton>
                <asp:Label ID="lbCheckBack" runat="server" CssClass="font_text text_center" Visible="false"></asp:Label>
                <asp:LinkButton ID="btnCreateLv3" CssClass="btn btn-primary" runat="server" data-original-title="เพิ่มฟอร์มย่อยรายการที่ตรวจ" data-toggle="tooltip"
                    OnCommand="btnCommand" CommandName="cmdCreateForm" CommandArgument="2" title=""><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่มฟอร์มย่อยรายการที่ตรวจ</asp:LinkButton>
            </div>
            <!--formview insert Level 3-->
            <asp:FormView ID="fvInsertRootSubSet" DefaultMode="Insert" runat="server" Width="100%">
                <InsertItemTemplate>

                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i>&nbsp;เพิ่มชื่อฟอร์มย่อยรายการที่ตรวจ</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">

                                    <label class="col-sm-2 control-label">ชื่อฟอร์มย่อย</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="tbFormNameLevel3" runat="server" CssClass="form-control" placeholder="กรอกชื่อฟอร์มย่อยรายการที่ตรวจ ..." Enabled="false" />
                                    </div>
                                    <%--  <asp:RequiredFieldValidator ID="ReqtbFormNameLevel3" runat="server"
                                        ControlToValidate="tbFormNameLevel3" Display="None" SetFocusOnError="true"
                                        ErrorMessage="*กรุณากรอกชื่อฟอร์มรายการที่ตรวจ" ValidationGroup="saveFormNameLV3" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValtbFormNameLevel3" runat="Server" PopupPosition="TopLeft"
                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqtbFormNameLevel3" Width="180" />--%>
                                    <div class="col-sm-1"></div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ชื่อฟอร์มย่อยรายการที่ตรวจ</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="tbTopicFormLv3" runat="server" CssClass="form-control" placeholder="กรอกชื่อฟอร์มย่อยรายการที่ตรวจ ..." Enabled="true"></asp:TextBox>
                                    </div>
                                    <asp:RequiredFieldValidator ID="ReqtbTopicFormLv3" runat="server"
                                        ControlToValidate="tbTopicFormLv3" Display="None" SetFocusOnError="true"
                                        ErrorMessage="*กรุณากรอกชื่อฟอร์มย่อยรายการที่ตรวจ" ValidationGroup="saveFormNameLV3" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValtbTopicFormLv3" runat="Server" PopupPosition="TopLeft"
                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqtbTopicFormLv3" Width="180" />
                                    <div class="col-sm-1"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">สถานะ</label>
                                    <div class="col-sm-9">
                                        <asp:DropDownList ID="ddlFormStatusLevel3" runat="server" CssClass="form-control" Enabled="true">
                                            <asp:ListItem Text=" Online " Value="1"></asp:ListItem>
                                            <asp:ListItem Text=" Offline " Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-sm-1"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <asp:LinkButton ID="lnkbtnSaveLv3" CssClass="btn btn-success" ValidationGroup="saveFormNameLV3" runat="server" data-original-title="Save" data-toggle="tooltip"
                                            Text="Save" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="2"></asp:LinkButton>
                                        <asp:LinkButton ID="lnkbtnCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                            Text="Cancel" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="2"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                </InsertItemTemplate>
            </asp:FormView>

            <asp:GridView ID="gvFormListLv3"
                runat="server"
                AutoGenerateColumns="false"
                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                HeaderStyle-CssClass="success"
                HeaderStyle-Height="30px"
                AllowPaging="true"
                PageSize="10"
                OnPageIndexChanging="gvPageIndexChanging"
                OnRowEditing="gvRowEditing"
                OnRowUpdating="gvRowUpdating"
                OnRowCancelingEdit="gvRowCancelingEdit"
                OnRowDataBound="gvRowDataBound">
                <PagerStyle CssClass="pageCustom" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                    FirstPageText="First" LastPageText="Last" />
                <EmptyDataTemplate>
                    <div style="text-align: center">Data Cannot Be Found</div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="left" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <div style="text-align: center;">
                                <asp:Label ID="lbFormR1idx" runat="server" CssClass=" font_text text_center" Visible="false" Text='<%# Eval("r1_form_create_idx")%>'></asp:Label>
                                <%# (Container.DataItemIndex +1) %>
                            </div>
                        </ItemTemplate>

                        <EditItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="_Update_R1FormNamelv3" runat="server" CssClass="form-control" Visible="False" Text='<%# Eval("r1_form_create_idx")%>' />

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label1" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อฟอร์มย่อย" />
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="tbupdate_R1formnameLv3" runat="server" Enabled="true" CssClass="form-control" Text='<%# Eval("text_name_setform")%>' />
                                            <asp:RequiredFieldValidator ID="Reqtbupdate_R1formnameLv3" runat="server" 
                                                ControlToValidate="tbupdate_R1formnameLv3" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกชื่อฟอร์มย่อย" ValidationGroup="saveUpdateDataLv3" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="Valtbupdate_R1formnameLv3" runat="Server" PopupPosition="TopLeft"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Reqtbupdate_R1formnameLv3" Width="180" />
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label2" CssClass="col-sm-3 control-label" runat="server" Text="ฟอร์มรายการที่ตรวจ" />
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="tbFormDetailIDXUpdate" runat="server" Visible="true" Enabled="false" CssClass="form-control" Text='<%# Eval("form_detail_name")%>' />
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="lbupdate_formname_status" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                        <div class="col-sm-7">
                                            <asp:DropDownList ID="ddlupdater2statuslv3" Text='<%# Eval("r1_form_create_status") %>'
                                                CssClass="form-control fa-align-left" runat="server">
                                                <asp:ListItem Value="1">online</asp:ListItem>
                                                <asp:ListItem Value="0">offline</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-9">
                                            <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" ValidationGroup="saveUpdateDataLv3" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                Text="Save" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"></asp:LinkButton>
                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ชื่อฟอร์มย่อย" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="lblR0FormName" runat="server" Text='<%# Eval("text_name_setform")%>'></asp:Label>
                                <%--      <asp:TextBox ID="tbFormIDXLevel2" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("form_detail_root_idx")%>' />--%>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ฟอร์มรายการที่ตรวจ" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="lblFormName" runat="server" Text='<%# Eval("form_detail_name")%>'></asp:Label>
                                <%--      <asp:TextBox ID="tbFormIDXLevel2" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("form_detail_root_idx")%>' />--%>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>

                                <asp:Label ID="lbR1status" Visible="false" runat="server"
                                    CssClass="col-sm-12" Text='<%# Eval("r1_form_create_status") %>'></asp:Label>
                                <asp:Label ID="lbstatus_offline" runat="server" Visible="false"
                                    data-toggle="tooltip" data-placement="top" title="offline"
                                    CssClass="col-sm-12">
                                                <div style="text-align: center; color: red;">
                                                    <span><b>offline</b></span>
                                </asp:Label>
                                <asp:Label ID="lbstatus_online" runat="server" Visible="false"
                                    data-toggle="tooltip" data-placement="top" title="online" CssClass="col-sm-12">
                                                <div style="text-align: center; color: green;">
                                                <span><b>online</b></span>
                                </asp:Label>

                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                               <asp:LinkButton ID="btnView" CssClass="text-read" runat="server" CommandName="cmdViewRoot"
                                data-toggle="tooltip" OnCommand="btnCommand"
                                CommandArgument='<%#Eval("r1_form_create_idx") + "," + "1" + "," + Eval("form_detail_idx") + "," + Eval("r1_form_detail_root_idx") %>' title="view">
                                <i class="glyphicon glyphicon-list-alt"></i></asp:LinkButton>
                            <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                data-toggle="tooltip" OnCommand="btnCommand"
                                CommandArgument='<%#Eval("r1_form_create_idx") %>' title="edit">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                            <asp:LinkButton ID="btndelettypeNews" CssClass="text-trash" runat="server" CommandName="cmdDeleteRoot"
                                data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('Do you want delete this item?')"
                                CommandArgument='<%#Eval("r1_form_create_idx") + "," + "1" %>' title="delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>

        </asp:View>

          <asp:View ID="pageCreateRootSubSet" runat="server">
            <div class="form-group">
                <asp:LinkButton ID="LinkButton1" CssClass="btn btn-default" runat="server" data-original-title="Back" data-toggle="tooltip"
                    OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="4" title=""><i class="fa fa-reply" aria-hidden="true"></i> กลับ</asp:LinkButton>
                <asp:LinkButton ID="LinkButton2" CssClass="btn btn-primary" runat="server" data-original-title="เพิ่มฟอร์มย่อยรายการที่ตรวจ" data-toggle="tooltip"
                    OnCommand="btnCommand" CommandName="cmdCreateForm" CommandArgument="3" title=""><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่มฟอร์มย่อยรายการที่ตรวจ</asp:LinkButton>
            </div>
            <!--formview insert Level 3-->
            <asp:FormView ID="fvInsertDataRootSubSet" DefaultMode="Insert" runat="server" Width="100%">
                <InsertItemTemplate>

                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i>&nbsp;เพิ่มชื่อฟอร์มย่อยรายการที่ตรวจ</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">

                                    <label class="col-sm-2 control-label">ชื่อฟอร์มย่อย</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="tbFormNameLevel4" runat="server" CssClass="form-control" placeholder="กรอกชื่อฟอร์มย่อยรายการที่ตรวจ ..." Enabled="false" />
                                    </div>
                                    <%--  <asp:RequiredFieldValidator ID="ReqtbFormNameLevel3" runat="server"
                                        ControlToValidate="tbFormNameLevel3" Display="None" SetFocusOnError="true"
                                        ErrorMessage="*กรุณากรอกชื่อฟอร์มรายการที่ตรวจ" ValidationGroup="saveFormNameLV3" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValtbFormNameLevel3" runat="Server" PopupPosition="TopLeft"
                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqtbFormNameLevel3" Width="180" />--%>
                                    <div class="col-sm-1"></div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ชื่อฟอร์มย่อยรายการที่ตรวจ</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="tbTopicFormLv4" runat="server" CssClass="form-control" placeholder="กรอกชื่อฟอร์มย่อยรายการที่ตรวจ ..." Enabled="true"></asp:TextBox>
                                    </div>
                                    <asp:RequiredFieldValidator ID="ReqtbTopicFormLv4" runat="server"
                                        ControlToValidate="tbTopicFormLv4" Display="None" SetFocusOnError="true"
                                        ErrorMessage="*กรุณากรอกชื่อฟอร์มย่อยรายการที่ตรวจ" ValidationGroup="saveFormNameLV4" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValtbTopicFormLv4" runat="Server" PopupPosition="TopLeft"
                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqtbTopicFormLv4" Width="180" />
                                    <div class="col-sm-1"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">สถานะ</label>
                                    <div class="col-sm-9">
                                        <asp:DropDownList ID="ddlFormStatusLevel4" runat="server" CssClass="form-control" Enabled="true">
                                            <asp:ListItem Text=" Online " Value="1"></asp:ListItem>
                                            <asp:ListItem Text=" Offline " Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-sm-1"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <asp:LinkButton ID="lnkbtnSaveLv4" CssClass="btn btn-success" ValidationGroup="saveFormNameLV4" runat="server" data-original-title="Save" data-toggle="tooltip"
                                            Text="Save" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="3"></asp:LinkButton>
                                        <asp:LinkButton ID="lnkbtnCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                            Text="Cancel" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="3"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                </InsertItemTemplate>
            </asp:FormView>

            <asp:GridView ID="gvDataFormListSubset"
                runat="server"
                AutoGenerateColumns="false"
                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                HeaderStyle-CssClass="success"
                HeaderStyle-Height="30px"
                AllowPaging="true"
                PageSize="10"
                OnPageIndexChanging="gvPageIndexChanging"
                OnRowEditing="gvRowEditing"
                OnRowUpdating="gvRowUpdating"
                OnRowCancelingEdit="gvRowCancelingEdit"
                OnRowDataBound="gvRowDataBound">
                <PagerStyle CssClass="pageCustom" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                    FirstPageText="First" LastPageText="Last" />
                <EmptyDataTemplate>
                    <div style="text-align: center">Data Cannot Be Found</div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="left" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <div style="text-align: center;">
                                <asp:Label ID="lbFormR1idx" runat="server" CssClass=" font_text text_center" Visible="false" Text='<%# Eval("r1_form_create_idx")%>'></asp:Label>
                                <%# (Container.DataItemIndex +1) %>
                            </div>
                        </ItemTemplate>

                        <EditItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="_Update_R1FormNamelv4" runat="server" CssClass="form-control" Visible="False" Text='<%# Eval("r1_form_create_idx")%>' />

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label1" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อฟอร์มย่อย" />
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="tbupdate_R1formnameLv4" runat="server" Enabled="true" CssClass="form-control" Text='<%# Eval("text_name_setform")%>' />
                                            <asp:RequiredFieldValidator ID="Reqtbupdate_R1formnameLv4" runat="server" 
                                                ControlToValidate="tbupdate_R1formnameLv4" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกชื่อฟอร์มย่อย" ValidationGroup="saveUpdateDataLv4" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="Valtbupdate_R1formnameLv4" runat="Server" PopupPosition="TopLeft"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Reqtbupdate_R1formnameLv4" Width="180" />
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label2" CssClass="col-sm-3 control-label" runat="server" Text="ฟอร์มรายการที่ตรวจ" />
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="tbFormDetailIDXUpdate" runat="server" Visible="true" Enabled="false" CssClass="form-control" Text='<%# Eval("form_detail_name")%>' />
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="lbupdate_formname_status" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                        <div class="col-sm-7">
                                            <asp:DropDownList ID="ddlupdater2statuslv4" Text='<%# Eval("r1_form_create_status") %>'
                                                CssClass="form-control fa-align-left" runat="server">
                                                <asp:ListItem Value="1">online</asp:ListItem>
                                                <asp:ListItem Value="0">offline</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-9">
                                            <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" ValidationGroup="saveUpdateDataLv4" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                Text="Save" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"></asp:LinkButton>
                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ชื่อฟอร์มย่อย" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="lblR0FormName" runat="server" Text='<%# Eval("text_name_setform")%>'></asp:Label>
                                <%--      <asp:TextBox ID="tbFormIDXLevel2" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("form_detail_root_idx")%>' />--%>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ฟอร์มรายการที่ตรวจ" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="lblFormName" runat="server" Text='<%# Eval("form_detail_name")%>'></asp:Label>
                                <%--      <asp:TextBox ID="tbFormIDXLevel2" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("form_detail_root_idx")%>' />--%>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>

                                <asp:Label ID="lbR1status" Visible="false" runat="server"
                                    CssClass="col-sm-12" Text='<%# Eval("r1_form_create_status") %>'></asp:Label>
                                <asp:Label ID="lbstatus_offline" runat="server" Visible="false"
                                    data-toggle="tooltip" data-placement="top" title="offline"
                                    CssClass="col-sm-12">
                                                <div style="text-align: center; color: red;">
                                                    <span><b>offline</b></span>
                                </asp:Label>
                                <asp:Label ID="lbstatus_online" runat="server" Visible="false"
                                    data-toggle="tooltip" data-placement="top" title="online" CssClass="col-sm-12">
                                                <div style="text-align: center; color: green;">
                                                <span><b>online</b></span>
                                </asp:Label>

                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <%--   <asp:LinkButton ID="btnView" CssClass="text-read" runat="server" CommandName="cmdViewRoot"
                                data-toggle="tooltip" OnCommand="btnCommand"
                                CommandArgument='<%#Eval("r1_form_create_idx") + "," + "0" + "," + Eval("form_detail_idx") %>' title="view">
                                <i class="glyphicon glyphicon-list-alt"></i></asp:LinkButton>--%>
                            <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                data-toggle="tooltip" OnCommand="btnCommand"
                                CommandArgument='<%#Eval("r1_form_create_idx") %>' title="edit">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                            <asp:LinkButton ID="btndelettypeNews" CssClass="text-trash" runat="server" CommandName="cmdDeleteRoot"
                                data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('Do you want delete this item?')"
                                CommandArgument='<%#Eval("r1_form_create_idx") + "," + "2" %>' title="delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>

        </asp:View>


    </asp:MultiView>



</asp:Content>

