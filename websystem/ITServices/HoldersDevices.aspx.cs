﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.IO;
using System.Configuration;
using System.Data.SqlClient;

using System.Web.Security;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Web.Services;
using System.Web.Services.Protocols;
using System.Net.Mail;
using System.Data;

public partial class websystem_ITServices_HoldersDevices : System.Web.UI.Page
{
    #region Connect

    service_execute serviceexcute = new service_execute();
    function_tool _funcTool = new function_tool();


    private string ODSP_Reletion = "ODSP_Reletion";


    data_employee _dtEmployee = new data_employee();
    //// PHON
    data_employee _dataEmployee = new data_employee();
    //// PHON

    dataODSP_Relation dtODSP = new dataODSP_Relation();
    DataReservation _dtreservation = new DataReservation();
    DataHoldersDevices _dtholder = new DataHoldersDevices();
    data_softwarelicense_devices _data_softwarelicense_devices = new data_softwarelicense_devices();
    private string BoxXML;
    string localXml = String.Empty;
    string localString = String.Empty;
    string _local_xml = "";
    string _ret_val = "";
    string _localJson = "";

    string linkquest = "http://mas.taokaenoi.co.th/holder-devices/";


    #region URL

    static string _serviceUrl = _serviceUrl + ConfigurationManager.AppSettings["serviceUrl"];
    static string urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string urlGetOrganization = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    static string urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];

    // PHON
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    // PHON


    static string urlselect_view_user7 = _serviceUrl + ConfigurationManager.AppSettings["urlselect_view_user7"];
    static string urlselect_view_user8and9 = _serviceUrl + ConfigurationManager.AppSettings["urlselect_view_user8and9"];
    static string urlselect_view_user10 = _serviceUrl + ConfigurationManager.AppSettings["urlselect_view_user10"];
    static string urlselect_approvelist = _serviceUrl + ConfigurationManager.AppSettings["urlselect_approvelist"];
    static string urlHDEMPIDXANDNHDEMPIDX = _serviceUrl + ConfigurationManager.AppSettings["urlHDEMPIDXANDNHDEMPIDX"];
    static string urlddl_Approve = _serviceUrl + ConfigurationManager.AppSettings["urlddl_Approve"];

    static string urlSelectDevices = _serviceUrl + ConfigurationManager.AppSettings["urlSelectDevices"];
    static string urlSelectMasterList = _serviceUrl + ConfigurationManager.AppSettings["urlSelectMasterList"];
    static string urlSelectDetailHolder = _serviceUrl + ConfigurationManager.AppSettings["urlSelectDetailHolder"];
    static string urlSelectHolder = _serviceUrl + ConfigurationManager.AppSettings["urlSelectHolder"];
    static string urlSelectLogHolder = _serviceUrl + ConfigurationManager.AppSettings["urlSelectLogHolder"];
    static string urlSelectApprove1Holder = _serviceUrl + ConfigurationManager.AppSettings["urlSelectApprove1Holder"];

    static string urlInsert_Holder = _serviceUrl + ConfigurationManager.AppSettings["urlInsert_Holder"];
    static string urlUpdate_Holder = _serviceUrl + ConfigurationManager.AppSettings["urlUpdate_Holder"];
    static string urlUpdate_ApproveChangeHolder = _serviceUrl + ConfigurationManager.AppSettings["urlUpdate_ApproveChangeHolder"];
    static string urlUpdate_ChangeHolder = _serviceUrl + ConfigurationManager.AppSettings["urlUpdate_ChangeHolder"];
    static string urlUpdate_LogHolder = _serviceUrl + ConfigurationManager.AppSettings["urlUpdate_LogHolder"];
    static string urlselect_user = _serviceUrl + ConfigurationManager.AppSettings["urlselect_user"];

    static string _urlSetSoftwareLicense = _serviceUrl + ConfigurationManager.AppSettings["urlSetSoftwareLicense"];
    static string urlGetSoftwareShowHolder = _serviceUrl + ConfigurationManager.AppSettings["urlGetSoftwareShowHolder"];
    static string urlGetAll = _serviceUrl + ConfigurationManager.AppSettings["urlGetAll"];
    static string _url_u0_device_update_17 = _serviceUrl + ConfigurationManager.AppSettings["urlupdate_u0_device_17_List"];
    static string urlSetApproveLicense = _serviceUrl + ConfigurationManager.AppSettings["urlSetApproveLicense"];
    static string _urlGetits_u_transfer = _serviceUrl + ConfigurationManager.AppSettings["urlGetits_u_transfer"];

    #endregion




    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            ViewState["EmpIDX"] =  int.Parse(Session["emp_idx"].ToString());

            Set_Defult_Index();
            select_empIdx_present();
            ViewState["RdeptIDX_Holder"] = "0";

            if (ViewState["rdept_idx"].ToString() != "20" && ViewState["rdept_idx"].ToString() != "21")
            {
                BoxSearch.Visible = false;

                if (int.Parse(ViewState["JobGradeIDX"].ToString()) <= 7)
                {
                    select_view_user7();
                }
                else if (int.Parse(ViewState["JobGradeIDX"].ToString()) == 8 || int.Parse(ViewState["JobGradeIDX"].ToString()) == 9)
                {
                    select_view_user8and9();
                }
                else if (int.Parse(ViewState["JobGradeIDX"].ToString()) >= 10)
                {
                    select_view_user10();
                }
            }
            else
            {
                BoxSearch.Visible = true;
                // ddlOrg_Search();
                getOrganizationList(ddlSearchOrg);
                SelectDevices();

            }

        }

        ViewState["link"] = "http://mas.taokaenoi.co.th/holder-devices";
        linkBtnTrigger(lblguide);
        linkBtnTrigger(lblflow);
    }
    #endregion

    #region Set_Defult_Index

    protected void Set_Defult_Index()
    {
        SETBoxAllSearch.Visible = true;
        lbindex.BackColor = System.Drawing.Color.LightGray;
        lbapprove.BackColor = System.Drawing.Color.Transparent;

    }

    #endregion

    #region CallService
    protected data_employee callServiceEmpProfile(string _cmdUrl)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dtEmployee;
    }

    protected data_employee callServicePostEmp(string _cmdUrl, data_employee _dtEmployee)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        // text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);


        return _dtEmployee;
    }

    protected DataHoldersDevices callServicePostHolder(string _cmdUrl, DataHoldersDevices _dtholder)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtholder);
        // text.Text = _cmdUrl + _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //  text.Text = _localJson;

        //// convert json to object
        _dtholder = (DataHoldersDevices)_funcTool.convertJsonToObject(typeof(DataHoldersDevices), _localJson);




        return _dtholder;
    }

    protected data_softwarelicense_devices callServiceSoftwareLicenseDevices(string _cmdUrl, data_softwarelicense_devices _data_softwarelicense_devices)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_softwarelicense_devices);
        // litDebug.Text = _localJson;

        // call services
        //_localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_softwarelicense_devices = (data_softwarelicense_devices)_funcTool.convertJsonToObject(typeof(data_softwarelicense_devices), _localJson);

        return _data_softwarelicense_devices;
    }

    protected data_device callServicePostDevice(string _cmdUrl, data_device _data_device)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_data_device);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //fss.Text = _cmdUrl + _localJson;

        ////// convert json to object
        _data_device = (data_device)_funcTool.convertJsonToObject(typeof(data_device), _localJson);

        return _data_device;
    }
    #endregion

    #region Select

    protected void select_empIdx_present()
    {

        _dtEmployee = callServiceEmpProfile(urlGetMyProfile + ViewState["EmpIDX"].ToString());
        ViewState["org_idx"] = _dtEmployee.employee_list[0].org_idx;
        ViewState["rdept_idx"] = _dtEmployee.employee_list[0].rdept_idx;
        ViewState["JobGradeIDX"] = _dtEmployee.employee_list[0].jobgrade_idx;
        ViewState["RSecIDX"] = _dtEmployee.employee_list[0].rsec_idx;

    }

    protected void select_view_user7()// View User Down Jobgrade 7
    {

        _dtholder = new DataHoldersDevices();
        _dtholder.BoxHoldersDevices = new HoldersDevices[1];
        HoldersDevices u1search = new HoldersDevices();

        u1search.EmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        u1search.JobGradeIDX = int.Parse(ViewState["JobGradeIDX"].ToString());

        _dtholder.BoxHoldersDevices[0] = u1search;
        _dtholder = callServicePostHolder(urlselect_view_user7, _dtholder);

        //ViewState["rdept_idx"] = _dtholder.BoxHoldersDevices[0].RDeptIDX;
        //ViewState["JobGradeIDX"] = _dtholder.BoxHoldersDevices[0].JobGradeIDX;
        setGridData(GvMaster, _dtholder.BoxHoldersDevices);
    }

    protected void select_view_user8and9() // View User Jobgrade 8-9
    {
        _dtholder = new DataHoldersDevices();
        _dtholder.BoxHoldersDevices = new HoldersDevices[1];
        HoldersDevices u1search = new HoldersDevices();

        u1search.EmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        u1search.JobGradeIDX = int.Parse(ViewState["JobGradeIDX"].ToString());
        u1search.RSecIDX = int.Parse(ViewState["RSecIDX"].ToString());

        _dtholder.BoxHoldersDevices[0] = u1search;
        _dtholder = callServicePostHolder(urlselect_view_user8and9, _dtholder);

        //ViewState["rdept_idx"] = _dtholder.BoxHoldersDevices[0].RDeptIDX;
        //ViewState["JobGradeIDX"] = _dtholder.BoxHoldersDevices[0].JobGradeIDX;
        setGridData(GvMaster, _dtholder.BoxHoldersDevices);
    }

    protected void select_view_user10() // View User Jobgrade 10>
    {
        _dtholder = new DataHoldersDevices();
        _dtholder.BoxHoldersDevices = new HoldersDevices[1];
        HoldersDevices u1search = new HoldersDevices();

        u1search.EmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        u1search.JobGradeIDX = int.Parse(ViewState["JobGradeIDX"].ToString());
        u1search.RDeptIDX = int.Parse(ViewState["rdept_idx"].ToString());

        _dtholder.BoxHoldersDevices[0] = u1search;
        _dtholder = callServicePostHolder(urlselect_view_user10, _dtholder);

        setGridData(GvMaster, _dtholder.BoxHoldersDevices);
    }

    protected void select_approvelist()
    {

        _dtholder = new DataHoldersDevices();
        _dtholder.BoxHoldersDevices = new HoldersDevices[1];
        HoldersDevices u1search = new HoldersDevices();
  
        u1search.EmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        u1search.RDeptIDX = int.Parse(ViewState["rdept_idx"].ToString());
        u1search.RSecIDX = int.Parse(ViewState["RSecIDX"].ToString());
        _dtholder.BoxHoldersDevices[0] = u1search;
       // text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtholder));
        _dtholder = callServicePostHolder(urlselect_approvelist, _dtholder);
      
        setGridData(GvApprove, _dtholder.BoxHoldersDevices);

    }

    protected void ddlOrg_Search()
    {

        ddlSearchOrg.Items.Clear();
        ddlSearchOrg.AppendDataBoundItems = true;
        ddlSearchOrg.Items.Add(new ListItem("--- เลือกองค์กร ---", "0"));


        _dtEmployee = new data_employee();

        _dtEmployee.organization_list = new organization_details[1];
        organization_details organite = new organization_details();

        _dtEmployee.organization_list[0] = organite;

        _dtEmployee = callServicePostEmp(urlGetOrganization, _dtEmployee);

        ddlSearchOrg.DataSource = _dtEmployee.organization_list;
        ddlSearchOrg.DataTextField = "org_name_th";
        ddlSearchOrg.DataValueField = "org_idx";
        ddlSearchOrg.DataBind();

    }

    protected void HDEMPIDXANDNHDEMPIDX()
    {
        var lblDeviceIDX = (Label)FvDetailUser.FindControl("lblDeviceIDX");

        _dtholder = new DataHoldersDevices();
        _dtholder.BoxHoldersDevices = new HoldersDevices[1];
        HoldersDevices u1search = new HoldersDevices();

        u1search.DeviceIDX = int.Parse(ViewState["U0IDX"].ToString());

        _dtholder.BoxHoldersDevices[0] = u1search;
        _dtholder = callServicePostHolder(urlHDEMPIDXANDNHDEMPIDX, _dtholder);

        var rtCode_Rdept = _dtholder.ReturnCode;
        ViewState["rtCode_Rdept"] = rtCode_Rdept;


    }

    protected void ddl_Approve()
    {

        _dtholder = new DataHoldersDevices();
        _dtholder.BoxHoldersDevices = new HoldersDevices[1];
        HoldersDevices u1search = new HoldersDevices();

        if (ViewState["rtCode_Rdept"].ToString() == "1")
        {
            u1search.status_approve = 1;
        }
        else if (ViewState["rtCode_Rdept"].ToString() == "2")
        {
            u1search.status_approve = 2;
        }

        _dtholder.BoxHoldersDevices[0] = u1search;
        _dtholder = callServicePostHolder(urlddl_Approve, _dtholder);

        ddl_changeApprove.DataSource = _dtholder.BoxHoldersDevices;
        ddl_changeApprove.DataValueField = "staidx";
        ddl_changeApprove.DataTextField = "status_name";
        ddl_changeApprove.DataBind();


    }

    protected void Select_Sec()
    {
        var lblorg = (Label)FvDetailUser.FindControl("lblorg");
        var lblrdept = (Label)FvDetailUser.FindControl("lblrdept");

        ddladdholder_sec.AppendDataBoundItems = true;
        ddladdholder_sec.Items.Clear();
        ddladdholder_sec.Items.Add(new ListItem("--- เลือกแผนก ---", "0"));


        _dtEmployee = new data_employee();

        _dtEmployee.section_list = new section_details[1];
        section_details organite = new section_details();

        organite.org_idx = int.Parse(lblorg.Text);
        organite.rdept_idx = int.Parse(lblrdept.Text);
        //    organite.rsec_idx = int.Parse(ddladdholder_sec.SelectedValue);

        _dtEmployee.section_list[0] = organite;

        _dtEmployee = callServicePostEmp(urlGetSectionList, _dtEmployee);


        ddladdholder_sec.DataSource = _dtEmployee.section_list;
        ddladdholder_sec.DataTextField = "sec_name_th";
        ddladdholder_sec.DataValueField = "rsec_idx";
        ddladdholder_sec.DataBind();
        //    ddladdholder_sec.SelectedValue = "0";
    }

    protected void SelectDevices()
    {

        ddlSearch_devices.Items.Clear();
        ddlSearch_devices.AppendDataBoundItems = true;
        ddlSearch_devices.Items.Add(new ListItem("--- เลือกอุปกรณ์ ---", "0"));


        _dtholder = new DataHoldersDevices();
        _dtholder.BoxHoldersDevices = new HoldersDevices[1];
        HoldersDevices u1search = new HoldersDevices();


        _dtholder.BoxHoldersDevices[0] = u1search;

        _dtholder = callServicePostHolder(urlSelectDevices, _dtholder);


        //   text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtholder));

        //_localJson = _funcTool.convertObjectToJson(_dtholder);
        //text.Text = _localJson;
        //   _dtholder = callServicePostHolder(urlSelectDevices, _dtholder);


        ddlSearch_devices.DataSource = _dtholder.BoxHoldersDevices;
        ddlSearch_devices.DataValueField = "m0_tdidx";
        ddlSearch_devices.DataTextField = "detail";
        ddlSearch_devices.DataBind();

    }

    protected void SelectMasterList()
    {

        _dtholder = new DataHoldersDevices();
        _dtholder.BoxHoldersDevices = new HoldersDevices[1];
        HoldersDevices u1search = new HoldersDevices();

        u1search.OrgIDX = int.Parse(ViewState["OrgIDX_search"].ToString());// int.Parse(ddlSearchOrg.SelectedValue);
        u1search.RDeptIDX = int.Parse(ViewState["RdepIDX_search"].ToString());// int.Parse(ddlSearchDep.SelectedValue);
        u1search.RSecIDX = int.Parse(ViewState["RsecIDX_search"].ToString());// int.Parse(ddlSearchSec.SelectedValue);
        u1search.EmpIDX = int.Parse(ViewState["EmpIDX_search"].ToString());// int.Parse(ddlSearchHolder.SelectedValue);
        u1search.EqtIDX = int.Parse(ViewState["Device_search"].ToString());// int.Parse(ddlSearch_devices.SelectedValue);
        u1search.Search = ViewState["txtSearchDevice"].ToString();// txtSearchDevice.Text;


        _dtholder.BoxHoldersDevices[0] = u1search;

        _dtholder = callServicePostHolder(urlSelectMasterList, _dtholder);

        setGridData(GvMaster, _dtholder.BoxHoldersDevices);


    }

    protected void SelectDetailHolder()
    {
        _dtholder = new DataHoldersDevices();
        _dtholder.BoxHoldersDevices = new HoldersDevices[1];
        HoldersDevices u1search = new HoldersDevices();

        u1search.DeviceIDX = int.Parse(ViewState["U0IDX"].ToString());

        _dtholder.BoxHoldersDevices[0] = u1search;

        _dtholder = callServicePostHolder(urlSelectDetailHolder, _dtholder);

        FvDetailUser.DataSource = _dtholder.BoxHoldersDevices;
        FvDetailUser.DataBind();

        var rt_NEmpIDX = _dtholder.ReturnEmpNode;
        ViewState["rt_NEmpIDX"] = rt_NEmpIDX;

    }

    protected void SelectHolder()
    {
        var lblrt_Holder = (Label)GvHolder.FindControl("lblrt_Holder");
        HiddenField hfM0NodeIDX = (HiddenField)FvDetailUser.FindControl("hfM0NodeIDX");
        HiddenField hfM0ActoreIDX = (HiddenField)FvDetailUser.FindControl("hfM0ActoreIDX");

        _dtholder = new DataHoldersDevices();
        _dtholder.BoxHoldersDevices = new HoldersDevices[1];
        HoldersDevices u1search = new HoldersDevices();

        u1search.DeviceIDX = int.Parse(ViewState["U0IDX"].ToString());

        _dtholder.BoxHoldersDevices[0] = u1search;

        _dtholder = callServicePostHolder(urlSelectHolder, _dtholder);



        setGridData(GvHolder, _dtholder.BoxHoldersDevices);

        var rt_Code = _dtholder.ReturnCode; // กรณีมีค่าหรือว่าไม่มีค่า
        ViewState["rt_Code"] = rt_Code;

        var rt_Holder = _dtholder.ReturnIDX; // กรณีส่งเลข EmpIDX มาเปิดปิด panel การอนุมัติ
        ViewState["rt_Holder"] = rt_Holder;

        var rt_Active = _dtholder.ReturnMsg; // กรณีหา EmpIDX มาวัดเปิดปิด panel ในการเพิ่มคนใหม่ หากคนเก่าเขาไม่อนุมัติ
        ViewState["rt_Active"] = rt_Active;

        var rt_Count = _dtholder.ReturnCount;
        ViewState["rt_Count"] = rt_Count;

    }

    protected void SelectLogHolder()
    {
        GridView GvHolder = (GridView)ViewIndex.FindControl("GvHolder");
        Repeater rptLog = (Repeater)GvHolder.FindControl("rptLog");


        _dtholder = new DataHoldersDevices();
        _dtholder.BoxHoldersDevices = new HoldersDevices[1];
        HoldersDevices u1search = new HoldersDevices();

        u1search.HolderIDX = int.Parse(ViewState["lblUIDX"].ToString());

        _dtholder.BoxHoldersDevices[0] = u1search;

        _dtholder = callServicePostHolder(urlSelectLogHolder, _dtholder);


        rptLog.DataSource = _dtholder.BoxHoldersDevices;
        rptLog.DataBind();


    }

    protected void SelectApprove1Holder()
    {
        Label lblNHDEmpIDX = (Label)FvDetailUser.FindControl("lblNHDEmpIDX");
        _dtholder = new DataHoldersDevices();
        _dtholder.BoxHoldersDevices = new HoldersDevices[1];
        HoldersDevices u1search = new HoldersDevices();

        u1search.RDeptIDX = int.Parse(ViewState["RdeptIDX_Holder"].ToString());
        u1search.EmpIDX = int.Parse(lblNHDEmpIDX.Text);
        u1search.EqtIDX = 1;
        _dtholder.BoxHoldersDevices[0] = u1search;
        
        _dtholder = callServicePostHolder(urlSelectApprove1Holder, _dtholder);


        ddlapp1.DataSource = _dtholder.BoxHoldersDevices;
        ddlapp1.DataValueField = "EmpIDX";
        ddlapp1.DataTextField = "FullNameTH";
        ddlapp1.DataBind();


    }

    protected void SelectViewSoftware()
    {
        //  text.Text = ViewState["U0IDX"].ToString();

        data_softwarelicense_devices _data_softwarelicense_devicesview = new data_softwarelicense_devices();
        _data_softwarelicense_devicesview.bind_softwarelicense_list = new bind_softwarelicense_detail[1];

        bind_softwarelicense_detail _bind_softwarelicense_detailview = new bind_softwarelicense_detail();

        _bind_softwarelicense_detailview.u0_didx = int.Parse(ViewState["U0IDX"].ToString());

        _data_softwarelicense_devicesview.bind_softwarelicense_list[0] = _bind_softwarelicense_detailview;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkview));

        _data_softwarelicense_devicesview = callServiceSoftwareLicenseDevices(urlGetSoftwareShowHolder, _data_softwarelicense_devicesview);

        chcksoft.DataSource = _data_softwarelicense_devicesview.bind_softwarelicense_list;
        if (_data_softwarelicense_devicesview.bind_softwarelicense_list == null)
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่สามารถเลือกผู้ถือครองได้ กรุณาเพิ่ม Software');", true);
            ViewState["Software"] = "1";
        }
        else
        {
            chcksoft.DataTextField = "software_name";
            chcksoft.DataValueField = "software_name_idx";
            chcksoft.DataBind();

            for (int i = 0; i < chcksoft.Items.Count; i++)
            {
                chcksoft.Items[i].Selected = true;
            }
            ViewState["Software"] = 2;
        }
        //  setFormData(FvViewDetail, FormViewMode.ReadOnly, _data_softwarelicense_devicesview.bind_softwarelicense_list);

    }


    #endregion

    #region reuse
    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //  convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected void getOrganizationList(DropDownList ddlName)
    {
        _dataEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dataEmployee.organization_list[0] = _orgList;

        _dataEmployee = callServicePostEmployee(_urlGetOrganizationList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกองค์กร ---", "0"));
    }

    protected void getDepartmentList(DropDownList ddlName, int _org_idx)
    {
        _dataEmployee.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _dataEmployee.department_list[0] = _deptList;

        _dataEmployee = callServicePostEmployee(_urlGetDepartmentList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกฝ่าย ---", "0"));
    }

    protected void getSectionList(DropDownList ddlName, int _org_idx, int _rdept_idx)
    {
        _dataEmployee.section_list = new section_details[1];
        section_details _secList = new section_details();
        _secList.org_idx = _org_idx;
        _secList.rdept_idx = _rdept_idx;
        _dataEmployee.section_list[0] = _secList;

        _dataEmployee = callServicePostEmployee(_urlGetSectionList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.section_list, "sec_name_th", "rsec_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกแผนก ---", "0"));
    }

    protected void getEmpList(DropDownList ddlName, int _org_idx, int _rdept_idx, int _rsec_idx)
    {
        _dataEmployee.employee_list = new employee_detail[1];
        employee_detail _empList = new employee_detail();
        _empList.org_idx = _org_idx;
        _empList.rdept_idx = _rdept_idx;
        _empList.rsec_idx = _rsec_idx;
        _empList.emp_status = 1;
        _dataEmployee.employee_list[0] = _empList;


        _dataEmployee = callServicePostEmployee(urlGetAll, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.employee_list, "emp_name_th", "emp_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกผู้ถือครอง ---", "0"));
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    #endregion reuse

    #region Insert & Update

    protected void Insert_Holder() // ผูกคนเข้ากับเครื่อง กรณีผู้ถือครองอนุมัติ เมื่อ Support เป็นคนผูก
    {

        var lblrdept = (Label)FvDetailUser.FindControl("lblrdept");
        var lblorg = (Label)FvDetailUser.FindControl("lblorg");
        var lblm0_tdidx = (Label)FvDetailUser.FindControl("lblm0_tdidx");


        _dtholder = new DataHoldersDevices();
        _dtholder.BoxAddHoldersDevices = new AddHoldersDevices[1];
        AddHoldersDevices u1search = new AddHoldersDevices();

        u1search.RDeptIDX_add = int.Parse(lblrdept.Text);
        u1search.RSecIDX_add = int.Parse(ddladdholder_sec.SelectedValue);
        //u1search.PosIDX_add = int.Parse(ddlSearchHolder.SelectedValue);
        u1search.EqtIDX_add = int.Parse(ddlSearch_devices.SelectedValue);
        u1search.EmpIDX_add = int.Parse(ViewState["EmpIDX"].ToString());
        u1search.DeviceIDX_add = int.Parse(ViewState["U0IDX"].ToString());
        u1search.HDEmpIDX_add = int.Parse(ddladdholder_emp.SelectedValue);
        u1search.acidx = 6;
        u1search.status = 7;

        _dtholder.BoxAddHoldersDevices[0] = u1search;

        //string localJson1 = _funcTool.convertObjectToJson(_dtholder);
        //text.Text = localJson1;

        //  text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtholder));
        _dtholder = callServicePostHolder(urlInsert_Holder, _dtholder);


        if (lblm0_tdidx.Text == "1" || lblm0_tdidx.Text == "2")
        {

            try
            {
                data_softwarelicense_devices _data_softwarelicense_devicesinsert = new data_softwarelicense_devices();
                _data_softwarelicense_devicesinsert.u0_softwarelicense_list = new u0_softwarelicense_detail[1];
                u0_softwarelicense_detail _u0_softwarelicense_detail = new u0_softwarelicense_detail();

                _u0_softwarelicense_detail.u0_didx = int.Parse(ViewState["U0IDX"].ToString());
                _u0_softwarelicense_detail.emp_idx = int.Parse(ddladdholder_emp.SelectedValue);

                _data_softwarelicense_devicesinsert.u0_softwarelicense_list[0] = _u0_softwarelicense_detail;
                // text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicense_devicesinsert));
                //string localJson1 = _funcTool.convertObjectToJson(_data_softwarelicense_devicesinsert);
                //text.Text = localJson1;

                _data_softwarelicense_devicesinsert = callServiceSoftwareLicenseDevices(_urlSetSoftwareLicense, _data_softwarelicense_devicesinsert);
            }
            catch
            {
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
            }
        }

      

    }

    protected void Update_Holder() // กรณีผู้รับถือครอง อนุมัติหรือไม่อนุมัติ
    {
        var lblholderidx = (Label)FvDetailUser.FindControl("lblholderidx");
        var lblDeviceIDX = (Label)FvDetailUser.FindControl("lblDeviceIDX");
        var lblm0_tdidx = (Label)FvDetailUser.FindControl("lblm0_tdidx");
        var lblNRDeptIDX = (Label)FvDetailUser.FindControl("lblNRDeptIDX");
        var lblNOrgIDX = (Label)FvDetailUser.FindControl("lblNOrgIDX");
        var lblNRSecIDX = (Label)FvDetailUser.FindControl("lblNRSecIDX");
        var lblNHDEmpIDX = (Label)FvDetailUser.FindControl("lblNHDEmpIDX");

        _dtholder = new DataHoldersDevices();
        _dtholder.BoxAddHoldersDevices = new AddHoldersDevices[1];
        AddHoldersDevices u1search = new AddHoldersDevices();

        u1search.status = int.Parse(ddl_approveholder.SelectedValue);
        u1search.acidx = int.Parse(ViewState["m0_actor"].ToString());
        u1search.noidx = int.Parse(ViewState["m0_node"].ToString());
        u1search.EmpIDX_add = int.Parse(ViewState["EmpIDX"].ToString());
        u1search.HolderIDX = int.Parse(lblholderidx.Text);

        _dtholder.BoxAddHoldersDevices[0] = u1search;
        _dtholder = callServicePostHolder(urlUpdate_Holder, _dtholder);

        if (lblm0_tdidx.Text == "1" || lblm0_tdidx.Text == "2")
        {

            if (ddl_approveholder.SelectedValue == "5")
            {
                try
                {

                    data_device _data_deviceregister = new data_device();

                    _data_deviceregister.u0_device_list = new u0_device_detail[1];
                    u0_device_detail _data_device_detail = new u0_device_detail();

                    ////dv_u0_device
                    _data_device_detail.u0_didx = int.Parse(ViewState["U0IDX"].ToString()); //รหัสอุปกรณ์
                    _data_device_detail.m0_orgidx = int.Parse(lblNOrgIDX.Text);
                    _data_device_detail.R0_depidx = int.Parse(lblNRDeptIDX.Text);
                    _data_device_detail.R0_secidx = int.Parse(lblNRSecIDX.Text);
                    _data_device_detail.doc_status = "1"; //ใช้งานปกติ
                    _data_device_detail.HDEmpIDX = int.Parse(lblNHDEmpIDX.Text); //รหัสพนักงาน ผู้ถือครอง ex. 173

                    ////dv_u1_device
                    _data_device_detail.u0_unidx = 2;
                    _data_device_detail.u0_acidx = 1;
                    _data_device_detail.u0_node_dec = 2;
                    _data_device_detail.u0_doc_decision = 1; //ผลการอนุมัติ,ไม่อนุมัติ
                    _data_device_detail.Approve_stauts = 1; //สถานะเอกสาร

                    _data_deviceregister.u0_device_list[0] = _data_device_detail;
                    ////text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_deviceregister));

                    _data_deviceregister = callServicePostDevice(_url_u0_device_update_17, _data_deviceregister);

                    data_softwarelicense_devices _data_softwarelicense_devicesinsert_ = new data_softwarelicense_devices();
                    _data_softwarelicense_devicesinsert_.u0_softwarelicense_list = new u0_softwarelicense_detail[chcksoft.Items.Count];


                    u0_softwarelicense_detail u0_softwarelicense_detail_dataset = new u0_softwarelicense_detail();
                    u0_softwarelicense_detail_dataset.u0_didx = int.Parse(lblDeviceIDX.Text);
                    u0_softwarelicense_detail_dataset.cemp_idx = int.Parse(ViewState["EmpIDX"].ToString());
                    u0_softwarelicense_detail_dataset.doc_decision = 5;

                    _data_softwarelicense_devicesinsert_.u0_softwarelicense_list[0] = u0_softwarelicense_detail_dataset;


                    //string localJson1 = _funcTool.convertObjectToJson(_data_softwarelicense_devicesinsert_);
                    //text.Text = localJson1;

                    // text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicense_devicesinsert_));


                    _data_softwarelicense_devicesinsert_ = callServiceSoftwareLicenseDevices(urlSetApproveLicense, _data_softwarelicense_devicesinsert_);
                   
                }
                catch
                {
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                }
            }
        }

       
    }

    protected void Update_ApproveChangeHolder() // ผู้รับโอนย้ายรับทราบการโอนย้าย
    {
        var lblholderidx = (Label)FvDetailUser.FindControl("lblholderidx");
        var lblDeviceIDX = (Label)FvDetailUser.FindControl("lblDeviceIDX");
        var lblrdept = (Label)FvDetailUser.FindControl("lblrdept");
        var lblorg = (Label)FvDetailUser.FindControl("lblorg");
        var lblm0_tdidx = (Label)FvDetailUser.FindControl("lblm0_tdidx");

        HiddenField hfM0NodeIDX = (HiddenField)FvDetailUser.FindControl("hfM0NodeIDX");
        HiddenField hfM0ActoreIDX = (HiddenField)FvDetailUser.FindControl("hfM0ActoreIDX");

        int node = int.Parse(hfM0NodeIDX.Value);
        int actor = int.Parse(hfM0ActoreIDX.Value);


        _dtholder = new DataHoldersDevices();
        _dtholder.BoxAddHoldersDevices = new AddHoldersDevices[1];
        AddHoldersDevices u1search = new AddHoldersDevices();

        u1search.status = int.Parse(ddl_changeApprove.SelectedValue);
        u1search.acidx = int.Parse(hfM0ActoreIDX.Value);
        u1search.noidx = int.Parse(hfM0NodeIDX.Value);
        u1search.EmpIDX_add = int.Parse(ViewState["EmpIDX"].ToString());
        u1search.HolderIDX = int.Parse(lblholderidx.Text);
        u1search.Remark = txtremark_approve.Text;
        u1search.DeviceIDX_add = int.Parse(lblDeviceIDX.Text);

        _dtholder.BoxAddHoldersDevices[0] = u1search;


        _dtholder = callServicePostHolder(urlUpdate_ApproveChangeHolder, _dtholder);

        if (ViewState["Software"].ToString() == "2")
        {

            if (int.Parse(hfM0NodeIDX.Value) == 8 && int.Parse(hfM0ActoreIDX.Value) == 2 && int.Parse(ddl_changeApprove.SelectedValue) == 1 ||
                int.Parse(hfM0NodeIDX.Value) == 8 && int.Parse(hfM0ActoreIDX.Value) == 2 && int.Parse(ddl_changeApprove.SelectedValue) == 3)
            {
                try
                {
                    data_device _data_deviceregister = new data_device();

                    _data_deviceregister.u0_device_list = new u0_device_detail[1];
                    u0_device_detail _data_device_detail = new u0_device_detail();

                    //dv_u0_device	
                    _data_device_detail.u0_didx = int.Parse(ViewState["U0IDX"].ToString()); //รหัสอุปกรณ์
                    _data_device_detail.m0_orgidx = int.Parse(ViewState["org_idx"].ToString());
                    _data_device_detail.R0_depidx = int.Parse(ViewState["rdept_idx"].ToString());
                    _data_device_detail.doc_status = "1";// ddl_changeApprove.SelectedValue; //ใช้งานปกติ
                    _data_device_detail.HDEmpIDX = int.Parse(ViewState["EmpIDX"].ToString()); //รหัสพนักงาน ผู้ถือครอง ex. 173

                    // dv_u1_device
                    _data_device_detail.u0_unidx = 2;
                    _data_device_detail.u0_acidx = 1;
                    _data_device_detail.u0_node_dec = 2;
                    _data_device_detail.u0_doc_decision = 1; //ผลการอนุมัติ,ไม่อนุมัติ
                    _data_device_detail.Approve_stauts = 1; //สถานะเอกสาร

                    _data_deviceregister.u0_device_list[0] = _data_device_detail;

                    _data_deviceregister = callServicePostDevice(_url_u0_device_update_17, _data_deviceregister);
                }
                catch
                {
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                }
            }
            else if (int.Parse(ddl_changeApprove.SelectedValue) == 2 || int.Parse(ddl_changeApprove.SelectedValue) == 4)
            {
                if (lblm0_tdidx.Text == "1" || lblm0_tdidx.Text == "2")
                {
                    try
                    {
                        data_softwarelicense_devices _data_softwarelicense_devicesinsert_ = new data_softwarelicense_devices();
                        _data_softwarelicense_devicesinsert_.u0_softwarelicense_list = new u0_softwarelicense_detail[chcksoft.Items.Count];


                        u0_softwarelicense_detail u0_softwarelicense_detail_dataset = new u0_softwarelicense_detail();
                        u0_softwarelicense_detail_dataset.u0_didx = int.Parse(ViewState["U0IDX"].ToString());
                        u0_softwarelicense_detail_dataset.cemp_idx = int.Parse(ViewState["EmpIDX"].ToString());
                        u0_softwarelicense_detail_dataset.doc_decision = 5;

                        _data_softwarelicense_devicesinsert_.u0_softwarelicense_list[0] = u0_softwarelicense_detail_dataset;


                        //string localJson1 = _funcTool.convertObjectToJson(_data_softwarelicense_devicesinsert_);
                        //text.Text = localJson1;

                        // text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicense_devicesinsert_));


                        _data_softwarelicense_devicesinsert_ = callServiceSoftwareLicenseDevices(urlSetApproveLicense, _data_softwarelicense_devicesinsert_);
                    }
                    catch
                    {
                        Page.Response.Redirect(Page.Request.Url.ToString(), true);
                    }
                }
            }
        }
    }

    #endregion

    #region bind Node

    protected void setFormData()
    {

        HiddenField hfM0NodeIDX = (HiddenField)FvDetailUser.FindControl("hfM0NodeIDX");
        ViewState["m0_node"] = hfM0NodeIDX.Value;

        int m0_node = int.Parse(ViewState["m0_node"].ToString());


        switch (m0_node)
        {
            case 1: // สร้าง/แก้ไข

                setFormDataActor();

                break;

            case 2: // พิจารณาผลผู้อนุมัติคนที่ 1
            case 3: // พิจารณาผลผู้อนุมัติคนที่ 2
          // case 4: // พิจารณาผล Asset
            case 5: // พิจารณาผล Support
            case 6: // พิจารณาผล Leader Support
            case 8: // พิจารณาผล ผู้รับโอน (รับโอน)
            case 10: // พิจารณาผลผู้ถือครอง

                setFormDataActor();

                break;

            case 7: // พิจารณาผลผู้รับโอน (รับทราบ)

                setFormDataActor();

                break;

            case 9: // จบการดำเนินการ

                setFormDataActor();

                break;

        }

    }

    #endregion

    #region bind Actor

    protected void setFormDataActor()
    {

        //HiddenField hfM0NodeIDX = (HiddenField)FvDetailUser.FindControl("hfM0NodeIDX");
        HiddenField hfM0ActoreIDX = (HiddenField)FvDetailUser.FindControl("hfM0ActoreIDX");
        //HiddenField hfM0StatusIDX = (HiddenField)FvDetailUser.FindControl("hfM0StatusIDX");

        ViewState["m0_actor"] = hfM0ActoreIDX.Value;
        int m0_actor = int.Parse(hfM0ActoreIDX.Value);

       // text.Text = ViewState["m0_node"].ToString() + "," + ViewState["m0_actor"].ToString() + "," + ViewState["rt_Holder"].ToString() + "," + ViewState["EmpIDX"].ToString();

        switch (m0_actor)
        {

            case 1: // ผู้สร้าง


                break;

            case 2: // ผู้รับโอนย้าย

                chcksoft.Enabled = false;

                if (int.Parse(ViewState["m0_node"].ToString()) == 9)
                {
                    panel_approvechangeHolder.Visible = false;
                }
                else
                {
                    if (int.Parse(ViewState["rt_Holder"].ToString()) == int.Parse(ViewState["EmpIDX"].ToString()) && int.Parse(ViewState["m0_node"].ToString()) == 7 ||
                        int.Parse(ViewState["rt_Holder"].ToString()) == int.Parse(ViewState["EmpIDX"].ToString()) && int.Parse(ViewState["m0_node"].ToString()) == 8)
                    {
                        panel_approvechangeHolder.Visible = true;
                    }
                }


                break;

            case 3: // ผู้อนุมัติคนที่ 1
                chcksoft.Enabled = false;

                if (int.Parse(ViewState["m0_node"].ToString()) == 9)
                {
                    panel_approvechangeHolder.Visible = false;
                }
                else if (int.Parse(ViewState["m0_node"].ToString()) == 2 && int.Parse(ViewState["EmpIDX"].ToString()) == int.Parse(ViewState["rt_NEmpIDX"].ToString()))
                {
                    panel_approvechangeHolder.Visible = true;
                }

                break;
            case 4: // ผู้อนุมัติคนที่ 2
                chcksoft.Enabled = false;

                if (int.Parse(ViewState["m0_node"].ToString()) == 9)
                {
                    panel_approvechangeHolder.Visible = false;
                }
                else if (int.Parse(ViewState["m0_node"].ToString()) == 3 && int.Parse(ViewState["EmpIDX"].ToString()) == int.Parse(ViewState["rt_NEmpIDX"].ToString()))
                {
                    panel_approvechangeHolder.Visible = true;
                }

                break;

            case 5: // Asset

                chcksoft.Enabled = false;

                if (int.Parse(ViewState["m0_node"].ToString()) == 9)
                {
                    panel_approvechangeHolder.Visible = false;
                }
                else if (int.Parse(ViewState["m0_node"].ToString()) == 4 && ViewState["rdept_idx"].ToString() == "9")
                {
                    panel_approvechangeHolder.Visible = true;
                }

                break;


            case 6: // Support

                chcksoft.Enabled = true;

                if (int.Parse(ViewState["m0_node"].ToString()) == 9)
                {
                    panel_approvechangeHolder.Visible = false;
                }
                else if (int.Parse(ViewState["m0_node"].ToString()) == 5 && ViewState["rdept_idx"].ToString() == "20" ||
                    int.Parse(ViewState["m0_node"].ToString()) == 5 && ViewState["rdept_idx"].ToString() == "21")
                {
                    panel_approvechangeHolder.Visible = true;
                }

                break;
            case 7: // leader Support
                chcksoft.Enabled = false;

                if (int.Parse(ViewState["m0_node"].ToString()) == 9)
                {
                    panel_approvechangeHolder.Visible = false;
                }
                else if (int.Parse(ViewState["m0_node"].ToString()) == 6 && ViewState["EmpIDX"].ToString() == "178")
                {
                    panel_approvechangeHolder.Visible = true;
                }


                break;

            case 8: // ผู้ถือครอง


                if (int.Parse(ViewState["m0_node"].ToString()) == 9)
                {
                    panel_approveholder.Visible = false;
                    chcksoft.Enabled = true;
                }
                else
                {
                    if (int.Parse(ViewState["rt_Holder"].ToString()) == int.Parse(ViewState["EmpIDX"].ToString()) && int.Parse(ViewState["m0_node"].ToString()) == 10)
                    {
                        panel_approveholder.Visible = true;

                    }
                    chcksoft.Enabled = false;
                }


                break;
        }

    }

    #endregion

    #region Gridview

    #region SetGrid
    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    #endregion

    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvMaster.EditIndex != e.Row.RowIndex) //to overlook header row
                    {

                        var lbEmpName = (Label)e.Row.FindControl("lbEmpName");
                        var btnsubholder = (LinkButton)e.Row.FindControl("btnsubholder");


                        if (lbEmpName.Text == "")
                        {
                            btnsubholder.Visible = false;
                        }
                        else
                        {
                            btnsubholder.Visible = true;
                        }

                    }

                }
                break;

            case "GvApprove":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                break;

            case "GvHolder":

                var Edit = (LinkButton)e.Row.FindControl("Edit");
                var lblrt_Holder = (Label)e.Row.FindControl("lblrt_Holder");

                // var Edit_Holder = (LinkButton)e.Row.FindControl("Edit_Holder");


                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvHolder.EditIndex != e.Row.RowIndex) //to overlook header row
                    {

                        var lblhoderstate = (Label)e.Row.FindControl("lblhoderstate");
                        var lbstate_ok = (Label)e.Row.FindControl("lbstate_ok");
                        var lbstate_no = (Label)e.Row.FindControl("lbstate_no");
                        var lbstate_wait = (Label)e.Row.FindControl("lbstate_wait");
                        var lblUIDX = (Label)e.Row.FindControl("lblUIDX");
                        var lblHDEmpIDX = (Label)e.Row.FindControl("lblHDEmpIDX");
                        var lblDept = (Label)e.Row.FindControl("lblDept");
                        var lblDept1 = (Label)e.Row.FindControl("lblDept1");
                        var lborgIDX = (Label)e.Row.FindControl("lborgIDX");
                        var lblCEmpIDX = (Label)e.Row.FindControl("lblCEmpIDX");
                        HiddenField hfM0NodeIDX = (HiddenField)FvDetailUser.FindControl("hfM0NodeIDX");
                        HiddenField hfM0ActoreIDX = (HiddenField)FvDetailUser.FindControl("hfM0ActoreIDX");



                        ViewState["lblUIDX"] = lblUIDX.Text;


                        if (int.Parse(lblhoderstate.Text) == 1)  // Active
                        {
                            lbstate_no.Visible = false;
                            lbstate_wait.Visible = false;
                            lbstate_ok.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00CC00");
                            lbstate_ok.Style["font-weight"] = "bold";
                            lbstate_ok.Visible = true;

                            Edit.Visible = false;
                            ViewState["lborgIDX"] = lborgIDX.Text;
                            //ViewState["owner"] = lblHDEmpIDX.Text;

                        }
                        else if (int.Parse(lblhoderstate.Text) == 3)  // Will Active
                        {
                            lbstate_no.Visible = false;
                            lbstate_wait.Visible = true;
                            lbstate_ok.ForeColor = System.Drawing.ColorTranslator.FromHtml("#FFA07A");
                            lbstate_ok.Style["font-weight"] = "bold";
                            lbstate_ok.Visible = false;


                            if (hfM0NodeIDX.Value == "7" && hfM0ActoreIDX.Value == "2" && lblCEmpIDX.Text == ViewState["EmpIDX"].ToString())
                            {

                                Edit.Visible = true;
                            }
                            else
                            {
                                Edit.Visible = false;
                            }
                        }
                        else
                        {
                            lbstate_ok.Visible = false;
                            lbstate_wait.Visible = false;
                            lbstate_no.ForeColor = System.Drawing.ColorTranslator.FromHtml("#A9A9A9");
                            lbstate_no.Style["font-weight"] = "bold";
                            lbstate_no.Visible = true;
                            Edit.Visible = false;
                        }


                        if (lblhoderstate.Text == "1")
                        {
                            ViewState["HDEmpIDX"] = lblHDEmpIDX.Text;
                            if (ViewState["RdeptIDX_Holder"].ToString() == "0")
                            {
                                ViewState["RdeptIDX_Holder"] = lblDept1.Text;
                            }

                        }


                    }
                    else
                    {
                        var ddlchangeholder_editorg = (DropDownList)e.Row.FindControl("ddlchangeholder_editorg");
                        var lbOrg_Edit = (Label)e.Row.FindControl("lbOrg_Edit");
                        var ddlapp1_edit = (DropDownList)e.Row.FindControl("ddlapp1_edit");
                        var ddlchangeholder_editdept = (DropDownList)e.Row.FindControl("ddlchangeholder_editdept");
                        var ddlchangeholder_editsec = (DropDownList)e.Row.FindControl("ddlchangeholder_editsec");
                        var ddlchangeholder_edituser = (DropDownList)e.Row.FindControl("ddlchangeholder_edituser");
                        var lbDept_Edit = (Label)e.Row.FindControl("lbDept_Edit");
                        var lbSec_Edit = (Label)e.Row.FindControl("lbSec_Edit");
                        var lbEmpIDX_Edit = (Label)e.Row.FindControl("lbEmpIDX_Edit");
                        var ddlapp2_edit = (DropDownList)e.Row.FindControl("ddlapp2_edit");
                        var lblUIDX = (Label)e.Row.FindControl("lblUIDX");
                        var lblNHDEmpIDX = (Label)FvDetailUser.FindControl("lblNHDEmpIDX");

                        getOrganizationList(ddlchangeholder_editorg);
                        ddlchangeholder_editorg.SelectedValue = lbOrg_Edit.Text;


                        getDepartmentList(ddlchangeholder_editdept, int.Parse(ddlchangeholder_editorg.SelectedValue));

                        ddlchangeholder_editdept.SelectedValue = lbDept_Edit.Text;




                        getSectionList(ddlchangeholder_editsec, int.Parse(ddlchangeholder_editorg.SelectedValue), int.Parse(ddlchangeholder_editdept.SelectedValue));
                        ddlchangeholder_editsec.SelectedValue = lbSec_Edit.Text;



                        getEmpList(ddlchangeholder_edituser, int.Parse(ddlchangeholder_editorg.SelectedValue), int.Parse(ddlchangeholder_editdept.SelectedValue), int.Parse(ddlchangeholder_editsec.SelectedValue));
                        ddlchangeholder_edituser.SelectedValue = lbEmpIDX_Edit.Text;




                        ///////////////////// เลือกผู้อนุมัติคนที่ 1  /////////////////////

                        _dtholder = new DataHoldersDevices();
                        _dtholder.BoxHoldersDevices = new HoldersDevices[1];
                        HoldersDevices u1search = new HoldersDevices();

                        u1search.DeviceIDX = int.Parse(ViewState["U0IDX"].ToString());
                        u1search.RDeptIDX = int.Parse(ViewState["RdeptIDX_Holder"].ToString());
                        u1search.EqtIDX = 1;
                        u1search.EmpIDX = int.Parse(lblNHDEmpIDX.Text);

                        _dtholder.BoxHoldersDevices[0] = u1search;

                        _dtholder = callServicePostHolder(urlSelectApprove1Holder, _dtholder);


                        ddlapp1_edit.DataSource = _dtholder.BoxHoldersDevices;
                        ddlapp1_edit.DataValueField = "EmpIDX";
                        ddlapp1_edit.DataTextField = "FullNameTH";
                        ddlapp1_edit.DataBind();


                        ///////////////////// เลือกผู้อนุมัติคนที่ 2  /////////////////////


                        _dtholder = new DataHoldersDevices();
                        _dtholder.BoxHoldersDevices = new HoldersDevices[1];
                        HoldersDevices u1search2 = new HoldersDevices();

                        u1search2.RDeptIDX = int.Parse(ddlchangeholder_editdept.SelectedValue);
                        u1search.EqtIDX = 2;
                        _dtholder.BoxHoldersDevices[0] = u1search2;

                        _dtholder = callServicePostHolder(urlSelectApprove1Holder, _dtholder);


                        ddlapp2_edit.DataSource = _dtholder.BoxHoldersDevices;
                        ddlapp2_edit.DataValueField = "EmpIDX";
                        ddlapp2_edit.DataTextField = "FullNameTH";
                        ddlapp2_edit.DataBind();


                    }
                }

                break;


        }

    }

    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();

                //SelectMasterList();

                if (ViewState["rdept_idx"].ToString() != "20" && ViewState["rdept_idx"].ToString() != "21")
                {
                    BoxSearch.Visible = false;

                    if (int.Parse(ViewState["JobGradeIDX"].ToString()) <= 7)
                    {
                        select_view_user7();
                    }
                    else if (int.Parse(ViewState["JobGradeIDX"].ToString()) == 8 || int.Parse(ViewState["JobGradeIDX"].ToString()) == 9)
                    {
                        select_view_user8and9();
                    }
                    else if (int.Parse(ViewState["JobGradeIDX"].ToString()) >= 10)
                    {
                        select_view_user10();
                    }
                }
                else
                {
                    //BoxSearch.Visible = true;
                    //// ddlOrg_Search();
                    //getOrganizationList(ddlSearchOrg);
                    //SelectDevices();
                    SelectMasterList();

                }

                break;

            case "GvApprove":

                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();

                select_approvelist();

                break;

        }
    }

    #endregion

    #region GvRowEditing

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvHolder":

                GvHolder.EditIndex = e.NewEditIndex;
                SelectHolder();

                break;

        }
    }

    #endregion


    #region GvRowCancelingEdit

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvHolder":
                GvHolder.EditIndex = -1;
                SelectHolder();
                break;

        }
    }

    #endregion



    #endregion

    #region ddlSelectedIndexChanged

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        var ddName = (DropDownList)sender;

        switch (ddName.ID)
        {
            case "ddlSearchOrg":
                getDepartmentList(ddlSearchDep, int.Parse(ddlSearchOrg.SelectedValue));


                break;

            case "ddlSearchDep":
                getSectionList(ddlSearchSec, int.Parse(ddlSearchOrg.SelectedValue), int.Parse(ddlSearchDep.SelectedValue));
                getEmpList(ddlSearchHolder, int.Parse(ddlSearchOrg.SelectedValue), int.Parse(ddlSearchDep.SelectedValue), int.Parse(ddlSearchSec.SelectedValue));

                break;

            case "ddlSearchSec":

                getEmpList(ddlSearchHolder, int.Parse(ddlSearchOrg.SelectedValue), int.Parse(ddlSearchDep.SelectedValue), int.Parse(ddlSearchSec.SelectedValue));


                break;



            case "ddladdholder_sec":

                var lblorg = (Label)FvDetailUser.FindControl("lblorg");
                var lblrdept = (Label)FvDetailUser.FindControl("lblrdept");


                getEmpList(ddladdholder_emp, int.Parse(lblorg.Text), int.Parse(lblrdept.Text), int.Parse(ddladdholder_sec.SelectedValue));


                break;



            case "ddlchangeholder_dept":

                getSectionList(ddlchangeholder_sec, int.Parse(ddlchangeholder_org.SelectedValue), int.Parse(ddlchangeholder_dept.SelectedValue));


                _dtholder = new DataHoldersDevices();
                _dtholder.BoxHoldersDevices = new HoldersDevices[1];
                HoldersDevices search1 = new HoldersDevices();

                search1.DeviceIDX = int.Parse(ViewState["U0IDX"].ToString());
                search1.RDeptIDX = int.Parse(ddlchangeholder_dept.SelectedValue);
                search1.EqtIDX = 2;
                _dtholder.BoxHoldersDevices[0] = search1;


                _dtholder = callServicePostHolder(urlSelectApprove1Holder, _dtholder);

                ddlapp2.DataSource = _dtholder.BoxHoldersDevices;
                ddlapp2.DataValueField = "EmpIDX";
                ddlapp2.DataTextField = "FullNameTH";
                ddlapp2.DataBind();

                break;

            case "ddlchangeholder_sec":

                getEmpList(ddlchangeholder_user, int.Parse(ddlchangeholder_org.SelectedValue), int.Parse(ddlchangeholder_dept.SelectedValue), int.Parse(ddlchangeholder_sec.SelectedValue));


                break;


            case "ddlchangeholder_editorg":
                var ddlchangeholder_editorg_1 = (DropDownList)GvHolder.FindControl("ddlchangeholder_editorg");
                var ddlchangeholder_editdept_1 = (DropDownList)GvHolder.FindControl("ddlchangeholder_editdept");
                var lbDept_Edit_1 = (Label)GvHolder.FindControl("lbDept_Edit_1");


                //_dtEmployee = new data_employee();

                //_dtEmployee.department_list = new department_details[1];
                //department_details organite = new department_details();

                //organite.org_idx = int.Parse(ddlchangeholder_editorg_1.SelectedValue);

                //_dtEmployee.department_list[0] = organite;

                //_dtEmployee = callServicePostEmp(urlGetDepartmentList, _dtEmployee);
                //ddlchangeholder_editdept_1.DataSource = _dtEmployee.department_list;
                //ddlchangeholder_editdept_1.DataTextField = "dept_name_th";
                //ddlchangeholder_editdept_1.DataValueField = "rdept_idx";
                //ddlchangeholder_editdept_1.DataBind();
                getDepartmentList(ddlchangeholder_editdept_1, int.Parse(ddlchangeholder_editorg_1.SelectedValue));
                ddlchangeholder_editdept_1.SelectedValue = lbDept_Edit_1.Text;
                break;



            case "ddlchangeholder_editdept":


                var ddNameDep = (DropDownList)sender;

                var row = (GridViewRow)ddNameDep.NamingContainer;

                var ddlchangeholder_editorg = (DropDownList)row.FindControl("ddlchangeholder_editorg");
                var ddlchangeholder_editdept = (DropDownList)row.FindControl("ddlchangeholder_editdept");
                var ddlchangeholder_editsec = (DropDownList)row.FindControl("ddlchangeholder_editsec");
                var ddlapp2_edit = (DropDownList)row.FindControl("ddlapp2_edit");



                //ddlchangeholder_editsec.AppendDataBoundItems = true;
                //ddlchangeholder_editsec.Items.Clear();
                //ddlchangeholder_editsec.Items.Add(new ListItem("กรุณาเลือกแผนก....", "0"));


                //_dtEmployee = new data_employee();

                //_dtEmployee.section_list = new section_details[1];
                //section_details organite13 = new section_details();

                //organite13.org_idx = int.Parse(ddlchangeholder_editorg.SelectedValue);
                //organite13.rdept_idx = int.Parse(ddlchangeholder_editdept.SelectedValue);

                //_dtEmployee.section_list[0] = organite13;

                //_dtEmployee = callServicePostEmp(urlGetSectionList, _dtEmployee);

                //ddlchangeholder_editsec.DataSource = _dtEmployee.section_list;
                //ddlchangeholder_editsec.DataTextField = "sec_name_th";
                //ddlchangeholder_editsec.DataValueField = "rsec_idx";
                //ddlchangeholder_editsec.DataBind();

                getSectionList(ddlchangeholder_editsec, int.Parse(ddlchangeholder_editorg.SelectedValue), int.Parse(ddlchangeholder_editdept.SelectedValue));

                _dtholder = new DataHoldersDevices();
                _dtholder.BoxHoldersDevices = new HoldersDevices[1];
                HoldersDevices u1search2 = new HoldersDevices();

                u1search2.RDeptIDX = int.Parse(ddlchangeholder_editdept.SelectedValue);
                u1search2.EqtIDX = 2;
                _dtholder.BoxHoldersDevices[0] = u1search2;

                _dtholder = callServicePostHolder(urlSelectApprove1Holder, _dtholder);


                ddlapp2_edit.DataSource = _dtholder.BoxHoldersDevices;
                ddlapp2_edit.DataValueField = "EmpIDX";
                ddlapp2_edit.DataTextField = "FullNameTH";
                ddlapp2_edit.DataBind();




                break;

            case "ddlchangeholder_editsec":

                var ddNameDep1 = (DropDownList)sender;

                var row1 = (GridViewRow)ddNameDep1.NamingContainer;

                var ddlchangeholder_editorg1 = (DropDownList)row1.FindControl("ddlchangeholder_editorg");
                var ddlchangeholder_editdept1 = (DropDownList)row1.FindControl("ddlchangeholder_editdept");
                var ddlchangeholder_editsec1 = (DropDownList)row1.FindControl("ddlchangeholder_editsec");
                var ddlchangeholder_edituser = (DropDownList)row1.FindControl("ddlchangeholder_edituser");


                //ddlchangeholder_edituser.AppendDataBoundItems = true;
                //ddlchangeholder_edituser.Items.Clear();
                //ddlchangeholder_edituser.Items.Add(new ListItem("กรุณาเลือกชื่อผู้ถือครอง....", "0"));


                //_dtEmployee.employee_list = new employee_detail[1];
                //employee_detail organite32 = new employee_detail();

                //organite32.org_idx = int.Parse(ddlchangeholder_editorg1.SelectedValue);
                //organite32.rdept_idx = int.Parse(ddlchangeholder_editdept1.SelectedValue);
                //organite32.rsec_idx = int.Parse(ddlchangeholder_editsec1.SelectedValue);
                //organite32.emp_status = 1;

                //_dtEmployee.employee_list[0] = organite32;

                //_dtEmployee = callServicePostEmp(urlGetAll, _dtEmployee);

                //ddlchangeholder_edituser.DataSource = _dtEmployee.employee_list;
                //ddlchangeholder_edituser.DataTextField = "emp_name_th";
                //ddlchangeholder_edituser.DataValueField = "emp_idx";
                //ddlchangeholder_edituser.DataBind();

                getEmpList(ddlchangeholder_edituser, int.Parse(ddlchangeholder_editorg1.SelectedValue), int.Parse(ddlchangeholder_editdept1.SelectedValue), int.Parse(ddlchangeholder_editsec1.SelectedValue));

                break;

        }

    }

    #endregion

    #region Trigger
    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger trigger1 = new PostBackTrigger();
        trigger1.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(trigger1);
    }


    #endregion

    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {

            case "btnIndex":
                lbindex.BackColor = System.Drawing.Color.LightGray;
                lbapprove.BackColor = System.Drawing.Color.Transparent;
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;


            case "btnapprove":

                lbindex.BackColor = System.Drawing.Color.Transparent;
                lbapprove.BackColor = System.Drawing.Color.LightGray;
                MvMaster.SetActiveView(ViewApprove);

                select_approvelist();

                break;


            case "btnsearch":
                ViewState["OrgIDX_search"] = ddlSearchOrg.SelectedValue;
                ViewState["RdepIDX_search"] = ddlSearchDep.SelectedValue;
                ViewState["RsecIDX_search"] = ddlSearchSec.SelectedValue;
                ViewState["EmpIDX_search"] = ddlSearchHolder.SelectedValue;
                ViewState["Device_search"] = ddlSearch_devices.SelectedValue;
                ViewState["txtSearchDevice"] = txtSearchDevice.Text;
                SelectMasterList();
                break;

            case "Edit_Log":

                string[] arg1_Edit = new string[0];
                arg1_Edit = e.CommandArgument.ToString().Split(';');
                int U0IDX_Edit = int.Parse(arg1_Edit[0]);

                _dtholder = new DataHoldersDevices();
                _dtholder.BoxHoldersDevices = new HoldersDevices[1];
                HoldersDevices u1search = new HoldersDevices();

                u1search.HolderIDX = U0IDX_Edit;// int.Parse(lblUIDX.Text);


                _dtholder.BoxHoldersDevices[0] = u1search;
                _dtholder = callServicePostHolder(urlSelectLogHolder, _dtholder);


                rptLog.DataSource = _dtholder.BoxHoldersDevices;
                rptLog.DataBind();

                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);

                break;



            case "ViewHolder":
                Control div_process = (Control)ViewIndex.FindControl("div_process");
             

                string[] arg1 = new string[1];
                arg1 = e.CommandArgument.ToString().Split(';');
                int U0IDX = int.Parse(arg1[0]);

                div_process.Visible = false;
                BoxGridView.Visible = false;
                BoxSearch.Visible = false;
                Div_ChangeHolder.Visible = false;
                ViewState["U0IDX"] = null;
                ViewState["U0IDX"] = U0IDX;

                div_showdata.Visible = true;
                SelectDetailHolder();

               

                SelectHolder();
                HDEMPIDXANDNHDEMPIDX();
                ddl_Approve();
                setFormData();
                SelectViewSoftware();

                if (ViewState["rdept_idx"].ToString() == "20" && ViewState["rt_Code"].ToString() == "1"
                    || ViewState["rdept_idx"].ToString() == "20" && ViewState["rt_Holder"].ToString() == "0"
                    || ViewState["rdept_idx"].ToString() == "20" && ViewState["rt_Active"].ToString() == "0"
                    || ViewState["rdept_idx"].ToString() == "21" && ViewState["rt_Code"].ToString() == "1"
                    || ViewState["rdept_idx"].ToString() == "21" && ViewState["rt_Holder"].ToString() == "0"
                    || ViewState["rdept_idx"].ToString() == "21" && ViewState["rt_Active"].ToString() == "0")
                {
                    btnaddholder.Visible = true;
                }
                else
                {
                    btnaddholder.Visible = false;
                }

                panel_approvechangeHolder.Visible = false;

                break;


            case "ViewChange":
                Control div_process1 = (Control)ViewIndex.FindControl("div_process");
               
                string[] arg1_change = new string[1];
                arg1_change = e.CommandArgument.ToString().Split(';');
                int U0IDX_change = int.Parse(arg1_change[0]);
                ViewState["U0IDX"] = null;
                ViewState["U0IDX"] = U0IDX_change;


                div_process1.Visible = false;
                BoxGridView.Visible = false;
                BoxSearch.Visible = false;
                Div_ChangeHolder.Visible = true;
                div_showdata.Visible = true;
                SelectDetailHolder();

                Control divshowapprover = (Control)FvDetailUser.FindControl("divshowapprover");
                Label lblstatus = (Label)FvDetailUser.FindControl("lblstatus");

                SelectHolder();
                SelectApprove1Holder();
                HDEMPIDXANDNHDEMPIDX();
                ddl_Approve();
                if (lblstatus.Text == "3")
                {
                    divshowapprover.Visible = true;
                }
                else
                {
                    divshowapprover.Visible = false;
                }
                lbchangeholder.Visible = true;

                btnaddholder.Visible = false;
                setFormData();
                //text.Text = ViewState["owner"].ToString();

                //if (ViewState["owner"].ToString() == ViewState["EmpIDX"].ToString())
                //{
                //    chcksoft.Enabled = true;
                //}
                SelectViewSoftware();

                if (ViewState["rt_Count"].ToString() != "3")
                {
                    Div_ChangeHolder.Visible = true;
                }
                else
                {
                    Div_ChangeHolder.Visible = false;
                }


                break;


            case "ViewHolderApprove":
                Control div_process2 = (Control)ViewIndex.FindControl("div_process");
                string[] arg1_app = new string[3];
                arg1_app = e.CommandArgument.ToString().Split(';');
                int U0IDX_app = int.Parse(arg1_app[0]);
                ViewState["U0IDX"] = U0IDX_app;

                MvMaster.SetActiveView(ViewIndex);

                div_process2.Visible = false;
                BoxSearch.Visible = false;
                BoxGridView.Visible = false;
                div_showdata.Visible = true;
                Div_ChangeHolder.Visible = false;
                btnaddholder.Visible = false;
                SelectDetailHolder();
                SelectHolder();
                HDEMPIDXANDNHDEMPIDX();
                ddl_Approve();
                chcksoft.Enabled = false;
                panel_approveholder.Visible = false;
                panel_approvechangeHolder.Visible = false;
                SelectViewSoftware();
                setFormData();

                break;


            case "CmdAddHolder":

                btnaddholder.Visible = false;
                Panel_Add.Visible = true;
                // ddlOrg_Holder();
                Select_Sec();
                Div_ChangeHolder.Visible = false;



                break;

            case "CmdChangeHolder":


                lbchangeholder.Visible = false;
                Panel_changeholder.Visible = true;

                //ddlOrg_ChangeHolder();

                getOrganizationList(ddlchangeholder_org);
                ddlchangeholder_org.SelectedValue = ViewState["lborgIDX"].ToString();

                getDepartmentList(ddlchangeholder_dept, int.Parse(ddlchangeholder_org.SelectedValue));
                //ddlDept_ChangeHolder();
                // text.Text = ddlchangeholder_org.SelectedValue;




                break;



            case "CmdSaveAddUser":

                lbchangeholder.Visible = false;

                if (ddladdholder_sec.SelectedValue == "0" || ddladdholder_emp.SelectedValue == "0")
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาเลือกข้อมูลผู้ถือครองให้ครบ');", true);

                }
                else
                {
                    Insert_Holder();
                    btnaddholder.Visible = false;
                    Panel_Add.Visible = false;
                    SelectHolder();

                    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                }

                break;


            case "CmdUpdateHolder":
                
                if (ViewState["m0_node"].ToString() == "10" && ViewState["m0_actor"].ToString() == "8")// ผูกคนเข้ากับเครื่อง กรณีผู้ถือครองอนุมัติ เมื่อ Support เป็นคนผูก
                {
                    Update_Holder();
                }
                else
                {

                    Update_ApproveChangeHolder();
                }
                panel_approveholder.Visible = false;

                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;


            case "CmdSaveChangeUser":

                var lblDeviceIDX1 = (Label)FvDetailUser.FindControl("lblDeviceIDX");
                var lblholderidx1 = (Label)FvDetailUser.FindControl("lblholderidx");

                if(getCheckPlant(int.Parse(lblDeviceIDX1.Text), int.Parse(ddlchangeholder_user.SelectedValue)) == false)
                {
                    _dtholder = new DataHoldersDevices();
                    _dtholder.BoxAddHoldersDevices = new AddHoldersDevices[1];
                    AddHoldersDevices u1search_ = new AddHoldersDevices();

                    u1search_.HDEmpIDX_add = int.Parse(ViewState["HDEmpIDX"].ToString());
                    u1search_.DeviceIDX_add = int.Parse(lblDeviceIDX1.Text);
                    u1search_.NOrgIDX_add = int.Parse(ddlchangeholder_org.SelectedValue);
                    u1search_.NRDeptIDX_add = int.Parse(ddlchangeholder_dept.SelectedValue);
                    u1search_.NRSecIDX_add = int.Parse(ddlchangeholder_sec.SelectedValue);
                    u1search_.HolderIDX = int.Parse(lblholderidx1.Text);
                    u1search_.EmpIDX_add = int.Parse(ViewState["EmpIDX"].ToString());
                    u1search_.NHDEmpIDX_add = int.Parse(ddlchangeholder_user.SelectedValue);
                    u1search_.NEmpIDX1 = int.Parse(ddlapp1.SelectedValue);
                    u1search_.NEmpIDX2 = int.Parse(ddlapp2.SelectedValue);
                    u1search_.acidx = 1;
                    u1search_.status = 0;
                    u1search_.noidx = 1;

                    if (txtcomment.Text != "")
                    {
                        u1search_.Remark = txtcomment.Text;

                    }
                    else
                    {
                        u1search_.Remark = "-";

                    }

                    _dtholder.BoxAddHoldersDevices[0] = u1search_;
                    _dtholder = callServicePostHolder(urlUpdate_ChangeHolder, _dtholder);


                    if (ViewState["Software"].ToString() == "2")
                    {
                        try
                        {
                            data_softwarelicense_devices _data_softwarelicense_devicesinsert = new data_softwarelicense_devices();
                            _data_softwarelicense_devicesinsert.u0_softwarelicense_list = new u0_softwarelicense_detail[chcksoft.Items.Count];


                            int i = 0;
                            int check = 0;

                            List<String> AddoingList = new List<string>();
                            foreach (ListItem item in chcksoft.Items)
                            {
                                if (item.Selected)
                                {
                                    check = 1;

                                }
                                else
                                {
                                    check = 0;
                                }
                                u0_softwarelicense_detail u0_softwarelicense_detail_dataset = new u0_softwarelicense_detail();
                                u0_softwarelicense_detail_dataset.u0_didx = int.Parse(lblDeviceIDX1.Text);
                                u0_softwarelicense_detail_dataset.emp_idx = int.Parse(ddlchangeholder_user.SelectedValue);
                                u0_softwarelicense_detail_dataset.org_idx = int.Parse(ddlchangeholder_org.SelectedValue);
                                u0_softwarelicense_detail_dataset.rdept_idx = int.Parse(ddlchangeholder_dept.SelectedValue);
                                u0_softwarelicense_detail_dataset.rsec_idx = int.Parse(ddlchangeholder_sec.SelectedValue);
                                u0_softwarelicense_detail_dataset.software_name_idx = int.Parse(item.Value);
                                u0_softwarelicense_detail_dataset.cemp_idx = int.Parse(ViewState["EmpIDX"].ToString());
                                u0_softwarelicense_detail_dataset.m0_node_idx = 1;
                                u0_softwarelicense_detail_dataset.m0_actor_idx = 1;
                                u0_softwarelicense_detail_dataset.doc_decision = 0;
                                u0_softwarelicense_detail_dataset.check_holder_tranfer = check;
                                _data_softwarelicense_devicesinsert.u0_softwarelicense_list[i] = u0_softwarelicense_detail_dataset;
                                i++;

                            }
                            //string localJson1 = _funcTool.convertObjectToJson(_data_softwarelicense_devicesinsert);
                            //text.Text = localJson1;

                            //   text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicense_devicesinsert));
                            _data_softwarelicense_devicesinsert = callServiceSoftwareLicenseDevices(_urlSetSoftwareLicense, _data_softwarelicense_devicesinsert);
                        }
                        catch
                        {
                            Page.Response.Redirect(Page.Request.Url.ToString(), true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาเพิ่ม Software ในภายหลัง');", true);
                    }


                    Page.Response.Redirect(Page.Request.Url.ToString(), true);


                }


                break;

            case "CmdSaveEditChangeUser":


                var lblholderidx = (Label)FvDetailUser.FindControl("lblholderidx");
                var lblDeviceIDX = (Label)FvDetailUser.FindControl("lblDeviceIDX");
                var lblm0_tdidx = (Label)FvDetailUser.FindControl("lblm0_tdidx");

                var ddNameDep = (LinkButton)sender;

                var row = (GridViewRow)ddNameDep.NamingContainer;
                var ddl_status = (DropDownList)row.FindControl("ddl_status");
                var ddlchangeholder_editorg = (DropDownList)row.FindControl("ddlchangeholder_editorg");
                var ddlchangeholder_editdept = (DropDownList)row.FindControl("ddlchangeholder_editdept");
                var ddlchangeholder_editsec = (DropDownList)row.FindControl("ddlchangeholder_editsec");
                var ddlchangeholder_edituser = (DropDownList)row.FindControl("ddlchangeholder_edituser");
                var ddlapp1_edit = (DropDownList)row.FindControl("ddlapp1_edit");
                var ddlapp2_edit = (DropDownList)row.FindControl("ddlapp2_edit");
                



                    _dtholder = new DataHoldersDevices();
                _dtholder.BoxAddHoldersDevices = new AddHoldersDevices[1];
                AddHoldersDevices u1search_edit = new AddHoldersDevices();

                u1search_edit.DeviceIDX_add = int.Parse(lblDeviceIDX.Text);
                u1search_edit.NOrgIDX_add = int.Parse(ddlchangeholder_editorg.SelectedValue);
                u1search_edit.NRDeptIDX_add = int.Parse(ddlchangeholder_editdept.SelectedValue);
                u1search_edit.NRSecIDX_add = int.Parse(ddlchangeholder_editsec.SelectedValue);
                u1search_edit.HolderIDX = int.Parse(lblholderidx.Text);
                u1search_edit.EmpIDX_add = int.Parse(ViewState["EmpIDX"].ToString());
                u1search_edit.NHDEmpIDX_add = int.Parse(ddlchangeholder_edituser.SelectedValue);
                u1search_edit.NEmpIDX1 = int.Parse(ddlapp1_edit.SelectedValue);
                u1search_edit.NEmpIDX2 = int.Parse(ddlapp2_edit.SelectedValue);
                u1search_edit.acidx = 1;
                u1search_edit.noidx = 1;
                u1search_edit.status = int.Parse(ddl_status.SelectedValue);

                _dtholder.BoxAddHoldersDevices[0] = u1search_edit;
                _dtholder = callServicePostHolder(urlUpdate_LogHolder, _dtholder);

                if (lblm0_tdidx.Text == "1" || lblm0_tdidx.Text == "2")
                {
                    if (ViewState["Software"].ToString() == "2")
                    {

                        if (ddl_status.SelectedValue == "0")
                        {
                            data_softwarelicense_devices _data_softwarelicense_devicesinsert_ = new data_softwarelicense_devices();
                            _data_softwarelicense_devicesinsert_.u0_softwarelicense_list = new u0_softwarelicense_detail[chcksoft.Items.Count];

                            int ii = 0;
                            int check1 = 0; ;

                            // List<String> AddoingList = new List<string>();
                            foreach (ListItem item in chcksoft.Items)
                            {
                                if (item.Selected)
                                {
                                    check1 = 1;

                                }
                                else
                                {
                                    check1 = 0;
                                }
                                u0_softwarelicense_detail u0_softwarelicense_detail_dataset = new u0_softwarelicense_detail();
                                u0_softwarelicense_detail_dataset.u0_didx = int.Parse(lblDeviceIDX.Text);
                                u0_softwarelicense_detail_dataset.emp_idx = int.Parse(ddlchangeholder_edituser.SelectedValue);
                                u0_softwarelicense_detail_dataset.org_idx = int.Parse(ddlchangeholder_editorg.SelectedValue);
                                u0_softwarelicense_detail_dataset.rdept_idx = int.Parse(ddlchangeholder_editdept.SelectedValue);
                                u0_softwarelicense_detail_dataset.rsec_idx = int.Parse(ddlchangeholder_editsec.SelectedValue);
                                u0_softwarelicense_detail_dataset.software_name_idx = int.Parse(item.Value);
                                u0_softwarelicense_detail_dataset.cemp_idx = int.Parse(ViewState["EmpIDX"].ToString());
                                u0_softwarelicense_detail_dataset.m0_node_idx = 1;
                                u0_softwarelicense_detail_dataset.m0_actor_idx = 1;
                                u0_softwarelicense_detail_dataset.doc_decision = 0;
                                u0_softwarelicense_detail_dataset.check_holder_tranfer = check1;
                                _data_softwarelicense_devicesinsert_.u0_softwarelicense_list[ii] = u0_softwarelicense_detail_dataset;

                                ii++;

                            }
                            //string localJson1 = _funcTool.convertObjectToJson(_data_softwarelicense_devicesinsert_);
                            //text.Text = localJson1;

                            // text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicense_devicesinsert_));


                            _data_softwarelicense_devicesinsert_ = callServiceSoftwareLicenseDevices(_urlSetSoftwareLicense, _data_softwarelicense_devicesinsert_);
                        }
                        else
                        {
                            data_softwarelicense_devices _data_softwarelicense_devicesinsert_ = new data_softwarelicense_devices();
                            _data_softwarelicense_devicesinsert_.u0_softwarelicense_list = new u0_softwarelicense_detail[chcksoft.Items.Count];

                            //  int ii = 0;

                            u0_softwarelicense_detail u0_softwarelicense_detail_dataset = new u0_softwarelicense_detail();
                            u0_softwarelicense_detail_dataset.u0_didx = int.Parse(lblDeviceIDX.Text);
                            u0_softwarelicense_detail_dataset.cemp_idx = int.Parse(ViewState["EmpIDX"].ToString());
                            u0_softwarelicense_detail_dataset.doc_decision = 4;

                            _data_softwarelicense_devicesinsert_.u0_softwarelicense_list[0] = u0_softwarelicense_detail_dataset;


                            //string localJson1 = _funcTool.convertObjectToJson(_data_softwarelicense_devicesinsert_);
                            //text.Text = localJson1;

                            // text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_softwarelicense_devicesinsert_));


                            _data_softwarelicense_devicesinsert_ = callServiceSoftwareLicenseDevices(urlSetApproveLicense, _data_softwarelicense_devicesinsert_);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาเพิ่ม Software ในภายหลัง');", true);
                    }
                }
                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;



            case "CmdCancelAddUser":

                Panel_changeholder.Visible = false;
                //lbchangeholder.Visible = true;
                btnaddholder.Visible = true;

                Panel_Add.Visible = false;

                //ddladdholder_org.AppendDataBoundItems = true;
                //ddladdholder_org.Items.Clear();
                //ddladdholder_org.Items.Add(new ListItem("กรุณาเลือกบริษัท....", "0"));


                //ddladdholder_dept.AppendDataBoundItems = true;
                //ddladdholder_dept.Items.Clear();
                //ddladdholder_dept.Items.Add(new ListItem("กรุณาเลือกฝ่าย....", "0"));


                ddladdholder_sec.AppendDataBoundItems = true;
                ddladdholder_sec.Items.Clear();
                ddladdholder_sec.Items.Add(new ListItem("กรุณาเลือกแผนก....", "0"));


                ddladdholder_emp.AppendDataBoundItems = true;
                ddladdholder_emp.Items.Clear();
                ddladdholder_emp.Items.Add(new ListItem("กรุณาเลือกชื่อผู้ถือครอง....", "0"));




                //ddladdholder_org.SelectedValue = "0";
                //ddladdholder_dept.SelectedValue = "0";
                ddladdholder_sec.SelectedValue = "0";
                ddladdholder_emp.SelectedValue = "0";




                break;

            case "CmdCancelChangeUser":

                lbchangeholder.Visible = true;
                Panel_changeholder.Visible = false;

                //ddlchangeholder_org.AppendDataBoundItems = true;
                //ddlchangeholder_org.Items.Clear();
                //ddlchangeholder_org.Items.Add(new ListItem("กรุณาเลือกบริษัท....", "0"));


                ddlchangeholder_dept.AppendDataBoundItems = true;
                ddlchangeholder_dept.Items.Clear();
                ddlchangeholder_dept.Items.Add(new ListItem("กรุณาเลือกฝ่าย....", "0"));


                ddlchangeholder_sec.AppendDataBoundItems = true;
                ddlchangeholder_sec.Items.Clear();
                ddlchangeholder_sec.Items.Add(new ListItem("กรุณาเลือกแผนก....", "0"));


                ddlchangeholder_user.AppendDataBoundItems = true;
                ddlchangeholder_user.Items.Clear();
                ddlchangeholder_user.Items.Add(new ListItem("กรุณาเลือกชื่อผู้ถือครอง....", "0"));



                // ddlchangeholder_org.SelectedValue = "0";
                ddlchangeholder_dept.SelectedValue = "0";
                ddlchangeholder_sec.SelectedValue = "0";
                ddlchangeholder_user.SelectedValue = "0";

                Panel_changeholder.Visible = false;

                break;

            case "btnRefresh":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "btnback":
                /* div_showdata.Visible = false;
                 BoxSearch.Visible = true;
                 BoxGridView.Visible = true;

                 MvMaster.SetActiveView(ViewIndex);
                 SelectMasterList();
                 Panel_changeholder.Visible = false;
                 Panel_Add.Visible = false;
                 txtcomment.Text = null;*/

                Page.Response.Redirect(Page.Request.Url.ToString(), true);


                break;


            case "btnguide":


                Response.Write("<script>window.open('https://docs.google.com/document/d/1wpi0fRoJ3ur4oEW1zroPexDkZCm1Ge0bUBgIkLFS1zI/edit?usp=sharing','_blank');</script>");

                break;


            case "btnflow":

                Response.Write("<script>window.open('http://mas.taokaenoi.co.th/images/holder-devices/Flowchart.jpg','_blank');</script>");

                break;
        }
    }

    #endregion

    #region check error plan
    private Boolean getCheckPlant(int _DeviceIDX,int _holder_user)
    {
        Boolean _Boolean = false;
        
        data_itasset dtitseet = new data_itasset();

        dtitseet.its_TransferDevice_u0_action = new its_TransferDevice_u0[1];
        its_TransferDevice_u0 TransferDevice_u0 = new its_TransferDevice_u0();
        TransferDevice_u0.device_idx = _DeviceIDX;
        TransferDevice_u0.emp_idx = _holder_user;
        TransferDevice_u0.operation_status_id = "check_error_plant";
        dtitseet.its_TransferDevice_u0_action[0] = TransferDevice_u0;

        dtitseet = callServicePostITAsset(_urlGetits_u_transfer, dtitseet);
        if(dtitseet.ReturnCode == "0")
        {
            _Boolean = false;
        }
        else
        {
            _Boolean = true;
            //+ _holder_user.ToString()
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('"+ dtitseet.ReturnMsg + "')", true);
        }
        return _Boolean;

    }


    protected data_itasset callServicePostITAsset(string _cmdUrl, data_itasset _dtitseet)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtitseet);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtitseet = (data_itasset)_funcTool.convertJsonToObject(typeof(data_itasset), _localJson);




        return _dtitseet;
    }


    #endregion check error plan

}