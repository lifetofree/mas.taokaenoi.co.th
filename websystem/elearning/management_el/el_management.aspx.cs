﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_el_management : System.Web.UI.Page
{
    #region initial function/data
    function_tool _funcTool = new function_tool();
    data_elearning _data_elearning = new data_elearning();
    function_dmu _func_dmu = new function_dmu();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetel_u_manage = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_u_manage"];
    static string _urlSetInsel_u_manage = _serviceUrl + ConfigurationManager.AppSettings["urlSetInsel_u_manage"];
    static string _urlSetUpdel_u_manage = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdel_u_manage"];
    static string _urlDelel_u_manage = _serviceUrl + ConfigurationManager.AppSettings["urlDelel_u_manage"];
    static string _urlGetErrorel_u_manage = _serviceUrl + ConfigurationManager.AppSettings["urlGetErrorel_u_manage"];


    string _localJson = "";
    int _tempInt = 0;

    string _defaultDdlText;
    string _defaultDdlValue;

    int emp_idx = 0;
    #endregion initial function/data


    protected void Page_Load(object sender, EventArgs e)
    {
        emp_idx = int.Parse(Session["emp_idx"].ToString());

        if (!IsPostBack)
        {
            initPage();
            actionIndex();

        }
    }

    #region selected   
    protected void actionIndex()
    {
        pnlsearch.Visible = true;
        data_elearning data_elearning = new data_elearning();
        data_elearning.el_u_manage_action = new u_manage[1];

        u_manage obj = new u_manage();

        obj.u0_manage_idx = 0;
        obj.operation_status_id = "U0-FULL";
        obj.filter_keyword = txtFilterKeyword.Text;

        data_elearning.el_u_manage_action[0] = obj;
        //litDebug.Text = _funcTool.convertObjectToXml(data_elearning);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_elearning));
        //litDebug.Text = _func_dmu.zJson(_urlGetel_u_manage, data_elearning);
        data_elearning = _func_dmu.zCallServiceNetwork(_urlGetel_u_manage, data_elearning);

        setGridData(GvMaster, data_elearning.el_u_manage_action);

    }
    #endregion selected 
    

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        int _u_manage_idx;
        string _txtemail_name, _txtemail_code, _txtemail_remark;
        int _cemp_idx;

        u_manage objM0_ProductType = new u_manage();

        switch (cmdName)
        {

            case "btnToInsert":
                MvMaster.SetActiveView(ViewInsert);
                break;
            case "btnCancel":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "btnInsert":
                //_txtemail_code = ((TextBox)ViewInsert.FindControl("txtemail_code")).Text.Trim();
                _txtemail_name = ((TextBox)ViewInsert.FindControl("txtemail_name")).Text.Trim();
                _txtemail_remark = ((TextBox)ViewInsert.FindControl("txtemail_remark")).Text.Trim();
                DropDownList _ddlemail_status = (DropDownList)ViewInsert.FindControl("ddlemail_status");
                _cemp_idx = emp_idx;
                //if(checkCodeError(0, _txtemail_code) == true)
                //{
                //    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('รหัสนี้มีในระบบแล้ว ไม่สามารถเพิ่มได้');", true);
                //    return;
                //}
                u_manage obj = new u_manage();
                _data_elearning.el_u_manage_action = new u_manage[1];
                obj.u0_manage_idx = 0;//_type_idx; 
                //obj.email_code = _txtemail_code;
                //obj.email_name = _txtemail_name;
                //obj.email_remark = _txtemail_remark;
                //obj.email_status = int.Parse(_ddlemail_status.SelectedValue);
                //obj.email_created_by = _cemp_idx;

                _data_elearning.el_u_manage_action[0] = obj;

               // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
               
                _data_elearning = _func_dmu.zCallServicePostNetwork(_urlSetInsel_u_manage, _data_elearning);

                if (_data_elearning.return_code == 0)
                {
                    actionIndex();
                    //MvMaster.SetActiveView(ViewIndex);
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                }
                else
                {
                    setError(_data_elearning.return_code.ToString() + " - " + _data_elearning.return_msg);
                }

                //actionCreate();
                //Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "btnDelete":

                _u_manage_idx = int.Parse(cmdArg);
                _cemp_idx = emp_idx;


                _data_elearning.el_u_manage_action = new u_manage[1];
                objM0_ProductType.u0_manage_idx = _u_manage_idx;
               // objM0_ProductType.CEmpIDX = _cemp_idx;

                _data_elearning.el_u_manage_action[0] = objM0_ProductType;

               // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));

                _data_elearning = _func_dmu.zCallServiceNetwork(_urlDelel_u_manage, _data_elearning);


                if (_data_elearning.return_code == 0)
                {

                    //actionIndex();
                    //MvMaster.SetActiveView(ViewIndex);
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);


                }
                else
                {
                    setError(_data_elearning.return_code.ToString() + " - " + _data_elearning.return_msg);
                }

                break;
            case "btnFilter":
                actionIndex();
                break;



        }
    }
    #endregion btnCommand

    #region bind data
    protected void getGridData(string _cmdName, int _actionType)
    {
        // setGridData(gvBookingType, ViewState["listData"]);
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected string getStatus(int status)
    {
        if (status == 1)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();
                actionIndex();
                break;
        }
    }

    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                
                break;
        }
    }

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.EditIndex = e.NewEditIndex;
                actionIndex();
                break;
        }
    }

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.EditIndex = -1;
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
        }
    }

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                
                break;
        }
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj, string sysidx_menu, string menuidx_per, string role_per)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();

        //switch (fvName.ID)
        //{



        //}
    }

    protected void setDataList(DataList dtlName, Object obj)
    {
        dtlName.DataSource = obj;
        dtlName.DataBind();
    }

    #endregion bind data

    #region reuse
    protected void initPage()
    {
        clearSession();
        clearViewState();

        // divAction.Visible = true;
        // gvBookingType.Visible = true;
        // fvBookingType.Visible = false;

        // getGridData("booking_type", 20);
        //setFormData(fvMenu, FormViewMode.Insert, null, "0", "0", "0");


    }

    protected void initPagePermission()
    {
        clearSession();
        clearViewState();

        // divAction.Visible = true;
        // gvBookingType.Visible = true;
        // fvBookingType.Visible = false;

        // getGridData("booking_type", 20);
        //setFormData(fvPermission, FormViewMode.Insert, null, "0", "0", "0");


    }

    protected void setVisible()
    {
        // divAction.Visible = !divAction.Visible;
        // gvBookingType.Visible = !gvBookingType.Visible;
        // fvBookingType.Visible = !fvBookingType.Visible;
    }

    protected void clearSession()
    {
        // Session["checkSubmit"] = null;
    }

    protected void clearViewState()
    {
        // ViewState["listData"] = null;
    }

    protected void setError(string _errorText)
    {
        // divShowError.Visible = !divShowError.Visible;
        // litErrorCode.Text = _dataEmployee.return_code.ToString();
        if (_errorText != "")
        {
            litDebug.Text = "error : " + _errorText;
        }
        else
        {
            litDebug.Text = String.Empty;
        }
    }
    
    public Boolean checkCodeError(int _u_manage_idx, string _email_code)
    {
        Boolean error = false;
        data_elearning data_elearning = new data_elearning();
        data_elearning.el_u_manage_action = new u_manage[1];

        u_manage obj = new u_manage();

        obj.u0_manage_idx = _u_manage_idx;
       // obj.email_code = _email_code;
        data_elearning.el_u_manage_action[0] = obj;
        data_elearning = _func_dmu.zCallServiceNetwork(_urlGetErrorel_u_manage, data_elearning);
        if(data_elearning.el_u_manage_action != null)
        {
            //obj = data_elearning.el_u_manage_action[0];
            //if (obj.email_code.ToString().Trim()== _email_code.Trim())
            //{

            //}
            error = true;

        }
        return error;
    }

    #endregion reuse

}