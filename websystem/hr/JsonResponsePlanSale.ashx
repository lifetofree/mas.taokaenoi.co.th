﻿<%@ WebHandler Language="C#" Class="JsonResponsePlanSale" %>

using System;
using System.Web;
using System.Collections;
using System.Collections.Generic;
using System.Web.SessionState;

public class JsonResponsePlanSale : IHttpHandler, IRequiresSessionState
{

    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/json";

        DateTime start = Convert.ToDateTime(context.Request.QueryString["start"]);
        //try { start = Convert.ToDateTime(context.Request.QueryString["start"]); } catch (Exception e) { start = DateTime.Now; }
        DateTime end = Convert.ToDateTime(context.Request.QueryString["end"]);
        //try { end = Convert.ToDateTime(context.Request.QueryString["end"]); } catch (Exception e) { end = DateTime.Now; }
        int emp_idx = int.Parse(context.Session["emp_idx"].ToString());
        //int u0_empshift_idx = 49;

        //int place_idx = int.Parse(context.Session["PlaceSearch_Room"].ToString());//3;//int.Parse(place_idx_value);//3;//Convert.ToDateTime(context.Request.QueryString["end"]);
        //int m0_room_idx = int.Parse(context.Session["RoomIDXSearch_Room"].ToString());//0;

        List<int> idList = new List<int>();
        List<ImproperPlanSaleCalendarEvent> tasksList = new List<ImproperPlanSaleCalendarEvent>();

        //Generate JSON serializable events dialog show
        foreach (CalendarPlanSaleEvent cevent in EventPlanSaleDAO.getEvents(start, end, emp_idx))
        {
            tasksList.Add(new ImproperPlanSaleCalendarEvent {
                id = cevent.id,
                title = cevent.title,
                start = String.Format("{0:s}", cevent.start),
                end = String.Format("{0:s}", cevent.end),

                description = cevent.description,
                allDay = cevent.allDay,

                u0_unidx = cevent.u0_unidx,
                u0_acidx = cevent.u0_acidx,
                doc_decision = cevent.doc_decision,
                ty_name = cevent.ty_name,
            });
            idList.Add(cevent.id);
        }

        context.Session["idList"] = idList;

        //Serialize events to string
        System.Web.Script.Serialization.JavaScriptSerializer oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
        string sJSON = oSerializer.Serialize(tasksList);

        //Write JSON to response object
        context.Response.Write(sJSON);
    }

    public bool IsReusable {
        get { return false; }
    }
}