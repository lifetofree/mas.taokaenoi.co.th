﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="employee_recurit_v2.aspx.cs" Inherits="websystem_hr_employee_recurit_v2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <script>

        $(document).ready(function () {

        });
    </script>
    <script>
        $(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepicker').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
            });
        });

    </script>

    <script>
        $(function () {
            $('.from-date-datetimepicker').datetimepicker({
                format: 'DD/MM/YYYY HH:mm'
            });
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datetimepicker').datetimepicker({
                    format: 'DD/MM/YYYY HH:mm'
                });
            });
        });

    </script>

    <script>
        $(document).ready(function () {
            window.Sys.WebForms.PageRequestManager.getInstance().add_endRequest(pageLoad);

            function pageLoad() {
                $("#UploadImage").MultiFile();
            }
        })
    </script>

    <script type="text/javascript">
        function printDiv(divName) {
            var panel = document.getElementById(divName);


            //var gvname_ = document.getElementById(gvname);
            var printWindow = window.open('', '', 'height=1000,width=800');
            printWindow.document.write('<html><head><title></title></head>');
            printWindow.document.write('<body style="font-size:12px;">');
            printWindow.document.write('<style>table{width:100%;color:#000000;font-size:10px;};</style>');
            printWindow.document.write(panel.innerHTML);
            printWindow.document.write('</body></html>');
            //document.getElementById(gvname).Enable = true;
            printWindow.document.close();
            setTimeout(function () {
                printWindow.print();
            }, 500);
            //document.getElementById('gvWork_scroll').Visible = true;
            //return false;
        }
    </script>

    <style type="text/css">
        .checkbox {
            padding-left: 20px;
        }

            .checkbox label {
                display: inline-block;
                vertical-align: middle;
                position: relative;
                padding-left: 5px;
            }

                .checkbox label::before {
                    content: "";
                    display: inline-block;
                    position: absolute;
                    width: 17px;
                    height: 17px;
                    left: 0;
                    margin-left: -20px;
                    border: 1px solid #cccccc;
                    border-radius: 3px;
                    background-color: #fff;
                    -webkit-transition: border 0.15s ease-in-out, color 0.15s ease-in-out;
                    -o-transition: border 0.15s ease-in-out, color 0.15s ease-in-out;
                    transition: border 0.15s ease-in-out, color 0.15s ease-in-out;
                }

                .checkbox label::after {
                    display: inline-block;
                    position: absolute;
                    width: 16px;
                    height: 16px;
                    left: 0;
                    top: 0;
                    margin-left: -20px;
                    padding-left: 3px;
                    padding-top: 1px;
                    font-size: 11px;
                    color: #555555;
                }

            .checkbox input[type="checkbox"] {
                opacity: 0;
                z-index: 1;
            }

                .checkbox input[type="checkbox"]:checked + label::after {
                    font-family: "FontAwesome";
                    content: "\f00c";
                }

        .checkbox-primary input[type="checkbox"]:checked + label::before {
            background-color: #337ab7;
            border-color: #337ab7;
        }

        .checkbox-primary input[type="checkbox"]:checked + label::after {
            color: #fff;
        }

        [cursor-set="no-drop"] {
            cursor: no-drop;
            background-color: #a5a5a5;
            border-color: #a5a5a5;
        }

            [cursor-set="no-drop"]:hover {
                background-color: #474747;
                border-color: #474747;
            }

        #upload_profile_pi_edit {
            width: 100%;
        }

        @media (max-width: 772px) {
            .sm-nopadding {
                padding: unset !important
            }
        }
    </style>

    <%----------- Menu Tab Start---------------%>

    <asp:Literal ID="litDebug" runat="server"></asp:Literal>
    <div id="BoxTabMenuIndex" runat="server">
        <div class="form-group">
            <nav class="navbar navbar-default" id="divBtnHead" runat="server" role="navigation">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#Menu1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <div class="sub-navbar">
                            <a class="navbar-brand " href="#"><b>Menu</b></a>
                        </div>
                    </div>

                    <div class="collapse navbar-collapse" id="Menu1">
                        <asp:LinkButton ID="lbindex" CssClass="btn_menulist" runat="server" CommandName="btnIndex" OnCommand="btnCommand" CommandArgument="1">ข้อมูลผู้สมัคร</asp:LinkButton>
                        <asp:LinkButton ID="lbscreen" CssClass="btn_menulist" runat="server" CommandName="btnscreen" OnCommand="btnCommand" CommandArgument="2">คัดกรองข้อมูล</asp:LinkButton>
                        <asp:LinkButton ID="lbrequest" CssClass="btn_menulist" runat="server" CommandName="btnrequest" OnCommand="btnCommand" CommandArgument="3">รายการ Request</asp:LinkButton>
                        <asp:LinkButton ID="lbapprove" CssClass="btn_menulist" runat="server" CommandName="btnapprove" OnCommand="btnCommand" CommandArgument="4">
                            <asp:Label ID="nav_approve" Font-Bold="true" runat="server"></asp:Label>
                        </asp:LinkButton>
                        <asp:LinkButton ID="lbladdscore" CssClass="btn_menulist" runat="server" CommandName="btnaddscore" OnCommand="btnCommand">ตรวจคะแนน</asp:LinkButton>
                        <asp:LinkButton ID="lblreport" CssClass="btn_menulist" runat="server" CommandName="btnreport" OnCommand="btnCommand" CommandArgument="5">รายงาน</asp:LinkButton>
                    </div>
                </div>
                <!-- /.container-fluid -->
            </nav>
        </div>
    </div>
    <%-------------- Menu Tab End--------------%>

    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

        <asp:View ID="ViewIndex" runat="server">
            <div class="col-lg-12">
                <asp:Label ID="fss" runat="server"></asp:Label>
                <asp:HyperLink runat="server" ID="txtfocus" />
                <%-------------- BoxSearch Start--------------%>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-list-alt"></i><strong>&nbsp; ค้นหาข้อมูล</strong></h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-horizontal" id="BoxSearch" runat="server">
                            <asp:FormView ID="Fv_Search_Emp_Index" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                                <InsertItemTemplate>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label2" runat="server" Text="Employee In" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label6" runat="server" Text="วันที่สามารถเริ่มงานได้" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:DropDownList ID="ddlstartdate_in_s" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">เลือกวันที่พร้อมเริ่มงาน.....</asp:ListItem>
                                                <asp:ListItem Value="1">ไม่แน่ใจ</asp:ListItem>
                                                <asp:ListItem Value="2">ทันที</asp:ListItem>
                                                <asp:ListItem Value="3">7 วัน</asp:ListItem>
                                                <asp:ListItem Value="4">15 วัน</asp:ListItem>
                                                <asp:ListItem Value="5">1 เดือน</asp:ListItem>
                                                <asp:ListItem Value="6">2 เดือน</asp:ListItem>
                                                <asp:ListItem Value="7">3 เดือน</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label143" runat="server" Text="Job Category" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label144" runat="server" Text="ประเภทงานที่ต้องการ" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:DropDownList ID="ddltypecategory_s" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">เลือกประเภทงานที่ต้องการ...</asp:ListItem>
                                                <asp:ListItem Value="1">ประจำ</asp:ListItem>
                                                <asp:ListItem Value="2">Part-time</asp:ListItem>
                                                <asp:ListItem Value="3">Freelance</asp:ListItem>
                                                <asp:ListItem Value="4">ฝึกงาน</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label38" runat="server" Text="Identity Card" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label39" runat="server" Text="รหัสบัตรประชาชน" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:TextBox ID="txtidcard_s" MaxLength="13" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label121" runat="server" Text="Employee Name" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label122" runat="server" Text="ชื่อผู้สมัคร" /></b></small>
                                            </h2>
                                        </label>
                                        <div class="col-sm-2">
                                            <asp:TextBox ID="txtname_s" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label101" runat="server" Text="Position" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label102" runat="server" Text="ตำแหน่งงานที่เปิดรับ" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:DropDownList ID="ddlpositionfocus_s" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">เลือกตำแหน่งงาน....</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label103" runat="server" Text="Position Group" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label104" runat="server" Text="กลุ่มประเภทงานที่ต้องการ" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:DropDownList ID="ddlposition_s" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">เลือกประเภทกลุ่มงาน ....</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label226" runat="server" Text="EmpStatus" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label227" runat="server" Text="สถานะพนักงาน" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:DropDownList ID="ddlstatus_s" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="-1">สถานะพนักงาน....</asp:ListItem>
                                                <asp:ListItem Value="1">พนักงาน</asp:ListItem>
                                                <asp:ListItem Value="0">พ้นสภาพ</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-2">
                                            <asp:LinkButton ID="btnsearch_index" class="btn btn-primary" runat="server" Text="ค้นหา" data-original-title="search" data-toggle="tooltip" CommandName="btn_search_index" OnCommand="btnCommand" />
                                            <asp:Button ID="btnclearsearch_index" class="btn btn-primary" runat="server" Text="Clear" data-original-title="search" data-toggle="tooltip" CommandName="Cmdsearch_clear" OnCommand="btnCommand" />
                                        </div>
                                    </div>

                                </InsertItemTemplate>
                            </asp:FormView>
                        </div>
                    </div>
                </div>
                <%-------------- BoxSearch End--------------%>
                <div class="panel-heading">
                    <h3 class="panel-title"><font color="blue"><strong><asp:Label ID="lblsum_list_index" runat="server" /></strong></font></h3>
                </div>

                <asp:GridView ID="GvEmployee"
                    runat="server"
                    AutoGenerateColumns="false"
                    CssClass="table table-striped table-bordered table-hover table-responsive"
                    HeaderStyle-CssClass="table_headCenter"
                    HeaderStyle-Height="40px"
                    AllowPaging="True"
                    PageSize="20"
                    DataKeyNames="emp_idx"
                    OnPageIndexChanging="Master_PageIndexChanging"
                    OnRowDataBound="Master_RowDataBound">

                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                    <EmptyDataTemplate>
                        <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                    </EmptyDataTemplate>

                    <Columns>
                        <asp:TemplateField HeaderText="รูปภาพ" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="1%">
                            <ItemTemplate>

                                <asp:Label ID="lblidentitycard" runat="server" Visible="false" Text='<%# Eval("identity_card") %>'></asp:Label>
                                <asp:Image ID="img_profile" runat="server" Style="width: 100px; height: 100px" class="img-thumbnail"></asp:Image>
                                <asp:Panel ID="ImagProfile_default" runat="server" HorizontalAlign="Center">
                                    <img class="media-object img-thumbnail pull-letf" src='<%=ResolveUrl("~/uploadfiles/profile_picture/default/user-default.png")%>' width="100px" height="100px" style="margin-right: 0px; margin-left: 10px; margin-top: -5px;">
                                </asp:Panel>

                                <asp:Label ID="lblemp_idx" runat="server" Visible="false" Text='<%# Eval("emp_idx") %>'></asp:Label>
                                <asp:Label ID="lblu0_unidx" runat="server" Visible="false" Text='<%# Eval("u0_unidx") %>'></asp:Label>
                                <asp:Label ID="lblu0_acidx" runat="server" Visible="false" Text='<%# Eval("u0_acidx") %>'></asp:Label>
                                <asp:Label ID="lblu0_doc_decision" runat="server" Visible="false" Text='<%# Eval("u0_doc_decision") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="วันที่สมัคร" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="4%">
                            <ItemTemplate>
                                <small>
                                    <b>
                                        <asp:Label ID="fsse" runat="server" Text="วันที่สมัคร : " /></b><asp:Label ID="lbldAsseta" runat="server" Text='<%# Eval("emp_createdate") %>'></asp:Label>
                                    </p>                                    
                                    <b>
                                        <asp:Label ID="Labesl53" runat="server" Text="พร้อมเริ่มงาน : " /></b><asp:Label ID="Lafbel59" runat="server" Text='<%# Eval("WName") %>'></asp:Label>
                                    </p>                                    
                                    <b>
                                        <asp:Label ID="Labsel30" runat="server" Text="สถานะ : " /></b><asp:Label ID="Labrel44" runat="server" Text='<%# Eval("empstatus") %>'></asp:Label>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ชื่อ-นามสกุล" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="8%">
                            <ItemTemplate>
                                <small>
                                    <b>
                                        <asp:Label ID="lblemp_name_th_" runat="server" Text="ชื่อ-สกุล : " /></b><asp:Label ID="lblemp_name_th" runat="server" Text='<%# Eval("emp_name_th") %>'></asp:Label>
                                    </p>                                     
                                    <b>
                                        <asp:Label ID="Label53" runat="server" Text="เพศ : " /></b><asp:Label ID="Label59" runat="server" Text='<%# Eval("sex_name_th") %>'></asp:Label>
                                    / 
                                        <b>
                                            <asp:Label ID="Label47" runat="server" Text="อายุ : " /></b><asp:Label ID="Label49" runat="server" Text='<%# Eval("emp_birthday") %>'></asp:Label>
                                    ปี
                                    </p>
                                    <b>
                                        <asp:Label ID="Label187" runat="server" Text="บัตรประชาชน : " /></b><asp:Label ID="Label188" runat="server" Text='<%# Eval("identity_card") %>'></asp:Label>
                                    </p>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="เพิ่มเติม" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="8%">
                            <ItemTemplate>
                                <small>
                                    <b>
                                        <asp:Label ID="Label185" runat="server" Text="เงินเดือน : " /></b><asp:Label ID="Label186" runat="server" Text='<%# Eval("salary") %>'></asp:Label>
                                    บาท
                                    </p>
                                    <b>
                                        <asp:Label ID="lblemp_mobile2_no_" runat="server" Text="ประเภทงาน : " /></b><asp:Label ID="lblemp_mobile_no" runat="server" Text='<%# Eval("JName") %>'></asp:Label>
                                    </p>                                    
                                    <b>
                                        <asp:Label ID="lblemp_e23mail_" runat="server" Text="ตำแหน่งงาน : " /></b><asp:Label ID="lblem32p_email" runat="server" Text='<%# Eval("P_Name") %>'></asp:Label>
                                    </p>
                                    <b>
                                        <asp:Label ID="Label64" runat="server" Text="กลุ่มตำแหน่งงาน : " /></b><asp:Label ID="Label184" runat="server" Text='<%# Eval("PosGroupNameTH") %>'></asp:Label>
                                    </p>
                                   
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ติดต่อ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="3%">
                            <ItemTemplate>
                                <small>
                                    <b>
                                        <asp:Label ID="lblemp_mobile_no_" runat="server" Text="เบอร์ติดต่อ : " /></b><asp:Label ID="lblemp_m32obile_no" runat="server" Text='<%# Eval("emp_mobile_no") %>'></asp:Label>
                                    </p>                             
                                    <b>
                                        <asp:Label ID="lblemp_email_" runat="server" Text="Email : " /></b><asp:Label ID="lblemp_email" runat="server" Text='<%# Eval("emp_email") %>'></asp:Label>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>


                        <asp:TemplateField HeaderText="แบบทดสอบ" ItemStyle-HorizontalAlign="left" ItemStyle-Width="10%">
                            <ItemTemplate>

                                <small>
                                    <asp:GridView ID="GvTopic" runat="server"
                                        AutoGenerateColumns="false"
                                        CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                        HeaderStyle-CssClass="info small"
                                        HeaderStyle-Height="40px"
                                        AllowPaging="false"
                                        DataKeyNames="m0_toidx"
                                        OnRowDataBound="Master_RowDataBound">

                                        <PagerStyle CssClass="pageCustom" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                        <EmptyDataTemplate>
                                            <div style="text-align: center">Data Cannot Be Found</div>
                                        </EmptyDataTemplate>
                                        <Columns>

                                            <asp:TemplateField ItemStyle-CssClass="text-left" HeaderText="ชุดคำถาม" HeaderStyle-CssClass="text-center" ItemStyle-Width="25%" Visible="true" ItemStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:Label ID="lbltopic_name" runat="server" Text='<%# Eval("topic_name") %>' />
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField ItemStyle-CssClass="text-left" HeaderText="วันที่ดำเนินการ" HeaderStyle-CssClass="text-center" ItemStyle-Width="5%" Visible="true" ItemStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:Label ID="lbldatedoing" runat="server" Text='<%# Eval("datedoing") %>' />
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>


                                            <asp:TemplateField ItemStyle-CssClass="text-left" HeaderText="รวมคะแนน" HeaderStyle-CssClass="text-center" ItemStyle-Width="50%" Visible="true" ItemStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <b>
                                                            <asp:Label ID="lblemp_mobile2_no_" runat="server" Text="คะแนนรวม : " /></b><asp:Label ID="lblemp_mobile_no" runat="server" Text='<%# Eval("total_score") %>'></asp:Label>
                                                        </p>                                    
                                    <b>
                                        <asp:Label ID="lblemp_e23mail_" runat="server" Text="ปรนัย : " /></b><asp:Label ID="lblem32p_email" runat="server" Text='<%# Eval("score_choice") %>'></asp:Label>
                                                        </p>
                                    <b>
                                        <asp:Label ID="Label64" runat="server" Text="อัตนัย : " /></b><asp:Label ID="Label184" runat="server" Text='<%# Eval("score_comment") %>'></asp:Label>
                                                        </p>


                                                          <br />
                                                        <strong>
                                                            <asp:Label ID="Label11" runat="server"> สถานะแบบทดสอบ : </asp:Label></strong>
                                                        <b>
                                                            <asp:Label ID="lbldetermine" runat="server" Text='<%# Eval("determine") %>'></asp:Label></b>
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="สถานะตัดสินใจ" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center" ItemStyle-Width="20%" Visible="true" ItemStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <b>
                                                            <asp:Label ID="lblnode_decision" Visible="false" runat="server" Text='<%# Eval("node_decision") %>'></asp:Label>
                                                            <asp:Label ID="lblm0_tqidx" Visible="false" runat="server" Text='<%# Eval("m0_tqidx") %>'></asp:Label>

                                                            <asp:Label ID="lblstatusinterview" runat="server" Text='<%# Eval("statusinterview") %>' /></b>
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>


                                </small>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="2%">
                            <ItemTemplate>
                                <small>
                                    <b>
                                        <asp:Label ID="lblstatus" runat="server" Text='<%# Eval("status_name") +  " " + Eval("node_name") +  " โดย " + Eval("actor_name") %>' />
                                    </b>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="จัดการ" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="2%">
                            <ItemTemplate>
                                <small>
                                    <asp:LinkButton ID="btnedit_emp" CssClass="btn btn-primary" runat="server" CommandName="btnedit_emp" OnCommand="btnCommand" data-toggle="tooltip" title="รายละเอียด" CommandArgument='<%# Eval("identity_card") %>'><i class="fa fa-edit"></i></asp:LinkButton>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>

            </div>
        </asp:View>

        <asp:View ID="ViewEdit" runat="server">
            <div class="col-lg-12 sm-nopadding">
                <asp:Label ID="Label3" runat="server"></asp:Label>
                <asp:HyperLink runat="server" ID="txtfocus_edit" />
                <div class="panel-body sm-nopadding">
                    <div class="form-horizontal" role="form">
                        <asp:Panel ID="BoxEditEmployee" runat="server">
                            <asp:Label ID="gsd" runat="server"></asp:Label>
                            <asp:Label ID="ffs" runat="server"></asp:Label>
                            <asp:FormView ID="FvEdit_Detail" runat="server" Width="100%" HorizontalAlign="Center" OnDataBound="FvDetail_DataBound">
                                <EditItemTemplate>
                                    <%-------------------------------- ข้อมูลทั่วไป --------------------------------%>
                                    <asp:Panel ID="BoxGen_Edit" runat="server">

                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <asp:Label ID="lblu0_unidx" runat="server" Visible="false" Text='<%# Eval("u0_unidx") %>'></asp:Label>
                                                <asp:Label ID="lblu0_acidx" runat="server" Visible="false" Text='<%# Eval("u0_acidx") %>'></asp:Label>
                                                <asp:Label ID="lblu0_doc_decision" runat="server" Visible="false" Text='<%# Eval("u0_doc_decision") %>'></asp:Label>
                                                <asp:Label ID="lblSRIDX" runat="server" Visible="false" Text=""></asp:Label>
                                                <asp:Label ID="lblemp_idx_approve" runat="server" Visible="false" Text='<%# Eval("emp_idx_approve1") %>'></asp:Label>
                                                <h3 class="panel-title"><i class="glyphicon glyphicon-list-alt"></i><strong>&nbsp; Position Interested / ประเภทงานที่สนใจ</strong></h3>
                                            </div>
                                            <asp:Panel ID="Panel17" runat="server">
                                                <div class="panel-body">
                                                    <div class="form-horizontal" role="form">

                                                        <div class="col-sm-7">
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label ">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label270" runat="server" Text="Employee In" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label271" runat="server" Text="วันที่สามารถเริ่มงานได้" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-5">
                                                                    <asp:Label ID="lblemp_in_idx" runat="server" Visible="false" Text='<%# Eval("EmpINID") %>'></asp:Label>
                                                                    <asp:DropDownList ID="ddlstartdate_in" runat="server" CssClass="form-control" ValidationGroup="FormEdit">
                                                                        <asp:ListItem Value="0">เลือกวันที่พร้อมเริ่มงาน.....</asp:ListItem>
                                                                        <asp:ListItem Value="1">ไม่แน่ใจ</asp:ListItem>
                                                                        <asp:ListItem Value="2">ทันที</asp:ListItem>
                                                                        <asp:ListItem Value="3">7 วัน</asp:ListItem>
                                                                        <asp:ListItem Value="4">15 วัน</asp:ListItem>
                                                                        <asp:ListItem Value="5">1 เดือน</asp:ListItem>
                                                                        <asp:ListItem Value="6">2 เดือน</asp:ListItem>
                                                                        <asp:ListItem Value="7">3 เดือน</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                    <asp:RequiredFieldValidator ID="Req1" ValidationGroup="FormEdit" runat="server" Display="None"
                                                                        ControlToValidate="ddlstartdate_in" Font-Size="11"
                                                                        ErrorMessage="วันที่สามารถเริ่มงานได้ ...."
                                                                        ValidationExpression="วันที่สามารถเริ่มงานได้ ...." InitialValue="0" />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="Valider4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req1" Width="160" />
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <asp:Label ID="Label272" runat="server" ForeColor="Red" Text="***" />
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label204" runat="server" Text="Salary" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label205" runat="server" Text="เงินเดือนที่ต้องการ" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-5">
                                                                    <asp:TextBox ID="txtsalary" CssClass="form-control" runat="server" Text='<%# Eval("salary") %>' ValidationGroup="FormEdit"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="RequirValidator1" ValidationGroup="FormEdit" runat="server" Display="None"
                                                                        ControlToValidate="txtsalary" Font-Size="11"
                                                                        ErrorMessage="เงินเดือนที่ต้องการ" />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="Validtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequirValidator1" Width="160" />
                                                                    <asp:RegularExpressionValidator ID="RetxtUnit" runat="server" ValidationGroup="FormEdit" Display="None"
                                                                        ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                                        ControlToValidate="txtsalary"
                                                                        ValidationExpression="^[0-9]{1,10}$" />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="Valitender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RetxtUnit" Width="160" />
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <asp:Label ID="Label202" runat="server" ForeColor="Red" Text="***" />
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label ">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label198" runat="server" Text="Job Category" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label201" runat="server" Text="ประเภทงานที่ต้องการ" /></b></small>
                                                                    </h2>
                                                                </label>

                                                                <div class="col-sm-5">
                                                                    <asp:Label ID="lbljob_type_idx" runat="server" Visible="false" Text='<%# Eval("JobTypeIDX") %>'></asp:Label>
                                                                    <asp:DropDownList ID="ddltypecategory" runat="server" CssClass="form-control" ValidationGroup="FormEdit">
                                                                        <asp:ListItem Value="0">เลือกประเภทงานที่ต้องการ...</asp:ListItem>
                                                                        <asp:ListItem Value="1">ประจำ</asp:ListItem>
                                                                        <asp:ListItem Value="2">Part-time</asp:ListItem>
                                                                        <asp:ListItem Value="3">Freelance</asp:ListItem>
                                                                        <asp:ListItem Value="4">ฝึกงาน</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                    <asp:RequiredFieldValidator ID="RequiredFr1" ValidationGroup="FormEdit" runat="server" Display="None"
                                                                        ControlToValidate="ddltypecategory" Font-Size="11"
                                                                        ErrorMessage="ประเภทงานที่ต้องการ ...."
                                                                        ValidationExpression="ประเภทงานที่ต้องการ ...." InitialValue="0" />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="Validaer1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFr1" Width="160" />
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <asp:Label ID="Label203" runat="server" ForeColor="Red" Text="***" />
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label ">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label74" runat="server" Text="Position" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label77" runat="server" Text="ตำแหน่งงานที่เปิดรับ" /></b></small>
                                                                    </h2>
                                                                </label>

                                                                <div class="col-sm-5">
                                                                    <asp:Label ID="lblPIDX_edit" runat="server" Visible="false" Text='<%# Eval("PIDX") %>'></asp:Label>
                                                                    <asp:DropDownList ID="ddlpositionfocus" runat="server" CssClass="form-control">
                                                                        <asp:ListItem Value="0">เลือกตำแหน่งงาน....</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label ">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Labeel99" runat="server" Text="Position Group" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label100" runat="server" Text="กลุ่มประเภทงานที่ต้องการ" /></b></small>
                                                                    </h2>
                                                                </label>

                                                                <div class="col-sm-5">
                                                                    <asp:Label ID="lblPosGroupIDX_1" runat="server" Visible="false" Text='<%# Eval("PosGrop1") %>'></asp:Label>
                                                                    <asp:DropDownList ID="ddlposition1" runat="server" CssClass="form-control" ValidationGroup="FormEdit">
                                                                        <asp:ListItem Value="0">เลือกประเภทกลุ่มงาน 1....</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldVal1" ValidationGroup="FormEdit" runat="server" Display="None"
                                                                        ControlToValidate="ddlposition1" Font-Size="11"
                                                                        ErrorMessage="กลุ่มประเภทงานที่ต้องการ ...."
                                                                        ValidationExpression="กลุ่มประเภทงานที่ต้องการ ...." InitialValue="0" />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCala" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldVal1" Width="160" />
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <asp:Label ID="Label206" runat="server" ForeColor="Red" Text="***" />
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label ">
                                                                </label>

                                                                <div class="col-sm-5">
                                                                    <asp:Label ID="lblPosGroupIDX_2" runat="server" Visible="false" Text='<%# Eval("PosGrop2") %>'></asp:Label>
                                                                    <asp:DropDownList ID="ddlposition2" runat="server" CssClass="form-control">
                                                                        <asp:ListItem Value="0">เลือกประเภทกลุ่มงาน 2....</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label ">
                                                                </label>

                                                                <div class="col-sm-5">
                                                                    <asp:Label ID="lblPosGroupIDX_3" runat="server" Visible="false" Text='<%# Eval("PosGrop3") %>'></asp:Label>
                                                                    <asp:DropDownList ID="ddlposition3" runat="server" CssClass="form-control">
                                                                        <asp:ListItem Value="0">เลือกประเภทกลุ่มงาน 3....</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label207" runat="server" Text="Description" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label208" runat="server" Text="รายละเอียดเพิ่มเติม/ความสามารถพิเศษ" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-5">
                                                                    <asp:TextBox ID="txtdescription" CssClass="form-control" runat="server" Text='<%# Eval("DetailProfile") %>' autocomplete="off" MaxLength="250" TextMode="MultiLine" Rows="3" ValidationGroup="AddReference1"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-5">
                                                            <asp:UpdatePanel ID="Up2dateP2anel4" runat="server">
                                                                <ContentTemplate>
                                                                    <div class="form-group">
                                                                        <div class="col-sm-8">
                                                                            <div class="panel panel-primary">
                                                                                <div class="form-group">
                                                                                    <div class="panel-body">
                                                                                        <center><asp:Image ID="img_profile_edit" runat="server" style="width: 100px; height: 100px" class="img-thumbnail"></asp:Image></center>
                                                                                        <center><asp:Panel id="ImagProfile_default_edit" runat="server">
                                                                            <img class="media-object img-thumbnail" src='<%=ResolveUrl("~/uploadfiles/profile_picture/default/user-default.png")%>' width="100px" height="100px" style="margin-right: 8px; margin-top: -5px;">
                                                                        </asp:Panel></center>

                                                                                        &nbsp;&nbsp;<asp:FileUpload ID="upload_profile_pi_edit" AutoPostBack="true" Font-Size="small" ClientIDMode="Static" runat="server" accept="jpg" CssClass="btn btn-sm multi max-1" />
                                                                                        <center><font color="red"><small class="text_right"><p>**เฉพาะนามสกุล jpg</small></font></center>
                                                                                        <hr />
                                                                                        <center><asp:LinkButton ID="btn_save_file1_edit" runat="server" CssClass="btn btn-warning btn-sm" data-toggle="tooltip" title="อัพโหลดภาพ" CommandName="savefile_picture_edit" OnCommand="btnCommand"><i class="glyphicon glyphicon-picture"></i> อัพโหลดภาพ</asp:LinkButton></center>
                                                                                        <center><font color="red"><small class="text_right"><p>กรุณากรอกข้อมูลบัตรประชาชนให้เรียบร้อยก่อนทำการ upload รูปภาพ</small></font></center>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:PostBackTrigger ControlID="btn_save_file1_edit" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                            <asp:HyperLink runat="server" ID="txtfocus_picture_edit" />
                                                        </div>

                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>

                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <h3 class="panel-title"><i class="glyphicon glyphicon-list-alt"></i><strong>&nbsp; Personal Information / ข้อมูลส่วนตัว</strong></h3>
                                                <asp:Label ID="lbl_emp_idx" runat="server" Visible="false" Text='<%# Eval("emp_idx") %>' />
                                            </div>

                                            <asp:Panel ID="pn_bg_genarol" runat="server">
                                                <asp:UpdatePanel ID="Upd3ateaP2anel1" runat="server" UpdateMode="Always">
                                                    <ContentTemplate>
                                                        <div class="panel-body">
                                                            <div class="form-horizontal" role="form">

                                                                <div class="form-group">
                                                                    <label class="col-sm-2 control-label">
                                                                        <h2 class="panel-title">
                                                                            <asp:Label ID="Label222" runat="server" Text="Name(Thai)" /><br />
                                                                            <small><b>
                                                                                <asp:Label ID="Label224" runat="server" Text="ชื่อ(ไทย)" /></b></small>
                                                                        </h2>
                                                                    </label>
                                                                    <div class="col-sm-1" style="padding-right: 0;">
                                                                        <asp:Label ID="lbl_prefix_idx" runat="server" Visible="false" Text='<%# Eval("prefix_idx") %>'></asp:Label>
                                                                        <asp:DropDownList ID="ddl_prefix_th_edit" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" Style="padding-right: 0;">
                                                                            <asp:ListItem Value="1">นาย</asp:ListItem>
                                                                            <asp:ListItem Value="2">นางสาว</asp:ListItem>
                                                                            <asp:ListItem Value="3">นาง</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="col-sm-2">
                                                                        <asp:TextBox ID="txt_name_th" CssClass="form-control" runat="server" Text='<%# Eval("emp_firstname_th") %>' ValidationGroup="FormEdit"></asp:TextBox>

                                                                        <asp:RequiredFieldValidator ID="C_NameTH" runat="server" ControlToValidate="txt_name_th" Display="None" SetFocusOnError="true" ErrorMessage="*กรอกข้อมูล" ValidationGroup="FormEdit" />
                                                                        <ajaxToolkit:ValidatorCalloutExtender ID="V_NameTH" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="C_NameTH" Width="160" />

                                                                        <asp:RegularExpressionValidator ID="R_NamTH" runat="server" ErrorMessage="*เฉพาะตัวอักษรภาษาไทยเท่านั้น" SetFocusOnError="true" Display="None" ControlToValidate="txt_name_th" ValidationExpression="^[ก-๙'.\s]{1,250}$" ValidationGroup="FormEdit" />
                                                                        <ajaxToolkit:ValidatorCalloutExtender ID="V_NameTH2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_NamTH" Width="160" />
                                                                    </div>
                                                                    <div class="col-sm-1">
                                                                        <asp:Label ID="Label215" runat="server" ForeColor="Red" Text="***" />
                                                                    </div>


                                                                    <label class="col-sm-1 control-label">
                                                                        <h2 class="panel-title">
                                                                            <asp:Label ID="Label1" runat="server" Text="Lastname" /><br />
                                                                            <small><b>
                                                                                <asp:Label ID="Label4" runat="server" Text="นามสกุล" /></b></small>
                                                                        </h2>
                                                                    </label>
                                                                    <div class="col-sm-2">
                                                                        <asp:TextBox ID="txt_surname_th" CssClass="form-control" runat="server" Text='<%# Eval("emp_lastname_th") %>' ValidationGroup="FormEdit"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="C_LastnameTH" runat="server" ControlToValidate="txt_surname_th" Display="None" SetFocusOnError="true" ErrorMessage="*กรอกข้อมูล" ValidationGroup="FormEdit" />
                                                                        <ajaxToolkit:ValidatorCalloutExtender ID="V_LastnameTH" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="C_LastnameTH" Width="160" />

                                                                        <asp:RegularExpressionValidator ID="R_LastNameTH" runat="server" ErrorMessage="*เฉพาะตัวอักษรภาษาไทยเท่านั้น" SetFocusOnError="true" Display="None" ControlToValidate="txt_surname_th" ValidationExpression="^[ก-๙'.\s]{1,250}$" ValidationGroup="FormEdit" />
                                                                        <ajaxToolkit:ValidatorCalloutExtender ID="V_LastNameTH2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_LastNameTH" Width="160" />
                                                                    </div>
                                                                    <div class="col-sm-1">
                                                                        <asp:Label ID="Label216" runat="server" ForeColor="Red" Text="***" />
                                                                    </div>

                                                                    <div class="col-sm-2">
                                                                        <asp:LinkButton ID="btnprint" runat="server" CssClass="btn btn-primary" OnCommand="btnCommand" OnClientClick="return printDiv('printableAreaHistoryEmp');" CommandName="btnprint"><i class="fa fa-print"></i>&nbsp;&nbsp; Print Form &nbsp;</asp:LinkButton>
                                                                    </div>

                                                                </div>


                                                                <div class="form-group">
                                                                    <label class="col-sm-2 control-label">
                                                                        <h2 class="panel-title">
                                                                            <asp:Label ID="Label5" runat="server" Text="Name(English)" /><br />
                                                                            <small><b>
                                                                                <asp:Label ID="Label22" runat="server" Text="ชื่อ(อังกฤษ)" /></b></small>
                                                                        </h2>
                                                                    </label>
                                                                    <div class="col-sm-1" style="padding-right: 0;">
                                                                        <asp:DropDownList ID="ddl_prefix_en_edit" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" Style="padding-right: 0;">
                                                                            <asp:ListItem Value="1">Mr.</asp:ListItem>
                                                                            <asp:ListItem Value="2">Miss</asp:ListItem>
                                                                            <asp:ListItem Value="3">Mrs.</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="col-sm-2">
                                                                        <asp:TextBox ID="txt_name_en" CssClass="form-control" runat="server" Text='<%# Eval("emp_firstname_en") %>' ValidationGroup="FormEdit"></asp:TextBox>

                                                                        <asp:RequiredFieldValidator ID="C_NameEN" runat="server" ControlToValidate="txt_name_en" Display="None" SetFocusOnError="true" ErrorMessage="*กรอกข้อมูล" ValidationGroup="FormEdit" />
                                                                        <ajaxToolkit:ValidatorCalloutExtender ID="V_NameEN" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="C_NameEN" Width="160" />

                                                                        <asp:RegularExpressionValidator ID="R_NameEN" runat="server" ErrorMessage="*เเฉพาะตัวอักษรภาษาอังกฤษเท่านั้น" SetFocusOnError="true" Display="None" ControlToValidate="txt_name_en" ValidationExpression="^[a-zA-Z'.\s]{1,250}$" ValidationGroup="FormEdit" />
                                                                        <ajaxToolkit:ValidatorCalloutExtender ID="V_NameEN2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_NameEN" Width="160" />
                                                                    </div>
                                                                    <div class="col-sm-1">
                                                                        <asp:Label ID="Label217" runat="server" ForeColor="Red" Text="***" />
                                                                    </div>

                                                                    <label class="col-sm-1 control-label">
                                                                        <h2 class="panel-title">
                                                                            <asp:Label ID="Label8" runat="server" Text="Lastname" /><br />
                                                                            <small><b>
                                                                                <asp:Label ID="Label24" runat="server" Text="นามสกุล" /></b></small>
                                                                        </h2>
                                                                    </label>
                                                                    <div class="col-sm-2">
                                                                        <asp:TextBox ID="txt_surname_en" CssClass="form-control" runat="server" Text='<%# Eval("emp_lastname_en") %>' ValidationGroup="FormEdit"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="C_LastnameEN" runat="server" ControlToValidate="txt_surname_en" Display="None" SetFocusOnError="true" ErrorMessage="*กรอกข้อมูล" ValidationGroup="FormEdit" />
                                                                        <ajaxToolkit:ValidatorCalloutExtender ID="V_LastnameEN" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="C_LastnameEN" Width="160" />

                                                                        <asp:RegularExpressionValidator ID="R_LastNameEN" runat="server" ErrorMessage="*เฉพาะตัวอักษรภาษาอังกฤษเท่านั้น" SetFocusOnError="true" Display="None" ControlToValidate="txt_surname_en" ValidationExpression="^[a-zA-Z'.\s]{1,250}$" ValidationGroup="FormEdit" />
                                                                        <ajaxToolkit:ValidatorCalloutExtender ID="V_LastnameEN2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_LastNameEN" Width="160" />
                                                                    </div>
                                                                    <div class="col-sm-1">
                                                                        <asp:Label ID="Label218" runat="server" ForeColor="Red" Text="***" />
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label class="col-sm-2 control-label">
                                                                        <h2 class="panel-title">
                                                                            <asp:Label ID="Label10" runat="server" Text="NickName(Thai)" /><br />
                                                                            <small><b>
                                                                                <asp:Label ID="Label25" runat="server" Text="ชื่อเล่น(ไทย)" /></b></small>
                                                                        </h2>
                                                                    </label>
                                                                    <div class="col-sm-2">
                                                                        <asp:TextBox ID="txt_nickname_th" CssClass="form-control" runat="server" Text='<%# Eval("emp_nickname_th") %>' ValidationGroup="FormEdit"></asp:TextBox>

                                                                        <asp:RegularExpressionValidator ID="R_NickNameTH" runat="server" ErrorMessage="*เฉพาะตัวอักษรภาษาไทยเท่านั้น" SetFocusOnError="true" Display="None" ControlToValidate="txt_nickname_th" ValidationExpression="^[ก-๙'.\s]{1,250}$" ValidationGroup="FormEdit" />
                                                                        <ajaxToolkit:ValidatorCalloutExtender ID="V_NickNameTH" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_NickNameTH" Width="160" />
                                                                    </div>
                                                                    <div class="col-sm-1">
                                                                        <asp:Label ID="Label14" runat="server" ForeColor="Red" Text="***" />
                                                                    </div>

                                                                    <label class="col-sm-2 control-label">
                                                                        <h2 class="panel-title">
                                                                            <asp:Label ID="Label11" runat="server" Text="NickName(English)" /><br />
                                                                            <small><b>
                                                                                <asp:Label ID="Label26" runat="server" Text="ชื่อเล่น(อังกฤษ)" /></b></small>
                                                                        </h2>
                                                                    </label>
                                                                    <div class="col-sm-2">
                                                                        <asp:TextBox ID="txt_nickname_en" CssClass="form-control" runat="server" Text='<%# Eval("emp_nickname_en") %>' ValidationGroup="FormEdit"></asp:TextBox>

                                                                        <asp:RegularExpressionValidator ID="R_NickNameEN" runat="server" ErrorMessage="*เฉพาะตัวอักษรภาษาไทยเท่านั้น" SetFocusOnError="true" Display="None" ControlToValidate="txt_nickname_en" ValidationExpression="^[a-zA-Z'.\s]{1,250}$" ValidationGroup="FormEdit" />
                                                                        <ajaxToolkit:ValidatorCalloutExtender ID="V_NickNameEN" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_NickNameEN" Width="160" />
                                                                    </div>
                                                                    <div class="col-sm-1">
                                                                        <asp:Label ID="Label18" runat="server" ForeColor="Red" Text="***" />
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label class="col-sm-2 control-label">
                                                                        <h2 class="panel-title">
                                                                            <asp:Label ID="Label15" runat="server" Text="Birthday" /><br />
                                                                            <small><b>
                                                                                <asp:Label ID="Label16" runat="server" Text="วันเกิด" /></b></small>
                                                                        </h2>
                                                                    </label>
                                                                    <div class="col-sm-2">
                                                                        <div class='input-group date'>
                                                                            <asp:TextBox ID="txtbirth" CssClass="form-control from-date-datepicker" MaxLengh="100%" runat="server" Text='<%# Eval("emp_birthday") %>' ValidationGroup="FormEdit"></asp:TextBox>
                                                                            <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                                        </div>
                                                                        <asp:RequiredFieldValidator ID="RdVasator2" ValidationGroup="FormEdit" runat="server" Display="None"
                                                                            ControlToValidate="txtbirth" Font-Size="11"
                                                                            ErrorMessage="วันเกิด ...." />
                                                                        <ajaxToolkit:ValidatorCalloutExtender ID="Vsalidlwoer6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RdVasator2" Width="160" />
                                                                    </div>
                                                                    <div class="col-sm-1">
                                                                        <asp:Label ID="Label108" runat="server" ForeColor="Red" Text="***" />
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label class="col-sm-2 control-label ">
                                                                        <h2 class="panel-title">
                                                                            <asp:Label ID="Label27" runat="server" Text="Sex" /><br />
                                                                            <small><b>
                                                                                <asp:Label ID="Label28" runat="server" Text="เพศ" /></b></small>
                                                                        </h2>
                                                                    </label>

                                                                    <div class="col-sm-2">
                                                                        <asp:Label ID="lbl_sex_idx" runat="server" Visible="false" Text='<%# Eval("sex_idx") %>'></asp:Label>
                                                                        <asp:DropDownList ID="ddl_sex" runat="server" CssClass="form-control" ValidationGroup="FormEdit">
                                                                            <asp:ListItem Value="0">เลือกสถานะเพศ....</asp:ListItem>
                                                                            <asp:ListItem Value="1">ชาย</asp:ListItem>
                                                                            <asp:ListItem Value="2">หญิง</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                        <asp:RequiredFieldValidator ID="Reqqu9iar1" ValidationGroup="FormEdit" runat="server" Display="None"
                                                                            ControlToValidate="ddl_sex" Font-Size="11"
                                                                            ErrorMessage="เพศ ...."
                                                                            ValidationExpression="เพศ ...." InitialValue="0" />
                                                                        <ajaxToolkit:ValidatorCalloutExtender ID="Valistoder1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Reqqu9iar1" Width="160" />
                                                                    </div>
                                                                    <div class="col-sm-1">
                                                                        <asp:Label ID="Label20" runat="server" ForeColor="Red" Text="***" />
                                                                    </div>

                                                                    <label class="col-sm-2 control-label">
                                                                        <h2 class="panel-title">
                                                                            <asp:Label ID="Labe3l15" runat="server" Text="Status" /><br />
                                                                            <small><b>
                                                                                <asp:Label ID="Lab2el16" runat="server" Text="สถานภาพ" /></b></small>
                                                                        </h2>
                                                                    </label>

                                                                    <div class="col-sm-2">
                                                                        <asp:Label ID="lbl_married_status_idx" runat="server" Visible="false" Text='<%# Eval("married_status") %>'></asp:Label>
                                                                        <asp:DropDownList ID="ddl_status" runat="server" CssClass="form-control">
                                                                            <asp:ListItem Value="0">เลือกสถานภาพ....</asp:ListItem>
                                                                            <asp:ListItem Value="1">โสด</asp:ListItem>
                                                                            <asp:ListItem Value="2">สมรส</asp:ListItem>
                                                                            <asp:ListItem Value="3">หย่าร้าง</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label class="col-sm-2 control-label">
                                                                        <h2 class="panel-title">
                                                                            <asp:Label ID="Labe4l231" runat="server" Text="Nationality" /><br />
                                                                            <small><b>
                                                                                <asp:Label ID="Lab3el232" runat="server" Text="สัญชาติ" /></b></small>
                                                                        </h2>
                                                                    </label>
                                                                    <div class="col-sm-2">
                                                                        <asp:Label ID="lbl_nation_idx" runat="server" Visible="false" Text='<%# Eval("nat_idx") %>'></asp:Label>
                                                                        <asp:DropDownList ID="ddlnation_edit" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                            <asp:ListItem Value="0">เลือกสัญชาติ....</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>

                                                                    <label class="col-sm-2 control-label col-sm-offset-1">
                                                                        <h2 class="panel-title">
                                                                            <asp:Label ID="La2bel42" runat="server" Text="Race" /><br />
                                                                            <small><b>
                                                                                <asp:Label ID="La4bel43" runat="server" Text="เชื้อชาติ" /></b></small>
                                                                        </h2>
                                                                    </label>

                                                                    <div class="col-sm-2">
                                                                        <asp:Label ID="lbl_race_idx" runat="server" Visible="false" Text='<%# Eval("race_idx") %>'></asp:Label>
                                                                        <asp:DropDownList ID="ddlrace" runat="server" CssClass="form-control">
                                                                            <asp:ListItem Value="0">เลือกเชื้อชาติ....</asp:ListItem>
                                                                            <asp:ListItem Value="1">ไทย</asp:ListItem>
                                                                            <asp:ListItem Value="2">ลาว</asp:ListItem>
                                                                            <asp:ListItem Value="3">พม่า</asp:ListItem>
                                                                            <asp:ListItem Value="4">กัมพูชา</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label class="col-sm-2 control-label">
                                                                        <h2 class="panel-title">
                                                                            <asp:Label ID="Lab4el47" runat="server" Text="Religion" /><br />
                                                                            <small><b>
                                                                                <asp:Label ID="Lab3el49" runat="server" Text="ศาสนา" /></b></small>
                                                                        </h2>
                                                                    </label>

                                                                    <div class="col-sm-2">
                                                                        <asp:Label ID="lbl_rel_idx" runat="server" Visible="false" Text='<%# Eval("rel_idx") %>'></asp:Label>
                                                                        <asp:DropDownList ID="ddlreligion" runat="server" CssClass="form-control">
                                                                            <asp:ListItem Value="0">เลือกศาสนา....</asp:ListItem>
                                                                            <asp:ListItem Value="1">ศาสนาพุทธ</asp:ListItem>
                                                                            <asp:ListItem Value="2">ศาสนาคริสต์</asp:ListItem>
                                                                            <asp:ListItem Value="3">ศาสนาอิศลาม</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label class="col-sm-2 control-label">
                                                                        <h2 class="panel-title">
                                                                            <asp:Label ID="Label29" runat="server" Text="MilitaryStatus" /><br />
                                                                            <small><b>
                                                                                <asp:Label ID="Label31" runat="server" Text="สถานะทางทหาร" /></b></small>
                                                                        </h2>
                                                                    </label>

                                                                    <div class="col-sm-2">
                                                                        <asp:Label ID="lbl_mil_idx" runat="server" Visible="false" Text='<%# Eval("mil_idx") %>'></asp:Label>
                                                                        <asp:DropDownList ID="ddlmilitary_edit" runat="server" CssClass="form-control" ValidationGroup="FormEdit">
                                                                            <asp:ListItem Value="1">ผ่านการเกณทหาร</asp:ListItem>
                                                                            <asp:ListItem Value="2">เรียนรักษาดินแดน</asp:ListItem>
                                                                            <asp:ListItem Value="3">ได้รับข้อยกเว้น</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                        <asp:RequiredFieldValidator ID="Re3atsor1" ValidationGroup="FormEdit" runat="server" Display="None"
                                                                            ControlToValidate="ddlmilitary_edit" Font-Size="11"
                                                                            ErrorMessage="สถานะทางทหาร ...."
                                                                            ValidationExpression="สถานะทางทหาร ...." InitialValue="0" />
                                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValirCalder1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re3atsor1" Width="160" />
                                                                    </div>
                                                                    <div class="col-sm-1">
                                                                        <asp:Label ID="Label21" runat="server" ForeColor="Red" Text="***" />
                                                                    </div>

                                                                    <label class="col-sm-2 control-label">
                                                                        <h2 class="panel-title">
                                                                            <asp:Label ID="Label32" runat="server" Text="ID Card" /><br />
                                                                            <small><b>
                                                                                <asp:Label ID="Label33" runat="server" Text="เลขบัตรประชาชน / Tax ID (ต่างชาติ)" /></b></small>
                                                                        </h2>
                                                                    </label>
                                                                    <div class="col-sm-2">
                                                                        <asp:Label ID="lblidentity_card_edit" runat="server" Visible="false" Text='<%# Eval("identity_card") %>'></asp:Label>
                                                                        <asp:TextBox ID="txtidcard" CssClass="form-control" runat="server" ValidationGroup="FormEdit" Text='<%# Eval("identity_card") %>'></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="Reatsor1" ValidationGroup="FormEdit" runat="server" Display="None"
                                                                            ControlToValidate="txtidcard" Font-Size="11"
                                                                            ErrorMessage="เลขบัตรประชาชน" />
                                                                        <ajaxToolkit:ValidatorCalloutExtender ID="Vaasalwr1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Reatsor1" Width="160" />
                                                                        <asp:RegularExpressionValidator ID="Regaddaur1" runat="server" ValidationGroup="FormEdit" Display="None"
                                                                            ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                                            ControlToValidate="txtidcard"
                                                                            ValidationExpression="^[0-9]{1,13}$" />
                                                                        <ajaxToolkit:ValidatorCalloutExtender ID="Valiasdader4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Regaddaur1" Width="160" />
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label class="col-sm-2 control-label">
                                                                        <h2 class="panel-title">
                                                                            <asp:Label ID="Label36" runat="server" Text="Issued At" /><br />
                                                                            <small><b>
                                                                                <asp:Label ID="Label37" runat="server" Text="อกกให้ ณ" /></b></small>
                                                                        </h2>
                                                                    </label>
                                                                    <div class="col-sm-2">
                                                                        <asp:TextBox ID="txtissued_at" CssClass="form-control" runat="server" Text='<%# Eval("issued_at") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                                    </div>

                                                                    <label class="col-sm-2 control-label col-sm-offset-1">
                                                                        <h2 class="panel-title">
                                                                            <asp:Label ID="Label34" runat="server" Text="Expiration Date" /><br />
                                                                            <small><b>
                                                                                <asp:Label ID="Label35" runat="server" Text="วันหมดอายุ" /></b></small>
                                                                        </h2>
                                                                    </label>
                                                                    <div class="col-sm-2">
                                                                        <div class='input-group date'>
                                                                            <asp:TextBox ID="txtexpcard" CssClass="form-control from-date-datepicker" MaxLengh="100%" runat="server" Text='<%# Eval("idate_expired") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                                            <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                                        </div>
                                                                    </div>
                                                                </div>


                                                                <div class="form-group">

                                                                    <%--<label class="col-sm-2 control-label">
                                                                        <h2 class="panel-title">
                                                                            <asp:Label ID="Labsel2271" runat="server" Text="Upload document" /><br />
                                                                            <small><b>
                                                                                <asp:Label ID="Labe2dl269" runat="server" Text="เอกสารเพิ่มเติม" /></b></small>
                                                                        </h2>
                                                                    </label>
                                                                    <div class="col-sm-3">
                                                                        <div class="panel panel-red">
                                                                            <div class="form-group">
                                                                                <div class="panel-body">
                                                                                    <asp:FileUpload ID="FileUpload_edit" ViewStateMode="Enabled" AutoPostBack="true" Font-Size="small" ClientIDMode="Static" runat="server" CssClass="btn btn-sm multi" accept="gif|jpg|png|JPEG|pdf" />
                                                                                    <center><font color="red"><small class="text_right"><p>**เฉพาะนามสกุล gif , jpg , png , pdf</small></font></center>
                                                                                    <hr />
                                                                                    <center><asp:LinkButton ID="btn_save_document_edit" runat="server" CssClass="btn btn-warning btn-sm" data-toggle="tooltip" title="อัพโหลดภาพ" CommandName="savefile_document_edit" OnCommand="btnCommand"><i class="glyphicon glyphicon-picture"></i> อัพโหลดภาพ</asp:LinkButton></center>
                                                                                    <center><font color="red"><small class="text_right"><p>กรุณากรอกข้อมูลบัตรประชาชนให้เรียบร้อยก่อนทำการ upload รูปภาพ</small></font></center>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>--%>

                                                                    <label class="col-sm-2 control-label">
                                                                        <h2 class="panel-title">
                                                                            <asp:Label ID="Labfel223" runat="server" Text="Document List" /><br />
                                                                            <small><b>
                                                                                <asp:Label ID="Lasb3el30" runat="server" Text="เอกสารแนบทั้งหมด" /></b></small>
                                                                        </h2>
                                                                    </label>
                                                                    <div class="col-sm-3">
                                                                        <asp:GridView ID="gvFileEmp_edit" Visible="true" runat="server"
                                                                            AutoGenerateColumns="false"
                                                                            CssClass="table table-striped table-bordered table-hover table-responsive"
                                                                            HeaderStyle-CssClass="warning"
                                                                            OnRowDataBound="Master_RowDataBound"
                                                                            BorderStyle="None"
                                                                            CellSpacing="2"
                                                                            Font-Size="Small">
                                                                            <EmptyDataTemplate>
                                                                                <div style="text-align: center">Data Cannot Be Found</div>
                                                                            </EmptyDataTemplate>

                                                                            <Columns>
                                                                                <asp:TemplateField HeaderText="ไฟล์เอกสารเพิ่มเติม">
                                                                                    <ItemTemplate>
                                                                                        <div class="col-lg-8">
                                                                                            <asp:Literal ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />

                                                                                        </div>
                                                                                        <div class="col-lg-2">
                                                                                            <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-default" data-original-title="Download" Target="_blank"><i class="fa fa-download"></i></asp:HyperLink>

                                                                                            <asp:HiddenField runat="server" ID="hidFile11" Value='<%# Eval("Download") %>' />
                                                                                        </div>

                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </div>

                                                                </div>

                                                                <asp:HyperLink runat="server" ID="txtfocus_detail_edit" />

                                                            </div>
                                                        </div>
                                                    </ContentTemplate>
                                                    <%--<Triggers>
                                                        <asp:PostBackTrigger ControlID="btn_save_document_edit" />
                                                    </Triggers>--%>
                                                </asp:UpdatePanel>
                                            </asp:Panel>
                                        </div>

                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <h3 class="panel-title"><i class="glyphicon glyphicon-road"></i><strong>&nbsp; Driving Status / สถานะการขับขี่ยานพาหนะ</strong></h3>
                                            </div>
                                            <asp:Panel ID="Panel16" runat="server">
                                                <div class="panel-body">
                                                    <div class="form-horizontal" role="form">

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label ">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label113" runat="server" Text="Driving Car" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label114" runat="server" Text="ขับขี่รถยนต์" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:Label ID="lbl_dvcar_idx" runat="server" Visible="false" Text='<%# Eval("dvcar_idx") %>'></asp:Label>
                                                                <asp:DropDownList ID="ddldvcar" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="0">เลือกสถานะการขับขี่....</asp:ListItem>
                                                                    <asp:ListItem Value="1">ขับได้</asp:ListItem>
                                                                    <asp:ListItem Value="2">ขับไม่ได้</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>

                                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label115" runat="server" Text="Licens Driving Car" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label116" runat="server" Text="เลขที่ใบขับขี่" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtlicen_car" CssClass="form-control" runat="server" Text='<%# Eval("dvlicen_car") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label ">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label275" runat="server" Text="Driving Car Status" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label276" runat="server" Text="รถยนต์ส่วนตัว" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:Label ID="lbl_dvcar_status" runat="server" Visible="false" Text='<%# Eval("dvcarstatus_idx") %>'></asp:Label>
                                                                <asp:DropDownList ID="ddldvcarstatus" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="0">เลือกสถานะรถ....</asp:ListItem>
                                                                    <asp:ListItem Value="1">มีรถยนต์ส่วนตัว</asp:ListItem>
                                                                    <asp:ListItem Value="2">ไม่มีรถยนต์ส่วนตัว</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label241" runat="server" Text="Driving Motocyle" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label242" runat="server" Text="ขับขี่รถจักรยานยนต์" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:Label ID="lbl_dvmt_idx" runat="server" Visible="false" Text='<%# Eval("dvmt_idx") %>'></asp:Label>
                                                                <asp:DropDownList ID="ddldvmt" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="0">เลือกสถานะการขับขี่....</asp:ListItem>
                                                                    <asp:ListItem Value="1">ขับได้</asp:ListItem>
                                                                    <asp:ListItem Value="2">ขับไม่ได้</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>

                                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label228" runat="server" Text="Licens Driving Motocyle" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label230" runat="server" Text="เลขที่ใบขับขี่" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtlicens_moto" CssClass="form-control" runat="server" Text='<%# Eval("dvlicen_moto") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label ">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label277" runat="server" Text="Driving Motocyle Status" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label278" runat="server" Text="รถจักรยานยนต์ส่วนตัว" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:Label ID="lbl_dvmoto_status" runat="server" Visible="false" Text='<%# Eval("dvmtstatus_idx") %>'></asp:Label>
                                                                <asp:DropDownList ID="ddldvmtstatus" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="0">เลือกสถานะรถ....</asp:ListItem>
                                                                    <asp:ListItem Value="1">มีรถจักรยานยนต์ส่วนตัว</asp:ListItem>
                                                                    <asp:ListItem Value="2">ไม่มีรถจักรยานยนต์ส่วนตัว</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label233" runat="server" Text="Driving Forklift" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label234" runat="server" Text="ขับขี่รถโฟล์คลิฟท์" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:Label ID="lbl_dvfork_idx" runat="server" Visible="false" Text='<%# Eval("dvfork_idx") %>'></asp:Label>
                                                                <asp:DropDownList ID="ddldvfork" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="0">เลือกสถานะการขับขี่....</asp:ListItem>
                                                                    <asp:ListItem Value="1">ขับได้</asp:ListItem>
                                                                    <asp:ListItem Value="2">ขับไม่ได้</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>

                                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label235" runat="server" Text="Licens Driving Forklift" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label236" runat="server" Text="เลขที่ใบขับขี่" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtlicens_fork" CssClass="form-control" runat="server" Text='<%# Eval("dvlicen_fork") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label237" runat="server" Text="Driving Truck" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label238" runat="server" Text="ขับขี่รถบรรทุก" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:Label ID="lbl_dvtruck_idx" runat="server" Visible="false" Text='<%# Eval("dvtruck_idx") %>'></asp:Label>
                                                                <asp:DropDownList ID="ddldvtruck" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="0">เลือกสถานะการขับขี่....</asp:ListItem>
                                                                    <asp:ListItem Value="1">ขับได้</asp:ListItem>
                                                                    <asp:ListItem Value="2">ขับไม่ได้</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>

                                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label239" runat="server" Text="Licens Driving Truck" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label240" runat="server" Text="เลขที่ใบขับขี่" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtlicens_truck" CssClass="form-control" runat="server" Text='<%# Eval("dvlicen_truck") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>

                                        <%-----------------------------------------------------------------------------------------------%>
                                        <%--Emergency--%>
                                        <%-----------------------------------------------------------------------------------------------%>

                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <h3 class="panel-title"><i class="glyphicon glyphicon-phone-alt"></i><strong>&nbsp; Emergency / ข้อมูลติดต่อกรณีฉุกเฉิน</strong></h3>
                                            </div>
                                            <asp:Panel ID="Panel18" runat="server">
                                                <div class="panel-body">
                                                    <div class="form-horizontal" role="form">

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label135" runat="server" Text="Emergency Contact" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label136" runat="server" Text="กรณีฉุกเฉินติดต่อ" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtemercon" CssClass="form-control" runat="server" Text='<%# Eval("emer_name") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                            </div>

                                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label133" runat="server" Text="Relationship" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label134" runat="server" Text="ความสัมพันธ์" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtrelation" CssClass="form-control" runat="server" Text='<%# Eval("emer_relation") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label137" runat="server" Text="Telephone" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label138" runat="server" Text="เบอร์โทร" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txttelemer" CssClass="form-control" runat="server" Text='<%# Eval("emer_tel") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>

                                        <%-----------------------------------------------------------------------------------------------%>
                                        <%--Family--%>
                                        <%-----------------------------------------------------------------------------------------------%>
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <h3 class="panel-title"><i class="glyphicon glyphicon-home"></i><strong>&nbsp; Family / ข้อมูลครอบครัว</strong></h3>
                                            </div>
                                            <asp:Panel ID="Panel19" runat="server">
                                                <div class="panel-body">
                                                    <div class="form-horizontal" role="form">

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label141" runat="server" Text="Father Name" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label142" runat="server" Text="ชื่อ-สกุล บิดา" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtfathername_edit" CssClass="form-control" runat="server" Text='<%# Eval("fathername") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                            </div>

                                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label243" runat="server" Text="Licen no." /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label244" runat="server" Text="เลขบัตรประชาชน" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtlicenfather_edit" CssClass="form-control" runat="server" Text='<%# Eval("licen_father") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label251" runat="server" Text="Telephone" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label252" runat="server" Text="เบอร์ติดต่อ" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txttelfather_edit" CssClass="form-control" runat="server" Text='<%# Eval("telfather") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label145" runat="server" Text="Mother Name" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label146" runat="server" Text="ชื่อ-สกุล มารดา" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtmothername_edit" CssClass="form-control" runat="server" Text='<%# Eval("mothername") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                            </div>

                                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label247" runat="server" Text="Licen no." /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label248" runat="server" Text="เลขบัตรประชาชน" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtlicenmother_edit" CssClass="form-control" runat="server" Text='<%# Eval("licen_mother") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label147" runat="server" Text="Telephone" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label148" runat="server" Text="เบอร์ติดต่อ" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txttelmother_edit" CssClass="form-control" runat="server" Text='<%# Eval("telmother") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label149" runat="server" Text="Wife Name" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label150" runat="server" Text="ชื่อ-สกุล คู่สมรส" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtwifename_edit" CssClass="form-control" runat="server" Text='<%# Eval("wifename") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                            </div>

                                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label249" runat="server" Text="Licen no." /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label250" runat="server" Text="เลขบัตรประชาชน" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtlicenwife_edit" CssClass="form-control" runat="server" Text='<%# Eval("licen_wife") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label151" runat="server" Text="Telephone" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label152" runat="server" Text="เบอร์ติดต่อ" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txttelwife_edit" CssClass="form-control" runat="server" Text='<%# Eval("telwife") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-12">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label3" runat="server" CssClass="h01" Text="Family" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label7" runat="server" Text="ข้อมูลบุตร" /></b></small>
                                                                </h2>
                                                            </label>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-lg-12">

                                                                <asp:GridView ID="GvChildAdd_View"
                                                                    runat="server"
                                                                    AutoGenerateColumns="false"
                                                                    CssClass="table table-striped table-bordered table-hover"
                                                                    HeaderStyle-CssClass="table_headCenter"
                                                                    HeaderStyle-Height="40px"
                                                                    ShowFooter="False"
                                                                    ShowHeaderWhenEmpty="True"
                                                                    AllowPaging="True"
                                                                    PageSize="10"
                                                                    BorderStyle="None"
                                                                    CellSpacing="2"
                                                                    DataKeyNames="CHIDX"
                                                                    OnRowEditing="gvRowEditing"
                                                                    OnPageIndexChanging="Master_PageIndexChanging"
                                                                    OnRowCancelingEdit="gvRowCancelingEdit"
                                                                    OnRowDataBound="Master_RowDataBound"
                                                                    OnRowUpdating="gvRowUpdating">

                                                                    <PagerStyle CssClass="PageCustom" />
                                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                                    <EmptyDataTemplate>
                                                                        <div style="text-align: center">Data Cannot Be Found</div>
                                                                    </EmptyDataTemplate>

                                                                    <Columns>

                                                                        <asp:TemplateField HeaderText="#">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <%# (Container.DataItemIndex +1) %>
                                                                                    <asp:Label ID="Lab2el5w82" runat="server" Visible="false" Text='<%# Eval("CHIDX")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>

                                                                            <EditItemTemplate>
                                                                                <div class="row">
                                                                                    <div class="col-lg-10">
                                                                                        <h3>Child FormEdit<small> (แบบฟอร์มแก้ไขข้อมูลบุตร)</small></h3>
                                                                                        <asp:Label ID="lbl_Chidx_edit" runat="server" Visible="false" Text='<%# Eval("CHIDX")%>'></asp:Label>
                                                                                    </div>
                                                                                </div>
                                                                                <hr />

                                                                                <div class="form-group">
                                                                                    <asp:Label ID="Label65" runat="server" Text="ชื่อ-นามสกุล" CssClass="col-sm-3 control-label"></asp:Label>
                                                                                    <div class="col-sm-4">
                                                                                        <asp:TextBox ID="txtChild_name_edit" runat="server" CssClass="form-control" Text='<%# Eval("Child_name") %>'></asp:TextBox>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group">
                                                                                    <asp:Label ID="Label66" runat="server" Text="บุตรคนที่" CssClass="col-sm-3 control-label"></asp:Label>
                                                                                    <div class="col-sm-4">
                                                                                        <asp:Label ID="lbl_Child_num" runat="server" Visible="false" Text='<%# Eval("Child_num") %>'></asp:Label>
                                                                                        <asp:DropDownList ID="ddlchildnumber" runat="server" CssClass="form-control">
                                                                                            <asp:ListItem Value="0">เลือกจำนวนบุตร....</asp:ListItem>
                                                                                            <asp:ListItem Value="1">1</asp:ListItem>
                                                                                            <asp:ListItem Value="2">2</asp:ListItem>
                                                                                            <asp:ListItem Value="3">3</asp:ListItem>
                                                                                            <asp:ListItem Value="4">4</asp:ListItem>
                                                                                            <asp:ListItem Value="5">5</asp:ListItem>
                                                                                            <asp:ListItem Value="6">6</asp:ListItem>
                                                                                            <asp:ListItem Value="7">7</asp:ListItem>
                                                                                            <asp:ListItem Value="8">8</asp:ListItem>
                                                                                            <asp:ListItem Value="9">9</asp:ListItem>
                                                                                        </asp:DropDownList>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group">
                                                                                    <div class="col-lg-offset-10">
                                                                                        <div class="control-label" style="text-align: center;">
                                                                                            <asp:LinkButton ID="LinkButton1" runat="server" Text="Update" ValidationGroup="SaveUpdate" CssClass="btn btn-success" OnClientClick="return confirm('คุณต้องการแก้ไขและบันทึกรายการนี้ใช่หรือไม่ ?')" CommandName="Update" />
                                                                                            <asp:LinkButton ID="LinkButton2" runat="server" Text="Cancel" CssClass="btn btn-danger" CommandName="Cancel" />
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </EditItemTemplate>

                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="ชื่อ-นามสกุล">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="Labe2l5w38" runat="server" Text='<%# Eval("Child_name")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="บุตรคนที่">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="lbP2a4g2e1" runat="server" Text='<%# Eval("Child_num")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="จัดการ">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="Edit" runat="server" ValidationGroup="fromgroup" Text="Edit" CssClass="btn btn-info" CommandName="Edit" />
                                                                                <asp:LinkButton ID="btnDeleteGV" runat="server" ValidationGroup="fromgroup" OnCommand="btnCommand" CommandName="DeleteGvChildAdd_View" CommandArgument='<%# Eval("CHIDX") %>' Text="Delete" CssClass="btn btn-danger" OnClientClick="return confirm('Do you want delete this item?')" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                    </Columns>

                                                                </asp:GridView>

                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-sm-12">
                                                                <div class="checkbox">
                                                                    <asp:CheckBox ID="chkchild_edit" runat="server" Text="เพิ่มข้อมูลบุตร" CssClass="col-sm-4" AutoPostBack="true" OnCheckedChanged="chkSelectedIndexChanged" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <asp:Panel ID="BoxChild" runat="server" Visible="false">
                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label155" runat="server" Text="Child Name" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label156" runat="server" Text="ชื่อ-สกุล บุตร" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-2">
                                                                    <asp:TextBox ID="txtchildname_edit" CssClass="form-control" runat="server" ValidationGroup="AddChild_Edit"></asp:TextBox>

                                                                    <asp:RequiredFieldValidator ID="RequirvceasddFieldValidator1" ValidationGroup="AddChild_Edit" runat="server" Display="None"
                                                                        ControlToValidate="txtchildname_edit" Font-Size="11"
                                                                        ErrorMessage="กรอกชื่อ ...." />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValdsidatorasCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequirvceasddFieldValidator1" Width="160" />
                                                                </div>

                                                                <label class="col-sm-2 control-label col-sm-offset-1">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label157" runat="server" Text="Child Number" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label158" runat="server" Text="เป็นบุตรคนที่" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-2">
                                                                    <asp:DropDownList ID="ddlchildnumber_edit" runat="server" CssClass="form-control">
                                                                        <asp:ListItem Value="0">บุตรคนที่....</asp:ListItem>
                                                                        <asp:ListItem Value="1">1</asp:ListItem>
                                                                        <asp:ListItem Value="2">2</asp:ListItem>
                                                                        <asp:ListItem Value="3">3</asp:ListItem>
                                                                        <asp:ListItem Value="4">4</asp:ListItem>
                                                                        <asp:ListItem Value="5">5</asp:ListItem>
                                                                        <asp:ListItem Value="6">6</asp:ListItem>
                                                                        <asp:ListItem Value="7">7</asp:ListItem>
                                                                        <asp:ListItem Value="8">8</asp:ListItem>
                                                                        <asp:ListItem Value="9">9</asp:ListItem>
                                                                    </asp:DropDownList>

                                                                    <asp:RequiredFieldValidator ID="R2equi3redFi5dews4e4ldValidator1" ValidationGroup="AddChild_Edit" runat="server" Display="None"
                                                                        ControlToValidate="ddlchildnumber_edit" Font-Size="11"
                                                                        ErrorMessage="เป็นบุตรคนที่ ...."
                                                                        ValidationExpression="เป็นบุตรคนที่ ...." InitialValue="0" />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="Validat4orCallosutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R2equi3redFi5dews4e4ldValidator1" Width="160" />
                                                                </div>

                                                                <div class="col-sm-1">
                                                                    <asp:LinkButton ID="btnAddChild" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="AddChild_Edit" ValidationGroup="AddChild_Edit">ADD + &nbsp;</asp:LinkButton>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <div class="col-lg-12">

                                                                    <asp:GridView ID="GvChildAdd_Edit"
                                                                        runat="server"
                                                                        AutoGenerateColumns="false"
                                                                        CssClass="table table-striped table-bordered table-hover"
                                                                        HeaderStyle-CssClass="table_headCenter"
                                                                        HeaderStyle-Height="40px"
                                                                        ShowFooter="False"
                                                                        ShowHeaderWhenEmpty="True"
                                                                        AllowPaging="True"
                                                                        PageSize="10"
                                                                        BorderStyle="None"
                                                                        CellSpacing="2"
                                                                        OnRowDeleting="gvRowDeleting">

                                                                        <PagerStyle CssClass="PageCustom" />
                                                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                                        <EmptyDataTemplate>
                                                                            <div style="text-align: center">Data Cannot Be Found</div>
                                                                        </EmptyDataTemplate>

                                                                        <Columns>

                                                                            <asp:TemplateField HeaderText="#">
                                                                                <ItemTemplate>
                                                                                    <div class="panel-heading">
                                                                                        <%# (Container.DataItemIndex +1) %>
                                                                                    </div>
                                                                                </ItemTemplate>

                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="ชื่อ-นามสกุล">
                                                                                <ItemTemplate>
                                                                                    <div class="panel-heading">
                                                                                        <asp:Label ID="Label5w8" runat="server" Text='<%# Eval("Child_name")%>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="บุตรคนที่">
                                                                                <ItemTemplate>
                                                                                    <div class="panel-heading">
                                                                                        <asp:Label ID="lbPag2e1" runat="server" Text='<%# Eval("Child_num")%>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="จัดการ">
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="btnDeleteGV" runat="server" ValidationGroup="fromgroup" CommandName="Delete" Text="Delete" CssClass="btn btn-danger" OnClientClick="return confirm('Do you want delete this item?')" />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                        </Columns>

                                                                    </asp:GridView>

                                                                </div>
                                                            </div>
                                                        </asp:Panel>

                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>

                                        <%-----------------------------------------------------------------------------------------------%>
                                        <%--Reference--%>
                                        <%-----------------------------------------------------------------------------------------------%>
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; Reference / บุคคลอ้างอิง</strong></h3>
                                            </div>
                                            <asp:Panel ID="Panel20" runat="server">
                                                <div class="panel-body">
                                                    <div class="form-horizontal" role="form">

                                                        <div class="form-group">
                                                            <div class="col-lg-12">

                                                                <asp:GridView ID="GvReference_View"
                                                                    runat="server"
                                                                    AutoGenerateColumns="false"
                                                                    CssClass="table table-striped table-bordered table-hover"
                                                                    HeaderStyle-CssClass="table_headCenter"
                                                                    HeaderStyle-Height="40px"
                                                                    ShowFooter="False"
                                                                    ShowHeaderWhenEmpty="True"
                                                                    AllowPaging="True"
                                                                    PageSize="5"
                                                                    BorderStyle="None"
                                                                    CellSpacing="2"
                                                                    DataKeyNames="ReIDX"
                                                                    OnRowEditing="gvRowEditing"
                                                                    OnPageIndexChanging="Master_PageIndexChanging"
                                                                    OnRowCancelingEdit="gvRowCancelingEdit"
                                                                    OnRowDataBound="Master_RowDataBound"
                                                                    OnRowUpdating="gvRowUpdating">

                                                                    <PagerStyle CssClass="PageCustom" />
                                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                                    <EmptyDataTemplate>
                                                                        <div style="text-align: center">Data Cannot Be Found</div>
                                                                    </EmptyDataTemplate>

                                                                    <Columns>

                                                                        <asp:TemplateField HeaderText="#">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <%# (Container.DataItemIndex +1) %>
                                                                                    <asp:Label ID="L2abs2del5w8" runat="server" Visible="false" Text='<%# Eval("ReIDX")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>

                                                                            <EditItemTemplate>
                                                                                <asp:Label ID="sf" runat="server"></asp:Label>
                                                                                <div class="row">
                                                                                    <div class="col-lg-10">
                                                                                        <h3>Experiences FormEdit<small> (แบบฟอร์มแก้ไขข้อมูลบุคคลอ้างอิง)</small></h3>
                                                                                        <asp:Label ID="lbl_Ref_edit" runat="server" Visible="false" Text='<%# Eval("ReIDX")%>'></asp:Label>
                                                                                    </div>
                                                                                </div>
                                                                                <hr />

                                                                                <div class="form-group">
                                                                                    <asp:Label ID="Label65" runat="server" Text="ชื่อ-นามสกุล" CssClass="col-sm-3 control-label"></asp:Label>
                                                                                    <div class="col-sm-4">
                                                                                        <asp:TextBox ID="txtRef_Name_edit" runat="server" CssClass="form-control" Text='<%# Eval("ref_fullname") %>'></asp:TextBox>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group">
                                                                                    <asp:Label ID="Label66" runat="server" Text="ตำแหน่ง" CssClass="col-sm-3 control-label"></asp:Label>
                                                                                    <div class="col-sm-4">
                                                                                        <asp:TextBox ID="txtRef_pos_edit" runat="server" CssClass="form-control" Text='<%# Eval("ref_position") %>'></asp:TextBox>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group">
                                                                                    <asp:Label ID="Label160" runat="server" Text="ความสัมพันธ์" CssClass="col-sm-3 control-label"></asp:Label>
                                                                                    <div class="col-sm-4">
                                                                                        <asp:TextBox ID="txtRef_relation_edit" runat="server" CssClass="form-control" Text='<%# Eval("ref_relation") %>'></asp:TextBox>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group">
                                                                                    <asp:Label ID="Label12" runat="server" Text="เบอร์โทรศัพท์" CssClass="col-sm-3 control-label"></asp:Label>
                                                                                    <div class="col-sm-4">
                                                                                        <asp:TextBox ID="txtRef_tel_edit" runat="server" CssClass="form-control" Text='<%# Eval("ref_tel") %>'></asp:TextBox>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group">
                                                                                    <div class="col-lg-offset-10">
                                                                                        <div class="control-label" style="text-align: center;">
                                                                                            <asp:LinkButton ID="LinkButton1" runat="server" Text="Update" ValidationGroup="SaveUpdate" CssClass="btn btn-success" OnClientClick="return confirm('คุณต้องการแก้ไขและบันทึกรายการนี้ใช่หรือไม่ ?')" CommandName="Update" />
                                                                                            <asp:LinkButton ID="LinkButton2" runat="server" Text="Cancel" CssClass="btn btn-danger" CommandName="Cancel" />
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </EditItemTemplate>

                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="ชื่อ-นามสกุล">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="Labe1" runat="server" Text='<%# Eval("ref_fullname")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="ตำแหน่ง">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="lb3P23ag2e1" runat="server" Text='<%# Eval("ref_position")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="ความสัมพันธ์">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="lb3P2a1g2e1" runat="server" Text='<%# Eval("ref_relation")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="เบอร์โทรศัพท์">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="lb3P2ag2e1" runat="server" Text='<%# Eval("ref_tel")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="จัดการ">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="btnEditView" runat="server" ValidationGroup="fromgroup" Text="Edit" CssClass="btn btn-info" CommandName="Edit" />
                                                                                <asp:LinkButton ID="btnDeleteGV" runat="server" ValidationGroup="fromgroup" OnCommand="btnCommand" CommandName="DeleteGvRefer_View" CommandArgument='<%# Eval("ReIDX") %>' Text="Delete" CssClass="btn btn-danger" OnClientClick="return confirm('Do you want delete this item?')" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                    </Columns>

                                                                </asp:GridView>

                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-sm-12">
                                                                <div class="checkbox">
                                                                    <asp:CheckBox ID="ckreference_edit" runat="server" Text="เพิ่มบุคคลอ้างอิง" CssClass="col-sm-4" AutoPostBack="true" OnCheckedChanged="chkSelectedIndexChanged" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <asp:Panel ID="BoxReference" runat="server" Visible="false">

                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label258" runat="server" Text="Name" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label259" runat="server" Text="ชื่อ-นามสกุล" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-2">
                                                                    <asp:TextBox ID="txtfullname_add" CssClass="form-control" runat="server" ValidationGroup="AddReference"></asp:TextBox>

                                                                    <asp:RequiredFieldValidator ID="Re1ator1" ValidationGroup="AddReference" runat="server" Display="None"
                                                                        ControlToValidate="txtfullname_add" Font-Size="11"
                                                                        ErrorMessage="ชื่อ-นามสกุล ...." />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="Valr8" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re1ator1" Width="160" />
                                                                </div>

                                                                <label class="col-sm-2 control-label col-sm-offset-1">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label260" runat="server" Text="Positions" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label261" runat="server" Text="ตำแหน่ง" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-2">
                                                                    <asp:TextBox ID="txtposition_add" CssClass="form-control" runat="server" ValidationGroup="AddReference"></asp:TextBox>

                                                                    <asp:RequiredFieldValidator ID="Re1or2" ValidationGroup="AddReference" runat="server" Display="None"
                                                                        ControlToValidate="txtposition_add" Font-Size="11"
                                                                        ErrorMessage="ตำแหน่งล่าสุด ...." />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValideatorCalloutExtender9" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re1or2" Width="160" />
                                                                </div>

                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label262" runat="server" Text="Relation" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label263" runat="server" Text="ความสัมพันธ์" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-2">
                                                                    <asp:TextBox ID="txtrelation_add" CssClass="form-control" runat="server" ValidationGroup="AddReference"></asp:TextBox>

                                                                    <asp:RequiredFieldValidator ID="Redawtor3" ValidationGroup="AddReference" runat="server" Display="None"
                                                                        ControlToValidate="txtrelation_add" Font-Size="11"
                                                                        ErrorMessage="ความสัมพันธ์ ...." />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValExtender10" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Redawtor3" Width="160" />
                                                                </div>

                                                                <label class="col-sm-2 control-label col-sm-offset-1">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label264" runat="server" Text="Tel" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label266" runat="server" Text="เบอร์โทรศัพท์" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-2">
                                                                    <asp:TextBox ID="txttel_add" CssClass="form-control" runat="server" ValidationGroup="AddReference"></asp:TextBox>

                                                                    <asp:RequiredFieldValidator ID="Requa4" ValidationGroup="AddReference" runat="server" Display="None"
                                                                        ControlToValidate="txttel_add" Font-Size="11"
                                                                        ErrorMessage="เบอร์โทรศัพท์ ...." />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValtExtender11" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requa4" Width="160" />
                                                                </div>
                                                                <div class="col-sm-1">
                                                                    <asp:LinkButton ID="LinkButton7" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="AddReference_Edit" ValidationGroup="AddReference">ADD + &nbsp;</asp:LinkButton>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <div class="col-lg-12">

                                                                    <asp:GridView ID="GvReference_Edit"
                                                                        runat="server"
                                                                        AutoGenerateColumns="false"
                                                                        CssClass="table table-striped table-bordered table-hover"
                                                                        HeaderStyle-CssClass="table_headCenter"
                                                                        HeaderStyle-Height="40px"
                                                                        ShowFooter="False"
                                                                        ShowHeaderWhenEmpty="True"
                                                                        AllowPaging="True"
                                                                        PageSize="5"
                                                                        BorderStyle="None"
                                                                        CellSpacing="2"
                                                                        OnRowDeleting="gvRowDeleting">

                                                                        <PagerStyle CssClass="PageCustom" />
                                                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                                        <EmptyDataTemplate>
                                                                            <div style="text-align: center">Data Cannot Be Found</div>
                                                                        </EmptyDataTemplate>

                                                                        <Columns>

                                                                            <asp:TemplateField HeaderText="#">
                                                                                <ItemTemplate>
                                                                                    <div class="panel-heading">
                                                                                        <%# (Container.DataItemIndex +1) %>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="ชื่อ-นามสกุล">
                                                                                <ItemTemplate>
                                                                                    <div class="panel-heading">
                                                                                        <asp:Label ID="Labe1" runat="server" Text='<%# Eval("ref_fullname")%>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="ตำแหน่ง">
                                                                                <ItemTemplate>
                                                                                    <div class="panel-heading">
                                                                                        <asp:Label ID="lb3P23ag2e1" runat="server" Text='<%# Eval("ref_position")%>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="ความสัมพันธ์">
                                                                                <ItemTemplate>
                                                                                    <div class="panel-heading">
                                                                                        <asp:Label ID="lb3P2a1g2e1" runat="server" Text='<%# Eval("ref_relation")%>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="เบอร์โทรศัพท์">
                                                                                <ItemTemplate>
                                                                                    <div class="panel-heading">
                                                                                        <asp:Label ID="lb3P2ag2e1" runat="server" Text='<%# Eval("ref_tel")%>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="จัดการ">
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="btnDeleteGV" runat="server" ValidationGroup="fromgroup" CommandName="Delete" Text="Delete" CssClass="btn btn-danger" OnClientClick="return confirm('Do you want delete this item?')" />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                        </Columns>

                                                                    </asp:GridView>

                                                                </div>
                                                            </div>

                                                        </asp:Panel>

                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>

                                        <%-----------------------------------------------------------------------------------------------%>
                                        <%--Prior Experiences--%>
                                        <%-----------------------------------------------------------------------------------------------%>
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <h3 class="panel-title"><i class="glyphicon glyphicon-briefcase"></i><strong>&nbsp; Prior Experiences / ประวัติการทำงาน</strong></h3>
                                            </div>
                                            <asp:Panel ID="Panel21" runat="server">
                                                <div class="panel-body">
                                                    <div class="form-horizontal" role="form">

                                                        <div class="form-group">
                                                            <div class="col-lg-12">

                                                                <asp:GridView ID="GvPri_View"
                                                                    runat="server"
                                                                    AutoGenerateColumns="false"
                                                                    CssClass="table table-striped table-bordered table-hover"
                                                                    HeaderStyle-CssClass="table_headCenter"
                                                                    HeaderStyle-Height="40px"
                                                                    ShowFooter="False"
                                                                    ShowHeaderWhenEmpty="True"
                                                                    AllowPaging="True"
                                                                    PageSize="10"
                                                                    BorderStyle="None"
                                                                    CellSpacing="2"
                                                                    DataKeyNames="ExperIDX"
                                                                    OnRowEditing="gvRowEditing"
                                                                    OnPageIndexChanging="Master_PageIndexChanging"
                                                                    OnRowCancelingEdit="gvRowCancelingEdit"
                                                                    OnRowDataBound="Master_RowDataBound"
                                                                    OnRowUpdating="gvRowUpdating">

                                                                    <PagerStyle CssClass="PageCustom" />
                                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                                    <EmptyDataTemplate>
                                                                        <div style="text-align: center">Data Cannot Be Found</div>
                                                                    </EmptyDataTemplate>

                                                                    <Columns>

                                                                        <asp:TemplateField HeaderText="#">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <%# (Container.DataItemIndex +1) %>
                                                                                    <asp:Label ID="L2ab2del5w8" runat="server" Visible="false" Text='<%# Eval("ExperIDX")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>

                                                                            <EditItemTemplate>
                                                                                <asp:Label ID="sf" runat="server"></asp:Label>
                                                                                <div class="row">
                                                                                    <div class="col-lg-10">
                                                                                        <h3>Experiences FormEdit<small> (แบบฟอร์มแก้ไขข้อมูลประสบการณ์ทำงาน)</small></h3>
                                                                                        <asp:Label ID="lbl_Pri_edit" runat="server" Visible="false" Text='<%# Eval("ExperIDX")%>'></asp:Label>
                                                                                    </div>
                                                                                </div>
                                                                                <hr />

                                                                                <div class="form-group">
                                                                                    <asp:Label ID="Label65" runat="server" Text="ชื่อบริษัท" CssClass="col-sm-3 control-label"></asp:Label>
                                                                                    <div class="col-sm-4">
                                                                                        <asp:TextBox ID="txtPri_Nameorg_edit" runat="server" CssClass="form-control" Text='<%# Eval("Pri_Nameorg") %>'></asp:TextBox>
                                                                                    </div>
                                                                                </div>


                                                                                <div class="form-group">
                                                                                    <asp:Label ID="Label66" runat="server" Text="ตำแหน่งล่าสุด" CssClass="col-sm-3 control-label"></asp:Label>
                                                                                    <div class="col-sm-4">
                                                                                        <asp:TextBox ID="txtPri_pos_edit" runat="server" CssClass="form-control" Text='<%# Eval("Pri_pos") %>'></asp:TextBox>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group">
                                                                                    <asp:Label ID="Label160" runat="server" Text="วันที่เริ่มงาน" CssClass="col-sm-3 control-label"></asp:Label>
                                                                                    <div class="col-sm-4">
                                                                                        <div class='input-group date'>
                                                                                            <asp:TextBox ID="txt_Pri_start_edit" runat="server" CssClass="form-control from-date-datepicker" Text='<%# Eval("Pri_startdate") %>'></asp:TextBox>
                                                                                            <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group">
                                                                                    <asp:Label ID="Label12" runat="server" Text="วันที่ออกจากงาน" CssClass="col-sm-3 control-label"></asp:Label>
                                                                                    <div class="col-sm-4">
                                                                                        <div class='input-group date'>
                                                                                            <asp:TextBox ID="txt_Pri_resign_edit" runat="server" CssClass="form-control from-date-datepicker" Text='<%# Eval("Pri_resigndate") %>'></asp:TextBox>
                                                                                            <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>


                                                                                <div class="form-group">
                                                                                    <div class="col-lg-offset-10">
                                                                                        <div class="control-label" style="text-align: center;">
                                                                                            <asp:LinkButton ID="LinkButton1" runat="server" Text="Update" ValidationGroup="SaveUpdate" CssClass="btn btn-success" OnClientClick="return confirm('คุณต้องการแก้ไขและบันทึกรายการนี้ใช่หรือไม่ ?')" CommandName="Update" />
                                                                                            <asp:LinkButton ID="LinkButton2" runat="server" Text="Cancel" CssClass="btn btn-danger" CommandName="Cancel" />
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </EditItemTemplate>

                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="ชื่อบริษัท">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="L2ab2el5w8" runat="server" Text='<%# Eval("Pri_Nameorg")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="ตำแหน่งล่าสุด">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="l2bP42ag2e1" runat="server" Text='<%# Eval("Pri_pos")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="วันที่เริ่มงาน">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="lb3wP2a1g2e1" runat="server" Text='<%# Eval("Pri_startdate")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="วันที่ออกจากงาน">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="lebwP32ag2e1" runat="server" Text='<%# Eval("Pri_resigndate")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="เงินเดือนล่าสุด">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="lebPr32ag2e1" runat="server" Text='<%# Eval("Pri_salary")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="รายละเอียดงาน">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="lebP3q2ag2e1" runat="server" Text='<%# Eval("Pri_description")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="จัดการ">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="btnEditView" runat="server" ValidationGroup="fromgroup" Text="Edit" CssClass="btn btn-info" CommandName="Edit" />
                                                                                <asp:LinkButton ID="btnDeleteGV" runat="server" ValidationGroup="fromgroup" OnCommand="btnCommand" CommandName="DeleteGvPri_View" CommandArgument='<%# Eval("ExperIDX") %>' Text="Delete" CssClass="btn btn-danger" OnClientClick="return confirm('Do you want delete this item?')" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                    </Columns>

                                                                </asp:GridView>

                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-sm-12">
                                                                <div class="checkbox">
                                                                    <asp:CheckBox ID="chkorg_edit" runat="server" Text="เพิ่มสถานที่ทำงาน" CssClass="col-sm-4" AutoPostBack="true" OnCheckedChanged="chkSelectedIndexChanged" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <asp:Panel ID="BoxOrgOld" runat="server" Visible="false">
                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label159" runat="server" Text="Organization Name" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="lblorgold" runat="server" Text="ชื่อบริษัท" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-2">
                                                                    <asp:TextBox ID="txtorgold_edit" CssClass="form-control" runat="server" ValidationGroup="AddPrior_Edit"></asp:TextBox>

                                                                    <asp:RequiredFieldValidator ID="R23equiredFieldValidator1" ValidationGroup="AddPrior_Edit" runat="server" Display="None"
                                                                        ControlToValidate="txtorgold_edit" Font-Size="11"
                                                                        ErrorMessage="ชื่อบริษัท ...." />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidsdatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R23equiredFieldValidator1" Width="160" />
                                                                </div>

                                                                <label class="col-sm-2 control-label col-sm-offset-1">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label161" runat="server" Text="Recent Positions" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label162" runat="server" Text="ตำแหน่งล่าสุด" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-2">
                                                                    <asp:TextBox ID="txtposold_edit" CssClass="form-control" runat="server" ValidationGroup="AddPrior_Edit"></asp:TextBox>

                                                                    <asp:RequiredFieldValidator ID="RdValidwator1" ValidationGroup="AddPrior_Edit" runat="server" Display="None"
                                                                        ControlToValidate="txtposold_edit" Font-Size="11"
                                                                        ErrorMessage="ตำแหน่งล่าสุด ...." />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="Va6lidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RdValidwator1" Width="160" />
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label253" runat="server" Text="Start Date" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label254" runat="server" Text="ปีที่เริ่มงาน" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-2">
                                                                    <div class='input-group date'>
                                                                        <asp:TextBox ID="txtstartdate_edit" CssClass="form-control from-date-datepicker" MaxLengh="100%" runat="server" ValidationGroup="AddPrior_Edit"></asp:TextBox>
                                                                        <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                                    </div>

                                                                    <asp:RequiredFieldValidator ID="Requir4er1" ValidationGroup="AddPrior_Edit" runat="server" Display="None"
                                                                        ControlToValidate="txtstartdate_edit" Font-Size="11"
                                                                        ErrorMessage="ปีที่เริ่มงาน ...." />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="Validator2CalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requir4er1" Width="160" />
                                                                </div>

                                                                <label class="col-sm-2 control-label col-sm-offset-1">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label255" runat="server" Text="End Date" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label256" runat="server" Text="ปีที่ออกจากงาน" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-2">
                                                                    <div class='input-group date'>
                                                                        <asp:TextBox ID="txtresigndate_edit" CssClass="form-control from-date-datepicker" MaxLengh="100%" runat="server" ValidationGroup="AddPrior_Edit"></asp:TextBox>
                                                                        <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                                    </div>

                                                                    <asp:RequiredFieldValidator ID="RdV2alidator2" ValidationGroup="AddPrior_Edit" runat="server" Display="None"
                                                                        ControlToValidate="txtresigndate_edit" Font-Size="11"
                                                                        ErrorMessage="ปีที่ออกจากงาน ...." />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RdV2alidator2" Width="160" />
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label47" runat="server" Text="salary" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label49" runat="server" Text="เงินเดือนล่าสุด" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-2">
                                                                    <asp:TextBox ID="txtprisalary_edit" CssClass="form-control" runat="server" ValidationGroup="AddPrior_Edit"></asp:TextBox>
                                                                </div>
                                                                <asp:RequiredFieldValidator ID="Reqor1" ValidationGroup="AddPrior_Edit" runat="server" Display="None"
                                                                    ControlToValidate="txtprisalary_edit" Font-Size="11"
                                                                    ErrorMessage="เงินเดือนล่าสุด ...." />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="Vaaer1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Reqor1" Width="160" />

                                                                <label class="col-sm-2 control-label col-sm-offset-1">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label42" runat="server" Text="Description" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label43" runat="server" Text="รายละเอียดงาน" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-2">
                                                                    <asp:TextBox ID="txtpridescription_edit" CssClass="form-control" runat="server" TextMode="MultiLine" Rows="3" ValidationGroup="AddPrior_Edit"></asp:TextBox>
                                                                </div>
                                                                <asp:RequiredFieldValidator ID="Requirator1" ValidationGroup="AddPrior_Edit" runat="server" Display="None"
                                                                    ControlToValidate="txtpridescription_edit" Font-Size="11"
                                                                    ErrorMessage="รายละเอียดงาน ...." />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="Valier1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requirator1" Width="160" />

                                                                <div class="col-sm-1">
                                                                    <asp:LinkButton ID="LinkButton6" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="AddPrior_Edit" ValidationGroup="AddPrior_Edit">ADD + &nbsp;</asp:LinkButton>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <div class="col-lg-12">

                                                                    <asp:GridView ID="GvPri_Edit"
                                                                        runat="server"
                                                                        AutoGenerateColumns="false"
                                                                        CssClass="table table-striped table-bordered table-hover"
                                                                        HeaderStyle-CssClass="table_headCenter"
                                                                        HeaderStyle-Height="40px"
                                                                        ShowFooter="False"
                                                                        ShowHeaderWhenEmpty="True"
                                                                        AllowPaging="True"
                                                                        PageSize="10"
                                                                        BorderStyle="None"
                                                                        CellSpacing="2"
                                                                        OnRowDeleting="gvRowDeleting">

                                                                        <PagerStyle CssClass="PageCustom" />
                                                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                                        <EmptyDataTemplate>
                                                                            <div style="text-align: center">Data Cannot Be Found</div>
                                                                        </EmptyDataTemplate>

                                                                        <Columns>

                                                                            <asp:TemplateField HeaderText="#">
                                                                                <ItemTemplate>
                                                                                    <div class="panel-heading">
                                                                                        <%# (Container.DataItemIndex +1) %>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="ชื่อบริษัท">
                                                                                <ItemTemplate>
                                                                                    <div class="panel-heading">
                                                                                        <asp:Label ID="Label5w8" runat="server" Text='<%# Eval("Pri_Nameorg")%>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="ตำแหน่งล่าสุด">
                                                                                <ItemTemplate>
                                                                                    <div class="panel-heading">
                                                                                        <asp:Label ID="lbP2ag2e1" runat="server" Text='<%# Eval("Pri_pos")%>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="วันที่เริ่มงาน">
                                                                                <ItemTemplate>
                                                                                    <div class="panel-heading">
                                                                                        <asp:Label ID="lb3P2a1g2e1" runat="server" Text='<%# Eval("Pri_startdate")%>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="วันที่ออกจากงาน">
                                                                                <ItemTemplate>
                                                                                    <div class="panel-heading">
                                                                                        <asp:Label ID="lbP32ag2e1" runat="server" Text='<%# Eval("Pri_resigndate")%>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="เงินเดือนล่าสุด">
                                                                                <ItemTemplate>
                                                                                    <div class="panel-heading">
                                                                                        <asp:Label ID="lebP3223ag2e1" runat="server" Text='<%# Eval("Pri_salary")%>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="รายละเอียดงาน">
                                                                                <ItemTemplate>
                                                                                    <div class="panel-heading">
                                                                                        <asp:Label ID="leb1P32ag2e1" runat="server" Text='<%# Eval("Pri_description")%>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="จัดการ">
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="btnDeleteGV" runat="server" ValidationGroup="fromgroup" CommandName="Delete" Text="Delete" CssClass="btn btn-danger" OnClientClick="return confirm('Do you want delete this item?')" />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                        </Columns>

                                                                    </asp:GridView>

                                                                </div>
                                                            </div>

                                                        </asp:Panel>

                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>


                                        <%-----------------------------------------------------------------------------------------------%>
                                        <%--Education--%>
                                        <%-----------------------------------------------------------------------------------------------%>
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <h3 class="panel-title"><i class="glyphicon glyphicon-education"></i><strong>&nbsp; Education / ประวัติการศึกษา</strong></h3>
                                            </div>
                                            <asp:Panel ID="Panel22" runat="server">
                                                <div class="panel-body">
                                                    <div class="form-horizontal" role="form">

                                                        <div class="form-group">
                                                            <div class="col-lg-12">

                                                                <asp:GridView ID="GvEducation_View"
                                                                    runat="server"
                                                                    AutoGenerateColumns="false"
                                                                    CssClass="table table-striped table-bordered table-hover"
                                                                    HeaderStyle-CssClass="table_headCenter"
                                                                    HeaderStyle-Height="40px"
                                                                    ShowFooter="False"
                                                                    ShowHeaderWhenEmpty="True"
                                                                    AllowPaging="True"
                                                                    PageSize="10"
                                                                    BorderStyle="None"
                                                                    CellSpacing="2"
                                                                    DataKeyNames="EDUIDX"
                                                                    OnRowEditing="gvRowEditing"
                                                                    OnPageIndexChanging="Master_PageIndexChanging"
                                                                    OnRowCancelingEdit="gvRowCancelingEdit"
                                                                    OnRowDataBound="Master_RowDataBound"
                                                                    OnRowUpdating="gvRowUpdating">

                                                                    <PagerStyle CssClass="PageCustom" />
                                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                                    <EmptyDataTemplate>
                                                                        <div style="text-align: center">Data Cannot Be Found</div>
                                                                    </EmptyDataTemplate>

                                                                    <Columns>

                                                                        <asp:TemplateField HeaderText="#">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <%# (Container.DataItemIndex +1) %>
                                                                                </div>
                                                                            </ItemTemplate>

                                                                            <EditItemTemplate>
                                                                                <asp:Label ID="sf" runat="server"></asp:Label>
                                                                                <div class="row">
                                                                                    <div class="col-lg-10">
                                                                                        <h3>Education FormEdit<small> (แบบฟอร์มแก้ไขข้อมูลประวัติการศึกษา)</small></h3>
                                                                                        <asp:Label ID="lbl_Edu_edit" runat="server" Visible="false" Text='<%# Eval("EDUIDX")%>'></asp:Label>
                                                                                    </div>
                                                                                </div>
                                                                                <hr />

                                                                                <div class="form-group">
                                                                                    <asp:Label ID="Label65" runat="server" Text="วุฒิการศึกษา" CssClass="col-sm-3 control-label"></asp:Label>
                                                                                    <div class="col-sm-4">
                                                                                        <asp:Label ID="lbl_Edu_qualification_ID" runat="server" Visible="false" Text='<%# Eval("Edu_qualification_ID")%>'></asp:Label>
                                                                                        <asp:DropDownList ID="ddleducationback" runat="server" CssClass="form-control">
                                                                                            <asp:ListItem Value="0">เลือกวุฒิการศึกษา....</asp:ListItem>
                                                                                            <asp:ListItem Value="7">ไม่มีวุฒิการศึกษา</asp:ListItem>
                                                                                            <asp:ListItem Value="1">ต่ำกว่ามัธยมต้น</asp:ListItem>
                                                                                            <asp:ListItem Value="2">มัธยมต้น</asp:ListItem>
                                                                                            <asp:ListItem Value="3">มัธยมปลาย</asp:ListItem>
                                                                                            <asp:ListItem Value="8">ปวช</asp:ListItem>
                                                                                            <asp:ListItem Value="9">ปวส</asp:ListItem>
                                                                                            <asp:ListItem Value="4">ป.ตรี</asp:ListItem>
                                                                                            <asp:ListItem Value="5">ป.โท</asp:ListItem>
                                                                                            <asp:ListItem Value="6">ป.เอก</asp:ListItem>
                                                                                        </asp:DropDownList>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group">
                                                                                    <asp:Label ID="Label66" runat="server" Text="ชื่อสถานศึกษา" CssClass="col-sm-3 control-label"></asp:Label>
                                                                                    <div class="col-sm-4">
                                                                                        <asp:TextBox ID="txtEdu_name_edit" runat="server" CssClass="form-control" Text='<%# Eval("Edu_name") %>'></asp:TextBox>
                                                                                    </div>
                                                                                </div>


                                                                                <div class="form-group">
                                                                                    <asp:Label ID="Label67" runat="server" Text="สาขาวิชาที่ศึกษา" CssClass="col-sm-3 control-label"></asp:Label>
                                                                                    <div class="col-sm-4">
                                                                                        <asp:TextBox ID="txtEdu_branch_edit" runat="server" CssClass="form-control" Text='<%# Eval("Edu_branch") %>'></asp:TextBox>
                                                                                    </div>
                                                                                </div>


                                                                                <div class="form-group">
                                                                                    <asp:Label ID="Label13" runat="server" Text="ปีที่เริ่มศึกษา" CssClass="col-sm-3 control-label"></asp:Label>
                                                                                    <div class="col-sm-4">
                                                                                        <div class='input-group date'>
                                                                                            <asp:TextBox ID="txtEdu_start_edit" runat="server" CssClass="form-control from-date-datepicker" Text='<%# Eval("Edu_start") %>'></asp:TextBox>
                                                                                            <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>


                                                                                <div class="form-group">
                                                                                    <asp:Label ID="Label9" runat="server" Text="ปีที่จบศึกษา" CssClass="col-sm-3 control-label"></asp:Label>
                                                                                    <div class="col-sm-4">
                                                                                        <div class='input-group date'>
                                                                                            <asp:TextBox ID="txtEdu_end_edit" runat="server" CssClass="form-control from-date-datepicker" Text='<%# Eval("Edu_end") %>'></asp:TextBox>
                                                                                            <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>


                                                                                <div class="form-group">
                                                                                    <div class="col-lg-offset-10">
                                                                                        <div class="control-label" style="text-align: center;">
                                                                                            <asp:LinkButton ID="LinkButton1" runat="server" Text="Update" ValidationGroup="SaveUpdate" CssClass="btn btn-success" OnClientClick="return confirm('คุณต้องการแก้ไขและบันทึกรายการนี้ใช่หรือไม่ ?')" CommandName="Update" />
                                                                                            <asp:LinkButton ID="LinkButton2" runat="server" Text="Cancel" CssClass="btn btn-danger" CommandName="Cancel" />
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </EditItemTemplate>

                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="วุฒิการศึกษา">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="La4bel5w8" runat="server" Text='<%# Eval("qualification_name")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="ชื่อสถานศึกษา">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="l4bP24ag2e1" runat="server" Text='<%# Eval("Edu_name")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="สาขาวิชาที่ศึกษา">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="l4b4Pag2e1" runat="server" Text='<%# Eval("Edu_branch")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="ปีที่จบศึกษา">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="l4bP3ag2e1" runat="server" Text='<%# Eval("Edu_end")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="จัดการ">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="btnEditView" runat="server" ValidationGroup="fromgroup" Text="Edit" CssClass="btn btn-info" CommandName="Edit" />
                                                                                <asp:LinkButton ID="btnDeleteGV" runat="server" ValidationGroup="fromgroup" OnCommand="btnCommand" CommandName="DeleteGvEducation_View" CommandArgument='<%# Eval("EDUIDX") %>' Text="Delete" CssClass="btn btn-danger" OnClientClick="return confirm('Do you want delete this item?')" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                    </Columns>

                                                                </asp:GridView>

                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-sm-12">
                                                                <div class="checkbox">
                                                                    <asp:CheckBox ID="ckeducation_edit" runat="server" Text="เพิ่มประวัติการศึกษา" CssClass="col-sm-4" AutoPostBack="true" OnCheckedChanged="chkSelectedIndexChanged" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <asp:Panel ID="BoxEducation" runat="server" Visible="false">
                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label ">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label176" runat="server" Text="Educational" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label177" runat="server" Text="วุฒิการศึกษา" /></b></small>
                                                                    </h2>
                                                                </label>

                                                                <div class="col-sm-2">
                                                                    <asp:DropDownList ID="ddleducationback_edit" runat="server" CssClass="form-control">
                                                                        <asp:ListItem Value="0">เลือกวุฒิการศึกษา....</asp:ListItem>
                                                                        <asp:ListItem Value="7">ไม่มีวุฒิการศึกษา</asp:ListItem>
                                                                        <asp:ListItem Value="1">ต่ำกว่ามัธยมต้น</asp:ListItem>
                                                                        <asp:ListItem Value="2">มัธยมต้น</asp:ListItem>
                                                                        <asp:ListItem Value="3">มัธยมปลาย</asp:ListItem>
                                                                        <asp:ListItem Value="8">ปวช</asp:ListItem>
                                                                        <asp:ListItem Value="9">ปวส</asp:ListItem>
                                                                        <asp:ListItem Value="4">ป.ตรี</asp:ListItem>
                                                                        <asp:ListItem Value="5">ป.โท</asp:ListItem>
                                                                        <asp:ListItem Value="6">ป.เอก</asp:ListItem>
                                                                    </asp:DropDownList>

                                                                    <asp:RequiredFieldValidator ID="Require6dFieldValidator1" ValidationGroup="AddEducation_Edit" runat="server" Display="None"
                                                                        ControlToValidate="ddleducationback_edit" Font-Size="11"
                                                                        ErrorMessage="วุฒิการศึกษา ...."
                                                                        ValidationExpression="วุฒิการศึกษา ...." InitialValue="0" />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="VaqlidatorCal32loutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Require6dFieldValidator1" Width="160" />
                                                                </div>

                                                                <label class="col-sm-2 control-label col-sm-offset-1">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label168" runat="server" Text="School Name" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label169" runat="server" Text="ชื่อสถานศึกษา" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-2">
                                                                    <asp:TextBox ID="txtschoolname_edit" CssClass="form-control" runat="server" ValidationGroup="AddEducation_Edit"></asp:TextBox>

                                                                    <asp:RequiredFieldValidator ID="ReqwuiredFieldVali4dator1" ValidationGroup="AddEducation_Edit" runat="server" Display="None"
                                                                        ControlToValidate="txtschoolname_edit" Font-Size="11"
                                                                        ErrorMessage="วุฒิการศึกษา ...." />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCal32loutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqwuiredFieldVali4dator1" Width="160" />
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label172" runat="server" Text="Start Date" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label173" runat="server" Text="ปีที่เริ่มศึกษา" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-2">
                                                                    <div class='input-group date'>
                                                                        <asp:TextBox ID="txtstarteducation_edit" CssClass="form-control from-date-datepicker" MaxLengh="100%" runat="server" ValidationGroup="AddEducation_Edit"></asp:TextBox>
                                                                        <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                                    </div>

                                                                    <asp:RequiredFieldValidator ID="RequiredFiel89dValidator1" ValidationGroup="AddEducation_Edit" runat="server" Display="None"
                                                                        ControlToValidate="txtstarteducation_edit" Font-Size="11"
                                                                        ErrorMessage="ปีที่เริ่มศึกษา ...." />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="Validat59orCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFiel89dValidator1" Width="160" />
                                                                </div>

                                                                <label class="col-sm-2 control-label col-sm-offset-1">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label174" runat="server" Text="End Date" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label175" runat="server" Text="ปีที่จบการศึกษา" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-2">
                                                                    <div class='input-group date'>
                                                                        <asp:TextBox ID="txtendeducation_edit" CssClass="form-control from-date-datepicker" MaxLengh="100%" runat="server" ValidationGroup="AddEducation_Edit"></asp:TextBox>
                                                                        <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                                    </div>
                                                                </div>

                                                                <asp:RequiredFieldValidator ID="Req63uiredFieldValidator1" ValidationGroup="AddEducation_Edit" runat="server" Display="None"
                                                                    ControlToValidate="txtendeducation_edit" Font-Size="11"
                                                                    ErrorMessage="ปีที่จบการศึกษา ...." />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="V32alidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req63uiredFieldValidator1" Width="160" />
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label170" runat="server" Text="Branch" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label171" runat="server" Text="สาขาวิชาที่ศึกษา" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-2">
                                                                    <asp:TextBox ID="txtstudy_edit" CssClass="form-control" runat="server" ValidationGroup="AddEducation_Edit"></asp:TextBox>

                                                                    <asp:RequiredFieldValidator ID="Re57quiredFieldValidator1" ValidationGroup="AddEducation_Edit" runat="server" Display="None"
                                                                        ControlToValidate="txtstudy_edit" Font-Size="11"
                                                                        ErrorMessage="สาขาวิชาที่ศึกษา ...." />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="Validator432CalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re57quiredFieldValidator1" Width="160" />
                                                                </div>

                                                                <div class="col-sm-1 col-sm-offset-2">
                                                                    <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="AddEducation_Edit" ValidationGroup="AddEducation_Edit">ADD + &nbsp;</asp:LinkButton>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <div class="col-lg-12">

                                                                    <asp:GridView ID="GvEducation_Edit"
                                                                        runat="server"
                                                                        AutoGenerateColumns="false"
                                                                        CssClass="table table-striped table-bordered table-hover"
                                                                        HeaderStyle-CssClass="table_headCenter"
                                                                        HeaderStyle-Height="40px"
                                                                        ShowFooter="False"
                                                                        ShowHeaderWhenEmpty="True"
                                                                        AllowPaging="True"
                                                                        PageSize="10"
                                                                        BorderStyle="None"
                                                                        CellSpacing="2"
                                                                        OnRowDeleting="gvRowDeleting">

                                                                        <PagerStyle CssClass="PageCustom" />
                                                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                                        <EmptyDataTemplate>
                                                                            <div style="text-align: center">Data Cannot Be Found</div>
                                                                        </EmptyDataTemplate>

                                                                        <Columns>

                                                                            <asp:TemplateField HeaderText="#">
                                                                                <ItemTemplate>
                                                                                    <div class="panel-heading">
                                                                                        <%# (Container.DataItemIndex +1) %>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="วุฒิการศึกษา">
                                                                                <ItemTemplate>
                                                                                    <div class="panel-heading">
                                                                                        <asp:Label ID="Label5w8" runat="server" Text='<%# Eval("Edu_qualification")%>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="ชื่อสถานศึกษา">
                                                                                <ItemTemplate>
                                                                                    <div class="panel-heading">
                                                                                        <asp:Label ID="lbP24ag2e1" runat="server" Text='<%# Eval("Edu_name")%>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="สาขาวิชาที่ศึกษา">
                                                                                <ItemTemplate>
                                                                                    <div class="panel-heading">
                                                                                        <asp:Label ID="lb4Pag2e1" runat="server" Text='<%# Eval("Edu_branch")%>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="ปีที่เริ่มศึกษา">
                                                                                <ItemTemplate>
                                                                                    <div class="panel-heading">
                                                                                        <asp:Label ID="lbP2ag2e1" runat="server" Text='<%# Eval("Edu_start")%>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="ปีที่จบศึกษา">
                                                                                <ItemTemplate>
                                                                                    <div class="panel-heading">
                                                                                        <asp:Label ID="lbP3ag2e1" runat="server" Text='<%# Eval("Edu_end")%>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="จัดการ">
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="btnDeleteGV" runat="server" ValidationGroup="fromgroup" CommandName="Delete" Text="Delete" CssClass="btn btn-danger" OnClientClick="return confirm('Do you want delete this item?')" />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                        </Columns>

                                                                    </asp:GridView>

                                                                </div>
                                                            </div>

                                                        </asp:Panel>

                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>


                                        <%-----------------------------------------------------------------------------------------------%>
                                        <%--Training History--%>
                                        <%-----------------------------------------------------------------------------------------------%>
                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <h3 class="panel-title"><i class="glyphicon glyphicon-retweet"></i><strong>&nbsp; Training History / ประวัติการฝึกอบรม</strong></h3>
                                            </div>
                                            <asp:Panel ID="Panel23" runat="server">
                                                <div class="panel-body">
                                                    <div class="form-horizontal" role="form">

                                                        <div class="form-group">
                                                            <div class="col-lg-12">

                                                                <asp:GridView ID="GvTrain_View"
                                                                    runat="server"
                                                                    AutoGenerateColumns="false"
                                                                    CssClass="table table-striped table-bordered table-hover"
                                                                    HeaderStyle-CssClass="table_headCenter"
                                                                    HeaderStyle-Height="40px"
                                                                    ShowFooter="False"
                                                                    ShowHeaderWhenEmpty="True"
                                                                    AllowPaging="True"
                                                                    PageSize="10"
                                                                    BorderStyle="None"
                                                                    CellSpacing="2"
                                                                    DataKeyNames="TNIDX"
                                                                    OnRowEditing="gvRowEditing"
                                                                    OnPageIndexChanging="Master_PageIndexChanging"
                                                                    OnRowCancelingEdit="gvRowCancelingEdit"
                                                                    OnRowDataBound="Master_RowDataBound"
                                                                    OnRowUpdating="gvRowUpdating">


                                                                    <PagerStyle CssClass="PageCustom" />
                                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                                    <EmptyDataTemplate>
                                                                        <div style="text-align: center">Data Cannot Be Found</div>
                                                                    </EmptyDataTemplate>

                                                                    <Columns>

                                                                        <asp:TemplateField HeaderText="#">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <%# (Container.DataItemIndex +1) %>
                                                                                </div>
                                                                            </ItemTemplate>

                                                                            <EditItemTemplate>
                                                                                <asp:Label ID="sf" runat="server"></asp:Label>
                                                                                <div class="row">
                                                                                    <div class="col-lg-10">
                                                                                        <h3>Training FormEdit<small> (แบบฟอร์มแก้ไขข้อมูลประวัติการอบรม)</small></h3>
                                                                                        <asp:Label ID="lbl_Tn_edit" runat="server" Visible="false" Text='<%# Eval("TNIDX")%>'></asp:Label>
                                                                                    </div>
                                                                                </div>
                                                                                <hr />

                                                                                <div class="form-group">
                                                                                    <asp:Label ID="Label265" runat="server" Text="หลักสูตรอาบรม" CssClass="col-sm-3 control-label"></asp:Label>
                                                                                    <div class="col-sm-4">
                                                                                        <asp:TextBox ID="txtTrain_courses_edit" runat="server" CssClass="form-control" Text='<%# Eval("Train_courses") %>'></asp:TextBox>
                                                                                    </div>
                                                                                </div>


                                                                                <div class="form-group">
                                                                                    <asp:Label ID="Label66" runat="server" Text="วันที่อบรม" CssClass="col-sm-3 control-label"></asp:Label>
                                                                                    <div class="col-sm-4">
                                                                                        <div class='input-group date'>
                                                                                            <asp:TextBox ID="txtTrain_date_edit" runat="server" CssClass="form-control from-date-datepicker" Text='<%# Eval("Train_date") %>'></asp:TextBox>
                                                                                            <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>


                                                                                <div class="form-group">
                                                                                    <asp:Label ID="Label67" runat="server" Text="ผลการประเมิน" CssClass="col-sm-3 control-label"></asp:Label>
                                                                                    <div class="col-sm-4">
                                                                                        <asp:TextBox ID="txtTrain_assessment_edit" runat="server" CssClass="form-control" Text='<%# Eval("Train_assessment") %>'></asp:TextBox>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group">
                                                                                    <div class="col-lg-offset-10">
                                                                                        <div class="control-label" style="text-align: center;">
                                                                                            <asp:LinkButton ID="LinkButton1" runat="server" Text="Update" ValidationGroup="SaveUpdate" CssClass="btn btn-success" OnClientClick="return confirm('คุณต้องการแก้ไขและบันทึกรายการนี้ใช่หรือไม่ ?')" CommandName="Update" />
                                                                                            <asp:LinkButton ID="LinkButton2" runat="server" Text="Cancel" CssClass="btn btn-danger" CommandName="Cancel" />
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </EditItemTemplate>

                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="หลักสูตรอาบรม">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="Labewl5w8" runat="server" Text='<%# Eval("Train_courses")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="วันที่อบรม">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="lbP2aw2g2e1" runat="server" Text='<%# Eval("Train_date")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="ผลการประเมิน">
                                                                            <ItemTemplate>
                                                                                <div class="panel-heading">
                                                                                    <asp:Label ID="lbPagw2e1" runat="server" Text='<%# Eval("Train_assessment")%>'></asp:Label>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="จัดการ">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="btnEditView" runat="server" ValidationGroup="fromgroup" Text="Edit" CssClass="btn btn-info" CommandName="Edit" />
                                                                                <asp:LinkButton ID="btnDeleteGV" runat="server" ValidationGroup="fromgroup" OnCommand="btnCommand" CommandName="DeleteGvTrain_View" CommandArgument='<%# Eval("TNIDX") %>' Text="Delete" CssClass="btn btn-danger" OnClientClick="return confirm('Do you want delete this item?')" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                    </Columns>

                                                                </asp:GridView>

                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-sm-12">
                                                                <div class="checkbox">
                                                                    <asp:CheckBox ID="cktraining_edit" runat="server" Text="เพิ่มประวัติฝึกอบรม" CssClass="col-sm-4" AutoPostBack="true" OnCheckedChanged="chkSelectedIndexChanged" />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <asp:Panel ID="BoxTrain" runat="server" Visible="false">
                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label178" runat="server" Text="Training Courses" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label179" runat="server" Text="หลักสูตรฝึกอบรม" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-2">
                                                                    <asp:TextBox ID="txttraincourses_edit" CssClass="form-control" runat="server" ValidationGroup="Addtraining_Edit"></asp:TextBox>

                                                                    <asp:RequiredFieldValidator ID="RequiredFi486eldValidator1" ValidationGroup="Addtraining_Edit" runat="server" Display="None"
                                                                        ControlToValidate="txttraincourses_edit" Font-Size="11"
                                                                        ErrorMessage="หลักสูตรฝึกอบรม ...." />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorC432alloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFi486eldValidator1" Width="160" />
                                                                </div>

                                                                <label class="col-sm-2 control-label col-sm-offset-1">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label180" runat="server" Text="Training Date" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label181" runat="server" Text="วันที่ฝึกอบรม" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-2">
                                                                    <div class='input-group date'>
                                                                        <asp:TextBox ID="txttraindate_edit" CssClass="form-control from-date-datepicker" MaxLengh="100%" runat="server" ValidationGroup="Addtraining_Edit"></asp:TextBox>
                                                                        <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                                    </div>

                                                                    <asp:RequiredFieldValidator ID="Require50dFieldValidator1" ValidationGroup="Addtraining_Edit" runat="server" Display="None"
                                                                        ControlToValidate="txttraindate_edit" Font-Size="11"
                                                                        ErrorMessage="วันที่ฝึกอบรม ...." />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="Validat44orCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Require50dFieldValidator1" Width="160" />
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label182" runat="server" Text="Assessment" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label183" runat="server" Text="ผลการประเมิน" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-2">
                                                                    <asp:TextBox ID="txttrainassessment_edit" CssClass="form-control" runat="server" ValidationGroup="Addtraining_Edit"></asp:TextBox>

                                                                    <asp:RequiredFieldValidator ID="Re41quiredFieldValidator1" ValidationGroup="Addtraining_Edit" runat="server" Display="None"
                                                                        ControlToValidate="txttrainassessment_edit" Font-Size="11"
                                                                        ErrorMessage="ผลการประเมิน ...." />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExt53ender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re41quiredFieldValidator1" Width="160" />
                                                                </div>

                                                                <div class="col-sm-1 col-sm-offset-2">
                                                                    <asp:LinkButton ID="LinkButton3" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="Addtraining_Edit" ValidationGroup="Addtraining_Edit">ADD + &nbsp;</asp:LinkButton>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <div class="col-lg-12">

                                                                    <asp:GridView ID="GvTrain_Edit"
                                                                        runat="server"
                                                                        AutoGenerateColumns="false"
                                                                        CssClass="table table-striped table-bordered table-hover"
                                                                        HeaderStyle-CssClass="table_headCenter"
                                                                        HeaderStyle-Height="40px"
                                                                        ShowFooter="False"
                                                                        ShowHeaderWhenEmpty="True"
                                                                        AllowPaging="True"
                                                                        PageSize="10"
                                                                        BorderStyle="None"
                                                                        CellSpacing="2"
                                                                        OnRowDeleting="gvRowDeleting">

                                                                        <PagerStyle CssClass="PageCustom" />
                                                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                                        <EmptyDataTemplate>
                                                                            <div style="text-align: center">Data Cannot Be Found</div>
                                                                        </EmptyDataTemplate>

                                                                        <Columns>

                                                                            <asp:TemplateField HeaderText="#">
                                                                                <ItemTemplate>
                                                                                    <div class="panel-heading">
                                                                                        <%# (Container.DataItemIndex +1) %>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="หลักสูตรอาบรม">
                                                                                <ItemTemplate>
                                                                                    <div class="panel-heading">
                                                                                        <asp:Label ID="Label5w8" runat="server" Text='<%# Eval("Train_courses")%>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="วันที่อบรม">
                                                                                <ItemTemplate>
                                                                                    <div class="panel-heading">
                                                                                        <asp:Label ID="lbP2a2g2e1" runat="server" Text='<%# Eval("Train_date")%>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="ผลการประเมิน">
                                                                                <ItemTemplate>
                                                                                    <div class="panel-heading">
                                                                                        <asp:Label ID="lbPag2e1" runat="server" Text='<%# Eval("Train_assessment")%>'></asp:Label>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField HeaderText="จัดการ">
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="btnDeleteGV" runat="server" ValidationGroup="fromgroup" CommandName="Delete" Text="Delete" CssClass="btn btn-danger" OnClientClick="return confirm('Do you want delete this item?')" />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                        </Columns>

                                                                    </asp:GridView>

                                                                </div>
                                                            </div>

                                                        </asp:Panel>

                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>

                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <h3 class="panel-title"><i class="glyphicon glyphicon-home"></i><strong>&nbsp; Address & Contact (Present) / ที่อยู่และการติดต่อ(ปัจจุบัน)</strong></h3>
                                            </div>
                                            <asp:Panel ID="BoxAddress" runat="server">
                                                <div class="panel-body">
                                                    <div class="form-horizontal" role="form">


                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label63" runat="server" Text="PresentAddress" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label66" runat="server" Text="ที่อยู่ปัจจุบัน" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-4">
                                                                <asp:TextBox ID="txtaddress_present" autocomplete="off" MaxLength="250" TextMode="MultiLine" Rows="3" CssClass="form-control" runat="server" Text='<%# Eval("emp_address") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label68" runat="server" Text="Country" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label69" runat="server" Text="ประเทศ" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:Label ID="lbl_country_edit" runat="server" Visible="false" Text='<%# Eval("country_idx") %>'></asp:Label>
                                                                <asp:DropDownList ID="ddlcountry_present_edit" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="0">เลือกประเทศ....</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>

                                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Lab3el15" runat="server" Text="Province" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label416" runat="server" Text="จังหวัด" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:Label ID="lbl_prov_edit" runat="server" Visible="false" Text='<%# Eval("prov_idx") %>'></asp:Label>
                                                                <asp:DropDownList ID="ddlprovince_present_edit" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                    <asp:ListItem Value="0">เลือกจังหวัด....</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label71" runat="server" Text="Amphoe" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label72" runat="server" Text="อำเภอ" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:Label ID="lbl_amp_edit" runat="server" Visible="false" Text='<%# Eval("amp_idx") %>'></asp:Label>
                                                                <asp:DropDownList ID="ddlamphoe_present_edit" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                    <asp:ListItem Value="0">เลือกอำเภอ....</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>

                                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label73" runat="server" Text="District" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label75" runat="server" Text="ตำบล" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:Label ID="lbl_dist_edit" runat="server" Visible="false" Text='<%# Eval("dist_idx") %>'></asp:Label>
                                                                <asp:DropDownList ID="ddldistrict_present_edit" runat="server" CssClass="form-control" AutoPostBack="true">
                                                                    <asp:ListItem Value="0">เลือกตำบล....</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label79" runat="server" Text="ZipCode" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label80" runat="server" Text="รหัสไปรษณีย์" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtzipcode_present" CssClass="form-control" runat="server" Text='<%# Eval("post_code") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                            </div>

                                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label83" runat="server" Text="Email" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label84" runat="server" Text="อีเมล" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtemail_present" CssClass="form-control" runat="server" Text='<%# Eval("emp_email") %>' ValidationGroup="SaveAdd"></asp:TextBox>

                                                                <asp:RegularExpressionValidator ID="R_Email" runat="server" SetFocusOnError="true" Display="None" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="txtemail_present" ErrorMessage="*กรอกในรูปแบบEmail" ValidationGroup="SaveAdd"></asp:RegularExpressionValidator>
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="V_Email2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_Email" Width="160" />
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label76" runat="server" Text="Mobile Phone" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label78" runat="server" Text="โทรศัพท์มือถือ" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txttelmobile_present" CssClass="form-control" runat="server" Text='<%# Eval("emp_mobile_no") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                            </div>

                                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label81" runat="server" Text="Home Phone" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label82" runat="server" Text="โทรศัพท์บ้าน" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txttelhome_present" CssClass="form-control" runat="server" Text='<%# Eval("emp_phone_no") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <hr />

                                                        <div class="form-group">
                                                            <label class="col-sm-12">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label85" runat="server" CssClass="h01" Text="Address & Contact (Permanent)" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label86" runat="server" Text="ที่อยู่และการติดต่อ(ทะเบียนบ้าน)" /></b></small>
                                                                </h2>
                                                            </label>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-labelnotop">
                                                                <asp:CheckBox ID="chkAddress_edit" CssClass="text-primary" Text="เหมือนกับที่อยู่ปัจจุบัน" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelectedIndexChanged" />
                                                            </label>
                                                        </div>

                                                        <asp:Panel ID="BoxAddress_edit" runat="server">

                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label87" runat="server" Text="Permanentaddress" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label88" runat="server" Text="ที่อยู่ตามทะเบียนบ้าน" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-4">
                                                                    <asp:TextBox ID="txtaddress_permanentaddress" autocomplete="off" MaxLength="250" TextMode="MultiLine" Rows="3" CssClass="form-control" runat="server" Text='<%# Eval("emp_address_permanent") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label89" runat="server" Text="Country" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label90" runat="server" Text="ประเทศ" /></b></small>
                                                                    </h2>
                                                                </label>

                                                                <div class="col-sm-2">
                                                                    <asp:Label ID="lbl_ddlcountry_permanent_edit" runat="server" Visible="false" Text='<%# Eval("country_idx_permanent") %>'></asp:Label>
                                                                    <asp:DropDownList ID="ddlcountry_permanentaddress_edit" runat="server" CssClass="form-control">
                                                                        <asp:ListItem Value="0">เลือกประเทศ....</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>

                                                                <label class="col-sm-2 control-label col-sm-offset-1">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label91" runat="server" Text="Province" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label92" runat="server" Text="จังหวัด" /></b></small>
                                                                    </h2>
                                                                </label>

                                                                <div class="col-sm-2">
                                                                    <asp:Label ID="lbl_ddlprovince_permanent_edit" runat="server" Visible="false" Text='<%# Eval("prov_idx_permanent") %>'></asp:Label>
                                                                    <asp:DropDownList ID="ddlprovince_permanentaddress_edit" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                        <asp:ListItem Value="0">เลือกจังหวัด....</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label93" runat="server" Text="Amphoe" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label94" runat="server" Text="อำเภอ" /></b></small>
                                                                    </h2>
                                                                </label>

                                                                <div class="col-sm-2">
                                                                    <asp:Label ID="lbl_ddlamphoe_permanent_edit" runat="server" Visible="false" Text='<%# Eval("amp_idx_permanent") %>'></asp:Label>
                                                                    <asp:DropDownList ID="ddlamphoe_permanentaddress_edit" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                        <asp:ListItem Value="0">เลือกอำเภอ....</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>

                                                                <label class="col-sm-2 control-label col-sm-offset-1">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label95" runat="server" Text="District" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label96" runat="server" Text="ตำบล" /></b></small>
                                                                    </h2>
                                                                </label>

                                                                <div class="col-sm-2">
                                                                    <asp:Label ID="lbl_ddldistrict_permanent_edit" runat="server" Visible="false" Text='<%# Eval("dist_idx_permanent") %>'></asp:Label>
                                                                    <asp:DropDownList ID="ddldistrict_permanentaddress_edit" runat="server" CssClass="form-control" AutoPostBack="true">
                                                                        <asp:ListItem Value="0">เลือกตำบล....</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-sm-2 control-label">
                                                                    <h2 class="panel-title">
                                                                        <asp:Label ID="Label97" runat="server" Text="Home Phone" /><br />
                                                                        <small><b>
                                                                            <asp:Label ID="Label98" runat="server" Text="โทรศัพท์บ้าน" /></b></small>
                                                                    </h2>
                                                                </label>
                                                                <div class="col-sm-2">
                                                                    <asp:TextBox ID="txttelmobile_permanentaddress" CssClass="form-control" runat="server" Text='<%# Eval("PhoneNumber_permanent") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                                </div>

                                                            </div>

                                                        </asp:Panel>

                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>

                                        <div class="panel panel-info">
                                            <div class="panel-heading">
                                                <h3 class="panel-title"><i class="glyphicon glyphicon-heart"></i><strong>&nbsp; Health  / ร่างกาย & สุขภาพ</strong></h3>
                                            </div>
                                            <asp:Panel ID="Panel25" runat="server">
                                                <div class="panel-body">
                                                    <div class="form-horizontal" role="form">

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label38" runat="server" Text="Currently used hospital" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label39" runat="server" Text="ปัจจุบันใช้สถานพยาบาล" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:Label ID="lbl_hos_edit" runat="server" Visible="false" Text='<%# Eval("soc_hos_idx") %>'></asp:Label>
                                                                <asp:DropDownList ID="ddlhospital" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="0">เลือกโรงพยาบาล</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>

                                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label121" runat="server" Text="Date of examination" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label122" runat="server" Text="วันที่ตรวจร่างกาย" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <div class='input-group date'>
                                                                    <%--Text='<%# formatDate((String)Eval("Examinationdate")) %>'--%>
                                                                    <asp:TextBox ID="txtexaminationdate" CssClass="form-control from-date-datepicker" MaxLengh="100%" runat="server" Text='<%# Eval("Examinationdate") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                                    <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label40" runat="server" Text="Social Security No" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label41" runat="server" Text="เลขที่ประกันสังคม" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtsecurityid" CssClass="form-control" runat="server" Text='<%# Eval("soc_no") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                            </div>

                                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label45" runat="server" Text="Expiration Date" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label46" runat="server" Text="วันหมดอายุ" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <div class='input-group date'>
                                                                    <%--Text='<%# formatDate((String)Eval("soc_expired_date")) %>' --%>
                                                                    <asp:TextBox ID="txtexpsecurity" CssClass="form-control from-date-datepicker" MaxLengh="100%" runat="server" Text='<%# Eval("soc_expired_date") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                                    <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                                </div>
                                                            </div>

                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label50" runat="server" Text="Height" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label51" runat="server" Text="ส่วนสูง" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtheight" CssClass="form-control" runat="server" Text='<%# Eval("height_s") %>' ValidationGroup="SaveAdd"></asp:TextBox>

                                                                <asp:RegularExpressionValidator ID="R_Height" runat="server" ErrorMessage="*เฉพาะตัวเลขเท่านั้น" SetFocusOnError="true" Display="None" ControlToValidate="txtheight" ValidationExpression="^[0-9'.\s]{1,3}$" ValidationGroup="SaveAdd" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="VS_Height2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_Height" Width="160" />
                                                            </div>
                                                            <label class="col-sm-1 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label54" runat="server" Text="Cm" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label55" runat="server" Text="ซม." /></b></small>
                                                                </h2>
                                                            </label>

                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label52" runat="server" Text="Weight" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label53" runat="server" Text="น้ำหนัก" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtweight" CssClass="form-control" runat="server" Text='<%# Eval("weight_s") %>' ValidationGroup="SaveAdd"></asp:TextBox>

                                                                <asp:RegularExpressionValidator ID="R_Weigth" runat="server" ErrorMessage="*เฉพาะตัวเลขเท่านั้น" SetFocusOnError="true" Display="None" ControlToValidate="txtweight" ValidationExpression="^[0-9'.\s]{1,3}$" ValidationGroup="SaveAdd" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="V_Weigth2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_Weigth" Width="160" />
                                                            </div>
                                                            <label class="col-sm-1 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label56" runat="server" Text="Kg" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label58" runat="server" Text="กก." /></b></small>
                                                                </h2>
                                                            </label>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label59" runat="server" Text="BloodGroup" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label60" runat="server" Text="กรุ๊ปเลือด" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:Label ID="lbl_BRHIDX" runat="server" Visible="false" Text='<%# Eval("BTypeIDX") %>' />
                                                                <asp:DropDownList ID="ddlblood" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="0">เลือกกรุ๊ปเลือด....</asp:ListItem>
                                                                    <asp:ListItem Value="1">เอ (A)</asp:ListItem>
                                                                    <asp:ListItem Value="2">บี (B)</asp:ListItem>
                                                                    <asp:ListItem Value="3">โอ (O)</asp:ListItem>
                                                                    <asp:ListItem Value="4">เอบี (AB)</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>

                                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label61" runat="server" Text="Scar" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label62" runat="server" Text="ตำหนิ" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtscar" CssClass="form-control" runat="server" Text='<%# Eval("scar_s") %>' ValidationGroup="SaveAdd"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label117" runat="server" Text="Medical certificate" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label118" runat="server" Text="ใบรับรองแพทย์" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:Label ID="lbl_medical_id" runat="server" Visible="false" Text='<%# Eval("medicalcertificate_id") %>' />
                                                                <asp:DropDownList ID="ddlmedicalcertificate" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="0">เลือกสถานะใบรับรอง....</asp:ListItem>
                                                                    <asp:ListItem Value="1">มี</asp:ListItem>
                                                                    <asp:ListItem Value="2">ไม่มี</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>

                                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label119" runat="server" Text="Result Lab" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label120" runat="server" Text="ผลตรวจ LAB" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:Label ID="lbl_resultlab_id" runat="server" Visible="false" Text='<%# Eval("resultlab_id") %>' />
                                                                <asp:DropDownList ID="ddlresultlab" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="0">เลือกสถานะผลตรวจ....</asp:ListItem>
                                                                    <asp:ListItem Value="1">มี</asp:ListItem>
                                                                    <asp:ListItem Value="2">ไม่มี</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>

                                    </asp:Panel>

                                </EditItemTemplate>

                            </asp:FormView>

                            <asp:Panel ID="box_print" runat="server" Visible="true">
                                <asp:FormView ID="FvTemplate_print" runat="server" HorizontalAlign="Center" DefaultMode="ReadOnly" OnDataBound="FvDetail_DataBound">
                                    <ItemTemplate>

                                        <div id="printableAreaHistoryEmp" class="print_report hidden">
                                            <div class="panel panel-default">
                                                <table class="table f-s-12 m-t-10" style="border: 1px solid black; border-collapse: collapse">
                                                    <h3 style="text-align: center;">บริษัท เถ้าแก่น้อย ฟู๊ดแอนด์มาร์เก็ตติ้ง จำกัด (มหาชน)</h3>
                                                    <h3 style="text-align: center;">Taokaenoi Food & Marketing Public Co.,Ltd</h3>
                                                    <h3 style="text-align: center;">ใบสมัครงานพนักงานรายวัน/รายเดือน (Application Form)</h3>

                                                    <tr>
                                                        <td style="text-align: center; vertical-align: middle; border: 1px solid black" rowspan="2"><b>ตำแหน่งที่ต้องการ : </b></h5><h5><b>(Position applied for) </b></h5>
                                                        </td>
                                                        <td style="text-align: left; border: 1px solid black">
                                                            <h5>1. <%# Eval("P_Name") %></h5>
                                                        </td>
                                                        <td style="text-align: left; border: 1px solid black">
                                                            <h5><b>เงินเดือนที่ต้องการ (Expected starting salary) : </b></h5>
                                                        </td>
                                                        <td style="text-align: left; border: 1px solid black">
                                                            <h5><%# Eval("salary") %> บาท</h5>
                                                        </td>
                                                        <td style="text-align: left; border: 1px solid black" rowspan="5">
                                                            <img class="media-object img-thumbnail" src='<%=ResolveUrl("~/uploadfiles/profile_picture/default/user-default.png")%>' width="100px" height="100px" style="margin-right: 8px; margin-top: -5px;"></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: left; border: 1px solid black">
                                                            <h5>2. </h5>
                                                        </td>
                                                        <td style="text-align: left; border: 1px solid black">
                                                            <h5><b>วันที่พร้อมจะเริ่มงาน (Start working) : </b></h5>
                                                        </td>
                                                        <td style="text-align: left; border: 1px solid black">
                                                            <h5><%# Eval("WName") %></h5>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: center; border: 1px solid black" colspan="4">
                                                            <h5><b>ใบสมัครเป็นส่วนหนึ่งในการพิจารณา โปรดกรอกข้อความให้ครบถ้วน</b></h5>
                                                            <h5><b>Application Form is a part of consideration , please fill this form completely </b></h5>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: left; border: 1px solid black" colspan="1">
                                                            <h5><b>ชื่อ-สกุล :</b></h5>
                                                        </td>
                                                        <td style="text-align: left; border: 1px solid black" colspan="3">
                                                            <h5><%# Eval("prefix_name_th") %><%# Eval("emp_name_th") %></h5>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: left; border: 1px solid black" colspan="1">
                                                            <h5><b>Name in English :</b></h5>
                                                        </td>
                                                        <td style="text-align: left; border: 1px solid black" colspan="3">
                                                            <h5><%# Eval("prefix_name_en") %><%# Eval("emp_name_en") %></h5>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: left; border: 1px solid black" colspan="2">
                                                            <asp:Label ID="lblJName" runat="server" Text='<%# Eval("JName") %>' Visible="false" />
                                                            <h5><b>ประเภทงาน (Job Category) :</b></h5>
                                                            <asp:CheckBoxList ID="Ckl_jobcate" runat="server" RepeatLayout="Table"
                                                                RepeatColumns="4" RepeatDirection="Vertical" Enabled="false">
                                                                <asp:ListItem Value="ประจำ"> ประจำ </asp:ListItem>
                                                                <asp:ListItem Value="part-time"> part-time </asp:ListItem>
                                                                <asp:ListItem Value="freelance"> freelance </asp:ListItem>
                                                                <asp:ListItem Value="ฝึกงาน"> ฝึกงาน </asp:ListItem>
                                                            </asp:CheckBoxList>
                                                        </td>
                                                        <td style="text-align: left; border: 1px solid black" colspan="1">
                                                            <asp:Label ID="lblmil_idx" runat="server" Text='<%# Eval("mil_idx") %>' Visible="false" />
                                                            <h5><b>สถานะทางทหาร (Military Service) : </b></h5>
                                                            <asp:CheckBoxList ID="ckl_military" runat="server" RepeatLayout="Table"
                                                                RepeatColumns="3" RepeatDirection="Vertical" Enabled="false">
                                                                <asp:ListItem Value="1"> ผ่านการเกณฑ์ </asp:ListItem>
                                                                <asp:ListItem Value="2"> เรียน รด. </asp:ListItem>
                                                                <asp:ListItem Value="3"> ได้รับข้อยกเว้น </asp:ListItem>
                                                            </asp:CheckBoxList>
                                                        </td>
                                                        <td style="text-align: left; border: 1px solid black" colspan="2">
                                                            <asp:Label ID="lblmarried_status" runat="server" Text='<%# Eval("married_status") %>' Visible="false" />
                                                            <h5><b>สถานะครอบครัว (Marital Status) :</b></h5>
                                                            <asp:CheckBoxList ID="ckl_mrried" runat="server" RepeatLayout="Table"
                                                                RepeatColumns="3" RepeatDirection="Vertical" Enabled="false">
                                                                <asp:ListItem Value="1"> โสด </asp:ListItem>
                                                                <asp:ListItem Value="2"> สมรส </asp:ListItem>
                                                                <asp:ListItem Value="3"> อย่าร้าง </asp:ListItem>
                                                            </asp:CheckBoxList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: left; border: 1px solid black" colspan="1">
                                                            <h5><b>วัน-เดือน-ปีเกิด :</b> <%# Eval("emp_birthday") %></h5>
                                                            <h5><b>Date of Birth</b></h5>
                                                        </td>
                                                        <td style="text-align: left; border: 1px solid black" colspan="1">
                                                            <h5><b>อายุ : </b><%# Eval("brh_idx") %> ปี</h5>
                                                            <h5><b>Age</b></h5>
                                                        </td>
                                                        <td style="text-align: left; border: 1px solid black" colspan="1">
                                                            <h5><b>สัญชาติ : </b><%# Eval("nat_name_th") %></h5>
                                                            <h5><b>Nationality</b></h5>
                                                        </td>
                                                        <td style="text-align: left; border: 1px solid black" colspan="1">
                                                            <h5><b>เชื้อชาติ : </b><%# Eval("race_name_th") %></h5>
                                                            <h5><b>Race</b></h5>
                                                        </td>
                                                        <td style="text-align: left; border: 1px solid black" colspan="1">
                                                            <asp:Label ID="lbl_SexNameTH_hr" runat="server" Text='<%# Eval("sex_name_th") %>' Visible="false" />
                                                            <h5><b>เพศ (Sex) :</b></h5>
                                                            <asp:CheckBoxList ID="check_SexNameTH" runat="server" RepeatLayout="Table"
                                                                RepeatColumns="2" RepeatDirection="Vertical" Enabled="false">
                                                                <asp:ListItem Value="ชาย">ชาย</asp:ListItem>
                                                                <asp:ListItem Value="หญิง">หญิง</asp:ListItem>
                                                            </asp:CheckBoxList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: left; border: 1px solid black" colspan="1">
                                                            <h5><b>ส่วนสูง : </b><%# Eval("height_s") %> ซม.</h5>
                                                            <h5><b>Height</b></h5>
                                                        </td>
                                                        <td style="text-align: left; border: 1px solid black" colspan="1">
                                                            <h5><b>น้ำหนัก : </b><%# Eval("weight_s") %> กก.</h5>
                                                            <h5><b>Weight</b></h5>
                                                        </td>
                                                        <td style="text-align: left; border: 1px solid black" colspan="1">
                                                            <h5><b>ตำหนิ : </b><%# Eval("scar_s") %></h5>
                                                            <h5><b>Scar</b></h5>
                                                        </td>
                                                        <td style="text-align: left; border: 1px solid black" colspan="1">
                                                            <asp:Label ID="lblBTypeIDX" runat="server" Text='<%# Eval("BTypeIDX") %>' Visible="false" />
                                                            <h5><b>กรุ๊ปเลือด : </b>
                                                                <asp:Label ID="lbl_BType" runat="server" /></h5>
                                                            <h5><b>Blood group</b></h5>
                                                        </td>
                                                        <td style="text-align: left; border: 1px solid black" colspan="1">
                                                            <h5><b>ศาสนา : </b><%# Eval("rel_name_th") %></h5>
                                                            <h5><b>Religion</b></h5>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: left; border: 1px solid black" colspan="4">
                                                            <h5><b>ที่อยู่ปัจจุบันที่ติดต่อได้สะดวก :</b> <%# Eval("emp_address") %> <%# Eval("dist_name") %> <%# Eval("amp_name") %> <%# Eval("prov_name") %> <%# Eval("post_code") %> <%# Eval("country_name") %></h5>
                                                            <h5><b>Present Address</b></h5>
                                                        </td>
                                                        <td style="text-align: left; border: 1px solid black" colspan="1">
                                                            <h5><b>เบอร์โทรศัพทติดต่อ (Tel) : </b></h5>
                                                            <h5><b><%# Eval("emp_mobile_no") %></b></h5>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: left; border: 1px solid black" colspan="4">
                                                            <h5><b>ที่อยู่ตามทะเบียนบ้าน :</b> <%# Eval("emp_address_permanent") %> <%# Eval("idist_name_th") %> <%# Eval("iamp_name_th") %> <%# Eval("iprov_name_th") %> <%# Eval("ipost_code") %> <%# Eval("icountry_name_th") %></h5>
                                                            <h5><b>Permanent Address</b></h5>
                                                        </td>
                                                        <td style="text-align: left; border: 1px solid black" colspan="1">
                                                            <h5><b>เบอร์โทรศัพทติดต่อ (Tel) : </b></h5>
                                                            <h5><b><%# Eval("PhoneNumber_permanent") %></b></h5>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: left; border: 1px solid black" colspan="2">
                                                            <h5><b>บัตรประชาชนเลขที่ : </b><%# Eval("identity_card") %></h5>
                                                            <h5><b>ID. Card No.</b></h5>
                                                        </td>
                                                        <td style="text-align: left; border: 1px solid black" colspan="1">
                                                            <h5><b>ออกให้ ณ : </b><%# Eval("issued_at") %></h5>
                                                            <h5><b>Issued at</b></h5>
                                                        </td>
                                                        <td style="text-align: left; border: 1px solid black" colspan="2">
                                                            <h5><b>บัตรหมดอายุ : </b><%# Eval("idate_expired") %></h5>
                                                            <h5><b>Expired date</b></h5>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: left; border: 1px solid black" colspan="2">
                                                            <h5><b>บัตรรับรองสิทธิประกันสังคมเลขที่ : </b><%# Eval("identity_card") %></h5>
                                                            <h5><b>Social. Securrity Card No.</b></h5>
                                                        </td>
                                                        <td style="text-align: left; border: 1px solid black" colspan="1">
                                                            <h5><b>สถานพยาบาล : </b><%# Eval("security_id") %></h5>
                                                            <h5><b>Hospital</b></h5>
                                                        </td>
                                                        <td style="text-align: left; border: 1px solid black" colspan="1">
                                                            <h5><b>ออกให้ ณ : </b>-</h5>
                                                            <h5><b>Issued at</b></h5>
                                                        </td>
                                                        <td style="text-align: left; border: 1px solid black" colspan="1">
                                                            <h5><b>บัตรหมดอายุ : </b><%# Eval("soc_expired_date") %></h5>
                                                            <h5><b>Expired date</b></h5>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: center; border: 1px solid black" colspan="6">
                                                            <h5><b>ข้อมูลครอบครัว</b></h5>
                                                            <h5><b>Family</b></h5>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: left; border: 1px solid black" colspan="2">
                                                            <h5><b>ชื่อ-สกุล บิดา : </b><%# Eval("fathername") %></h5>
                                                            <h5><b>Name of father</b></h5>
                                                        </td>
                                                        <td style="text-align: left; border: 1px solid black" colspan="1">
                                                            <h5><b>เลขบัตรประชาชน : </b><%# Eval("licen_father") %></h5>
                                                            <h5><b>ID. Card No.</b></h5>
                                                        </td>
                                                        <td style="text-align: left; border: 1px solid black" colspan="2">
                                                            <h5><b>เบอร์ติดต่อ : </b><%# Eval("telfather") %></h5>
                                                            <h5><b>Tel</b></h5>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: left; border: 1px solid black" colspan="2">
                                                            <h5><b>ชื่อ-สกุล มารดา : </b><%# Eval("mothername") %></h5>
                                                            <h5><b>Name of mother</b></h5>
                                                        </td>
                                                        <td style="text-align: left; border: 1px solid black" colspan="1">
                                                            <h5><b>เลขบัตรประชาชน : </b><%# Eval("licen_mother") %></h5>
                                                            <h5><b>ID. Card No.</b></h5>
                                                        </td>
                                                        <td style="text-align: left; border: 1px solid black" colspan="2">
                                                            <h5><b>เบอร์ติดต่อ : </b><%# Eval("telmother") %></h5>
                                                            <h5><b>Tel</b></h5>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: left; border: 1px solid black" colspan="2">
                                                            <h5><b>ชื่อ-สกุล คู่สมรศ : </b><%# Eval("wifename") %></h5>
                                                            <h5><b>Name of wife</b></h5>
                                                        </td>
                                                        <td style="text-align: left; border: 1px solid black" colspan="1">
                                                            <h5><b>เลขบัตรประชาชน : </b><%# Eval("licen_wife") %></h5>
                                                            <h5><b>ID. Card No.</b></h5>
                                                        </td>
                                                        <td style="text-align: left; border: 1px solid black" colspan="2">
                                                            <h5><b>เบอร์ติดต่อ : </b><%# Eval("telwife") %></h5>
                                                            <h5><b>Tel</b></h5>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: left; border: 1px solid black" colspan="2">
                                                            <h5><b>ชื่อ-สกุล กรณีติดต่อฉุกเฉิน : </b><%# Eval("emer_name") %></h5>
                                                            <h5><b>Emergency Contact</b></h5>
                                                        </td>
                                                        <td style="text-align: left; border: 1px solid black" colspan="1">
                                                            <h5><b>ความสัมพันธ์ : </b><%# Eval("emer_relation") %></h5>
                                                            <h5><b>Relation Contact</b></h5>
                                                        </td>
                                                        <td style="text-align: left; border: 1px solid black" colspan="2">
                                                            <h5><b>เบอร์ติดต่อ : </b><%# Eval("emer_tel") %></h5>
                                                            <h5><b>Tel</b></h5>
                                                        </td>
                                                    </tr>

                                                </table>
                                            </div>

                                            <div style="page-break-after: always;"></div>

                                            <div class="panel panel-default">
                                                <table class="table f-s-12 m-t-10" style="border: 1px solid black; border-collapse: collapse">

                                                    <tr>
                                                        <td style="text-align: center; vertical-align: middle; border: 1px solid black" colspan="6">
                                                            <h5><b>ข้อมูลบุตร</b></h5>
                                                            <h5><b>Children</b></h5>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: left; border: 1px solid black" colspan="6">
                                                            <asp:GridView ID="GvChildAdd_print"
                                                                runat="server"
                                                                AutoGenerateColumns="false"
                                                                CssClass="table table-striped table-bordered table-hover"
                                                                HeaderStyle-CssClass="table_headCenter"
                                                                HeaderStyle-Height="40px"
                                                                ShowFooter="False"
                                                                ShowHeaderWhenEmpty="True"
                                                                AllowPaging="True"
                                                                PageSize="20"
                                                                BorderStyle="None"
                                                                CellSpacing="2"
                                                                DataKeyNames="CHIDX"
                                                                OnRowDataBound="Master_RowDataBound">

                                                                <EmptyDataTemplate>
                                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                                </EmptyDataTemplate>

                                                                <Columns>

                                                                    <asp:TemplateField HeaderText="#">
                                                                        <ItemTemplate>
                                                                            <div class="panel-heading">
                                                                                <%# (Container.DataItemIndex +1) %>
                                                                                <asp:Label ID="Lab2el5w82" runat="server" Visible="false" Text='<%# Eval("CHIDX")%>'></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>

                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="ชื่อ-นามสกุล">
                                                                        <ItemTemplate>
                                                                            <div class="panel-heading">
                                                                                <asp:Label ID="Labe2l5w38" runat="server" Text='<%# Eval("Child_name")%>'></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="บุตรคนที่">
                                                                        <ItemTemplate>
                                                                            <div class="panel-heading">
                                                                                <asp:Label ID="lbP2a4g2e1" runat="server" Text='<%# Eval("Child_num")%>'></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="text-align: center; vertical-align: middle; border: 1px solid black" colspan="6">
                                                            <h5><b>ประวัติการศึกษา</b></h5>
                                                            <h5><b>The chronicle studies</b></h5>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: left; border: 1px solid black" colspan="6">
                                                            <asp:GridView ID="GvEducation_print"
                                                                runat="server"
                                                                AutoGenerateColumns="false"
                                                                CssClass="table table-striped table-bordered table-hover"
                                                                HeaderStyle-CssClass="table_headCenter"
                                                                HeaderStyle-Height="40px"
                                                                ShowFooter="False"
                                                                ShowHeaderWhenEmpty="True"
                                                                AllowPaging="True"
                                                                PageSize="20"
                                                                BorderStyle="None"
                                                                CellSpacing="2"
                                                                DataKeyNames="EDUIDX"
                                                                OnRowDataBound="Master_RowDataBound">

                                                                <EmptyDataTemplate>
                                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                                </EmptyDataTemplate>

                                                                <Columns>

                                                                    <asp:TemplateField HeaderText="#">
                                                                        <ItemTemplate>
                                                                            <div class="panel-heading">
                                                                                <%# (Container.DataItemIndex +1) %>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="วุฒิการศึกษา">
                                                                        <ItemTemplate>
                                                                            <div class="panel-heading">
                                                                                <asp:Label ID="La4bel5w8" runat="server" Text='<%# Eval("qualification_name")%>'></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="ชื่อสถานศึกษา">
                                                                        <ItemTemplate>
                                                                            <div class="panel-heading">
                                                                                <asp:Label ID="l4bP24ag2e1" runat="server" Text='<%# Eval("Edu_name")%>'></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="สาขาวิชาที่ศึกษา">
                                                                        <ItemTemplate>
                                                                            <div class="panel-heading">
                                                                                <asp:Label ID="l4b4Pag2e1" runat="server" Text='<%# Eval("Edu_branch")%>'></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="ปีที่จบศึกษา">
                                                                        <ItemTemplate>
                                                                            <div class="panel-heading">
                                                                                <asp:Label ID="l4bP3ag2e1" runat="server" Text='<%# Eval("Edu_end")%>'></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                </Columns>

                                                            </asp:GridView>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td style="text-align: center; border: 1px solid black" colspan="6">
                                                            <h5><b>ประวัติการทำงาน</b></h5>
                                                            <h5><b>The Chronicle works</b></h5>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: left; border: 1px solid black" colspan="6">
                                                            <asp:GridView ID="GvPri_print"
                                                                runat="server"
                                                                AutoGenerateColumns="false"
                                                                CssClass="table table-striped table-bordered table-hover"
                                                                HeaderStyle-CssClass="table_headCenter"
                                                                HeaderStyle-Height="40px"
                                                                ShowFooter="False"
                                                                ShowHeaderWhenEmpty="True"
                                                                AllowPaging="True"
                                                                PageSize="10"
                                                                BorderStyle="None"
                                                                CellSpacing="2"
                                                                DataKeyNames="ExperIDX"
                                                                OnRowDataBound="Master_RowDataBound">

                                                                <PagerStyle CssClass="PageCustom" />
                                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                                <EmptyDataTemplate>
                                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                                </EmptyDataTemplate>

                                                                <Columns>

                                                                    <asp:TemplateField HeaderText="#">
                                                                        <ItemTemplate>
                                                                            <div class="panel-heading">
                                                                                <%# (Container.DataItemIndex +1) %>
                                                                                <asp:Label ID="L2ab2del5w8" runat="server" Visible="false" Text='<%# Eval("ExperIDX")%>'></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>

                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="ชื่อบริษัท">
                                                                        <ItemTemplate>
                                                                            <div class="panel-heading">
                                                                                <asp:Label ID="L2ab2el5w8" runat="server" Text='<%# Eval("Pri_Nameorg")%>'></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="ตำแหน่งล่าสุด">
                                                                        <ItemTemplate>
                                                                            <div class="panel-heading">
                                                                                <asp:Label ID="l2bP42ag2e1" runat="server" Text='<%# Eval("Pri_pos")%>'></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="วันที่เริ่มงาน">
                                                                        <ItemTemplate>
                                                                            <div class="panel-heading">
                                                                                <asp:Label ID="lb3wP2a1g2e1" runat="server" Text='<%# Eval("Pri_startdate")%>'></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="วันที่ออกจากงาน">
                                                                        <ItemTemplate>
                                                                            <div class="panel-heading">
                                                                                <asp:Label ID="lebwP32ag2e1" runat="server" Text='<%# Eval("Pri_resigndate")%>'></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="เงินเดือนล่าสุด">
                                                                        <ItemTemplate>
                                                                            <div class="panel-heading">
                                                                                <asp:Label ID="lebPr32ag2e1" runat="server" Text='<%# Eval("Pri_salary")%>'></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="รายละเอียดงาน">
                                                                        <ItemTemplate>
                                                                            <div class="panel-heading">
                                                                                <asp:Label ID="lebP3q2ag2e1" runat="server" Text='<%# Eval("Pri_description")%>'></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                </Columns>
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: center; vertical-align: middle; border: 1px solid black" colspan="6">
                                                            <h5><b>สถานะการขับขี่ยานพาหนะ</b></h5>
                                                            <h5><b>Ability in the driving</b></h5>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: left; border: 1px solid black" colspan="2">
                                                            <asp:Label ID="lbldvcar_idx" runat="server" Text='<%# Eval("dvcar_idx") %>' Visible="false" />
                                                            <h5><b>รถยนต์ส่วนตัว (Driving Car) :</b></h5>
                                                            <asp:CheckBoxList ID="ckl_dvcar" runat="server" RepeatLayout="Table"
                                                                RepeatColumns="2" RepeatDirection="Vertical" Enabled="false">
                                                                <asp:ListItem Value="1"> ขับได้ </asp:ListItem>
                                                                <asp:ListItem Value="2"> ขับไม่ได้ </asp:ListItem>
                                                            </asp:CheckBoxList>
                                                        </td>
                                                        <td style="text-align: left; border: 1px solid black" colspan="1">
                                                            <h5><b>เลขที่ใบขับขี่ : </b><%# Eval("dvlicen_car") %></h5>
                                                            <h5><b>Licens Driving Car</b></h5>
                                                        </td>
                                                        <td style="text-align: left; border: 1px solid black" colspan="2">
                                                            <asp:Label ID="lbldvcarstatus_idx" runat="server" Text='<%# Eval("dvcarstatus_idx") %>' Visible="false" />
                                                            <h5><b>มีรถยนต์ส่วนตัว (Driving Car Status) :</b></h5>
                                                            <asp:CheckBoxList ID="ckl_carstatus" runat="server" RepeatLayout="Table"
                                                                RepeatColumns="2" RepeatDirection="Vertical" Enabled="false">
                                                                <asp:ListItem Value="1"> มี </asp:ListItem>
                                                                <asp:ListItem Value="2"> ไม่มี </asp:ListItem>
                                                            </asp:CheckBoxList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: left; border: 1px solid black" colspan="2">
                                                            <asp:Label ID="lbldvmt_idx" runat="server" Text='<%# Eval("dvmt_idx") %>' Visible="false" />
                                                            <h5><b>รถจักรยานยนต์ส่วนตัว (Driving Motocyle) :</b></h5>
                                                            <asp:CheckBoxList ID="ckl_dvmt" runat="server" RepeatLayout="Table"
                                                                RepeatColumns="2" RepeatDirection="Vertical" Enabled="false">
                                                                <asp:ListItem Value="1"> ขับได้ </asp:ListItem>
                                                                <asp:ListItem Value="2"> ขับไม่ได้ </asp:ListItem>
                                                            </asp:CheckBoxList>
                                                        </td>
                                                        <td style="text-align: left; border: 1px solid black" colspan="1">
                                                            <h5><b>เลขที่ใบขับขี่ : </b><%# Eval("dvlicen_moto") %></h5>
                                                            <h5><b>Licens Driving Motocyle</b></h5>
                                                        </td>
                                                        <td style="text-align: left; border: 1px solid black" colspan="2">
                                                            <asp:Label ID="lbldvmtstatus_idx" runat="server" Text='<%# Eval("dvmtstatus_idx") %>' Visible="false" />
                                                            <h5><b>มีรถจักรยานยนต์ส่วนตัว (Driving Motocyle Status) :</b></h5>
                                                            <asp:CheckBoxList ID="ckl_mtstatus" runat="server" RepeatLayout="Table"
                                                                RepeatColumns="2" RepeatDirection="Vertical" Enabled="false">
                                                                <asp:ListItem Value="1"> มี </asp:ListItem>
                                                                <asp:ListItem Value="2"> ไม่มี </asp:ListItem>
                                                            </asp:CheckBoxList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: left; border: 1px solid black" colspan="2">
                                                            <asp:Label ID="lbldvfork_idx" runat="server" Text='<%# Eval("dvfork_idx") %>' Visible="false" />
                                                            <h5><b>ขับขี่รถโฟล์คลิฟท์ (Driving Forklift) :</b></h5>
                                                            <asp:CheckBoxList ID="ckl_fvfork" runat="server" RepeatLayout="Table"
                                                                RepeatColumns="2" RepeatDirection="Vertical" Enabled="false">
                                                                <asp:ListItem Value="1"> ขับได้ </asp:ListItem>
                                                                <asp:ListItem Value="2"> ขับไม่ได้ </asp:ListItem>
                                                            </asp:CheckBoxList>
                                                        </td>
                                                        <td style="text-align: left; border: 1px solid black" colspan="1">
                                                            <h5><b>เลขที่ใบขับขี่ : </b><%# Eval("dvlicen_fork") %></h5>
                                                            <h5><b>Licens Driving Forklift</b></h5>
                                                        </td>
                                                        <td style="text-align: left; border: 1px solid black" colspan="1">
                                                            <asp:Label ID="lbldvtruck_idx" runat="server" Text='<%# Eval("dvtruck_idx") %>' Visible="false" />
                                                            <h5><b>ขับขี่รถบรรทุก (Driving Truck) :</b></h5>
                                                            <asp:CheckBoxList ID="ckl_dvtruck" runat="server" RepeatLayout="Table"
                                                                RepeatColumns="2" RepeatDirection="Vertical" Enabled="false">
                                                                <asp:ListItem Value="1"> ขับได้ </asp:ListItem>
                                                                <asp:ListItem Value="2"> ขับไม่ได้ </asp:ListItem>
                                                            </asp:CheckBoxList>
                                                        </td>
                                                        <td style="text-align: left; border: 1px solid black" colspan="1">
                                                            <h5><b>เลขที่ใบขับขี่ : </b><%# Eval("dvlicen_truck") %></h5>
                                                            <h5><b>Licens Driving Truck</b></h5>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: center; vertical-align: middle; border: 1px solid black" colspan="6">
                                                            <h5><b>บุคคลที่ไม่ใช่ญาติซึ่งทราบประวัติของท่าน และบริษัทฯ สามารถสอบถามได้</b></h5>
                                                            <h5><b>Persons other than relatives can be contaced</b></h5>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: left; border: 1px solid black" colspan="6">
                                                            <asp:GridView ID="GvReference_print"
                                                                runat="server"
                                                                AutoGenerateColumns="false"
                                                                CssClass="table table-striped table-bordered table-hover"
                                                                HeaderStyle-CssClass="table_headCenter"
                                                                HeaderStyle-Height="40px"
                                                                ShowFooter="False"
                                                                ShowHeaderWhenEmpty="True"
                                                                AllowPaging="True"
                                                                PageSize="5"
                                                                BorderStyle="None"
                                                                CellSpacing="2"
                                                                DataKeyNames="ReIDX"
                                                                OnRowDataBound="Master_RowDataBound">

                                                                <PagerStyle CssClass="PageCustom" />
                                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                                <EmptyDataTemplate>
                                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                                </EmptyDataTemplate>

                                                                <Columns>

                                                                    <asp:TemplateField HeaderText="#">
                                                                        <ItemTemplate>
                                                                            <div class="panel-heading">
                                                                                <%# (Container.DataItemIndex +1) %>
                                                                                <asp:Label ID="L2abs2del5w8" runat="server" Visible="false" Text='<%# Eval("ReIDX")%>'></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>

                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="ชื่อ-นามสกุล">
                                                                        <ItemTemplate>
                                                                            <div class="panel-heading">
                                                                                <asp:Label ID="Labe1" runat="server" Text='<%# Eval("ref_fullname")%>'></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="ตำแหน่ง">
                                                                        <ItemTemplate>
                                                                            <div class="panel-heading">
                                                                                <asp:Label ID="lb3P23ag2e1" runat="server" Text='<%# Eval("ref_position")%>'></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="ความสัมพันธ์">
                                                                        <ItemTemplate>
                                                                            <div class="panel-heading">
                                                                                <asp:Label ID="lb3P2a1g2e1" runat="server" Text='<%# Eval("ref_relation")%>'></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="เบอร์โทรศัพท์">
                                                                        <ItemTemplate>
                                                                            <div class="panel-heading">
                                                                                <asp:Label ID="lb3P2ag2e1" runat="server" Text='<%# Eval("ref_tel")%>'></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: center; vertical-align: middle; border: 1px solid black" colspan="6">
                                                            <h5><b>ประวัติการอบรม</b></h5>
                                                            <h5><b>Traning History</b></h5>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: left; border: 1px solid black" colspan="6">
                                                            <asp:GridView ID="GvTrain_print"
                                                                runat="server"
                                                                AutoGenerateColumns="false"
                                                                CssClass="table table-striped table-bordered table-hover"
                                                                HeaderStyle-CssClass="table_headCenter"
                                                                HeaderStyle-Height="40px"
                                                                ShowFooter="False"
                                                                ShowHeaderWhenEmpty="True"
                                                                AllowPaging="True"
                                                                PageSize="10"
                                                                BorderStyle="None"
                                                                CellSpacing="2"
                                                                DataKeyNames="TNIDX"
                                                                OnRowDataBound="Master_RowDataBound">

                                                                <PagerStyle CssClass="PageCustom" />
                                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                                <EmptyDataTemplate>
                                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                                </EmptyDataTemplate>

                                                                <Columns>

                                                                    <asp:TemplateField HeaderText="#">
                                                                        <ItemTemplate>
                                                                            <div class="panel-heading">
                                                                                <%# (Container.DataItemIndex +1) %>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="หลักสูตรอาบรม">
                                                                        <ItemTemplate>
                                                                            <div class="panel-heading">
                                                                                <asp:Label ID="Labewl5w8" runat="server" Text='<%# Eval("Train_courses")%>'></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="วันที่อบรม">
                                                                        <ItemTemplate>
                                                                            <div class="panel-heading">
                                                                                <asp:Label ID="lbP2aw2g2e1" runat="server" Text='<%# Eval("Train_date")%>'></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="ผลการประเมิน">
                                                                        <ItemTemplate>
                                                                            <div class="panel-heading">
                                                                                <asp:Label ID="lbPagw2e1" runat="server" Text='<%# Eval("Train_assessment")%>'></asp:Label>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                </Columns>
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: center; border: 0px" colspan="6">
                                                            <h5><b>ข้าพเจ้าขอรับรองว่าข้อความข้างต้นทั้งหมดนี้ เป็นความจริงทุกประการ หากข้อความตอนหนึ่งตอนใดไม่ตรงกับความเป็นจริง</b></h5>
                                                            <h5><b>I beg for to assure that , messages all this above , be every points truth , if messages is while , one while,any askew with the fact.</b></h5>

                                                            <h5><b>ข้าพเจ้าขอยอมรับว่า การว่าจ้างที่ตกลงนี้เป็นโมฆะทันที</b></h5>
                                                            <h5><b>I beg for to am regarded as , the grumbie hires that agree this is invalid immediately</b></h5>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="text-align: center; border: 0px" colspan="3">
                                                            <h5><b>ลงชื่อ ...............<%# Eval("prefix_name_th") %><%# Eval("emp_name_th") %>.......................</b></h5>
                                                            <h5><b>Applicant Signature</b></h5>
                                                        </td>
                                                        <td style="text-align: left; border: 0px" colspan="3">
                                                            <h5><b>วันที่ ..........<%--<asp:label id="Label110" runat="server" text='<%# Eval(System.DateTime.Now.ToString() , "{MMMM d, yyyy}") %>' />--%>..............</b></h5>
                                                            <h5><b>Date</b></h5>
                                                        </td>

                                                    </tr>

                                                </table>
                                            </div>
                                        </div>

                                    </ItemTemplate>
                                </asp:FormView>
                            </asp:Panel>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="col-sm-2">
                                        <asp:LinkButton ID="lbnrequest_register" runat="server" Visible="false" CssClass="btn btn-primary" OnCommand="btnCommand" CommandName="btnrequest_employee"><i class="fa fa-user-plus"></i>&nbsp;&nbsp; Request Profile &nbsp;</asp:LinkButton>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-2">
                                        <asp:LinkButton ID="lbntransfer_register" runat="server" Visible="false" CssClass="btn btn-primary" OnCommand="btnCommand" CommandName="btntransfer_employee"><i class="fa fa-mail-forward"></i>&nbsp;&nbsp; Transfer Profile &nbsp;</asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>



                            <asp:Panel ID="box_check_request_more" runat="server" Visible="false">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h3 class="panel-title"><i class="glyphicon glyphicon-list-alt"></i><strong>&nbsp; Request more / ขอรายละเอียดเพิ่มเติม</strong></h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-horizontal" role="form">

                                            <div class="btn btn-primary m-b-5" data-target="#view_video" runat="server" visible="false" id="btn_show_vdo">View Video</div>

                                            <asp:LinkButton runat="server" ID="go_to_examview" Text="ตรวจข้อสอบ " CssClass="btn btn-primary m-b-5" Visible="false"></asp:LinkButton>

                                            <div id="view_video" class="collapse text-center">
                                                <video class="video-responesive" controls="controls" style="max-width: 540px;">
                                                    <source runat="server" id="vdo_src" type="video/mp4" />

                                                    Your browser does not support the video tag.
                                                </video>
                                            </div>
                                            <div id="request_more_wait" runat="server" visible="false">รอผู้สมัครกรอก ข้อมูลเพิ่มเติม</div>

                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="boxviewApprove" runat="server" Visible="false">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h3 class="panel-title"><i class="glyphicon glyphicon-list-alt"></i><strong>&nbsp; Approve / ผลการอนุมัติ</strong></h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-horizontal" role="form">
                                            <asp:FormView ID="FvViewApprove" runat="server" OnDataBound="FvDetail_DataBound" Width="100%">
                                                <InsertItemTemplate>

                                                    <asp:Panel ID="BoxddlApprove" runat="server" Visible="true">
                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label48" runat="server" Text="Approve" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label57" runat="server" Text="ผลอนุมัติ" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <asp:DropDownList ID="ddl_view_Approve" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="0">กรุณาเลือกผลการอนุมัติ....</asp:ListItem>
                                                                    <asp:ListItem Value="2">อนุมัติ</asp:ListItem>
                                                                    <asp:ListItem Value="3">ไม่อนุมัติ</asp:ListItem>
                                                                </asp:DropDownList>

                                                                <asp:RequiredFieldValidator ID="ddl_view_ApproveValidator" ValidationGroup="SaveView" runat="server" Display="None"
                                                                    ControlToValidate="ddl_view_Approve" Font-Size="11"
                                                                    ErrorMessage="Plese Choose Approve" InitialValue="0" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ddl_view_ApproveExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="ddl_view_ApproveValidator" Width="160" />


                                                            </div>
                                                        </div>
                                                    </asp:Panel>
                                                    <asp:Panel runat="server" ID="boxinterview" Visible="false">
                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label17" runat="server" Text="Interview" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label19" runat="server" Text="วันที่สัมภาษณ์" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <div class='input-group date'>
                                                                    <asp:TextBox ID="txt_startdate" CssClass="form-control from-date-datetimepicker" MaxLengh="100%" runat="server" ValidationGroup="SaveAdd"></asp:TextBox>
                                                                    <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                                    <asp:RequiredFieldValidator ID="RqDocCwreate" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                                        ControlToValidate="txt_startdate" Font-Size="11"
                                                                        ErrorMessage="Plese Startdate" />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqDocCwreate" Width="160" />
                                                                </div>
                                                            </div>

                                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label30" runat="server" Text="To" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label44" runat="server" Text="ถึง" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <div class='input-group date'>
                                                                    <asp:TextBox ID="txt_enddate" CssClass="form-control from-date-datetimepicker" MaxLengh="100%" runat="server" ValidationGroup="SaveAdd"></asp:TextBox>
                                                                    <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                                        ControlToValidate="txt_startdate" Font-Size="11"
                                                                        ErrorMessage="Plese Enddate" />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqDocCwreate" Width="160" />
                                                                </div>
                                                            </div>

                                                        </div>

                                                    </asp:Panel>
                                                    <asp:Panel runat="server" ID="boxdateinterview_selected" Visible="false">
                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label123" runat="server" Text="Date Interview." /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label124" runat="server" Text="วันที่สัมภาษณ์" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-3" style="padding-top: 7px;">
                                                                <asp:Label runat="server" ID="date_interview_s_show" Text=""></asp:Label>

                                                            </div>

                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label125" runat="server" Text="To" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label126" runat="server" Text="ถึง" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-3" style="padding-top: 7px;">
                                                                <asp:Label runat="server" ID="date_interview_e_show" Text=""></asp:Label>

                                                            </div>
                                                        </div>
                                                    </asp:Panel>
                                                    <asp:Panel runat="server" ID="boxrequestmore" Visible="false">
                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label111" runat="server" Text="Request More." /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label112" runat="server" Text="สิ่งที่ต้องการเพิ่ม" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <div class="row">
                                                                    <%-- <div class="col-sm-12 checkbox">

                                                                        <asp:CheckBoxList ID="requestvdo" runat="server">

                                                                            <asp:ListItem Value="1"> VDO แนะนำตัว </asp:ListItem>
                                                                            <asp:ListItem Value="1"> แบบทดสอบพื้นฐาน </asp:ListItem>
                                                                            <asp:ListItem Value="1"> ข้อมูลแบบเต็ม </asp:ListItem>
                                                                        </asp:CheckBoxList>
                                                                    </div>--%>

                                                                    <div class="col-sm-12 checkbox">
                                                                        <asp:CheckBox ID="requestvdo" runat="server" Text="VDO แนะนำตัว"></asp:CheckBox>
                                                                    </div>
                                                                    <div class="col-sm-12 checkbox">
                                                                        <asp:CheckBox ID="basicexam" runat="server" Text="ข้อสอบพื้นฐาน"></asp:CheckBox>
                                                                    </div>
                                                                    <div class="col-sm-12 checkbox">
                                                                        <asp:CheckBox ID="fullrequest" runat="server" Text="ประวัติแบบเต็ม"></asp:CheckBox>
                                                                    </div>
                                                                </div>



                                                            </div>

                                                        </div>
                                                    </asp:Panel>

                                                    <asp:Panel ID="pn_chekchasexam" runat="server" Visible="false">
                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label1542" runat="server" Text="Have a specific exam" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label1632" runat="server" Text="มีข้อสอบเฉพาะทาง" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2" style="padding-top: 7px;">
                                                                <asp:Label runat="server" ID="nothas_exam_sp" CssClass="text-danger" Visible="false" Text="ไม่มี"></asp:Label>
                                                                <asp:Label runat="server" ID="has_exam_sp" CssClass="text-success" Visible="false" Text="มี"></asp:Label>
                                                            </div>


                                                        </div>
                                                    </asp:Panel>

                                                    <asp:Panel ID="pnnameapprove" runat="server" Visible="false">


                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label101" runat="server" Text="Organization" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label102" runat="server" Text="บริษัท" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:DropDownList ID="ddlorg_s" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                </asp:DropDownList>
                                                            </div>

                                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label103" runat="server" Text="Department" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label104" runat="server" Text="ฝ่าย" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:DropDownList ID="ddldep_s" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                    <asp:ListItem Value="0">เลือกฝ่าย....</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label105" runat="server" Text="Section" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label106" runat="server" Text="หน่วยงาน" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:DropDownList ID="ddlsec_s" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                    <asp:ListItem Value="0">เลือกหน่วยงาน....</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>

                                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label107" runat="server" Text="Name Interview" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label108" runat="server" Text="ชื่อผู้สัมภาษณ์" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:DropDownList ID="ddlempidx_s" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="0">เลือกชื่อผู้สัมภาษณ์....</asp:ListItem>
                                                                </asp:DropDownList>

                                                                <asp:RequiredFieldValidator ID="ddlempidx_sFieldValidator2" ValidationGroup="SaveView" runat="server" Display="None"
                                                                    ControlToValidate="ddlempidx_s" Font-Size="11"
                                                                    ErrorMessage="โปรดเลือกชื่อผู้สัมภาษณ์ ..." InitialValue="0" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ddlempidx_sValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="ddlempidx_sFieldValidator2" Width="160" />

                                                            </div>
                                                            <div class="col-sm-1">
                                                                <asp:Label ID="Label272" runat="server" ForeColor="Red" Text="***" />
                                                            </div>
                                                        </div>
                                                    </asp:Panel>
                                                    <asp:Panel ID="pn_jobinterview" runat="server" Visible="false">
                                                        <%--ตำแหน่งผู้สมัคร--%>
                                                        <hr />
                                                        <div class="form-group">
                                                            <label class="col-sm-12">
                                                                <h2 class="panel-title">
                                                                    <span class="h01">Job Interviewer</span><br>
                                                                    <small><b>
                                                                        <span>ผู้สัมภาษณ์งาน</span></b></small>
                                                                </h2>
                                                            </label>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label127" runat="server" Text="Organization" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label128" runat="server" Text="บริษัท" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:DropDownList ID="ddl_orgidx_recruit" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                </asp:DropDownList>
                                                            </div>

                                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label129" runat="server" Text="Department" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label130" runat="server" Text="ฝ่าย" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:DropDownList ID="ddl_rdeptidx_recruit" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                    <asp:ListItem Value="0">เลือกฝ่าย....</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label131" runat="server" Text="Section" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label132" runat="server" Text="หน่วยงาน" /></b></small>
                                                                </h2>
                                                            </label>

                                                            <div class="col-sm-2">
                                                                <asp:DropDownList ID="ddl_rsecidx_recruit" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                    <asp:ListItem Value="0">เลือกหน่วยงาน....</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                                <h2 class="panel-title">
                                                                    <asp:Label ID="Label139" runat="server" Text="Position" /><br />
                                                                    <small><b>
                                                                        <asp:Label ID="Label140" runat="server" Text="ตำแหน่งงาน" /></b></small>
                                                                </h2>
                                                            </label>
                                                            <div class="col-sm-2">
                                                                <asp:DropDownList ID="ddl_rposidx_recruit" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                    <asp:ListItem Value="0">เลือกตำแหน่งงาน....</asp:ListItem>
                                                                </asp:DropDownList>

                                                                <asp:RequiredFieldValidator ID="ddl_rposidx_recruit_RequiredFieldValidator2" ValidationGroup="SaveView" runat="server" Display="None"
                                                                    ControlToValidate="ddl_rposidx_recruit" Font-Size="11"
                                                                    ErrorMessage="โปรดเลือกตำแหน่งงาน ..." InitialValue="0" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ddl_rposidx_recruit_ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="ddl_rposidx_recruit_RequiredFieldValidator2" Width="160" />

                                                            </div>
                                                            <div class="col-sm-1">
                                                                <asp:Label ID="Label153" runat="server" ForeColor="Red" Text="***" />
                                                            </div>


                                                        </div>
                                                    </asp:Panel>

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label70" runat="server" Text="Comment" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label109" runat="server" Text="หมายเหตุ" /></b></small>
                                                            </h2>
                                                        </label>
                                                        <div class="col-sm-9">
                                                            <asp:TextBox ID="tbDocRemark" runat="server" CssClass="form-control" TextMode="multiline" Rows="5"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-sm-4 col-sm-offset-2">
                                                            <asp:LinkButton ID="btn_Approve_request" CssClass="btn btn-success" runat="server" CommandName="btn_Approve_request" OnCommand="btnCommand" Visible="false" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save"><i class="fa fa-save"> บันทึกข้อมูล Request</i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnApprove" CssClass="btn btn-success" runat="server" CommandName="btn_Approve" OnCommand="btnCommand" ValidationGroup="SaveView" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save"><i class="fa fa-save"> บันทึกข้อมูล</i></asp:LinkButton>
                                                            <asp:LinkButton ID="ViewCancel" CssClass="btn btn-default" runat="server" ValidationGroup="Cancel" CommandName="BtnBack" OnCommand="btnCommand" CommandArgument="1" title="Cancel"><i class="fa fa-times"> ยกเลิก</i></asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </InsertItemTemplate>
                                            </asp:FormView>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>

                            <asp:Panel ID="box_tranfer" runat="server" Visible="false">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h3 class="panel-title"><i class="glyphicon glyphicon-list-alt"></i><strong>&nbsp; Transfer data employee / โอนย้ายข้อมูลเข้าระบบพนักงานหลัก</strong></h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-horizontal" role="form">
                                            <asp:FormView ID="FvTranfer" runat="server" OnDataBound="FvDetail_DataBound" Width="100%">
                                                <InsertItemTemplate>

                                                    <div class="form-group">
                                                        <label class="col-sm-2 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label48" runat="server" Text="Start Date" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label57" runat="server" Text="วันที่เริ่มงาน" /></b></small>
                                                            </h2>
                                                        </label>
                                                        <div class="col-sm-2">
                                                            <div class='input-group date'>
                                                                <asp:TextBox ID="txt_startdate" CssClass="form-control from-date-datetimepicker" MaxLengh="100%" runat="server" ValidationGroup="SaveAdd"></asp:TextBox>
                                                                <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                                <asp:RequiredFieldValidator ID="RqDocCwreate" ValidationGroup="transfer" runat="server" Display="None"
                                                                    ControlToValidate="txt_startdate" Font-Size="11"
                                                                    ErrorMessage="Plese startdate" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqDocCwreate" Width="160" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-6">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="lbl_reorganization_en_cen" runat="server" CssClass="h01" Text="Re Organization" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="lbl_reorganization_th_cen" runat="server" Text="ข้อมูลเพิ่มเติม" /></b></small>
                                                            </h2>
                                                        </label>
                                                        <label class="col-md-6 text-right">
                                                        </label>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="lbl_EmployeeGroupTypeEN_cen" runat="server" Text="EmployeeGroupType" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="lbl_EmployeeGroupTypeTH_cen" runat="server" Text="ประเภทกลุ่มพนักงาน" /></b></small>
                                                            </h2>
                                                        </label>

                                                        <div class="col-md-2">
                                                            <asp:DropDownList ID="ddl_target_cen" runat="server" CssClass="form-control">
                                                                <asp:ListItem Value="0">ประเภทกลุ่มพนักงาน....</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>


                                                        <label class="col-md-2 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Labe4l228" runat="server" Text="Location" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label3230" runat="server" Text="ประจำสำนักงาน" /></b></small>
                                                            </h2>
                                                        </label>

                                                        <div class="col-md-2">
                                                            <asp:DropDownList ID="ddllocation" runat="server" CssClass="form-control">
                                                                <asp:ListItem Value="0">เลือกสำนักงาน....</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label ">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label20" runat="server" Text="EmployeeType" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label21" runat="server" Text="ประเภทพนักงาน" /></b></small>
                                                            </h2>
                                                        </label>

                                                        <div class="col-md-2">
                                                            <asp:DropDownList ID="ddlemptype" runat="server" CssClass="form-control">
                                                                <asp:ListItem Value="0">ประเภทพนักงาน....</asp:ListItem>
                                                                <asp:ListItem Value="1">รายวัน</asp:ListItem>
                                                                <asp:ListItem Value="2">รายเดือน</asp:ListItem>
                                                            </asp:DropDownList>

                                                            <asp:RequiredFieldValidator ID="Requir094edFieldValidator1" ValidationGroup="transfer" runat="server" Display="None"
                                                                ControlToValidate="ddlemptype" Font-Size="11"
                                                                ErrorMessage="ประเภทพนักงาน ...."
                                                                ValidationExpression="ประเภทพนักงาน ...." InitialValue="0" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCal09loutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requir094edFieldValidator1" Width="160" />
                                                        </div>
                                                        <%--<div class="col-md-2">--%>
                                                            <div class="col-md-2">
                                                                <asp:CheckBox ID="chk_Affiliation_add" CssClass="checkbox pull-right" Text="สังกัด" AutoPostBack="true" OnCheckedChanged="chkSelectedIndexChanged" runat="server" />
                                                                <asp:HyperLink ID="setchk_Affiliation_add" runat="server"></asp:HyperLink>
                                                            </div>
                                                        <div class="col-md-2">

                                                    
                                                            <asp:Panel ID="box_ddlAffiliation_add" runat="server" Visible="false">
                                                                <%--<div class="col-md-8">--%>
                                                                    <asp:DropDownList ID="ddlAffiliation" runat="server" CssClass="form-control">
                                                                        <asp:ListItem Value="0">เลือก....</asp:ListItem>
                                                                        <asp:ListItem Value="1">MOU-JWS</asp:ListItem>
                                                                        <asp:ListItem Value="2">MOU-UDL</asp:ListItem>
                                                                        <asp:ListItem Value="3">MOU-FASAI</asp:ListItem>
                                                                        <asp:ListItem Value="4">MOU-API</asp:ListItem>
                                                                        <asp:ListItem Value="5">UDL</asp:ListItem>
                                                                        <asp:ListItem Value="6">FASAI</asp:ListItem>
                                                                        <asp:ListItem Value="7">API</asp:ListItem>
                                                                        <asp:ListItem Value="8">PN</asp:ListItem>
                                                                        <asp:ListItem Value="9">TKN</asp:ListItem>
                                                                        <asp:ListItem Value="12">SSCK</asp:ListItem>
                                                                        <asp:ListItem Value="17">TMK</asp:ListItem>
                                                                        <asp:ListItem Value="20">JSR</asp:ListItem>
                                                                        <asp:ListItem Value="21">JWS</asp:ListItem>
                                                                        <asp:ListItem Value="26">MOU-TACT</asp:ListItem>
                                                                        <asp:ListItem Value="27">MOU-NSR</asp:ListItem>
                                                                        <asp:ListItem Value="28">MOU-SNJ</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                <%--</div>--%>
                                                            </asp:Panel>
                                                                </div>
                                                        <%--</div>--%>
                                                    </div>
                                                    <div class="form-group">




                                                        <label class="col-md-2 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="lbl_CostCenterEN_cen" runat="server" Text="CostCenter" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="lbl_CostCenterTH_cen" runat="server" Text="ค่าใช้จ่าย" /></b></small>
                                                            </h2>
                                                        </label>

                                                        <div class="col-md-2">
                                                            <asp:DropDownList ID="ddl_costcenter_cen" runat="server" CssClass="form-control">
                                                            </asp:DropDownList>
                                                        </div>

                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-12">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="lbl_PositionEN_cen" runat="server" CssClass="h01" Text="Re Main Position" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="lbl_PositionTH_cen" runat="server" Text="ตำแหน่งงานหลัก" /></b></small>
                                                            </h2>
                                                        </label>
                                                    </div>

                                                  <%--  <div class="panel-body">

                                                        <div class="col-md-12">
                                                            <div class="form-horizontal">--%>

                                                                <div class="form-group">
                                                                    <label class="col-md-2 control-label">Organization <small>องค์กร</small><span class="text-danger"> *</span></label>
                                                                    <div class="col-md-2">
                                                                        <asp:DropDownList ID="ddlOrganization" runat="server"
                                                                            CssClass="form-control"
                                                                            OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                                            AutoPostBack="true">
                                                                        </asp:DropDownList>

                                                                    </div>
                                                                    <label class="col-md-2 control-label">Workgroup <small>กลุ่มงาน</small><span class="text-danger"> *</span></label>
                                                                    <div class="col-md-2">
                                                                        <asp:DropDownList ID="ddlWorkGroup" runat="server"
                                                                            CssClass="form-control"
                                                                            OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                                            AutoPostBack="true">
                                                                                      <asp:ListItem Value="0">--- เลือกกลุ่มงาน ---</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-md-2 control-label">Linework <small>สายงาน</small><span class="text-danger"> *</span></label>
                                                                    <div class="col-md-2">
                                                                        <asp:DropDownList ID="ddlLineWork" runat="server"
                                                                            CssClass="form-control"
                                                                            OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                                            AutoPostBack="true">
                                                                             <asp:ListItem Value="0">--- เลือกสายงาน ---</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <label class="col-md-2 control-label">Division <small>ฝ่าย</small><span class="text-danger"> *</span></label>
                                                                    <div class="col-md-2">
                                                                        <asp:DropDownList ID="ddlDepartment" runat="server"
                                                                            CssClass="form-control"
                                                                            OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                                            AutoPostBack="true">
                                                                            <asp:ListItem Value="0">--- เลือกฝ่าย ---</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-md-2 control-label">Department/ Section <small>แผนก / ส่วน</small><span class="text-danger"> *</span></label>
                                                                    <div class="col-md-2">
                                                                        <asp:DropDownList ID="ddlSection" runat="server" CssClass="form-control"
                                                                            OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                                            AutoPostBack="true">
                                                                               <asp:ListItem Value="0">--- เลือกแผนก ---</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <label class="col-md-2 control-label">Position <small>ตำแหน่ง</small><span class="text-danger"> *</span></label>
                                                                    <div class="col-md-2">
                                                                        <asp:DropDownList ID="ddlPosition" runat="server"
                                                                            CssClass="form-control">
                                                                            <asp:ListItem Value="0">--- เลือกตำแหน่ง ---</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                             <%--   </div>


                                                            </div>
                                                        </div>--%>
                                                        <!-- <label class="col-md-1"></label> -->

                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-sm-10 col-sm-offset-2">
                                                            <asp:LinkButton ID="btn_Approve_tranfer" CssClass="btn btn-success" runat="server" CommandName="btn_Approve_tranfer" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการโอนย้ายรายการนี้ใช่หรือไม่ ?')" title="Save" ValidationGroup="transfer"><i class="fa fa-save"> โอนย้ายข้อมูล</i></asp:LinkButton>
                                                            <asp:LinkButton ID="ViewCancel" CssClass="btn btn-default" runat="server" ValidationGroup="Cancel" CommandName="BtnBack" OnCommand="btnCommand" CommandArgument="1" title="Cancel"><i class="fa fa-times"> ยกเลิก</i></asp:LinkButton>
                                                        </div>
                                                    </div>

                                                </InsertItemTemplate>
                                            </asp:FormView>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>

                            <asp:Panel ID="box_log" runat="server" Visible="true">
                                <div class="panel panel-success">
                                    <div class="panel-heading">
                                        <h3 class="panel-title"><i class="glyphicon glyphicon-list-alt"></i><strong>&nbsp; Log / ประวัติการดำเนินการ</strong></h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-horizontal" role="form">
                                            <asp:Repeater ID="rptLog" runat="server">
                                                <HeaderTemplate>
                                                    <div class="form-group">
                                                        <label class="col-sm-2" style="text-align: center;"><small>วัน / เวลา</small></label>
                                                        <label class="col-sm-3" style="text-align: left;"><small>ผู้ดำเนินการ</small></label>
                                                        <label class="col-sm-2" style="text-align: left;"><small>ดำเนินการ</small></label>
                                                        <label class="col-sm-2" style="text-align: left;"><small>ผลการดำเนินการ</small></label>
                                                        <label class="col-sm-2" style="text-align: left;"><small>ความคิดเห็น</small></label>
                                                    </div>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <div class="row">
                                                        <div class="col-sm-2">
                                                            <span><small><%#Eval("emp_createdate")%></small></span>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <span><small><%# Eval("emp_name_th") %>(<%# Eval("actor_name") %>)</small></span>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <span><small><%# Eval("node_name") %></small></span>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <span><small><%# Eval("status_name") %></small></span>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <span><small><%# Eval("comment_approve") %></small></span>
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>



                            <%--<div class="col-sm-1">
                                <asp:LinkButton ID="btnupdate" runat="server" CssClass="btn btn-default" OnCommand="btnCommand" CommandName="btnUpdate" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" ValidationGroup="FormEdit">Save &nbsp;</asp:LinkButton>
                            </div>--%>
                        </asp:Panel>
                    </div>
                </div>
            </div>
        </asp:View>

        <asp:View ID="ViewScreen" runat="server">
            <div class="col-lg-12">
                <asp:Label ID="Label99" runat="server"></asp:Label>
                <asp:HyperLink runat="server" ID="HyperLink1" />
                <%-------------- BoxSearch Start--------------%>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-list-alt"></i><strong>&nbsp; ค้นหาข้อมูล</strong></h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-horizontal" id="Div2" runat="server">
                            <asp:FormView ID="Fv_Search_screen_Index" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                                <InsertItemTemplate>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label100" runat="server" Text="หมวดงานที่ต้องการสมัคร" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:DropDownList ID="ddlgroup_screen" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                <asp:ListItem Value="0">เลือกกลุ่มงานที่ต้องการ ...</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label144" runat="server" Text="ตำแหน่งงานที่สนใจ" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:DropDownList ID="ddlposition_screen" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                <asp:ListItem Value="0">เลือกตำแหน่งงานที่ต้องการ ...</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label39" runat="server" Text="ประสบการณ์การทำงาน" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:DropDownList ID="ddlexperiences_screen" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                <asp:ListItem Value="0">ระบุ ...</asp:ListItem>
                                                <asp:ListItem Value="1">0-1 ปี</asp:ListItem>
                                                <asp:ListItem Value="2">1-3 ปี</asp:ListItem>
                                                <asp:ListItem Value="3">3-5 ปี</asp:ListItem>
                                                <asp:ListItem Value="4">5-10 ปี</asp:ListItem>
                                                <asp:ListItem Value="5">มากกว่า 10 ปี</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label122" runat="server" Text="เงินเดือนที่ต้องการ" /></b></small>
                                            </h2>
                                        </label>
                                        <div class="col-sm-2">
                                            <asp:DropDownList ID="ddlsalary_screen" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                <asp:ListItem Value="0">ระบุ ...</asp:ListItem>
                                                <asp:ListItem Value="1">ไม่เกิน 10,000 บาท</asp:ListItem>
                                                <asp:ListItem Value="2">10,001 - 15,000 บาท</asp:ListItem>
                                                <asp:ListItem Value="3">15,001 - 20,000 บาท</asp:ListItem>
                                                <asp:ListItem Value="4">20,001 - 30,000 บาท</asp:ListItem>
                                                <asp:ListItem Value="5">30,001 - 50,000 บาท</asp:ListItem>
                                                <asp:ListItem Value="6">50,001 - 100,000 บาท</asp:ListItem>
                                                <asp:ListItem Value="7">มากกว่า 100,000 บาท</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group">

                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label19" runat="server" Text="วันที่สามารถเริ่มงานได้" /></b></small>
                                            </h2>
                                        </label>
                                        <div class="col-sm-2">
                                            <asp:DropDownList ID="ddlstartdate_in" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">เลือกวันที่พร้อมเริ่มงาน.....</asp:ListItem>
                                                <asp:ListItem Value="1">ไม่แน่ใจ</asp:ListItem>
                                                <asp:ListItem Value="2">ทันที</asp:ListItem>
                                                <asp:ListItem Value="3">7 วัน</asp:ListItem>
                                                <asp:ListItem Value="4">15 วัน</asp:ListItem>
                                                <asp:ListItem Value="5">1 เดือน</asp:ListItem>
                                                <asp:ListItem Value="6">2 เดือน</asp:ListItem>
                                                <asp:ListItem Value="7">3 เดือน</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label110" runat="server" Text="กลุ่มงานที่สนใจ" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:TextBox ID="txtgroup_screen" CssClass="form-control" runat="server" ValidationGroup="SaveAdd"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label224" runat="server" Text="ชื่อ(ไทย)" /></b></small>
                                            </h2>
                                        </label>
                                        <div class="col-sm-2">
                                            <asp:TextBox ID="txt_name_th" CssClass="form-control" runat="server" ValidationGroup="InsertData"></asp:TextBox>
                                        </div>

                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label4" runat="server" Text="นามสกุล" /></b></small>
                                            </h2>
                                        </label>
                                        <div class="col-sm-2">
                                            <asp:TextBox ID="txt_surname_th" CssClass="form-control" runat="server" ValidationGroup="InsertData"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label210" runat="server" Text="อายุ" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:DropDownList ID="ddlage_screen" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">ไม่จำกัดอายุ ...</asp:ListItem>
                                                <asp:ListItem Value="1">น้อยกว่า 20 ปี</asp:ListItem>
                                                <asp:ListItem Value="2">ระหว่าง 20 ถึง 25 ปี</asp:ListItem>
                                                <asp:ListItem Value="3">ระหว่าง 25 ถึง 30 ปี</asp:ListItem>
                                                <asp:ListItem Value="4">ระหว่าง 30 ถึง 35 ปี</asp:ListItem>
                                                <asp:ListItem Value="5">มากกว่า 35 ปี</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label211" runat="server" Text="เพศ" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:DropDownList ID="ddlsex_screen" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">ไม่จำกัดเพส ...</asp:ListItem>
                                                <asp:ListItem Value="1">ชาย</asp:ListItem>
                                                <asp:ListItem Value="2">หญิง</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label202" runat="server" Text="ระดับการศึกษา" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:DropDownList ID="ddleducation_screen" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">เลือกวุฒิการศึกษา....</asp:ListItem>
                                                <asp:ListItem Value="7">ไม่มีวุฒิการศึกษา</asp:ListItem>
                                                <asp:ListItem Value="1">ต่ำกว่ามัธยมต้น</asp:ListItem>
                                                <asp:ListItem Value="2">มัธยมต้น</asp:ListItem>
                                                <asp:ListItem Value="3">มัธยมปลาย</asp:ListItem>
                                                <asp:ListItem Value="8">ปวช</asp:ListItem>
                                                <asp:ListItem Value="9">ปวส</asp:ListItem>
                                                <asp:ListItem Value="4">ป.ตรี</asp:ListItem>
                                                <asp:ListItem Value="5">ป.โท</asp:ListItem>
                                                <asp:ListItem Value="6">ป.เอก</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label203" runat="server" Text="สาขาวิชาที่จบ" /></b></small>
                                            </h2>
                                        </label>
                                        <div class="col-sm-2">
                                            <asp:TextBox ID="txtbranch_screen" CssClass="form-control" runat="server" ValidationGroup="SaveAdd"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label205" runat="server" Text="สถาบันการศึกษา" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:TextBox ID="txtunivercity_screen" CssClass="form-control" runat="server" ValidationGroup="SaveAdd"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label207" runat="server" Text="ความสามารถในการขับขี่" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-3">
                                            <div class="checkbox checkbox-primary">
                                                <asp:CheckBox ID="ChkdriverAll" Text="เลือกทั้งหมด" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelectedIndexChanged"
                                                    CellPadding="10"
                                                    CellSpacing="10"
                                                    RepeatColumns="2"
                                                    RepeatDirection="Vertical"
                                                    RepeatLayout="Table"
                                                    TextAlign="Right"
                                                    Width="100%" />
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="checkbox checkbox-primary">
                                                    <asp:CheckBoxList ID="Chkdriver"
                                                        runat="server"
                                                        CellPadding="10"
                                                        CellSpacing="10"
                                                        RepeatColumns="1"
                                                        RepeatDirection="Horizontal"
                                                        RepeatLayout="Table"
                                                        TextAlign="Right"
                                                        Width="100%">
                                                    </asp:CheckBoxList>
                                                </div>
                                            </div>
                                        </div>

                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label199" runat="server" Text="ประเภทงานที่ต้องการ" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-3">
                                            <div class="checkbox checkbox-primary">
                                                <asp:CheckBox ID="chkAll" Text="เลือกทั้งหมด" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelectedIndexChanged"
                                                    CellPadding="10"
                                                    CellSpacing="10"
                                                    RepeatColumns="2"
                                                    RepeatDirection="Vertical"
                                                    RepeatLayout="Table"
                                                    TextAlign="Right"
                                                    Width="100%" />
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="checkbox checkbox-primary">
                                                    <asp:CheckBoxList ID="YrChkBox"
                                                        runat="server"
                                                        CellPadding="10"
                                                        CellSpacing="10"
                                                        RepeatColumns="1"
                                                        RepeatDirection="Horizontal"
                                                        RepeatLayout="Table"
                                                        TextAlign="Right"
                                                        Width="100%">
                                                    </asp:CheckBoxList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-2">
                                            <asp:Button ID="btnsearch_screen" class="btn btn-primary" ValidationGroup="Search" runat="server" Text="ค้นหา" data-original-title="search" data-toggle="tooltip" CommandName="btn_search_screen" OnCommand="btnCommand" />
                                            <asp:Button ID="btnclearsearch_index" class="btn btn-primary" runat="server" Text="Clear" data-original-title="search" data-toggle="tooltip" CommandName="Cmdsearch_clear" OnCommand="btnCommand" />
                                        </div>
                                    </div>

                                </InsertItemTemplate>
                            </asp:FormView>
                        </div>
                    </div>
                </div>
                <%-------------- BoxSearch End--------------%>
                <div class="panel-heading">
                    <h3 class="panel-title"><font color="blue"><strong><asp:Label ID="lblsum_list_screen_index" runat="server" /></strong></font></h3>
                </div>

                <asp:GridView ID="GvEmployee_Screen"
                    runat="server"
                    AutoGenerateColumns="false"
                    CssClass="table table-striped table-bordered table-hover table-responsive"
                    HeaderStyle-CssClass="table_headCenter"
                    HeaderStyle-Height="40px"
                    AllowPaging="True"
                    PageSize="20"
                    DataKeyNames="emp_idx"
                    OnPageIndexChanging="Master_PageIndexChanging"
                    OnRowDataBound="Master_RowDataBound">

                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                    <EmptyDataTemplate>
                        <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                    </EmptyDataTemplate>

                    <Columns>
                        <asp:TemplateField HeaderText="รูปภาพ" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="1%">
                            <ItemTemplate>

                                <asp:Label ID="lblidentitycard" runat="server" Visible="false" Text='<%# Eval("identity_card") %>'></asp:Label>
                                <asp:Image ID="img_profile" runat="server" Style="width: 100px; height: 100px" class="img-thumbnail"></asp:Image>
                                <asp:Panel ID="ImagProfile_default" runat="server" HorizontalAlign="Center">
                                    <img class="media-object img-thumbnail pull-letf" src='<%=ResolveUrl("~/uploadfiles/profile_picture/default/user-default.png")%>' width="100px" height="100px" style="margin-right: 0px; margin-left: 10px; margin-top: -5px;">
                                </asp:Panel>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="วันที่สมัคร" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="4%">
                            <ItemTemplate>
                                <small>
                                    <b>
                                        <asp:Label ID="fsse" runat="server" Text="วันที่สมัคร : " /></b><asp:Label ID="lbldAsseta" runat="server" Text='<%# Eval("emp_createdate") %>'></asp:Label>
                                    </p>                                    
                                    <b>
                                        <asp:Label ID="Labesl53" runat="server" Text="พร้อมเริ่มงาน : " /></b><asp:Label ID="Lafbel59" runat="server" Text='<%# Eval("WName") %>'></asp:Label>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ชื่อ-นามสกุล" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="8%">
                            <ItemTemplate>
                                <small>
                                    <b>
                                        <asp:Label ID="lblemp_name_th_" runat="server" Text="ชื่อ-สกุล : " /></b><asp:Label ID="lblemp_name_th" runat="server" Text='<%# Eval("emp_name_th") %>'></asp:Label>
                                    </p>                                     
                                    <b>
                                        <asp:Label ID="Label53" runat="server" Text="เพศ : " /></b><asp:Label ID="Label59" runat="server" Text='<%# Eval("sex_name_th") %>'></asp:Label>
                                    / 
                                        <b>
                                            <asp:Label ID="Label47" runat="server" Text="อายุ : " /></b><asp:Label ID="Label49" runat="server" Text='<%# Eval("emp_birthday") %>'></asp:Label>
                                    ปี
                                    </p>
                                    <b>
                                        <asp:Label ID="Label187" runat="server" Text="บัตรประชาชน : " /></b><asp:Label ID="Label188" runat="server" Text='<%# Eval("identity_card") %>'></asp:Label>
                                    </p>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="เพิ่มเติม" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="8%">
                            <ItemTemplate>
                                <small>
                                    <b>
                                        <asp:Label ID="Label185" runat="server" Text="เงินเดือน : " /></b><asp:Label ID="Label186" runat="server" Text='<%# Eval("salary") %>'></asp:Label>
                                    บาท
                                    </p>
                                    <b>
                                        <asp:Label ID="lblemp_mobile2_no_" runat="server" Text="ประเภทงาน : " /></b><asp:Label ID="lblemp_mobile_no" runat="server" Text='<%# Eval("JName") %>'></asp:Label>
                                    </p>                                    
                                    <b>
                                        <asp:Label ID="lblemp_e23mail_" runat="server" Text="ตำแหน่งงาน : " /></b><asp:Label ID="lblem32p_email" runat="server" Text='<%# Eval("P_Name") %>'></asp:Label>
                                    </p>
                                    <b>
                                        <asp:Label ID="Label64" runat="server" Text="กลุ่มตำแหน่งงาน : " /></b><asp:Label ID="Label184" runat="server" Text='<%# Eval("PosGroupNameTH") %>'></asp:Label>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ติดต่อ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="3%">
                            <ItemTemplate>
                                <small>
                                    <b>
                                        <asp:Label ID="lblemp_mobile_no_" runat="server" Text="เบอร์ติดต่อ : " /></b><asp:Label ID="lblemp_m32obile_no" runat="server" Text='<%# Eval("emp_mobile_no") %>'></asp:Label>
                                    </p>                             
                                    <b>
                                        <asp:Label ID="lblemp_email_" runat="server" Text="Email : " /></b><asp:Label ID="lblemp_email" runat="server" Text='<%# Eval("emp_email") %>'></asp:Label>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="จัดการ" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="2%">
                            <ItemTemplate>
                                <small>
                                    <asp:LinkButton ID="btnedit_emp" CssClass="btn btn-primary" runat="server" CommandName="btnedit_emp" OnCommand="btnCommand" data-toggle="tooltip" title="รายละเอียด" CommandArgument='<%# Eval("identity_card") %>'><i class="fa fa-edit"></i></asp:LinkButton>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>
            </div>
        </asp:View>

        <asp:View ID="ViewRequest" runat="server">
            <div class="col-lg-12">
                <asp:Label ID="Label23" runat="server"></asp:Label>
                <asp:HyperLink runat="server" ID="HyperLink3" />

                <asp:GridView ID="GvRequest"
                    runat="server"
                    AutoGenerateColumns="false"
                    CssClass="table table-striped table-bordered table-hover table-responsive"
                    HeaderStyle-CssClass="table_headCenter"
                    HeaderStyle-Height="40px"
                    AllowPaging="True"
                    PageSize="20"
                    DataKeyNames="emp_idx"
                    OnPageIndexChanging="Master_PageIndexChanging"
                    OnRowDataBound="Master_RowDataBound">

                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                    <EmptyDataTemplate>
                        <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                    </EmptyDataTemplate>

                    <Columns>

                        <%--<asp:TemplateField HeaderText="รูปรถ" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                           <ItemTemplate>

                               <asp:HyperLink runat="server" ID="btnViewFileCar" CssClass="pull-letf" data-toggle="tooltip" title="View" data-original-title="" Target="_blank">
                                   <i class="fa fa-file-text-o"></i> ดูรายละเอียด</asp:HyperLink>

                           </ItemTemplate>
                       </asp:TemplateField>--%>

                        <asp:TemplateField HeaderText="รูปภาพ" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="1%">
                            <ItemTemplate>

                                <asp:Label ID="lblidentitycard" runat="server" Visible="false" Text='<%# Eval("identity_card") %>'></asp:Label>
                                <asp:Image ID="img_profile" runat="server" Style="width: 100px; height: 100px" class="img-thumbnail"></asp:Image>
                                <asp:Panel ID="ImagProfile_default" runat="server" HorizontalAlign="Center">
                                    <img class="media-object img-thumbnail pull-letf" src='<%=ResolveUrl("~/uploadfiles/profile_picture/default/user-default.png")%>' width="100px" height="100px" style="margin-right: 0px; margin-left: 10px; margin-top: -5px;">
                                </asp:Panel>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="วันที่สมัคร" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="4%">
                            <ItemTemplate>
                                <small>
                                    <b>
                                        <asp:Label ID="fsse" runat="server" Text="วันที่สมัคร : " /></b><asp:Label ID="lbldAsseta" runat="server" Text='<%# Eval("emp_createdate") %>'></asp:Label>
                                    </p>                                    
                                    <b>
                                        <asp:Label ID="Labesl53" runat="server" Text="พร้อมเริ่มงาน : " /></b><asp:Label ID="Lafbel59" runat="server" Text='<%# Eval("WName") %>'></asp:Label>
                                    </p>                                    
                                    <b>
                                        <asp:Label ID="Labssdel30" runat="server" Text="สถานะ : " /></b><asp:Label ID="Labrdael44" runat="server" Text='<%# Eval("empstatus") %>'></asp:Label>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ชื่อ-นามสกุล" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="8%">
                            <ItemTemplate>
                                <small>
                                    <b>
                                        <asp:Label ID="lblemp_name_th_" runat="server" Text="ชื่อ-สกุล : " /></b><asp:Label ID="lblemp_name_th" runat="server" Text='<%# Eval("emp_name_th") %>'></asp:Label>
                                    </p>                                     
                                    <b>
                                        <asp:Label ID="Label53" runat="server" Text="เพศ : " /></b><asp:Label ID="Label59" runat="server" Text='<%# Eval("sex_name_th") %>'></asp:Label>
                                    / 
                                        <b>
                                            <asp:Label ID="Label47" runat="server" Text="อายุ : " /></b><asp:Label ID="Label49" runat="server" Text='<%# Eval("emp_birthday") %>'></asp:Label>
                                    ปี
                                    </p>
                                    <b>
                                        <asp:Label ID="Label187" runat="server" Text="บัตรประชาชน : " /></b><asp:Label ID="Label188" runat="server" Text='<%# Eval("identity_card") %>'></asp:Label>
                                    </p>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="เพิ่มเติม" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="8%">
                            <ItemTemplate>
                                <small>
                                    <b>
                                        <asp:Label ID="Label185" runat="server" Text="เงินเดือน : " /></b><asp:Label ID="Label186" runat="server" Text='<%# Eval("salary") %>'></asp:Label>
                                    บาท
                                    </p>
                                    <b>
                                        <asp:Label ID="lblemp_mobile2_no_" runat="server" Text="ประเภทงาน : " /></b><asp:Label ID="lblemp_mobile_no" runat="server" Text='<%# Eval("JName") %>'></asp:Label>
                                    </p>                                    
                                    <b>
                                        <asp:Label ID="lblemp_e23mail_" runat="server" Text="ตำแหน่งงาน : " /></b><asp:Label ID="lblem32p_email" runat="server" Text='<%# Eval("P_Name") %>'></asp:Label>
                                    </p>
                                    <b>
                                        <asp:Label ID="Label64" runat="server" Text="กลุ่มตำแหน่งงาน : " /></b><asp:Label ID="Label184" runat="server" Text='<%# Eval("PosGroupNameTH") %>'></asp:Label>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ติดต่อ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="3%">
                            <ItemTemplate>
                                <small>
                                    <b>
                                        <asp:Label ID="lblemp_mobile_no_" runat="server" Text="เบอร์ติดต่อ : " /></b><asp:Label ID="lblemp_m32obile_no" runat="server" Text='<%# Eval("emp_mobile_no") %>'></asp:Label>
                                    </p>                             
                                    <b>
                                        <asp:Label ID="lblemp_email_" runat="server" Text="Email : " /></b><asp:Label ID="lblemp_email" runat="server" Text='<%# Eval("emp_email") %>'></asp:Label>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="2%">
                            <ItemTemplate>
                                <small>
                                    <span><%# Eval("status_name") %>&nbsp;<%# Eval("node_name") %>&nbsp;โดย&nbsp;<%# Eval("actor_name") %></span>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="จัดการ" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="2%">
                            <ItemTemplate>
                                <small>
                                    <asp:LinkButton ID="btnedit_emp" CssClass="btn btn-primary" runat="server" CommandName="btnedit_emp" OnCommand="btnCommand" data-toggle="tooltip" title="รายละเอียด" CommandArgument='<%# Eval("identity_card") %>'><i class="fa fa-edit"></i></asp:LinkButton>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>

                <%--<asp:GridView ID="GvRequest"
                    DataKeyNames="u0_didx"
                    runat="server"
                    AutoGenerateColumns="false"
                    CssClass="table table-striped table-bordered table-hover"
                    HeaderStyle-CssClass="table_headCenter"
                    HeaderStyle-Height="40px"
                    AllowPaging="True"
                    PageSize="10"
                    BorderStyle="None"
                    CellSpacing="2"
                    OnPageIndexChanging="Master_PageIndexChanging">

                    <PagerStyle CssClass="PageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">Data Cannot Be Found</div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="ID" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <div class="panel-heading">
                                    <asp:Label ID="lbDeviceIDX" runat="server" Text='<%# Eval("u0_code") %>' />
                                    <asp:Label ID="lbl_u0_didx_approve" runat="server" Text='<%# Eval("u0_didx")%>' Visible="false"></asp:Label>
                                    <asp:Label ID="lbl_u0_relation_didx_new" runat="server" Text='<%# Eval("u0_relation_didx")%>' Visible="false"></asp:Label>
                                    <asp:Label ID="lbl_doc_status_new" runat="server" Text='<%# Eval("doc_status")%>' Visible="false"></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Asset Code" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <div class="panel-heading">
                                    <asp:Label ID="lblAsseta" runat="server" Text='<%# Eval("u0_acc") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Type" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <div class="panel-heading">
                                    <asp:Label ID="lb_m0_tdidx" runat="server" Text='<%# Eval("m0_tdidx") %>' Visible="false" />
                                    <asp:Label ID="lblAssetaq" runat="server" Text='<%# Eval("name_m0_typedevice") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Band" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <div class="panel-heading">
                                    <asp:Label ID="lblAssetsaq" runat="server" Text='<%# Eval("name_m0_band") %>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Management">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnView_approve_transfer" CssClass="btn btn-primary" runat="server" CommandName="btnView_approve_transfer" OnCommand="btnCommand" CommandArgument='<%#Eval("u0_didx") + ";" + Eval("u0_referent") %>' data-toggle="tooltip" title="รายละเอียด"><i class="fa fa-edit"></i></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>--%>
            </div>
        </asp:View>

        <asp:View ID="ViewApprove" runat="server">
            <div class="col-lg-12">
                <asp:Label ID="Label17" runat="server"></asp:Label>
                <asp:HyperLink runat="server" ID="HyperLink2" />
                <div class="panel-heading">
                    <h3 class="panel-title"><font color="blue"><strong><asp:Label ID="sum_approve" runat="server" /></strong></font></h3>
                </div>
                <asp:GridView ID="GvApprove"
                    DataKeyNames="emp_idx"
                    runat="server"
                    AutoGenerateColumns="false"
                    CssClass="table table-striped table-bordered table-hover"
                    HeaderStyle-CssClass="table_headCenter"
                    HeaderStyle-Height="40px"
                    AllowPaging="True"
                    PageSize="10"
                    BorderStyle="None"
                    CellSpacing="2"
                    OnPageIndexChanging="Master_PageIndexChanging"
                    OnRowDataBound="Master_RowDataBound">

                    <PagerStyle CssClass="PageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">Data Cannot Be Found</div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="รูปภาพ" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="1%">
                            <ItemTemplate>
                                <asp:Label ID="lblemp_idx1" runat="server" Visible="false" Text='<%# Eval("emp_idx") %>'></asp:Label>

                                <asp:Label ID="lblidentitycard" runat="server" Visible="false" Text='<%# Eval("identity_card") %>'></asp:Label>
                                <asp:Image ID="img_profile" runat="server" Style="width: 100px; height: 100px" class="img-thumbnail"></asp:Image>
                                <asp:Panel ID="ImagProfile_default" runat="server" HorizontalAlign="Center">
                                    <img class="media-object img-thumbnail pull-letf" src='<%=ResolveUrl("~/uploadfiles/profile_picture/default/user-default.png")%>' width="100px" height="100px" style="margin-right: 0px; margin-left: 10px; margin-top: -5px;">
                                </asp:Panel>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="วันที่สมัคร" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="4%">
                            <ItemTemplate>
                                <small>
                                    <b>
                                        <asp:Label ID="fsse" runat="server" Text="วันที่สมัคร : " /></b><asp:Label ID="lbldAsseta" runat="server" Text='<%# Eval("emp_createdate") %>'></asp:Label>
                                    </p>                                    
                                    <b>
                                        <asp:Label ID="Labesl53" runat="server" Text="พร้อมเริ่มงาน : " /></b><asp:Label ID="Lafbel59" runat="server" Text='<%# Eval("WName") %>'></asp:Label>
                                    </p>                                    
                                    <b>
                                        <asp:Label ID="Labsdel30" runat="server" Text="สถานะ : " /></b><asp:Label ID="Labrael44" runat="server" Text='<%# Eval("empstatus") %>'></asp:Label>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="วันที่นัดหมายสัมภาษณ์" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="4%">
                            <ItemTemplate>
                                <small>
                                    <b>
                                        <asp:Label ID="fwsse" runat="server" Text="วันที่-เวลาเริ่ม : " /></b><asp:Label ID="lbldAssweta" runat="server" Text='<%# Eval("startdate_search") %>'></asp:Label>
                                    </p>                                    
                                    <b>
                                        <asp:Label ID="Labersl53" runat="server" Text="วันที่-เวลาจบ : " /></b><asp:Label ID="Laefbel59" runat="server" Text='<%# Eval("enddate_search") %>'></asp:Label>
                                    </p>                                    
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ชื่อ-นามสกุล" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="8%">
                            <ItemTemplate>
                                <small>
                                    <b>
                                        <asp:Label ID="lblemp_name_th_" runat="server" Text="ชื่อ-สกุล : " /></b><asp:Label ID="lblemp_name_th" runat="server" Text='<%# Eval("emp_name_th") %>'></asp:Label>
                                    </p>                                     
                                    <b>
                                        <asp:Label ID="Label53" runat="server" Text="เพศ : " /></b><asp:Label ID="Label59" runat="server" Text='<%# Eval("sex_name_th") %>'></asp:Label>
                                    / 
                                        <b>
                                            <asp:Label ID="Label47" runat="server" Text="อายุ : " /></b><asp:Label ID="Label49" runat="server" Text='<%# Eval("emp_birthday") %>'></asp:Label>
                                    ปี
                                    </p>
                                    <b>
                                        <asp:Label ID="Label187" runat="server" Text="บัตรประชาชน : " /></b><asp:Label ID="Label188" runat="server" Text='<%# Eval("identity_card") %>'></asp:Label>
                                    </p>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="เพิ่มเติม" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="8%">
                            <ItemTemplate>
                                <small>
                                    <b>
                                        <asp:Label ID="Label185" runat="server" Text="เงินเดือน : " /></b><asp:Label ID="Label186" runat="server" Text='<%# Eval("salary") %>'></asp:Label>
                                    บาท
                                    </p>
                                    <b>
                                        <asp:Label ID="lblemp_mobile2_no_" runat="server" Text="ประเภทงาน : " /></b><asp:Label ID="lblemp_mobile_no" runat="server" Text='<%# Eval("JName") %>'></asp:Label>
                                    </p>                                    
                                    <b>
                                        <asp:Label ID="lblemp_e23mail_" runat="server" Text="ตำแหน่งงาน : " /></b><asp:Label ID="lblem32p_email" runat="server" Text='<%# Eval("P_Name") %>'></asp:Label>
                                    </p>
                                    <b>
                                        <asp:Label ID="Label64" runat="server" Text="กลุ่มตำแหน่งงาน : " /></b><asp:Label ID="Label184" runat="server" Text='<%# Eval("PosGroupNameTH") %>'></asp:Label>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="แบบทดสอบ" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="8%">
                            <ItemTemplate>
                                <small>
                                    <asp:GridView ID="GvTopic_approve" runat="server"
                                        AutoGenerateColumns="false"
                                        CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                        HeaderStyle-CssClass="info small"
                                        HeaderStyle-Height="40px"
                                        AllowPaging="false"
                                        DataKeyNames="m0_toidx"
                                        OnRowDataBound="Master_RowDataBound">

                                        <PagerStyle CssClass="pageCustom" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                        <EmptyDataTemplate>
                                            <div style="text-align: center">Data Cannot Be Found</div>
                                        </EmptyDataTemplate>
                                        <Columns>

                                            <asp:TemplateField ItemStyle-CssClass="text-left" HeaderText="ชุดคำถาม" HeaderStyle-CssClass="text-center" ItemStyle-Width="30%" Visible="true" ItemStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:Label ID="lbltopic_name" runat="server" Text='<%# Eval("topic_name") %>' />
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>


                                            <asp:TemplateField ItemStyle-CssClass="text-left" HeaderText="วันที่ดำเนินการ" HeaderStyle-CssClass="text-center" ItemStyle-Width="5%" Visible="true" ItemStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:Label ID="lbldatedoing" runat="server" Text='<%# Eval("datedoing") %>' />
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>


                                            <asp:TemplateField ItemStyle-CssClass="text-left" HeaderText="รวมคะแนน" HeaderStyle-CssClass="text-center" ItemStyle-Width="30%" Visible="true" ItemStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <b>
                                                            <asp:Label ID="lblemp_mobile2_no_" runat="server" Text="คะแนนรวม : " /></b><asp:Label ID="lblemp_mobile_no" runat="server" Text='<%# Eval("total_score") %>'></asp:Label>
                                                        </p>                                    
                                    <b>
                                        <asp:Label ID="lblemp_e23mail_" runat="server" Text="ปรนัย : " /></b><asp:Label ID="lblem32p_email" runat="server" Text='<%# Eval("score_choice") %>'></asp:Label>
                                                        </p>
                                    <b>
                                        <asp:Label ID="Label64" runat="server" Text="อัตนัย : " /></b><asp:Label ID="Label184" runat="server" Text='<%# Eval("score_comment") %>'></asp:Label>
                                                        </p>
                                                        
                                                          <br />
                                                        <strong>
                                                            <asp:Label ID="Label11" runat="server"> สถานะแบบทดสอบ : </asp:Label></strong>
                                                        <b>
                                                            <asp:Label ID="lbldetermine" runat="server" Text='<%# Eval("determine") %>'></asp:Label></b>
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="สถานะตัดสินใจ" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center" ItemStyle-Width="30%" Visible="true" ItemStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <b>
                                                            <asp:Label ID="lblnode_decision" Visible="false" runat="server" Text='<%# Eval("node_decision") %>'></asp:Label>
                                                            <asp:Label ID="lblm0_tqidx" Visible="false" runat="server" Text='<%# Eval("m0_tqidx") %>'></asp:Label>

                                                            <asp:Label ID="lblstatusinterview" runat="server" Text='<%# Eval("statusinterview") %>' /></b>
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>

                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>


                        <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="2%">
                            <ItemTemplate>
                                <small>
                                    <span><%# Eval("status_name") %>&nbsp;<%# Eval("node_name") %>&nbsp;โดย&nbsp;<%# Eval("actor_name") %></span>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="จัดการ" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="2%">
                            <ItemTemplate>
                                <small>
                                    <asp:LinkButton ID="btnedit_emp" CssClass="btn btn-primary" runat="server" CommandName="btnedit_emp" OnCommand="btnCommand" data-toggle="tooltip" title="รายละเอียด" CommandArgument='<%# Eval("identity_card") %>'><i class="fa fa-edit"></i></asp:LinkButton>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </asp:View>

        <asp:View ID="ViewReport" runat="server">
            <div class="col-lg-12">
                <asp:Label ID="adf" runat="server"></asp:Label>
                <%-------------- BoxSearch Start--------------%>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-list-alt"></i><strong>&nbsp; ค้นหาข้อมูล Report</strong></h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-horizontal" id="Div1" runat="server">
                            <asp:FormView ID="Fv_Search_Emp_Report" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                                <InsertItemTemplate>

                                    <asp:Panel ID="BoxSearch_ddlDate" runat="server" Visible="false">
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">
                                                <h2 class="panel-title">
                                                    <asp:Label ID="Label273" runat="server" Text="DateType" /><br />
                                                    <small><b>
                                                        <asp:Label ID="Label274" runat="server" Text="ประเภทวันที่" /></b></small>
                                                </h2>
                                            </label>

                                            <div class="col-sm-2">
                                                <asp:DropDownList ID="ddldatetype" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                    <asp:ListItem Value="0">ประเภทวันที่....</asp:ListItem>
                                                    <asp:ListItem Value="1">วันเริ่มงาน</asp:ListItem>
                                                    <asp:ListItem Value="2">วันลาออก</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </asp:Panel>

                                    <asp:Panel ID="BoxSearch_Date" runat="server" Visible="false">
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">
                                                <h2 class="panel-title">
                                                    <asp:Label ID="Label269" runat="server" Text="Start Date" /><br />
                                                    <small><b>
                                                        <asp:Label ID="Label270" runat="server" Text="วันที่" /></b></small>
                                                </h2>
                                            </label>
                                            <div class="col-sm-2">
                                                <div class='input-group date'>
                                                    <asp:TextBox ID="txtdatestart" runat="server" CssClass="form-control from-date-datepicker" AutoComplete="off" MaxLengh="100%"></asp:TextBox>
                                                    <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                </div>
                                            </div>

                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                <h2 class="panel-title">
                                                    <asp:Label ID="Label271" runat="server" Text="End Date" /><br />
                                                    <small><b>
                                                        <asp:Label ID="Label272" runat="server" Text="ถึงวันที่" /></b></small>
                                                </h2>
                                            </label>
                                            <div class="col-sm-2">
                                                <div class='input-group date'>
                                                    <asp:TextBox ID="txtdateend" AutoComplete="off" MaxLength="10" CssClass="form-control from-date-datepicker" runat="server"></asp:TextBox>
                                                    <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label2" runat="server" Text="Employee In" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label6" runat="server" Text="วันที่สามารถเริ่มงานได้" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:DropDownList ID="ddlstartdate_in_rp" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">เลือกวันที่พร้อมเริ่มงาน.....</asp:ListItem>
                                                <asp:ListItem Value="1">ไม่แน่ใจ</asp:ListItem>
                                                <asp:ListItem Value="2">ทันที</asp:ListItem>
                                                <asp:ListItem Value="3">7 วัน</asp:ListItem>
                                                <asp:ListItem Value="4">15 วัน</asp:ListItem>
                                                <asp:ListItem Value="5">1 เดือน</asp:ListItem>
                                                <asp:ListItem Value="6">2 เดือน</asp:ListItem>
                                                <asp:ListItem Value="7">3 เดือน</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label143" runat="server" Text="Job Category" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label144" runat="server" Text="ประเภทงานที่ต้องการ" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:DropDownList ID="ddltypecategory_rp" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">เลือกประเภทงานที่ต้องการ...</asp:ListItem>
                                                <asp:ListItem Value="1">ประจำ</asp:ListItem>
                                                <asp:ListItem Value="2">Part-time</asp:ListItem>
                                                <asp:ListItem Value="3">Freelance</asp:ListItem>
                                                <asp:ListItem Value="4">ฝึกงาน</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label101" runat="server" Text="Position" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label102" runat="server" Text="ตำแหน่งงานที่เปิดรับ" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:DropDownList ID="ddlpositionfocus_rp" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">เลือกตำแหน่งงาน....</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label103" runat="server" Text="Position Group" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label104" runat="server" Text="กลุ่มประเภทงานที่ต้องการ" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-2">
                                            <asp:DropDownList ID="ddlgroup_rp" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">เลือกประเภทกลุ่มงาน ....</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-2">
                                            <asp:Button ID="btnsearch_report" class="btn btn-primary" ValidationGroup="Search" runat="server" Text="ค้นหา" data-original-title="search" data-toggle="tooltip" CommandName="btn_search_report" OnCommand="btnCommand" />
                                            <asp:Button ID="btnclearsearch_index" class="btn btn-primary" runat="server" Text="Clear" data-original-title="search" data-toggle="tooltip" CommandName="Cmdsearch_clear" OnCommand="btnCommand" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-4">
                                            <asp:UpdatePanel ID="UpdatePanel21" runat="server">
                                                <ContentTemplate>
                                                    <asp:Button ID="btnexport" CssClass="btn btn-success" runat="server" data-toggle="tooltip" Text="Export Excel (ข้อมูลทั่วไป)" CommandName="btnExport" OnCommand="btnCommand" title="Export"></asp:Button>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:PostBackTrigger ControlID="btnexport" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>

                                </InsertItemTemplate>
                            </asp:FormView>
                        </div>
                    </div>
                </div>
                <%-------------- BoxSearch End--------------%>

                <div id="BoxExport" runat="server" visible="true">
                    <asp:GridView ID="GvExport" runat="server" AutoGenerateColumns="false" Visible="true" AllowPaging="false" CssClass="table table-striped table-bordered table-hover table-responsive">
                        <PagerStyle CssClass="PageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                        <EmptyDataTemplate>
                            <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                        </EmptyDataTemplate>
                        <Columns>

                            <asp:TemplateField HeaderText="วันที่สมัคร">
                                <HeaderTemplate>
                                    <asp:Label ID="lblCol00" runat="server" Text="วันที่สมัคร" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lbl_emp_code" runat="server" Text='<%# Eval("emp_createdate") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="พร้อมเริ่มงาน">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_emp_type_name" runat="server" Text='<%# Eval("WName") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="สถานะ">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_sex_name_th" runat="server" Text='<%# Eval("empstatus") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ชื่อ-สกุล">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_emp_name_th" runat="server" Text='<%# Eval("emp_name_th") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="เพศ">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_emp_name_en" runat="server" Text='<%# Eval("sex_name_th") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="อายุ">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_emp_nickname_th" runat="server" Text='<%# Eval("emp_birthday") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="บัตรประชาชน">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_emp_nicknam2e_th" runat="server" Text='<%# Eval("identity_card") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="เงินเดือน">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_emp_nickn3ame_th" runat="server" Text='<%# Eval("salary") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ประเภทงาน">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_emp3_nickn3ame_th" runat="server" Text='<%# Eval("JName") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ตำแหน่งงาน">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_emp4_nickn3ame_th" runat="server" Text='<%# Eval("P_Name") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="กลุ่มตำแหน่งงาน">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_emp4_nicken3ame_th" runat="server" Text='<%# Eval("PosGroupNameTH") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="เบอร์ติดต่อ">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_emp4_nickeen3ame_th" runat="server" Text='<%# Eval("emp_mobile_no") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="อีเมล์">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_3emp4_nicken3ame_th" runat="server" Text='<%# Eval("emp_email") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>


                        </Columns>
                    </asp:GridView>
                </div>

                <div class="panel-heading">
                    <h3 class="panel-title"><font color="blue"><strong><asp:Label ID="lblsum_list" runat="server" /></strong></font></h3>
                </div>

                <asp:GridView ID="GvEmployee_Report"
                    runat="server"
                    AutoGenerateColumns="false"
                    CssClass="table table-striped table-bordered table-hover table-responsive"
                    HeaderStyle-CssClass="table_headCenter"
                    HeaderStyle-Height="40px"
                    AllowPaging="True"
                    PageSize="20"
                    DataKeyNames="emp_idx"
                    OnPageIndexChanging="Master_PageIndexChanging"
                    OnRowDataBound="Master_RowDataBound">

                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                    <EmptyDataTemplate>
                        <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                    </EmptyDataTemplate>

                    <Columns>

                        <asp:TemplateField HeaderText="รูปภาพ" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="1%">
                            <ItemTemplate>

                                <asp:Label ID="lblidentitycard" runat="server" Visible="false" Text='<%# Eval("identity_card") %>'></asp:Label>
                                <asp:Image ID="img_profile" runat="server" Style="width: 100px; height: 100px" class="img-thumbnail"></asp:Image>
                                <asp:Panel ID="ImagProfile_default" runat="server" HorizontalAlign="Center">
                                    <img class="media-object img-thumbnail pull-letf" src='<%=ResolveUrl("~/uploadfiles/profile_picture/default/user-default.png")%>' width="100px" height="100px" style="margin-right: 0px; margin-left: 10px; margin-top: -5px;">
                                </asp:Panel>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="วันที่สมัคร" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="4%">
                            <ItemTemplate>
                                <small>
                                    <b>
                                        <asp:Label ID="fsse" runat="server" Text="วันที่สมัคร : " /></b><asp:Label ID="lbldAsseta" runat="server" Text='<%# Eval("emp_createdate") %>'></asp:Label>
                                    </p>                                    
                                    <b>
                                        <asp:Label ID="Labesl53" runat="server" Text="พร้อมเริ่มงาน : " /></b><asp:Label ID="Lafbel59" runat="server" Text='<%# Eval("WName") %>'></asp:Label>
                                    </p>                                    
                                    <b>
                                        <asp:Label ID="Labsel30" runat="server" Text="สถานะ : " /></b><asp:Label ID="Labrel44" runat="server" Text='<%# Eval("empstatus") %>'></asp:Label>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ชื่อ-นามสกุล" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="8%">
                            <ItemTemplate>
                                <small>
                                    <b>
                                        <asp:Label ID="lblemp_name_th_" runat="server" Text="ชื่อ-สกุล : " /></b><asp:Label ID="lblemp_name_th" runat="server" Text='<%# Eval("emp_name_th") %>'></asp:Label>
                                    </p>                                     
                                    <b>
                                        <asp:Label ID="Label53" runat="server" Text="เพศ : " /></b><asp:Label ID="Label59" runat="server" Text='<%# Eval("sex_name_th") %>'></asp:Label>
                                    / 
                                        <b>
                                            <asp:Label ID="Label47" runat="server" Text="อายุ : " /></b><asp:Label ID="Label49" runat="server" Text='<%# Eval("emp_birthday") %>'></asp:Label>
                                    ปี
                                    </p>
                                    <b>
                                        <asp:Label ID="Label187" runat="server" Text="บัตรประชาชน : " /></b><asp:Label ID="Label188" runat="server" Text='<%# Eval("identity_card") %>'></asp:Label>
                                    </p>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="เพิ่มเติม" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="8%">
                            <ItemTemplate>
                                <small>
                                    <b>
                                        <asp:Label ID="Label185" runat="server" Text="เงินเดือน : " /></b><asp:Label ID="Label186" runat="server" Text='<%# Eval("salary") %>'></asp:Label>
                                    บาท
                                    </p>
                                    <b>
                                        <asp:Label ID="lblemp_mobile2_no_" runat="server" Text="ประเภทงาน : " /></b><asp:Label ID="lblemp_mobile_no" runat="server" Text='<%# Eval("JName") %>'></asp:Label>
                                    </p>                                    
                                    <b>
                                        <asp:Label ID="lblemp_e23mail_" runat="server" Text="ตำแหน่งงาน : " /></b><asp:Label ID="lblem32p_email" runat="server" Text='<%# Eval("P_Name") %>'></asp:Label>
                                    </p>
                                    <b>
                                        <asp:Label ID="Label64" runat="server" Text="กลุ่มตำแหน่งงาน : " /></b><asp:Label ID="Label184" runat="server" Text='<%# Eval("PosGroupNameTH") %>'></asp:Label>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ติดต่อ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="3%">
                            <ItemTemplate>
                                <small>
                                    <b>
                                        <asp:Label ID="lblemp_mobile_no_" runat="server" Text="เบอร์ติดต่อ : " /></b><asp:Label ID="lblemp_m32obile_no" runat="server" Text='<%# Eval("emp_mobile_no") %>'></asp:Label>
                                    </p>                             
                                    <b>
                                        <asp:Label ID="lblemp_email_" runat="server" Text="Email : " /></b><asp:Label ID="lblemp_email" runat="server" Text='<%# Eval("emp_email") %>'></asp:Label>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>
            </div>

        </asp:View>

    </asp:MultiView>
    <script type="text/javascript">

        $(function () {
            $('.start-date').datetimepicker({
                format: 'YYYY-MM-DD HH:mm',
                useCurrent: false
            });
            $('.end-date').datetimepicker({
                format: 'YYYY-MM-DD HH:mm',
                useCurrent: false
            });
            $(".start-date").on("dp.change", function (e) {
                $('.end-date').data("DateTimePicker").minDate(e.date);
            });
            $(".end-date").on("dp.change", function (e) {
                $('.start-date').data("DateTimePicker").maxDate(e.date);
            });
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.start-date').datetimepicker({
                    format: 'YYYY-MM-DD HH:mm',
                    useCurrent: false
                });
                $('.end-date').datetimepicker({
                    format: 'YYYY-MM-DD HH:mm',
                    useCurrent: false
                });
                $(".start-date").on("dp.change", function (e) {
                    $('.end-date').data("DateTimePicker").minDate(e.date);
                });
                $(".end-date").on("dp.change", function (e) {
                    $('.start-date').data("DateTimePicker").maxDate(e.date);
                });
            });
        });
    </script>
</asp:Content>

