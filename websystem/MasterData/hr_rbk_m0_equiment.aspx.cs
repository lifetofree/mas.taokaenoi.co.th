﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_hr_rbk_m0_equiment : System.Web.UI.Page
{

    #region Connect
    function_tool _funcTool = new function_tool();

    data_employee _dataEmployee = new data_employee();
    data_roombooking _data_roombooking = new data_roombooking();

    data_qa_cims _data_qa_cims = new data_qa_cims();


    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    //-- employee --//

    //-- roombooking --//
    static string _urlSetRbkm0Equiment = _serviceUrl + ConfigurationManager.AppSettings["urlSetRbkm0Equiment"];
    static string _urlGetRbkm0Equiment = _serviceUrl + ConfigurationManager.AppSettings["urlGetRbkm0Equiment"];
    static string _urlSetRbkm0EquimentDel = _serviceUrl + ConfigurationManager.AppSettings["urlSetRbkm0EquimentDel"];


    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;
    bool _flag_qmr = false;
    #endregion Connect

    #region Page_Init
    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());
        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];
        //check permission
        //foreach (int item in rdept_qmr)
        //{
        //    if (_dataEmployee.employee_list[0].rdept_idx == item)
        //    {
        //        _flag_qmr = true;
        //        break;
        //    }
        //}
    }
    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //ViewState["empIDX"] = Session["emp_idx"];
            SelectEquimentRoom();

        }
    }
    #endregion Page Load

    #region initPage
    protected void initPage()
    {
        MvSystem.SetActiveView(ViewIndex);
        SelectEquimentRoom();
    }
    #endregion

    #region getEmployeeProfile
    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }
    #endregion

    #region setformdata
    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    #endregion setformdata

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        switch (cmdName)
        {
            case "cmdAddEquimentRoom":

                GvEquimentRoom.EditIndex = -1;
                SelectEquimentRoom();
                btnAddEquimentRoom.Visible = false;
                setFormData(FvInsert, FormViewMode.Insert, null);
                FvInsert.Visible = true;  
                
                break;

            case "CmdSave":
                InsertEquimentRoom();
                SelectEquimentRoom();
                btnAddEquimentRoom.Visible = true;
                FvInsert.Visible = false;
                break;

            case "CmdCancel":
                btnAddEquimentRoom.Visible = true;
                FvInsert.Visible = false;

                break;

            case "CmdDelete":

                int equiment_idx_del = int.Parse(cmdArg);

                data_roombooking data_m0_equiment_del = new data_roombooking();
                rbk_m0_equiment_detail m0_equiment_del = new rbk_m0_equiment_detail();
                data_m0_equiment_del.rbk_m0_equiment_list = new rbk_m0_equiment_detail[1];

                m0_equiment_del.equiment_idx = equiment_idx_del;
                m0_equiment_del.cemp_idx = _emp_idx;

                data_m0_equiment_del.rbk_m0_equiment_list[0] = m0_equiment_del;
                data_m0_equiment_del = callServicePostRoomBooking(_urlSetRbkm0EquimentDel, data_m0_equiment_del);

                SelectEquimentRoom();
                //Gv_select_unit.Visible = false;
                btnAddEquimentRoom.Visible = true;
                FvInsert.Visible = false;
                break;
        }
    }
    #endregion btnCommand

    #region Custom Functions
    protected void InsertEquimentRoom()
    {

        TextBox txtequiment_name = (TextBox)FvInsert.FindControl("txtequiment_name");
        DropDownList ddlEquimentName = (DropDownList)FvInsert.FindControl("ddlEquimentName");

        data_roombooking data_m0_equiment = new data_roombooking();
        rbk_m0_equiment_detail m0_equiment_insert = new rbk_m0_equiment_detail();
        data_m0_equiment.rbk_m0_equiment_list = new rbk_m0_equiment_detail[1];

        m0_equiment_insert.equiment_idx = 0;
        m0_equiment_insert.equiment_name = txtequiment_name.Text;
        m0_equiment_insert.cemp_idx = _emp_idx;
        m0_equiment_insert.equiment_status = int.Parse(ddlEquimentName.SelectedValue);

        data_m0_equiment.rbk_m0_equiment_list[0] = m0_equiment_insert;
        data_m0_equiment = callServicePostRoomBooking(_urlSetRbkm0Equiment, data_m0_equiment);

        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Unit_b.return_code));
        if (data_m0_equiment.return_code == 1)
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);

        }
        else
        {
            txtequiment_name.Text = String.Empty;
            //tex_place_code.Text = String.Empty;
        }
    }

    protected void SelectEquimentRoom()
    {

        data_roombooking data_m0_equiment_detail = new data_roombooking();
        rbk_m0_equiment_detail m0_equiment_detail = new rbk_m0_equiment_detail();
        data_m0_equiment_detail.rbk_m0_equiment_list = new rbk_m0_equiment_detail[1];

        m0_equiment_detail.condition = 0;

        data_m0_equiment_detail.rbk_m0_equiment_list[0] = m0_equiment_detail;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        data_m0_equiment_detail = callServicePostRoomBooking(_urlGetRbkm0Equiment, data_m0_equiment_detail);

        setGridData(GvEquimentRoom, data_m0_equiment_detail.rbk_m0_equiment_list);
        //Gv_select_place.DataSource = m0_place_detail.qa_cims_m0_place_list;
        //Gv_select_place.DataBind();
    }
    #endregion Custom Functions

    #region setGridData
    private void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    #endregion

    #region Masterbound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvEquimentRoom":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbl_equiment_status = (Label)e.Row.Cells[3].FindControl("lbl_equiment_status");
                    Label equiment_statusOnline = (Label)e.Row.Cells[3].FindControl("equiment_statusOnline");
                    Label equiment_statusOffline = (Label)e.Row.Cells[3].FindControl("equiment_statusOffline");

                    ViewState["vs_equiment_status"] = lbl_equiment_status.Text;


                    if (ViewState["vs_equiment_status"].ToString() == "1")
                    {
                        equiment_statusOnline.Visible = true;
                    }
                    else if (ViewState["vs_equiment_status"].ToString() == "0")
                    {
                        equiment_statusOffline.Visible = true;
                    }

                }


                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                  
                    btnAddEquimentRoom.Visible = true;
                    FvInsert.Visible = false;

                }



                break;


        }
    }
    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvEquimentRoom":
                GvEquimentRoom.EditIndex = e.NewEditIndex;
                SelectEquimentRoom();
                break;

        }
    }
    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

        var GvName = (GridView)sender;
        //Label5.Text = GvName.ID;
        switch (GvName.ID)
        {
            case "GvEquimentRoom":

                var txt_equiment_idx_edit = (TextBox)GvEquimentRoom.Rows[e.RowIndex].FindControl("txt_equiment_idx_edit");
                var txt_equiment_name_edit = (TextBox)GvEquimentRoom.Rows[e.RowIndex].FindControl("txt_equiment_name_edit");
                var ddlequiment_status_edit = (DropDownList)GvEquimentRoom.Rows[e.RowIndex].FindControl("ddlequiment_status_edit");


                GvEquimentRoom.EditIndex = -1;


                data_roombooking data_m0_equiment_edit = new data_roombooking();
                rbk_m0_equiment_detail m0_equiment_edit = new rbk_m0_equiment_detail();
                data_m0_equiment_edit.rbk_m0_equiment_list = new rbk_m0_equiment_detail[1];

                m0_equiment_edit.equiment_idx = int.Parse(txt_equiment_idx_edit.Text);
                m0_equiment_edit.equiment_name = txt_equiment_name_edit.Text;
                m0_equiment_edit.cemp_idx = _emp_idx;
                m0_equiment_edit.equiment_status = int.Parse(ddlequiment_status_edit.SelectedValue);

                data_m0_equiment_edit.rbk_m0_equiment_list[0] = m0_equiment_edit;
                data_m0_equiment_edit = callServicePostRoomBooking(_urlSetRbkm0Equiment, data_m0_equiment_edit);

                if (data_m0_equiment_edit.return_code == 1)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ข้อมูลซ้ำ กรุณาแก้ไขใหม่อีกครั้ง!!!');", true);
                    SelectEquimentRoom();
                }
                else
                {
                    SelectEquimentRoom();
                }



                break;

        }
    }
    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        //Label5.Text = GvName.ID;
        switch (GvName.ID)
        {
            case "GvEquimentRoom":
                GvEquimentRoom.EditIndex = -1;
                SelectEquimentRoom();
               
                btnAddEquimentRoom.Visible = true;
                FvInsert.Visible = false;
                break;

        }
    }
    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvEquimentRoom":
                GvEquimentRoom.PageIndex = e.NewPageIndex;
                GvEquimentRoom.DataBind();
                SelectEquimentRoom();
                break;

        }
    }
    #endregion

    #region callService 
    protected data_qa_cims callServicePostMasterQACIMS(string _cmdUrl, data_qa_cims _data_qa_cims)
    {
        _localJson = _funcTool.convertObjectToJson(_data_qa_cims);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        _data_qa_cims = (data_qa_cims)_funcTool.convertJsonToObject(typeof(data_qa_cims), _localJson);


        return _data_qa_cims;
    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_roombooking callServicePostRoomBooking(string _cmdUrl, data_roombooking _data_roombooking)
    {
        _localJson = _funcTool.convertObjectToJson(_data_roombooking);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        _data_roombooking = (data_roombooking)_funcTool.convertJsonToObject(typeof(data_roombooking), _localJson);


        return _data_roombooking;
    }
    #endregion callService Functions
}
