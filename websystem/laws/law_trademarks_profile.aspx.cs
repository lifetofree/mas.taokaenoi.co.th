﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class websystem_laws_law_trademarks_profile : System.Web.UI.Page
{
    #region initial function/data
    function_tool _funcTool = new function_tool();
    service_execute serviceexcute = new service_execute();
    data_employee _dataEmployee = new data_employee();
    data_law _data_law = new data_law();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];

    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    static string urlGetAll = _serviceUrl + ConfigurationManager.AppSettings["urlGetAll"];
    //-- employee --//

    static string _urlGetDetailTrademarksProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetDetailTrademarksProfile"];
    static string _urlGetLawContryDetail = _serviceUrl + ConfigurationManager.AppSettings["urlGetLawContryDetail"];
    static string _urlGetSearchDetailTrademark = _serviceUrl + ConfigurationManager.AppSettings["urlGetSearchDetailTrademark"];


    //path file
    static string _path_file_law_trademarks_profile = ConfigurationManager.AppSettings["path_file_law_trademarks_profile"];

    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;


    #endregion initial function/data

    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());

        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];

        ViewState["org_permission"] = _dataEmployee.employee_list[0].org_idx;
        ViewState["rsec_permission"] = _dataEmployee.employee_list[0].rsec_idx;
        ViewState["rdept_permission"] = _dataEmployee.employee_list[0].rdept_idx;
        ViewState["rpos_permission"] = _dataEmployee.employee_list[0].rpos_idx;
        ViewState["joblevel_permission"] = _dataEmployee.employee_list[0].jobgrade_level;
        ViewState["emp_type_permission"] = _dataEmployee.employee_list[0].emp_type_idx;

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            initPage();
            initPageLoad();

 
        }
    }

    #region set/get bind data

    protected void getDetailTrademarkProfile(GridView gvName, int _trademark_profile_idx, int _condition)
    {

        data_law data_m0_trademarkprofile = new data_law();
        tcm_record_trademarkprofile_detail m0_trademarkprofile_detail = new tcm_record_trademarkprofile_detail();
        data_m0_trademarkprofile.tcm_record_trademarkprofile_list = new tcm_record_trademarkprofile_detail[1];

        m0_trademarkprofile_detail.condition = 0;
        m0_trademarkprofile_detail.trademark_profile_idx = _trademark_profile_idx;
 
        data_m0_trademarkprofile.tcm_record_trademarkprofile_list[0] = m0_trademarkprofile_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_trademark_status));
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_trademarkprofile));
        data_m0_trademarkprofile = callServicePostLaw(_urlGetDetailTrademarksProfile, data_m0_trademarkprofile);

        ViewState["Vs_trademarkprofile"] = data_m0_trademarkprofile.tcm_record_trademarkprofile_list;

        setGridData(gvName, ViewState["Vs_trademarkprofile"]);
        

    }

    protected void getCountry(DropDownList ddlName, int _country_idx)
    {

        data_law data_m0_country_detail = new data_law();
        tcm_m0_country_detail m0_country_detail = new tcm_m0_country_detail();
        data_m0_country_detail.tcm_m0_country_list = new tcm_m0_country_detail[1];

        data_m0_country_detail.tcm_m0_country_list[0] = m0_country_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));

        //
        data_m0_country_detail = callServicePostLaw(_urlGetLawContryDetail, data_m0_country_detail);
        setDdlData(ddlName, data_m0_country_detail.tcm_m0_country_list, "country_name_en", "country_idx");
        ddlName.Items.Insert(0, new ListItem("--- Select Country ---", "0"));
        ddlName.SelectedValue = _country_idx.ToString();


        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_country_detail));

        //setChkData(chkName, data_m0_country_detail.tcm_m0_country_list, "country_name_en", "country_idx");
        //chkName.Items.Insert(0, new ListItem("--- Select ---", "0"));

    }

    #endregion set/get bind data


    #region Formview Databind
    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        if (sender is FormView)
        {
            FormView FvName = (FormView)sender;

            switch (FvName.ID)
            {

                case "fvRegistrationDetail":
                    if (FvName.CurrentMode == FormViewMode.Edit)
                    {

                    }
                    if (FvName.CurrentMode == FormViewMode.Insert)
                    {

                       

                    }
                    if (FvName.CurrentMode == FormViewMode.ReadOnly)
                    {

                        

                    }
                    break;
               

            }
        }
    }
    #endregion Formview Databind

    #region SelectedIndexChanged
    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddlName = (DropDownList)sender;

        switch (ddlName.ID)
        {

            case "ddlowner_idx":

                break;
        }
    }

    protected void rdoSelectedIndexChanged(object sender, EventArgs e)
    {
        RadioButtonList rdoName = (RadioButtonList)sender;
        switch (rdoName.ID)
        {
            case "rdoStepAssignment":

                //RadioButtonList rdoStepAssignment = (RadioButtonList)fvAssignment.FindControl("rdoStepAssignment");
                //TextBox txt_step_remark = (TextBox)fvAssignment.FindControl("txt_step_remark");
                //if (rdoStepAssignment.SelectedValue == "2")
                //{
                //    txt_step_remark.Visible = true;
                //}
                //else
                //{
                //    txt_step_remark.Visible = false;
                //}

                break;
        }

    }

    protected void chkSelectedIndexChanged(object sender, EventArgs e)
    {
        CheckBox chkName = (CheckBox)sender;
        switch (chkName.ID)
        {
            case "chkAddRenewalDate":

                break;
          
        }

    }


    #endregion SelectedIndexChanged

    #region gridview
    protected void gvPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView gvName = (GridView)sender;
        gvName.PageIndex = e.NewPageIndex;

        switch (gvName.ID)
        {
            case "gvTrademarkProfile":

                setGridData(gvTrademarkProfile, ViewState["Vs_trademarkprofile"]);
                //getDetailWaitApproveDocument(gvWaitApprove, 0);

                setOntop.Focus();
                break;


        }
    }

    protected void gvRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "gvTrademarkProfile":

                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {

                    Label lbl_trademark_profile_idx = (Label)e.Row.FindControl("lbl_trademark_profile_idx");
                    GridView gvFileTrademarkDetail = (GridView)e.Row.FindControl("gvFileTrademarkDetail");

                    //select path file
                    string getPath_file_law_trademark = ConfigurationSettings.AppSettings["path_file_law_trademarks_profile"];
                    //string docrequest_code = "doc-" + tbdocrequest_code_viewdetail.Text;
                    string trademark_code = "trademark" + "-" + lbl_trademark_profile_idx.Text;//data_u0doc_insert.tcm_u0_document_list[0].docrequest_code.ToString();

                    //litDebug.Text = trademark_code.ToString();
                    SelectPathFile(trademark_code.ToString(), getPath_file_law_trademark, gvFileTrademarkDetail);
                }
                break;

            case "gvFileTrademarkDetail":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HyperLink btnDL11 = (HyperLink)e.Row.FindControl("btnDL11");
                    HiddenField hidFile = (HiddenField)e.Row.FindControl("hidFile");
                    string LinkHost = string.Format("http://{0}", Request.Url.Host);

                    HtmlImage img = new HtmlImage();

                    //btnDL11.NavigateUrl = LinkHost11_permwait + MapURL(hidFile.Value);    
                    string path = HttpContext.Current.Request.Url.AbsolutePath;
                    string[] result_path = path.Split(new string[] { "/" }, StringSplitOptions.None);

                    if (LinkHost.ToString() == "http://localhost")
                    {
                        //litDebug1.Text = LinkHost11_permwait;
                        btnDL11.NavigateUrl = LinkHost + "/" + result_path[1].ToString() + MapURL(hidFile.Value);//path + MapURL(hidFile11_permwait.Value);

                       
                        img.Src = LinkHost + "/" + result_path[1].ToString() + MapURL(hidFile.Value);
                        img.Height = 100;
                        btnDL11.Controls.Add(img);


                    }
                    else
                    {
                        btnDL11.NavigateUrl = LinkHost + MapURL(hidFile.Value);//path + MapURL(hidFile11_permwait.Value);


                        img.Src = LinkHost + MapURL(hidFile.Value);
                        img.Height = 100;
                        btnDL11.Controls.Add(img);

                    }


                }
                break;
        }

    }

    protected void onRowCommand(Object sender, GridViewCommandEventArgs e)
    {
        if (sender is GridView)
        {
            string cmdName = e.CommandName;
            switch (cmdName)
            {
                case "cmdRemovePriorities":
                    //GridView gvPrioritiesList = (GridView)fvPrioritiesDetail.FindControl("gvPrioritiesList");
                    //GridViewRow rowSelect = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                    //int rowIndex = rowSelect.RowIndex;
                    //DataSet dsContacts = (DataSet)ViewState["vsPrioritiesList"];
                    //dsContacts.Tables["dsPrioritiesListTable"].Rows[rowIndex].Delete();
                    //dsContacts.AcceptChanges();
                    //setGridData(gvPrioritiesList, dsContacts.Tables["dsPrioritiesListTable"]);
                    //if (dsContacts.Tables["dsPrioritiesListTable"].Rows.Count < 1)
                    //{
                    //    gvPrioritiesList.Visible = false;
                    //}
                    break;
              

            }
        }
    }

    protected void gvRowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "gvDetailRenewal":

                //GridView gvDetailRenewal = (GridView)fvRegistrationDetail.FindControl("gvDetailRenewal");

                //gvDetailRenewal.EditIndex = e.NewEditIndex;
                //setGridData(gvDetailRenewal, ViewState["vs_renewal_recordsheet"]);

                break;
         

        }
    }

    protected void gvRowUpdating(object sender, GridViewUpdateEventArgs e)
    {

        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "gvDetailRenewal":

                //GridView gvDetailRenewal = (GridView)fvRegistrationDetail.FindControl("gvDetailRenewal");
                //Label lblrenewal_idx = (Label)gvDetailRenewal.Rows[e.RowIndex].FindControl("lblrenewal_idx");
                //TextBox tb_Next_Renewal_Due_edit = (TextBox)gvDetailRenewal.Rows[e.RowIndex].FindControl("tb_Next_Renewal_Due_edit");
                //RadioButtonList rdoRenewal_edit = (RadioButtonList)gvDetailRenewal.Rows[e.RowIndex].FindControl("rdoRenewal_edit");
                //DropDownList ddlRenewalStatus_edit = (DropDownList)gvDetailRenewal.Rows[e.RowIndex].FindControl("ddlRenewalStatus_edit");
                //DropDownList ddlRenewalSubStatus_edit = (DropDownList)gvDetailRenewal.Rows[e.RowIndex].FindControl("ddlRenewalSubStatus_edit");

                //gvDetailRenewal.EditIndex = -1;

                //data_law data_renewal_detail_edit = new data_law();
                //tcm_record_renewal_detail m0_renewal_detail_edit = new tcm_record_renewal_detail();
                //data_renewal_detail_edit.tcm_record_renewal_list = new tcm_record_renewal_detail[1];

                //m0_renewal_detail_edit.renewal_idx = int.Parse(lblrenewal_idx.Text);
                //m0_renewal_detail_edit.NextRenewalDue_date = tb_Next_Renewal_Due_edit.Text;
                //m0_renewal_detail_edit.cemp_idx = _emp_idx;
                //m0_renewal_detail_edit.Selected_value = int.Parse(rdoRenewal_edit.SelectedValue);
                //m0_renewal_detail_edit.Selected_name = rdoRenewal_edit.SelectedItem.Text;
                //m0_renewal_detail_edit.Renewal_Status = int.Parse(ddlRenewalStatus_edit.SelectedValue);
                //m0_renewal_detail_edit.Renewal_SubStatus = int.Parse(ddlRenewalSubStatus_edit.SelectedValue);

                //data_renewal_detail_edit.tcm_record_renewal_list[0] = m0_renewal_detail_edit;

                ////litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_renewal_detail_edit));
                //data_renewal_detail_edit = callServicePostLaw(_urlSetUpdateLawRenewal, data_renewal_detail_edit);

                //getViewRecordsheet(int.Parse(hfU1IDX.Value), int.Parse(tbtype_idx_viewdetail.Text));

                //setGridData(gvDetailRenewal, ViewState["vs_renewal_recordsheet"]);

                break;
            
        }
    }

    protected void gvRowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        //Label5.Text = GvName.ID;
        switch (GvName.ID)
        {
            case "gvDetailRenewal":
                //GridView gvDetailRenewal = (GridView)fvRegistrationDetail.FindControl("gvDetailRenewal");
                //gvDetailRenewal.EditIndex = -1;
                //setGridData(gvDetailRenewal, ViewState["vs_renewal_recordsheet"]);


                break;
         
        }
    }


    #endregion gridview

    #region event command
    protected void navCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        setActiveTab(cmdArg, 0, 0, 0, 0, 0, 0, 0, 0);

    }

    protected void ClickPrint(object sender, EventArgs e)
    {
        //  litDebug.Text = "lals";
    }

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        //linkBtnTrigger(lbDocSaveImport);

        switch (cmdName)
        {

            case "cmdBackToDetail":
                setActiveTab("docProfileDetail", 0, 0, 0, 0, 0, 0, 0, 0);
                break;

            case "cmdViewRecordSheet":
                string[] _viewrecordsheet = new string[6];
                _viewrecordsheet = e.CommandArgument.ToString().Split(';');
                int _u0_doc_idx_viewrecord = int.Parse(_viewrecordsheet[0]);
                int _u1_doc_idx_viewrecord = int.Parse(_viewrecordsheet[1]);
                int _noidx_viewrecord = int.Parse(_viewrecordsheet[2]);
                int _acidx_viewrecord = int.Parse(_viewrecordsheet[3]);
                int _staidx_viewrecord = int.Parse(_viewrecordsheet[4]);
                int _cemp_idx_viewrecord = int.Parse(_viewrecordsheet[5]);
                int _type_idx_viewrecord = int.Parse(_viewrecordsheet[6]);

                setActiveTab("docProfileDetail", _u0_doc_idx_viewrecord, _noidx_viewrecord, _staidx_viewrecord, _acidx_viewrecord, 0, _cemp_idx_viewrecord, _u1_doc_idx_viewrecord, _type_idx_viewrecord);
                break;

            case "cmdSearchDetail":

                DropDownList ddlCountrySearch = (DropDownList)fvSearchDetail.FindControl("ddlCountrySearch");
                TextBox txt_Tademarkname_Search = (TextBox)fvSearchDetail.FindControl("txt_Tademarkname_Search");

                data_law data_u0_document_searchdetail = new data_law();
                tcm_record_trademarkprofile_detail u0_document_searchdetail = new tcm_record_trademarkprofile_detail();
                data_u0_document_searchdetail.tcm_record_trademarkprofile_list = new tcm_record_trademarkprofile_detail[1];

                u0_document_searchdetail.country_idx = int.Parse(ddlCountrySearch.SelectedValue);
                u0_document_searchdetail.Name = txt_Tademarkname_Search.Text;

                data_u0_document_searchdetail.tcm_record_trademarkprofile_list[0] = u0_document_searchdetail;
                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));
                //
                data_u0_document_searchdetail = callServicePostLaw(_urlGetSearchDetailTrademark, data_u0_document_searchdetail);

                ViewState["Vs_trademarkprofile"] = data_u0_document_searchdetail.tcm_record_trademarkprofile_list;

                setGridData(gvTrademarkProfile, ViewState["Vs_trademarkprofile"]);

                break;
            case "cmdResetDetail":
                setActiveTab("docProfileDetail", 0, 0, 0, 0, 0, 0, 0, 0);
                break;

       
          

        }

    }
    //endbtn

    #endregion event command

    #region set data table
    //piorities
    protected void getPrioritiesList()
    {
        //ViewState["vsFoodMaterialList"] = null;
        DataSet dsPrioritiesList = new DataSet();
        dsPrioritiesList.Tables.Add("dsPrioritiesListTable");

        dsPrioritiesList.Tables["dsPrioritiesListTable"].Columns.Add("drTypeofpriorityText", typeof(String));
        dsPrioritiesList.Tables["dsPrioritiesListTable"].Columns.Add("drPriorityDateText", typeof(String));
        dsPrioritiesList.Tables["dsPrioritiesListTable"].Columns.Add("drPriorityNoText", typeof(String));
        dsPrioritiesList.Tables["dsPrioritiesListTable"].Columns.Add("drCountrypriorityIdx", typeof(String));
        dsPrioritiesList.Tables["dsPrioritiesListTable"].Columns.Add("drCountrypriorityText", typeof(String));
        dsPrioritiesList.Tables["dsPrioritiesListTable"].Columns.Add("drStatuspriorityIdx", typeof(String));
        dsPrioritiesList.Tables["dsPrioritiesListTable"].Columns.Add("drStatuspriorityText", typeof(String));

        ViewState["vsPrioritiesList"] = dsPrioritiesList;
    }

    protected void setPrioritiesList()
    {
        if (ViewState["vsPrioritiesList"] != null)
        {
            //TextBox tbType_of_priority = (TextBox)fvPrioritiesDetail.FindControl("tbType_of_priority");
            //TextBox tb_Priority_Date = (TextBox)fvPrioritiesDetail.FindControl("tb_Priority_Date");
            //TextBox tb_Priority_No = (TextBox)fvPrioritiesDetail.FindControl("tb_Priority_No");
            //DropDownList ddlCountry_priority = (DropDownList)fvPrioritiesDetail.FindControl("ddlCountry_priority");
            //DropDownList ddlStatus_priority = (DropDownList)fvPrioritiesDetail.FindControl("ddlStatus_priority");
            //GridView gvPrioritiesList = (GridView)fvPrioritiesDetail.FindControl("gvPrioritiesList");

            //CultureInfo culture = new CultureInfo("en-US");
            //Thread.CurrentThread.CurrentCulture = culture;

            //DataSet dsContacts = (DataSet)ViewState["vsPrioritiesList"];

            //foreach (DataRow dr in dsContacts.Tables["dsPrioritiesListTable"].Rows)
            //{
            //    if (dr["drTypeofpriorityText"].ToString() == tbType_of_priority.Text)
            //    {
            //        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- Check Data Again ---');", true);
            //        return;
            //    }
            //}
            //DataRow drContacts = dsContacts.Tables["dsPrioritiesListTable"].NewRow();
            //drContacts["drTypeofpriorityText"] = tbType_of_priority.Text;
            //drContacts["drPriorityDateText"] = tb_Priority_Date.Text;
            //drContacts["drPriorityNoText"] = tb_Priority_No.Text;
            //drContacts["drCountrypriorityIdx"] = ddlCountry_priority.SelectedValue;
            //drContacts["drCountrypriorityText"] = ddlCountry_priority.SelectedItem.Text;
            //drContacts["drStatuspriorityIdx"] = ddlStatus_priority.SelectedValue;
            //drContacts["drStatuspriorityText"] = ddlStatus_priority.SelectedItem.Text;

            //dsContacts.Tables["dsPrioritiesListTable"].Rows.Add(drContacts);
            //ViewState["vsPrioritiesList"] = dsContacts;


            //setGridData(gvPrioritiesList, dsContacts.Tables["dsPrioritiesListTable"]);
            //gvPrioritiesList.Visible = true;
        }
    }

    protected void CleardataSetPrioritiesList()
    {
        //var gvPrioritiesList = (GridView)fvPrioritiesDetail.FindControl("gvPrioritiesList");
        //ViewState["vsPrioritiesList"] = null;
        //gvPrioritiesList.DataSource = ViewState["vsPrioritiesList"];
        //gvPrioritiesList.DataBind();
        //getPrioritiesList();
    }

   
    #endregion set data table

    #region pathfile
    protected string MapURL(string path)
    {

        string appPath = Server.MapPath("~/").ToLower();
        return string.Format("/{0}", path.ToLower().Replace(appPath, "").Replace(@"\", "/"));
    }

    protected void SelectPathFile(string _docrequest_code, string path, GridView gvName)
    {
        try
        {
            string filePathView = Server.MapPath(path + _docrequest_code);

            //litDebug.Text = filePathView.ToString();
            DirectoryInfo myDirLotus = new DirectoryInfo(filePathView);
            SearchDirectories(myDirLotus, _docrequest_code, gvName);
            gvName.Visible = true;

        }
        catch (Exception ex)
        {
            //gvName.Visible = false;
            //txt.Text = ex.ToString();
        }
    }

    public void SearchDirectories(DirectoryInfo dir, String target, GridView gvName)
    {

        string dirfiles = dir.ToString();

        DataSet ds1 = new DataSet();
        DataTable dt1 = new DataTable();
        DataRow dr1 = dt1.NewRow();
        DataColumn dc1 = new DataColumn("FileName", typeof(string));
        dc1 = new DataColumn("Download", typeof(string));
        dt1.Columns.Add("FileName");
        dt1.Columns.Add("Download");


        FileInfo[] files = dir.GetFiles();
        int i = 0;
        foreach (FileInfo file in files)
        {
            //if (file.Name.ToLower().IndexOf(target.ToLower()) > -1)
            //{
            string[] f = Directory.GetFiles(dirfiles, file.Name, SearchOption.AllDirectories);
            dt1.Rows.Add(file.Name);
            dt1.Rows[i][1] = f[0];
            i++;
            //}
        }

        if (dt1.Rows.Count > 0)
        {

            ds1.Tables.Add(dt1);
            gvName.DataSource = ds1.Tables[0]; //กรณี ที่กำหนดให้ดึง DataTable ใน DataSet 
            gvName.DataBind();
            ds1.Dispose();
        }
        else
        {

            gvName.DataSource = null;
            gvName.DataBind();

        }


    }

    #endregion path file

    #region reuse

    protected void initPageLoad()
    {

    }

    protected void initPage()
    {

        setActiveTab("docProfileDetail", 0, 0, 0, 0, 0, 0, 0, 0);

    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }

    protected void setDir(string dirName)
    {
        if (!Directory.Exists(Server.MapPath(dirName)))
        {
            Directory.CreateDirectory(Server.MapPath(dirName));
        }
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void setRepeaterData(Repeater rptName, Object obj)
    {
        rptName.DataSource = obj;
        rptName.DataBind();
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void setChkData(CheckBoxList chkName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        chkName.Items.Clear();
        // bind items
        chkName.DataSource = obj;
        chkName.DataTextField = _data_text;
        chkName.DataValueField = _data_value;
        chkName.DataBind();
    }

    protected void setRdoData(RadioButtonList rdoName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        rdoName.Items.Clear();
        // bind items
        rdoName.DataSource = obj;
        rdoName.DataTextField = _data_text;
        rdoName.DataValueField = _data_value;
        rdoName.DataBind();
    }

    protected data_employee callServiceEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //fss.Text = _localJson.ToString();

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //fsa.Text = _cmdUrl + _localJson;

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);


        return _dataEmployee;
    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_law callServicePostLaw(string _cmdUrl, data_law _data_law)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_law);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_law = (data_law)_funcTool.convertJsonToObject(typeof(data_law), _localJson);

        return _data_law;
    }

    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }

    protected void setActiveView(string activeTab, int uidx, int nodeidx, int staidx, int actor_idx, int _chk_tab, int _cempidx, int u1idx, int _type_idx)
    {
        //set tab
        mvSystem.SetActiveView((View)mvSystem.FindControl(activeTab));

        //tab detail
        setFormData(fvSearchDetail, FormViewMode.ReadOnly, null);
        //setFormData(FvCreate, FormViewMode.ReadOnly, null);
        switch (activeTab)
        {

            case "docProfileDetail": //record sheet

                switch(uidx)
                {
                    case 0: //select detail
                        setFormData(fvSearchDetail, FormViewMode.Insert, null);
                        DropDownList ddlCountrySearch = (DropDownList)fvSearchDetail.FindControl("ddlCountrySearch");

                        getCountry(ddlCountrySearch, 0);


                        getDetailTrademarkProfile(gvTrademarkProfile, 0, 0);

                        
                        break;
                }

                setOntop.Focus();
                break;


        }
    }

    protected void setActiveTab(string activeTab, int uidx, int nodeidx, int staidx, int actor_idx, int _chk_tab, int _cemp_idx, int u1idx, int _type_idx)
    {
        setActiveView(activeTab, uidx, nodeidx, staidx, actor_idx, _chk_tab, _cemp_idx, u1idx, _type_idx);
        switch (activeTab)
        {
            case "docProfileDetail":
                li0.Attributes.Add("class", "active");
                //li1.Attributes.Add("class", "");
                //li2.Attributes.Add("class", "");
                //li3.Attributes.Add("class", "");
                break;
                //case "docCreate":
                //    li0.Attributes.Add("class", "");
                //    li1.Attributes.Add("class", "active");
                //    li2.Attributes.Add("class", "");
                //    li3.Attributes.Add("class", "");
                //    break;
                //case "docWaitApprove":
                //    li0.Attributes.Add("class", "");
                //    li1.Attributes.Add("class", "");
                //    li2.Attributes.Add("class", "active");
                //    li3.Attributes.Add("class", "");
                //    break;
                //case "docRegistration":
                //    li0.Attributes.Add("class", "");
                //    li1.Attributes.Add("class", "");
                //    li2.Attributes.Add("class", "");
                //    li3.Attributes.Add("class", "active");
                //    break;
        }
    }

    protected string getOnlyDate(string _dateIn)
    {
        return _funcTool.getOnlyDate(_dateIn);
    }

    protected string getStatus(int status)
    {
        if (status == 0)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Certificate'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='No Certificate'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        // Confirms that an HtmlForm control is rendered for the
        //specified ASP.NET server control at run time.
    }

    protected void rptOnRowDataBound(Object Sender, RepeaterItemEventArgs e)
    {
        var rptName = (Repeater)Sender;
        switch (rptName.ID)
        {

            case "":
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {



                }
                break;

        }
    }

    protected void gridViewTrigger(GridView linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }

    #endregion reuse
}