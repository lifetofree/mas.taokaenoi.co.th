﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_el_m0_iso : System.Web.UI.Page
{
    #region initial function/data
    function_tool _funcTool = new function_tool();
    data_elearning _data_elearning = new data_elearning();
    function_dmu _func_dmu = new function_dmu();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetel_m0_iso = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_m0_iso"];
    static string _urlSetInsel_m0_iso = _serviceUrl + ConfigurationManager.AppSettings["urlSetInsel_m0_iso"];
    static string _urlSetUpdel_m0_iso = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdel_m0_iso"];
    static string _urlDelel_m0_iso = _serviceUrl + ConfigurationManager.AppSettings["urlDelel_m0_iso"];
    static string _urlGetErrorel_m0_iso = _serviceUrl + ConfigurationManager.AppSettings["urlGetErrorel_m0_iso"];


    string _localJson = "";
    int _tempInt = 0;

    string _defaultDdlText;
    string _defaultDdlValue;

    int emp_idx = 0;
    #endregion initial function/data


    protected void Page_Load(object sender, EventArgs e)
    {
        emp_idx = int.Parse(Session["emp_idx"].ToString());

        if (!IsPostBack)
        {
            initPage();
            actionIndex();

        }
    }

    #region selected   
    protected void actionIndex()
    {
        pnlsearch.Visible = true;
        data_elearning data_elearning = new data_elearning();
        data_elearning.el_m0_iso_action = new m0_iso[1];

        m0_iso obj = new m0_iso();

        obj.m0_iso_idx = 0;
        obj.filter_keyword = txtFilterKeyword.Text;

        data_elearning.el_m0_iso_action[0] = obj;
        data_elearning = _func_dmu.zCallServiceNetwork(_urlGetel_m0_iso, data_elearning);

        setGridData(GvMaster, data_elearning.el_m0_iso_action);

    }
    #endregion selected 
    

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        int _m0_iso_idx;
        string _txtiso_name, _txtiso_remark;
        int _cemp_idx;

        m0_iso objM0_ProductType = new m0_iso();

        switch (cmdName)
        {

            case "btnToInsert":
                MvMaster.SetActiveView(ViewInsert);
                break;
            case "btnCancel":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "btnInsert":
                _txtiso_name = ((TextBox)ViewInsert.FindControl("txtiso_name")).Text.Trim();
                _txtiso_remark = ((TextBox)ViewInsert.FindControl("txtiso_remark")).Text.Trim();
                DropDownList _ddliso_status = (DropDownList)ViewInsert.FindControl("ddliso_status");
                _cemp_idx = emp_idx;
                if (_txtiso_name == "")
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณากรอกรหัส ISO');", true);
                    return;
                }
                if(checkCodeError(0, _txtiso_name) == true)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('รหัส ISO นี้มีในระบบแล้ว ไม่สามารถเพิ่มได้');", true);
                    return;
                }
                m0_iso obj = new m0_iso();
                _data_elearning.el_m0_iso_action = new m0_iso[1];
                obj.m0_iso_idx = 0;//_type_idx; 
                obj.iso_name = _txtiso_name;
                obj.iso_remark = _txtiso_remark;
                obj.iso_status = int.Parse(_ddliso_status.SelectedValue);
                obj.iso_created_by = _cemp_idx;

                _data_elearning.el_m0_iso_action[0] = obj;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
                
                _data_elearning = _func_dmu.zCallServicePostNetwork(_urlSetInsel_m0_iso, _data_elearning);

                if (_data_elearning.return_code == 0)
                {
                    actionIndex();
                    //MvMaster.SetActiveView(ViewIndex);
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                }
                else
                {
                    setError(_data_elearning.return_code.ToString() + " - " + _data_elearning.return_msg);
                }

                //actionCreate();
                //Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "btnDelete":

                _m0_iso_idx = int.Parse(cmdArg);
                _cemp_idx = emp_idx;


                _data_elearning.el_m0_iso_action = new m0_iso[1];
                objM0_ProductType.m0_iso_idx = _m0_iso_idx;
               // objM0_ProductType.CEmpIDX = _cemp_idx;

                _data_elearning.el_m0_iso_action[0] = objM0_ProductType;

               // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));

                _data_elearning = _func_dmu.zCallServiceNetwork(_urlDelel_m0_iso, _data_elearning);


                if (_data_elearning.return_code == 0)
                {

                    //actionIndex();
                    //MvMaster.SetActiveView(ViewIndex);
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);


                }
                else
                {
                    setError(_data_elearning.return_code.ToString() + " - " + _data_elearning.return_msg);
                }

                break;
            case "btnFilter":
                actionIndex();
                break;



        }
    }
    #endregion btnCommand

    #region bind data
    protected void getGridData(string _cmdName, int _actionType)
    {
        // setGridData(gvBookingType, ViewState["listData"]);
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected string getStatus(int status)
    {
        if (status == 1)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();
                actionIndex();
                break;
        }
    }

    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    pnlsearch.Visible = false;
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }
                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvMaster.EditIndex == e.Row.RowIndex) //to overlook header row
                    {
                        
                    }
                }

                break;
        }
    }

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.EditIndex = e.NewEditIndex;
                actionIndex();
                break;
        }
    }

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.EditIndex = -1;
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
        }
    }

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":


                int m0_iso_idx_update = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                var txtiso_name = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtiso_nameUpdate");
                var txtiso_remark = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtiso_remarkUpdate");
                var ddliso_status = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddliso_statusUpdate");
                int icode = 0;
                
                if (txtiso_name.Text == "")
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณากรอกรหัส ISO');", true);
                    return;
                }
                if (checkCodeError(m0_iso_idx_update, txtiso_name.Text.Trim()) == true)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('รหัส ISO นี้มีในระบบแล้ว ไม่สามารถเพิ่มได้');", true);
                    return;
                }

                GvMaster.EditIndex = -1;

                _data_elearning.el_m0_iso_action = new m0_iso[1];
                m0_iso obj = new m0_iso();

                obj.m0_iso_idx = m0_iso_idx_update;
                obj.iso_name = txtiso_name.Text;
                obj.iso_remark = txtiso_remark.Text;
                obj.iso_status = int.Parse(ddliso_status.SelectedValue);
                obj.iso_updated_by = emp_idx;

                _data_elearning.el_m0_iso_action[0] = obj;

                // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
                _data_elearning = _func_dmu.zCallServicePostNetwork(_urlSetUpdel_m0_iso, _data_elearning);

                if (_data_elearning.return_code == 0)
                {
                    //initPage();
                    //setDataList(dtlMenu, _dataMenu.m0_menu_list);  
                    actionIndex();


                }
                else
                {
                    setError(_data_elearning.return_code.ToString() + " - " + _data_elearning.return_msg);
                }


                break;
        }
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj, string sysidx_menu, string menuidx_per, string role_per)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();

        //switch (fvName.ID)
        //{



        //}
    }

    protected void setDataList(DataList dtlName, Object obj)
    {
        dtlName.DataSource = obj;
        dtlName.DataBind();
    }

    #endregion bind data

    #region reuse
    protected void initPage()
    {
        clearSession();
        clearViewState();
        

    }

    protected void initPagePermission()
    {
        clearSession();
        clearViewState();
        

    }

    protected void setVisible()
    {
    }

    protected void clearSession()
    {
        // Session["checkSubmit"] = null;
    }

    protected void clearViewState()
    {
        // ViewState["listData"] = null;
    }

    protected void setError(string _errorText)
    {
        // divShowError.Visible = !divShowError.Visible;
        // litErrorCode.Text = _dataEmployee.return_code.ToString();
        if (_errorText != "")
        {
            litDebug.Text = "error : " + _errorText;
        }
        else
        {
            litDebug.Text = String.Empty;
        }
    }
    
    public Boolean checkCodeError(int _m0_iso_idx, string _iso_code)
    {
        Boolean error = false;
        data_elearning data_elearning = new data_elearning();
        data_elearning.el_m0_iso_action = new m0_iso[1];

        m0_iso obj = new m0_iso();

        obj.m0_iso_idx = _m0_iso_idx;
        obj.iso_name = _iso_code;
        data_elearning.el_m0_iso_action[0] = obj;
        data_elearning = _func_dmu.zCallServiceNetwork(_urlGetErrorel_m0_iso, data_elearning);
        if(data_elearning.el_m0_iso_action != null)
        {
            //obj = data_elearning.el_m0_iso_action[0];
            //if (obj.iso_code.ToString().Trim()== _iso_code.Trim())
            //{

            //}
            error = true;

        }
        return error;
    }

    #endregion reuse

}