﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class websystem_MasterData_ecom_m0_channel : System.Web.UI.Page
{

    #region Connect
    function_tool _funcTool = new function_tool();
    data_ecom_channel data_ecom_channel = new data_ecom_channel();
    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];


    static string _urlGetEcomM0Channel = _serviceUrl + ConfigurationManager.AppSettings["urlGetEcomM0Channel"];
    static string _urlSetEcomM0Channel = _serviceUrl + ConfigurationManager.AppSettings["urlSetEcomM0Channel"];
    static string _urlDelEcomM0Channel = _serviceUrl + ConfigurationManager.AppSettings["urlDelEcomM0Channel"];

    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;
    bool _flag_qmr = false;
    #endregion Connect

    #region Page_Init
    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());

    }
    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {


        if (!IsPostBack)
        {
            Select_Place();

        }
    }
    #endregion Page Load

    #region initPage
    protected void initPage()
    {
        MvSystem.SetActiveView(ViewIndex);

    }
    #endregion
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        switch (cmdName)
        {
            case "cmdAddUnit":
                btn_addplace.Visible = false;
                setFormData(FvInsert, FormViewMode.Insert, null);
                FvInsert.Visible = true;
                div_searchmaster_page.Visible = false;
                break;
            case "CmdCancel":
                btn_addplace.Visible = true;
                FvInsert.Visible = false;
                div_searchmaster_page.Visible = true;
                break;
            case "CmdSave":
                Insert_Place();
                Select_Place();

                break;
            case "CmdSearchMaster_src":
                Select_Place();
                break;
            case "btnRefreshMaster_Supplier":
                txtmaster_search.Text = String.Empty;
                Src_status.Text = "999";
                Select_Place();
                break;
            case "btnTodelete":
                int un_idx_del = int.Parse(cmdArg);

                data_ecom_channel = new data_ecom_channel(); ;
                ecom_m0_channel m0_detail = new ecom_m0_channel();
                data_ecom_channel.ecom_m0_channel_list = new ecom_m0_channel[1];

                m0_detail.channel_idx = un_idx_del;
                data_ecom_channel.ecom_m0_channel_list[0] = m0_detail;

                data_ecom_channel = callServicePostApi(_urlDelEcomM0Channel, data_ecom_channel);
                //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_place_del));
                Select_Place();
                btn_addplace.Visible = true;
                FvInsert.Visible = false;
                break;



        }
    }

    protected void Insert_Place()
    {

        TextBox txtchannel_name = (TextBox)FvInsert.FindControl("txtchannel_name");
        TextBox txtsale_org = (TextBox)FvInsert.FindControl("txtsale_org");
        TextBox txtchannel = (TextBox)FvInsert.FindControl("txtchannel");
        TextBox txtdivision = (TextBox)FvInsert.FindControl("txtdivision");
        TextBox txtsales_office = (TextBox)FvInsert.FindControl("txtsales_office");
        TextBox txtsales_group = (TextBox)FvInsert.FindControl("txtsales_group");
        TextBox txtcustomer_no = (TextBox)FvInsert.FindControl("txtcustomer_no");
        TextBox txtorder_type = (TextBox)FvInsert.FindControl("txtorder_type");


        DropDownList dropD_status = (DropDownList)FvInsert.FindControl("ddPlace");
        //litdebug.Text = tex_place_name.Text + dropD_status_place.SelectedValue;

        data_ecom_channel = new data_ecom_channel();
        ecom_m0_channel m0_detail = new ecom_m0_channel();
        data_ecom_channel.ecom_m0_channel_list = new ecom_m0_channel[1];

        m0_detail.channel_name = txtchannel_name.Text;
        m0_detail.m0_sale_org = txtsale_org.Text;
        m0_detail.m0_channel = txtchannel.Text;
        m0_detail.m0_division = txtdivision.Text;
        m0_detail.m0_sales_office = txtsales_office.Text;
        m0_detail.m0_sales_group = txtsales_group.Text;
        m0_detail.m0_customer_no = txtcustomer_no.Text;
        m0_detail.m0_order_type = txtorder_type.Text;


        m0_detail.channel_status = int.Parse(dropD_status.SelectedValue);
        m0_detail.cemp_idx = _emp_idx;

        data_ecom_channel.ecom_m0_channel_list[0] = m0_detail;

        data_ecom_channel = callServicePostApi(_urlSetEcomM0Channel, data_ecom_channel);
        // litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_ecom_promotion));
        if (data_ecom_channel.return_code == 1)
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ข้อมูลซ้ำ กรุณาแก้ไขใหม่อีกครั้ง!!!');", true);

        }
        else
        {
            btn_addplace.Visible = true;
            FvInsert.Visible = false;
            div_searchmaster_page.Visible = true;
        }



    }
    protected void Select_Place()
    {
        //TextBox txtmaster_search = (TextBox)FvInsert.FindControl("txtmaster_search");
        //DropDownList dropD_status_place = (DropDownList)FvInsert.FindControl("Src_status");

        data_ecom_channel = new data_ecom_channel();
        ecom_m0_channel m0_detail = new ecom_m0_channel();

        data_ecom_channel.ecom_m0_channel_list = new ecom_m0_channel[1];

        m0_detail.channel_name = txtmaster_search.Text;
        m0_detail.channel_status = int.Parse(Src_status.SelectedValue);
        data_ecom_channel.ecom_m0_channel_list[0] = m0_detail;

       // litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_ecom_channel));
        data_ecom_channel = callServicePostApi(_urlGetEcomM0Channel, data_ecom_channel);
        // litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_ecom_channel));
        setGridData(Gv_select_place, data_ecom_channel.ecom_m0_channel_list);

    }

    #region setformdata
    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    #endregion setformdata


    #region setGridData
    private void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    #endregion

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "Gv_select_place":
                Gv_select_place.PageIndex = e.NewPageIndex;
                Gv_select_place.DataBind();
                Select_Place();
                break;

        }
    }
    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "Gv_select_place":
                Gv_select_place.EditIndex = -1;
                Select_Place();

                btn_addplace.Visible = true;
                FvInsert.Visible = false;
                break;

        }
    }
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "Gv_select_place":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbpplace_status = (Label)e.Row.Cells[2].FindControl("lbpplace_status");
                    Label place_statusOnline = (Label)e.Row.Cells[2].FindControl("place_statusOnline");
                    Label place_statusOffline = (Label)e.Row.Cells[2].FindControl("place_statusOffline");

                    ViewState["_place_status"] = lbpplace_status.Text;


                    if (ViewState["_place_status"].ToString() == "1")
                    {
                        place_statusOnline.Visible = true;
                    }
                    else if (ViewState["_place_status"].ToString() == "0")
                    {
                        place_statusOffline.Visible = true;
                    }

                }


                if (e.Row.RowState.ToString().Contains("Edit"))
                {

                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                    //btn_addplace.Visible = true;
                    //setFormData(Fv_Insert_Result, FormViewMode.ReadOnly, null);

                    //MultiView1.Visible = true;
                    //Gv_select_unit.Visible = false;
                    btn_addplace.Visible = true;
                    FvInsert.Visible = false;

                }



                break;


        }
    }

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "Gv_select_place":
                Gv_select_place.EditIndex = e.NewEditIndex;
                Select_Place();
                break;

        }
    }
    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "Gv_select_place":

                TextBox txt_place_idx_edit = (TextBox)Gv_select_place.Rows[e.RowIndex].FindControl("txt_channel_idx_edit");
                TextBox txtchannel_name = (TextBox)Gv_select_place.Rows[e.RowIndex].FindControl("Name_channel_name");
                TextBox txtsale_org = (TextBox)Gv_select_place.Rows[e.RowIndex].FindControl("Name_sale_org");
                TextBox txtchannel = (TextBox)Gv_select_place.Rows[e.RowIndex].FindControl("Name_channel");
                TextBox txtdivision = (TextBox)Gv_select_place.Rows[e.RowIndex].FindControl("Name_division");
                TextBox txtsales_office = (TextBox)Gv_select_place.Rows[e.RowIndex].FindControl("Name_sales_office");
                TextBox txtsales_group = (TextBox)Gv_select_place.Rows[e.RowIndex].FindControl("Name_sales_group");
                TextBox txtcustomer_no = (TextBox)Gv_select_place.Rows[e.RowIndex].FindControl("Name_customer_no");
                TextBox txtorder_type = (TextBox)Gv_select_place.Rows[e.RowIndex].FindControl("Name_order_type");
                //TextBox Name_material = (TextBox)Gv_select_place.Rows[e.RowIndex].FindControl("Name_material");

                DropDownList dropD_status = (DropDownList)Gv_select_place.Rows[e.RowIndex].FindControl("ddEdit_place");


                //data_ecom_saleunit = new data_ecom_saleunit(); ;
                //ecom_m0_saleunit m0_detail = new ecom_m0_saleunit();
                //data_ecom_saleunit.ecom_m0_saleunit_list = new ecom_m0_saleunit[1];

                //m0_detail.un_idx = int.Parse(txt_place_idx_edit.Text);
                //m0_detail.unit_name = unit_name.Text;
                //m0_detail.material = Name_material.Text;
                //m0_detail.un_status = int.Parse(un_status.SelectedValue);
                //m0_detail.emp_idx_update = _emp_idx;
                //data_ecom_saleunit.ecom_m0_saleunit_list[0] = m0_detail;
                ////litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_ecom_saleunit));
                //data_ecom_saleunit = callServicePostSaleUnit(_urlSetEcomM0SaleUnit, data_ecom_saleunit);



                //litdebug.Text = tex_place_name.Text + dropD_status_place.SelectedValue;

                data_ecom_channel = new data_ecom_channel();
                ecom_m0_channel m0_detail = new ecom_m0_channel();
                data_ecom_channel.ecom_m0_channel_list = new ecom_m0_channel[1];

                m0_detail.channel_idx = int.Parse(txt_place_idx_edit.Text);
                m0_detail.channel_name = txtchannel_name.Text;
                m0_detail.m0_sale_org = txtsale_org.Text;
                m0_detail.m0_channel = txtchannel.Text;
                m0_detail.m0_division = txtdivision.Text;
                m0_detail.m0_sales_office = txtsales_office.Text;
                m0_detail.m0_sales_group = txtsales_group.Text;
                m0_detail.m0_customer_no = txtcustomer_no.Text;
                m0_detail.m0_order_type = txtorder_type.Text;


                m0_detail.channel_status = int.Parse(dropD_status.SelectedValue);
                m0_detail.cemp_idx = _emp_idx;

                data_ecom_channel.ecom_m0_channel_list[0] = m0_detail;

                data_ecom_channel = callServicePostApi(_urlSetEcomM0Channel, data_ecom_channel);

                Gv_select_place.EditIndex = -1;

                if (data_ecom_channel.return_code == 1)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ข้อมูลซ้ำ กรุณาแก้ไขใหม่อีกครั้ง!!!');", true);

                }
                Select_Place();
                btn_addplace.Visible = true;
                FvInsert.Visible = false;
                break;
        }
    }



    #region callService 

    //protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    //{
    //    // call services
    //    _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

    //    // convert json to object
    //    _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

    //    return _dataEmployee;
    //}


    protected data_ecom_channel callServicePostApi(string _cmdUrl, data_ecom_channel _data)
    {
        _localJson = _funcTool.convertObjectToJson(_data);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // litdebug.Text = _localJson;
        _data = (data_ecom_channel)_funcTool.convertJsonToObject(typeof(data_ecom_channel), _localJson);


        return _data;
    }
    #endregion callService Functions
}