<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="qa_lab.aspx.cs" Inherits="websystem_qa_qa_lab" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <asp:HyperLink ID="SETFOCUS" runat="server"></asp:HyperLink>
    <!--tab menu-->
    <nav class="navbar navbar-default" id="divBtnHead" runat="server" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-bar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="sub-navbar">
                    <a class="navbar-brand" href="#"><b>Menu</b></a>
                </div>
            </div>

            <!--Collect the nav links, forms, and other content for toggling-->
            <div class="collapse navbar-collapse" id="menu-bar">
                <ul class="nav navbar-nav" id="uiNav" runat="server">
                    <li id="li0" runat="server">
                        <asp:LinkButton ID="lbtab1" runat="server" CommandName="cmdtab2" OnCommand="navCommand" CommandArgument="tab1"> สร้างรายการ</asp:LinkButton>
                    </li>
                    <li id="li1" runat="server">
                        <asp:LinkButton ID="lbtab2" runat="server" CommandName="cmdtab2" OnCommand="navCommand" CommandArgument="tab2"> รายการทั่วไป</asp:LinkButton>
                    </li>
                    <li id="li2" runat="server" class="dropdown">
                        <asp:LinkButton ID="lbtab3" runat="server" CommandName="cmdtab3" OnCommand="navCommand" class="dropdown-toggle" data-toggle="dropdown"
                            role="button" aria-haspopup="true" aria-expanded="false" CommandArgument="tab3"> Master data <span class="caret"></span></asp:LinkButton>
                        <!-- tab manu dropdownlist -->
                        <ul class="dropdown-menu">
                            <li>
                                <asp:LinkButton ID="btnview1" runat="server" data-original-title="Meterial" data-toggle="tooltip" OnCommand="btnCommand"
                                    CommandArgument="0" CommandName="cmdView"> ข้อมูล Master material</asp:LinkButton>
                            </li>
                            <li role="separator" class="divider"></li>
                            <li>
                                <asp:LinkButton ID="btnview2" runat="server" data-original-title="ประเภทการตรวจวิเคราะห์" data-toggle="tooltip" OnCommand="btnCommand"
                                    CommandArgument="1" CommandName="cmdView"> ประเภทการตรวจวิเคราะห์</asp:LinkButton>
                            </li>
                            <%--   <li role="separator" class="divider"></li>--%>
                            <%--     <li>
                                <asp:LinkButton ID="btnview3" runat="server" data-original-title="ข้อมูลห้องปฏิบัติการเคมี" data-toggle="tooltip" OnCommand="btnCommand"
                                    CommandArgument="2" CommandName="cmdView"> ข้อมูลห้องปฏิบัติการเคมี</asp:LinkButton>
                            </li>--%>
                        </ul>

                    </li>
                    <li id="li3" runat="server">
                        <asp:LinkButton ID="lbtab4" runat="server" CommandName="cmdtab4" OnCommand="navCommand" CommandArgument="tab4"> Lab</asp:LinkButton>
                    </li>

                    <li id="li4" runat="server">
                        <asp:LinkButton ID="LinkButton27" runat="server" CommandName="cmdtab4" OnCommand="navCommand" CommandArgument="tab5"> กรอกผลวิเคราะห์ตัวอย่าง</asp:LinkButton>
                    </li>
                    <li id="li5" runat="server">
                        <asp:LinkButton ID="LinkButton44" runat="server" CommandName="cmdtab4" OnCommand="navCommand" CommandArgument="tab6"> Supervisor</asp:LinkButton>
                    </li>

                </ul>

                <%--   <ul class="nav navbar-nav navbar-right" runat="server">
                 
                    <li>


                        <a href='<%=ResolveUrl("~/print-form") %>' target="_blank"><i class="fa fa-print" aria-hidden="true"></i> ฟอร์มบันทึกการรับตัวอย่างส่งตรวจ</a>
                    </li>

                </ul>--%>
            </div>
        </div>
    </nav>
    <!--tab menu-->
    <div class="col-sm-12">
        <asp:Literal ID="litDebug" runat="server"></asp:Literal>
    </div>
    <!--multiview-->
    <asp:MultiView ID="mvSystem" runat="server">
        <asp:View ID="tab1" runat="server">
            <!--user info-->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">รายละเอียดผู้ใช้งาน</h3>
                </div>
                <div class="panel-body">
                    <div class="form-horizontal" role="form">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">รหัสพนักงาน</label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="tbEmpCode" runat="server" CssClass="form-control" Text='xxxxxxxx' Enabled="false" />
                            </div>
                            <label class="col-sm-2 control-label">ชื่อ - นามสกุล</label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="tbEmpName" runat="server" CssClass="form-control" Text='ทดสอบ ทดสอบ' Enabled="false" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">องค์กร</label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="tbEmpOrg" runat="server" CssClass="form-control" Text='ทดสอบ' Enabled="false" />
                            </div>
                            <label class="col-sm-2 control-label">ฝ่าย</label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="tbEmpDept" runat="server" CssClass="form-control" Text='ทดสอบ ทดสอบ' Enabled="false" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">แผนก</label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="tbEmpSec" runat="server" CssClass="form-control" Text='ทดสอบ ทดสอบ' Enabled="false" />
                            </div>
                            <label class="col-sm-2 control-label">ตำแหน่ง</label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="tbEmpPos" runat="server" CssClass="form-control" Text='ทดสอบ ทดสอบ' Enabled="false" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--user info-->

            <!--document info-->
            <asp:FormView ID="fvDocDetail" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <asp:HiddenField ID="hfNodeIDX" runat="server" Value="1" />
                    <asp:HiddenField ID="hfActorIDX" runat="server" Value="1" />
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">รายละเอียดรายการ</h3>
                        </div>
                        <div class="panel-body">

                            <div class="form-horizontal" role="form">

                                <%-- <div class="panel-body">--%>
                                <%--<div class="form-horizontal" role="form">--%>

                                <div class="form-group">
                                    <div class="panel-heading f-bold">รายละเอียดรายการตรวจวิเคราะห์</div>
                                </div>

                                <div class="form-group">

                                    <label class="col-sm-2 control-label">Certificate</label>
                                    <div class="col-sm-4 control-label textleft">
                                        <asp:RadioButton ID="certificate_1" Width="50%" runat="server" AutoPostBack="true" OnCheckedChanged="RadioButton_CheckedChanged" Text=" มีใบ certificate" GroupName="cer1" ValidationGroup="Saveinsertdetail" />

                                        <asp:RadioButton ID="certificate_2" Width="50%" runat="server" AutoPostBack="true" OnCheckedChanged="RadioButton_CheckedChanged" Text=" ไม่มีใบ certificate" GroupName="cer1" ValidationGroup="Saveinsertdetail" />
                                    </div>


                                    <label class="col-sm-2 control-label">แนบไฟล์</label>
                                    <div class="col-sm-4 control-label">
                                        <%--<div class="form-group">--%>
                                        <asp:FileUpload ID="upload_file_material" runat="server" />
                                        <%--  </div>--%>
                                    </div>



                                </div>

                                <div class="form-group">

                                    <label class="col-sm-2 control-label">เลือกการตรวจ</label>
                                    <div class="col-sm-4 control-label textleft">
                                        <%--<div class="col-lg-offset-2 col-sm-10">--%>
                                        <asp:RadioButton ID="selected_datail" Width="50%" runat="server" AutoPostBack="true" OnCheckedChanged="RadioButton_CheckedChanged" Text="แบบรายการ " GroupName="machine" ValidationGroup="Saveinsertdetail" />

                                        <asp:RadioButton ID="selected_alldatail" Width="50%" runat="server" AutoPostBack="true" OnCheckedChanged="RadioButton_CheckedChanged" Text="แบบชุด" GroupName="machine" ValidationGroup="Saveinsertdetail" />


                                    </div>

                                    <label class="col-sm-2 control-label">สถานที่ตรวจสอบ</label>
                                    <div class="col-sm-4">

                                        <asp:DropDownList ID="plac_idx_insert" runat="server" CssClass="form-control">
                                            <asp:ListItem Text="-- เลือกสถานที่ตรวจสอบ --" Value="00"></asp:ListItem>
                                            <asp:ListItem Text="นพวงศ์" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="โรจนะ" Value="2"></asp:ListItem>
                                        </asp:DropDownList>

                                    </div>

                                    <%-- <label class="col-sm-6 control-label"></label>--%>
                                </div>

                                <div class="form-group">
                                    <asp:Panel ID="show_detail_before" runat="server" Visible="false">

                                        <label class="col-sm-2 control-label">รายการที่สั่งตรวจ(M)</label>
                                        <div class="col-sm-4 control-label textleft">
                                            <table id="PanelBody_YrChkBoxColumns" cellspacing="10" style="width: 100%;">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <input id="PanelBody_YrChkBoxColumns_0" type="checkbox" name="ctl00$PanelBody$YrChkBoxColumns$0" value=""><label for="PanelBody_YrChkBoxColumns_0">Total Plate Count(CFU/g)</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input id="PanelBody_YrChkBoxColumns_1" type="checkbox" name="ctl00$PanelBody$YrChkBoxColumns$1" value=""><label for="PanelBody_YrChkBoxColumns_1">Total Plate Count(CFU/Unit)</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input id="PanelBody_YrChkBoxColumns_2" type="checkbox" name="ctl00$PanelBody$YrChkBoxColumns$2" value=""><label for="PanelBody_YrChkBoxColumns_2">Total Plate Count(CFU/ml)</label></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <label class="col-sm-2 control-label">รายการที่สั่งตรวจ(C)</label>
                                        <div class="col-sm-4 control-label textleft">
                                            <table id="PanelBody_YrChkBoxColumns" cellspacing="10" style="width: 100%;">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <input id="PanelBody_YrChkBoxColumns_0" type="checkbox" name="ctl00$PanelBody$YrChkBoxColumns$0" value=""><label for="PanelBody_YrChkBoxColumns_0">Moisture(%MC)</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input id="PanelBody_YrChkBoxColumns_1" type="checkbox" name="ctl00$PanelBody$YrChkBoxColumns$1" value=""><label for="PanelBody_YrChkBoxColumns_1">pH</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input id="PanelBody_YrChkBoxColumns_2" type="checkbox" name="ctl00$PanelBody$YrChkBoxColumns$0" value="รหัสพนักงาน"><label for="PanelBody_YrChkBoxColumns_0">Salt(%)</label></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                        <%-- <div class="col-lg-offset-2 col-sm-10">
                                                    <asp:LinkButton ID="btninsert_detail" CssClass="btn btn-default" runat="server" CommandName="btninsert_detail" OnCommand="btnCommand" ValidationGroup="Saveinsertext" title="เพิ่มตัวอย่าง"><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่ม</asp:LinkButton>
                                                </div>--%>
                                    </asp:Panel>

                                    <asp:Panel ID="show_alldetail_before" runat="server" Visible="false">

                                        <label class="col-sm-2 control-label">รายการที่สั่งตรวจแบบชุด</label>
                                        <div class="col-sm-4 control-label textleft">
                                            <table id="PanelBody_YrChkBoxColumns" cellspacing="10" style="width: 100%;">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <input id="PanelBody_YrChkBoxColumns_0" type="checkbox" name="ctl00$PanelBody$YrChkBoxColumns$0" value=""><label for="PanelBody_YrChkBoxColumns_0">T01</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input id="PanelBody_YrChkBoxColumns_1" type="checkbox" name="ctl00$PanelBody$YrChkBoxColumns$1" value="ชื่อพนักงาน"><label for="PanelBody_YrChkBoxColumns_1">T02</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input id="PanelBody_YrChkBoxColumns_0" type="checkbox" name="ctl00$PanelBody$YrChkBoxColumns$0" value="รหัสพนักงาน"><label for="PanelBody_YrChkBoxColumns_0">T03</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input id="PanelBody_YrChkBoxColumns_0" type="checkbox" name="ctl00$PanelBody$YrChkBoxColumns$0" value="รหัสพนักงาน"><label for="PanelBody_YrChkBoxColumns_0">T04</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input id="PanelBody_YrChkBoxColumns_0" type="checkbox" name="ctl00$PanelBody$YrChkBoxColumns$0" value="รหัสพนักงาน"><label for="PanelBody_YrChkBoxColumns_0">Swab</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input id="PanelBody_YrChkBoxColumns_0" type="checkbox" name="ctl00$PanelBody$YrChkBoxColumns$0" value="รหัสพนักงาน"><label for="PanelBody_YrChkBoxColumns_0">Water</label></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                        <label class="col-sm-6 control-label"></label>

                                        <%-- <div class="col-lg-offset-2 col-sm-10">
                                                    <asp:LinkButton ID="btninsert_alldetail" CssClass="btn btn-default" runat="server" CommandName="btninsert_alldetail" OnCommand="btnCommand" ValidationGroup="Saveinsertext" title="เพิ่มตัวอย่าง"><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่ม</asp:LinkButton>
                                                </div>--%>
                                    </asp:Panel>

                                </div>

                                <hr />
                                <div class="form-group">
                                    <div class="panel-heading f-bold">รายละเอียดรายการตัวอย่าง</div>
                                </div>

                                <div class="form-group">

                                    <label class="col-sm-2 control-label">ค้นหาเลข Mat.</label>
                                    <div class="col-sm-4">

                                        <asp:TextBox ID="txt_material_search" runat="server" MaxLength="7" CssClass="form-control" OnTextChanged="TextBoxChanged" AutoPostBack="true" placeholder="Search Mat ...">                                                      
                                        </asp:TextBox>

                                    </div>
                                    <%--<label class="col-sm-6 control-label"></label>--%>

                                    <div class="col-sm-2 ">
                                        <div class="pull-left">
                                            <asp:LinkButton ID="btnSearchMat" CssClass="btn btn-primary" runat="server" ValidationGroup="SearchIndex" OnCommand="btnCommand" CommandName="btnSearchMat" Font-Size="Small" data-toggle="tooltip" title="Search"><span class="glyphicon glyphicon-search"></span></asp:LinkButton>


                                            <%--<asp:LinkButton ID="btnrefresh" CssClass="btn btn-default" runat="server" OnCommand="btnCommand" data-toggle="tooltip" Font-Size="Small" CommandName="btnrefresh" title="Refresh"><i class="fa fa-refresh"></i></asp:LinkButton>--%>
                                        </div>

                                        </label>
                                    </div>
                                    <label class="col-sm-4 control-label"></label>

                                </div>

                                <%--<div class="form-group">--%>
                                <asp:Panel ID="show_another_insert" runat="server" Visible="false">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">อื่นๆ</label>
                                        <div class="col-sm-4 control-label textleft">
                                            <asp:RadioButton ID="rd_check_another_ex" Width="50%" runat="server" AutoPostBack="true" OnCheckedChanged="RadioButton_CheckedChanged" Text=" กรอกข้อมูลตัวอย่าง" GroupName="machine1" ValidationGroup="Saveinsertdetail" />

                                            <%-- <asp:RadioButton ID="RadioButton2" Width="50%" runat="server" AutoPostBack="true" OnCheckedChanged="RadioButton_CheckedChanged" Text=" ไม่มีใบ certificate" GroupName="machine1" ValidationGroup="Saveinsertdetail" />--%>
                                        </div>

                                        <label class="col-sm-6 control-label"></label>
                                    </div>
                                </asp:Panel>
                                <%--</div>--%>

                                <hr />


                                <asp:Panel ID="show_noresult" runat="server" Visible="false">
                                    <div class="form-group">
                                        <label class="col-sm-6 control-label">-- ไม่พบข้อมูล -- </label>

                                        <label class="col-sm-6 control-label"></label>
                                    </div>
                                </asp:Panel>


                                <asp:Panel ID="show_detailsample_insert" runat="server" Visible="false">

                                    <div class="form-group">

                                        <asp:Panel ID="show_mat_have" runat="server" Visible="false">

                                            <label class="col-sm-2 control-label">Mat.</label>
                                            <div class="col-sm-4">

                                                <asp:TextBox ID="txt_mat_insert" runat="server" CssClass="form-control" Text='1202884' Enabled="false">                                                      
                                                </asp:TextBox>

                                            </div>


                                            <label class="col-sm-2 control-label">ชื่อตัวอย่าง</label>
                                            <div class="col-sm-4">

                                                <asp:TextBox ID="txt_samplecode" runat="server" CssClass="form-control" Text='BB 60g. TYG()' Enabled="false">                                                      
                                                </asp:TextBox>

                                            </div>

                                        </asp:Panel>


                                        <asp:Panel ID="show_mat_nohave" runat="server" Visible="false">
                                            <%--<div class="form-group">--%>

                                            <label class="col-sm-2 control-label">Mat.</label>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="TextBox8" runat="server" MaxLength="7" CssClass="form-control" Text='' Enabled="true">                                                     
                                                </asp:TextBox>

                                            </div>


                                            <label class="col-sm-2 control-label">ชื่อตัวอย่าง</label>
                                            <div class="col-sm-4">

                                                <asp:TextBox ID="TextBox9" runat="server" CssClass="form-control" Text='' Enabled="true">                                                      
                                                </asp:TextBox>

                                            </div>
                                            <%-- </div>--%>
                                        </asp:Panel>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">MFD</label>
                                        <div class="col-sm-4">

                                            <asp:TextBox ID="txt_mfd" runat="server" CssClass="form-control" placeholder="MFD ...">                                                      
                                            </asp:TextBox>

                                        </div>

                                        <label class="col-sm-2 control-label">EXP</label>
                                        <div class="col-sm-4">

                                            <asp:TextBox ID="txt_exp" runat="server" CssClass="form-control" placeholder="EXP ...">                                                      
                                            </asp:TextBox>

                                        </div>

                                    </div>

                                    <div class="form-group">

                                        <label class="col-sm-2 control-label">Batch</label>
                                        <div class="col-sm-4">

                                            <asp:TextBox ID="txt_batch" runat="server" CssClass="form-control" placeholder="Batch ...">                                                      
                                            </asp:TextBox>

                                        </div>

                                        <label class="col-sm-2 control-label">ชื่อลูกค้า</label>
                                        <div class="col-sm-4">

                                            <asp:TextBox ID="txt_empname_insert" runat="server" CssClass="form-control" placeholder="กรอกชื่อลูกค้า ...">                                                      
                                            </asp:TextBox>

                                        </div>

                                    </div>

                                    <div class="form-group">

                                        <label class="col-sm-2 control-label">เลขตู้</label>
                                        <div class="col-sm-4">

                                            <asp:TextBox ID="txt_cabinet" runat="server" CssClass="form-control" placeholder="กรอกเลขตู้ ...">                                                      
                                            </asp:TextBox>

                                        </div>

                                        <label class="col-sm-2 control-label">กะ</label>
                                        <div class="col-sm-4">

                                            <asp:TextBox ID="txt_shit_insert" runat="server" CssClass="form-control" placeholder="กรอกกะ ...">                                                      
                                            </asp:TextBox>

                                        </div>

                                    </div>

                                    <div class="form-group">

                                        <label class="col-sm-2 control-label">เวลา</label>
                                        <div class="col-sm-4">

                                            <asp:TextBox ID="txt_time_insert" runat="server" CssClass="form-control" placeholder="กรอกเวลา ...">                                                      
                                            </asp:TextBox>

                                        </div>
                                        <label class="col-sm-2 control-label">รายละเอียดอื่นๆ</label>
                                        <div class="col-sm-4">

                                            <asp:TextBox ID="txt_detail" runat="server" CssClass="form-control" placeholder="รายละเอียดอื่นๆ ...">                                                      
                                            </asp:TextBox>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-lg-offset-2 col-sm-10">
                                            <asp:LinkButton ID="btninsert_detail_ex" CssClass="btn btn-default" runat="server" CommandName="btninsert_detail_ex" OnCommand="btnCommand" ValidationGroup="Saveinsertext" title="เพิ่มตัวอย่าง"><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่ม</asp:LinkButton>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:Panel ID="show_selecteddetail_ex" runat="server" Visible="false">
                                                <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" id="gvDoingList" style="border-collapse: collapse;">
                                                    <tbody>
                                                        <tr class="info" style="font-size: Small; height: 40px;">
                                                            <th scope="col" style="width: 10%;">ชื่อตัวอย่าง</th>
                                                            <th scope="col" style="width: 10%;">Mat.</th>
                                                            <th scope="col" style="width: 10%;">MFD</th>
                                                            <th scope="col" style="width: 10%;">EXP</th>
                                                            <th scope="col" style="width: 10%;">Batch</th>
                                                            <th scope="col" style="width: 10%;">ชื่อลูกค้า</th>
                                                            <th scope="col" style="width: 10%;">เลขตู้</th>
                                                            <th scope="col" style="width: 10%;">กะ</th>
                                                            <th scope="col" style="width: 10%;">เวลา</th>
                                                            <th scope="col" style="width: 10%;">รายละเอียดอื่นๆ</th>
                                                            <th scope="col" style="width: 10%;">การจัดการ</th>


                                                            <tr style="font-size: Small;">
                                                                <td>
                                                                    <span id="ContentMain_gvDoingList_lblDocCode_0">BB 60g.</span>
                                                                </td>
                                                                <td>
                                                                    <span id="ContentMain_gvDoingList_lblDocCode_0">1202884</span>
                                                                </td>
                                                                <td>
                                                                    <span id="ContentMain_gvDoingList_lblDocCode_0">-</span>
                                                                </td>
                                                                <td>
                                                                    <span id="ContentMain_gvDoingList_lblDocCode_0">120817</span>
                                                                </td>
                                                                <td>
                                                                    <span id="ContentMain_gvDoingList_lblDocCode_0">T2</span>
                                                                </td>
                                                                <td>
                                                                    <span id="ContentMain_gvDoingList_lblDocCode_0">ทดสอบ</span>
                                                                </td>
                                                                <td>
                                                                    <span id="ContentMain_gvDoingList_lblDocCode_0">6361</span>
                                                                </td>
                                                                <td>
                                                                    <span id="ContentMain_gvDoingList_lblDocCode_0">A</span>
                                                                </td>
                                                                <td>
                                                                    <span id="ContentMain_gvDoingList_lblDocCode_0">8.30</span>
                                                                </td>
                                                                <td>
                                                                    <span id="ContentMain_gvDoingList_lblDocCode_0">ทดสอบตัวอย่าง</span>
                                                                </td>
                                                                <td><i class="fa fa-trash" data-original-title="delete" data-toggle="tooltip" aria-hidden="true"></i></td>

                                                            </tr>
                                                            <tr style="font-size: Small;">
                                                                <td>
                                                                    <span id="ContentMain_gvDoingList_lblDocCode_0">BR 3.6g.</span>
                                                                </td>
                                                                <td>
                                                                    <span id="ContentMain_gvDoingList_lblDocCode_0">1201407</span>
                                                                </td>
                                                                <td>
                                                                    <span id="ContentMain_gvDoingList_lblDocCode_0">13052016</span>
                                                                </td>
                                                                <td>
                                                                    <span id="ContentMain_gvDoingList_lblCreateDate_0">13052017</span>
                                                                </td>
                                                                <td>
                                                                    <span id="ContentMain_gvDoingList_lblCreateDate_0">T1</span>
                                                                </td>
                                                                <td>
                                                                    <span id="ContentMain_gvDoingList_lblCreateDate_0">ทดสอบ1</span>
                                                                </td>
                                                                <td>
                                                                    <span id="ContentMain_gvDoingList_lblCreateDate_0">6236</span>
                                                                </td>
                                                                <td>
                                                                    <span id="ContentMain_gvDoingList_lblCreateDate_0">B</span>
                                                                </td>
                                                                <td>
                                                                    <span id="ContentMain_gvDoingList_lblCreateDate_0">15.00</span>
                                                                </td>
                                                                <td>
                                                                    <span id="ContentMain_gvDoingList_lblCreateDate_0">-</span>
                                                                </td>
                                                                <td><i class="fa fa-trash" data-original-title="delete" data-toggle="tooltip" aria-hidden="true"></i></td>
                                                            </tr>
                                                    </tbody>
                                                </table>
                                            </asp:Panel>

                                            <asp:Panel ID="show_selecteddetail_all" runat="server" Visible="false">
                                                <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" id="Content1" style="border-collapse: collapse;">
                                                    <tbody>
                                                        <tr class="info" style="font-size: Small; height: 40px;">
                                                            <th scope="col" style="width: 10%;">ชื่อตัวอย่าง</th>
                                                            <th scope="col" style="width: 10%;">Mat.</th>
                                                            <th scope="col" style="width: 10%;">MFD</th>
                                                            <th scope="col" style="width: 10%;">EXP</th>
                                                            <th scope="col" style="width: 10%;">Batch</th>
                                                            <th scope="col" style="width: 10%;">ชื่อลูกค้า</th>
                                                            <th scope="col" style="width: 10%;">เลขตู้</th>
                                                            <th scope="col" style="width: 10%;">กะ</th>
                                                            <th scope="col" style="width: 10%;">เวลา</th>
                                                            <th scope="col" style="width: 10%;">รายละเอียดอื่นๆ</th>
                                                            <th scope="col" style="width: 10%;">การจัดการ</th>


                                                            <tr style="font-size: Small;">
                                                                <td>
                                                                    <span id="ContentMain_gvDoingList_lblDocCode_0">BB 60g.</span>
                                                                </td>
                                                                <td>
                                                                    <span id="ContentMain_gvDoingList_lblDocCode_0">1202884</span>
                                                                </td>
                                                                <td>
                                                                    <span id="ContentMain_gvDoingList_lblDocCode_0">-</span>
                                                                </td>
                                                                <td>
                                                                    <span id="ContentMain_gvDoingList_lblDocCode_0">120817</span>
                                                                </td>
                                                                <td>
                                                                    <span id="ContentMain_gvDoingList_lblDocCode_0">T2</span>
                                                                </td>
                                                                <td>
                                                                    <span id="ContentMain_gvDoingList_lblDocCode_0">ทดสอบ</span>
                                                                </td>
                                                                <td>
                                                                    <span id="ContentMain_gvDoingList_lblDocCode_0">6361</span>
                                                                </td>
                                                                <td>
                                                                    <span id="ContentMain_gvDoingList_lblDocCode_0">A</span>
                                                                </td>
                                                                <td>
                                                                    <span id="ContentMain_gvDoingList_lblDocCode_0">8.30</span>
                                                                </td>
                                                                <td>
                                                                    <span id="ContentMain_gvDoingList_lblDocCode_0">ทดสอบตัวอย่าง</span>
                                                                </td>
                                                                <td><i class="fa fa-trash" data-original-title="delete" data-toggle="tooltip" aria-hidden="true"></i></td>

                                                            </tr>
                                                            <tr style="font-size: Small;">
                                                                <td>
                                                                    <span id="ContentMain_gvDoingList_lblDocCode_0">BR 3.6g.</span>
                                                                </td>
                                                                <td>
                                                                    <span id="ContentMain_gvDoingList_lblDocCode_0">1201407</span>
                                                                </td>
                                                                <td>
                                                                    <span id="ContentMain_gvDoingList_lblDocCode_0">13052016</span>
                                                                </td>
                                                                <td>
                                                                    <span id="ContentMain_gvDoingList_lblCreateDate_0">13052017</span>
                                                                </td>
                                                                <td>
                                                                    <span id="ContentMain_gvDoingList_lblCreateDate_0">T1</span>
                                                                </td>
                                                                <td>
                                                                    <span id="ContentMain_gvDoingList_lblCreateDate_0">ทดสอบ1</span>
                                                                </td>
                                                                <td>
                                                                    <span id="ContentMain_gvDoingList_lblCreateDate_0">6236</span>
                                                                </td>
                                                                <td>
                                                                    <span id="ContentMain_gvDoingList_lblCreateDate_0">B</span>
                                                                </td>
                                                                <td>
                                                                    <span id="ContentMain_gvDoingList_lblCreateDate_0">15.00</span>
                                                                </td>
                                                                <td>
                                                                    <span id="ContentMain_gvDoingList_lblCreateDate_0">-</span>
                                                                </td>
                                                                <td><i class="fa fa-trash" data-original-title="delete" data-toggle="tooltip" aria-hidden="true"></i></td>
                                                            </tr>
                                                    </tbody>
                                                </table>
                                            </asp:Panel>
                                        </div>
                                    </div>


                                    <hr />
                                    <div class="form-group">
                                        <div class="panel-heading f-bold">แผนกที่เห็นผลการตรวจสอบ</div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">เลือกองค์กร</label>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddl_orgidx" runat="server" CssClass="form-control" Enabled="true">
                                                <%--<asp:ListItem Text="กรุณาเลือก Lab ...." Value="00"></asp:ListItem>--%>
                                                <asp:ListItem Text=" --ทดสอบ-- " Value="00"></asp:ListItem>
                                                <%-- <asp:ListItem Text="ภายนอก" Value="8"></asp:ListItem>--%>
                                            </asp:DropDownList>
                                        </div>

                                        <label class="col-sm-2 control-label">เลือกฝ่าย</label>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddl_rdept" runat="server" CssClass="form-control" Enabled="true">
                                                <%--<asp:ListItem Text="กรุณาเลือก Lab ...." Value="00"></asp:ListItem>--%>
                                                <asp:ListItem Text=" --ทดสอบ-- " Value="00"></asp:ListItem>
                                                <%-- <asp:ListItem Text="ภายนอก" Value="8"></asp:ListItem>--%>
                                            </asp:DropDownList>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">เลือกแผนก</label>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddl_rsecidx" runat="server" CssClass="form-control" Enabled="true">
                                                <%--<asp:ListItem Text="กรุณาเลือก Lab ...." Value="00"></asp:ListItem>--%>
                                                <asp:ListItem Text=" --ทดสอบ-- " Value="00"></asp:ListItem>
                                                <%-- <asp:ListItem Text="ภายนอก" Value="8"></asp:ListItem>--%>
                                            </asp:DropDownList>
                                        </div>

                                        <label class="col-sm-2 control-label"></label>
                                        <div class="col-sm-4">

                                            <asp:LinkButton ID="btninsert_rdept" CssClass="btn btn-default" runat="server" CommandName="btninsert_rdept" OnCommand="btnCommand" ValidationGroup="Saveinsertdetail" title="เพิ่มแผนก"><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่ม</asp:LinkButton>

                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:Panel ID="show_detailselected_rdept" runat="server" Visible="false">
                                                <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" id="ContentMain_gvDoingList" style="border-collapse: collapse;">
                                                    <tbody>
                                                        <tr class="info" style="font-size: Small; height: 40px;">
                                                            <th scope="col" style="width: 10%;">องค์กร</th>
                                                            <th scope="col" style="width: 10%;">ฝ่าย</th>
                                                            <th scope="col" style="width: 10%;">แผนก</th>
                                                            <th scope="col" style="width: 10%;">การจัดการ</th>

                                                            <tr style="font-size: Small;">
                                                                <td>
                                                                    <span id="ContentMain_gvDoingList_lblDocCode_0">ทดสอบ ทดสอบ1</span>
                                                                </td>
                                                                <td>
                                                                    <span id="ContentMain_gvDoingList_lblCreateDate_0">ทดสอบ ทดสอบ1</span>
                                                                </td>
                                                                <td>
                                                                    <span id="ContentMain_gvDoingList_lblCreateDate_0">ทดสอบ ทดสอบ1</span>
                                                                </td>
                                                                <td><i class="fa fa-trash" data-original-title="delete" data-toggle="tooltip" aria-hidden="true"></i></td>

                                                            </tr>
                                                            <tr style="font-size: Small;">
                                                                <td>
                                                                    <span id="ContentMain_gvDoingList_lblDocCode_0">ทดสอบ ทดสอบ</span>
                                                                </td>
                                                                <td>
                                                                    <span id="ContentMain_gvDoingList_lblCreateDate_0">ทดสอบ ทดสอบ</span>
                                                                </td>
                                                                <td>
                                                                    <span id="ContentMain_gvDoingList_lblCreateDate_0">ทดสอบ ทดสอบ</span>
                                                                </td>
                                                                <td><i class="fa fa-trash" data-original-title="delete" data-toggle="tooltip" aria-hidden="true"></i></td>

                                                            </tr>
                                                    </tbody>
                                                </table>
                                            </asp:Panel>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <div class="pull-right">
                                                <asp:LinkButton ID="lbDocSave" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument="1,1,2,2,0"></asp:LinkButton>
                                                <asp:LinkButton ID="lbDocCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="Cancel" OnCommand="btnCommand" CommandName="cmdDocCancel"></asp:LinkButton>

                                                <%--<a href='<%=ResolveUrl("~/print-form") %>' class="btn btn-default" target="_blank"><i class="fa fa-print" aria-hidden="true" style="font-size: 14px;"></i>Print</a>--%>
                                            </div>
                                        </div>
                                    </div>

                                </asp:Panel>

                            </div>


                        </div>

                    </div>
                    </div>
                    <%--</div>--%>
                </InsertItemTemplate>
            </asp:FormView>

            <asp:FormView ID="fvDocDetail2" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">รายละเอียดรายการ</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Document No</label>
                                    <div class="col-sm-4 control-label textleft">
                                        QA600003
                                    </div>
                                    <label class="col-sm-2 control-label">ผู้ทำรายการ</label>
                                    <div class="col-sm-4 control-label textleft">
                                        ทดสอบ ทดสอบ
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">องค์กร</label>
                                    <div class="col-sm-4 control-label textleft">
                                        ทดสอบ ทดสอบ
                                    </div>
                                    <label class="col-sm-2 control-label">ฝ่าย</label>
                                    <div class="col-sm-4 control-label textleft">
                                        ทดสอบ ทดสอบ
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">แผนก</label>
                                    <div class="col-sm-4 control-label textleft">
                                        ทดสอบ ทดสอบ
                                    </div>
                                    <label class="col-sm-2 control-label">ตำแหน่ง</label>
                                    <div class="col-sm-4 control-label textleft">
                                        ทดสอบ ทดสอบ
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">รายการที่สั่งตรวจ</label>
                                    <div class="col-sm-4 control-label textleft">
                                        Total Plate Count(CFU/g),Moistue(%MC)
                                    </div>
                                    <label class="col-sm-6 control-label"></label>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <asp:LinkButton ID="lbDocCancel" CssClass="btn btn-default" runat="server" data-original-title="Back" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocCancel"><i class="fa fa-reply" aria-hidden="true"></i> Back</asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>

            <asp:FormView ID="fvDocDetail3" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <asp:HiddenField ID="hfNodeIDX" runat="server" Value="3" />
                    <asp:HiddenField ID="hfActorIDX" runat="server" Value="2" />
                    <asp:HiddenField ID="to_hfNodeIDX" runat="server" Value="4" />

                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">สถานที่ส่งตรวจวิเคราะห์ Admin(In)</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">


                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Sample Code</label>

                                    <div class="col-sm-4">
                                        <asp:TextBox ID="tbSampleCode" runat="server" CssClass="form-control" Text='7G07002' Enabled="false" />
                                    </div>
                                    <%-- <div class="col-sm-4 control-label textleft">
                                        7G07003
                                    </div>--%>
                                    <label class="col-sm-2 control-label">Tested Detail</label>

                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_tested_detail" runat="server" CssClass="form-control" Text='Total Plate Count(CFU/g)' Enabled="false" />
                                    </div>

                                    <%--<label class="col-sm-6 control-label"></label>--%>
                                </div>

                                <%-- <div class="form-group">
                                    <label class="col-sm-2 control-label">พิจารณารับงาน</label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="ddlrecieveWork" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" CssClass="form-control" Enabled="true">
                                            <asp:ListItem Text="รับงาน" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="แก้ไขเอกสาร" Value="2"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                  

                                </div>--%>

                                <%--   <asp:Panel ID="formSelectLab" runat="server" Visible="false">--%>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">เลือก Lab</label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="ddl_lab_in" runat="server" CssClass="form-control" Enabled="true">
                                            <asp:ListItem Text="ภายใน" Value="4"></asp:ListItem>
                                            <asp:ListItem Text="ภายนอก" Value="8"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <label class="col-sm-2 control-label">สถานที่</label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="ddl_place" runat="server" CssClass="form-control" Enabled="true">
                                            <asp:ListItem Text="RJN" Value="4"></asp:ListItem>
                                            <asp:ListItem Text="NPW" Value="5"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                </div>
                                <%--  </asp:Panel>--%>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Comment</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_comment_recieve" runat="server" CssClass="form-control" placeholder="กรอกความคิดเห็น ..." Enabled="true">
                                           
                                        </asp:TextBox>
                                    </div>

                                    <label class="col-sm-6 control-label"></label>


                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <asp:LinkButton ID="lbDocSave" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument="2,3,4,3,4"></asp:LinkButton>
                                        <asp:LinkButton ID="btnDontSave" CssClass="btn btn-success" runat="server" Visible="false" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument="2,3,1,3,29"></asp:LinkButton>
                                        <asp:LinkButton ID="lbDocCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="Cancel" OnCommand="btnCommand" CommandName="cmdDocCancel"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>

            <asp:FormView ID="fvDocDetail3_Out" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <asp:HiddenField ID="hfNodeIDX" runat="server" Value="3" />
                    <asp:HiddenField ID="hfActorIDX" runat="server" Value="2" />
                    <asp:HiddenField ID="to_hfNodeIDX" runat="server" Value="8" />

                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">สถานที่ส่งตรวจวิเคราะห์ Admin(Out)</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Sample Code</label>

                                    <div class="col-sm-4">
                                        <asp:TextBox ID="tbSampleCodeOut" runat="server" CssClass="form-control" Text='7G07004' Enabled="false" />
                                    </div>

                                    <label class="col-sm-2 control-label">Tested Detail</label>

                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_tested_detailout" runat="server" CssClass="form-control" Text='Moistue(%MC)' Enabled="false" />
                                    </div>

                                    <%-- <div class="col-sm-4 control-label textleft">
                                        7G07003
                                    </div>--%>
                                    <%--<label class="col-sm-6 control-label"></label>--%>
                                </div>


                                <div class="form-group">
                                    <label class="col-sm-2 control-label">เลือก Lab</label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="ddl_lab_out" runat="server" CssClass="form-control" Enabled="true">
                                            <asp:ListItem Text="ภายนอก" Value="8"></asp:ListItem>
                                            <asp:ListItem Text="ภายใน" Value="4"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <label class="col-sm-2 control-label">สถานที่</label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="ddl_place_out" runat="server" CssClass="form-control" Enabled="true">
                                            <asp:ListItem Text="ทดสอบภายนอก" Value="8"></asp:ListItem>
                                            <asp:ListItem Text="ทดสอบภายนอก-1" Value="9"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">แนบไฟล์</label>
                                    <div class="col-sm-4 control-label">
                                        <%--<div class="form-group">--%>
                                        <asp:FileUpload ID="upload_file_material" runat="server" />
                                        <%--  </div>--%>
                                    </div>

                                    <label class="col-sm-6 control-label"></label>
                                </div>

                                <%-- <div class="form-group">
                                    <label class="col-sm-2 control-label">สถานที่</label>
                                    <div class="col-sm-4 control-label textleft">
                                        BKK
                                    </div>
                                    <label class="col-sm-2 control-label">อาคาร</label>
                                    <div class="col-sm-4 control-label textleft">
                                        -
                                    </div>
                                </div>--%>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <asp:LinkButton ID="lbDocSave" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument="2,3,8,1,8"></asp:LinkButton>
                                        <asp:LinkButton ID="lbDocCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="Cancel" OnCommand="btnCommand" CommandName="cmdDocCancel"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>

            <asp:FormView ID="fvDocDetail3_OutUserDecision" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <asp:HiddenField ID="hfNodeIDX" runat="server" Value="8" />
                    <asp:HiddenField ID="hfActorIDX" runat="server" Value="1" />
                    <asp:HiddenField ID="to_hfNodeIDX" runat="server" Value="9" />
                    <asp:HiddenField ID="to_hfActorIDX" runat="server" Value="2" />
                    <asp:HiddenField ID="hfDocisionIDX" runat="server" Value="1" />

                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">สถานที่ส่งตรวจวิเคราะห์ User</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Lab</label>

                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_labout_place" runat="server" CssClass="form-control" Text='ภายนอก' Enabled="false" />
                                    </div>

                                    <label class="col-sm-2 control-label">สถานที่</label>

                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_place_place" runat="server" CssClass="form-control" Text='BKK' Enabled="false" />
                                    </div>



                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Sample Code</label>

                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_samplecode_place" runat="server" CssClass="form-control" Text='7G07004' Enabled="false" />
                                    </div>

                                    <label class="col-sm-2 control-label">Tested Detail</label>

                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_testeddetail_place" runat="server" CssClass="form-control" Text='Moistue(%MC)' Enabled="false" />
                                    </div>



                                </div>

                                <%--<div class="form-group">
                                    <label class="col-sm-2 control-label">สถานที่</label>
                                    <div class="col-sm-4 control-label textleft">
                                        BKK
                                    </div>
                                    <label class="col-sm-2 control-label">อาคาร</label>
                                    <div class="col-sm-4 control-label textleft">
                                        -
                                    </div>
                                </div>--%>

                                <div class="form-group">
                                    <%-- <label class="col-sm-2 control-label">ยืนยัน</label>
                                    <div class="col-sm-4 control-label textleft">
                                        อนุมัติ
                                    </div>--%>

                                    <label class="col-sm-2 control-label">ยืนยัน</label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="ddl_approveuser_out" runat="server" AutoPostBack="true" CssClass="form-control" Enabled="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                            <asp:ListItem Text="อนุมัติ" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="ไม่อนุมัติ" Value="2"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <label class="col-sm-2 control-label">Comment</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_comment_userout" runat="server" CssClass="form-control" placeholder="กรอกความคิดเห็น ..." Enabled="true">
                                           
                                        </asp:TextBox>
                                    </div>

                                    <%--<label class="col-sm-2 control-label">Comment</label>
                                    <div class="col-sm-4 control-label textleft">
                                        -
                                    </div>--%>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <asp:LinkButton ID="lbDocSave" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument="1,8,9,2,1"></asp:LinkButton>
                                        <asp:LinkButton ID="lbDocNoSave" CssClass="btn btn-success" Visible="false" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save1" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument="1,8,10,2,2"></asp:LinkButton>
                                        <asp:LinkButton ID="lbDocCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="Cancel" OnCommand="btnCommand" CommandName="cmdDocCancel"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>

            <asp:FormView ID="fvDocDetail3_OutAdmin" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <asp:HiddenField ID="hfNodeIDX" runat="server" Value="9" />
                    <asp:HiddenField ID="hfActorIDX" runat="server" Value="2" />
                    <asp:HiddenField ID="to_hfNodeIDX" runat="server" Value="7" />
                    <asp:HiddenField ID="to_hfActorIDX" runat="server" Value="5" />
                    <asp:HiddenField ID="hfDocisionIDX" runat="server" Value="1" />

                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">สถานที่ส่งตรวจวิเคราะห์ Admin Send Out</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Lab</label>

                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_labout_place" runat="server" CssClass="form-control" Text='ภายนอก' Enabled="false" />
                                    </div>

                                    <label class="col-sm-2 control-label">สถานที่</label>

                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_place_place" runat="server" CssClass="form-control" Text='BKK' Enabled="false" />
                                    </div>



                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Sample Code</label>

                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_samplecode_place" runat="server" CssClass="form-control" Text='7G07004' Enabled="false" />
                                    </div>

                                    <label class="col-sm-2 control-label">Tested Detail</label>

                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_testeddetail_place" runat="server" CssClass="form-control" Text='Moistue(%MC)' Enabled="false" />
                                    </div>


                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ผลการยืนยัน(user)</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_approveuser_out" runat="server" CssClass="form-control" Text='อนุมัติ' Enabled="false" />
                                    </div>
                                    <%-- <div class="col-sm-4 control-label textleft">
                                        อนุมัติ
                                    </div>--%>
                                    <label class="col-sm-2 control-label">Comment</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_comment_userout" runat="server" CssClass="form-control" Text='-' Enabled="false" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <asp:LinkButton ID="lbDocSave" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument="2,9,11,2,0"></asp:LinkButton>
                                        <asp:LinkButton ID="lbDocCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="Cancel" OnCommand="btnCommand" CommandName="cmdDocCancel"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>

            <asp:FormView ID="fvInsert_Result_LabIn" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <%-- <asp:HiddenField ID="hfNodeIDX" runat="server" Value="4" />
                    <asp:HiddenField ID="hfActorIDX" runat="server" Value="3" />
                    <asp:HiddenField ID="hfDocisionIDX" runat="server" Value="1" />
                    <asp:HiddenField ID="to_hfNodeIDX" runat="server" Value="5" />
                    <asp:HiddenField ID="to_hfActorIDX" runat="server" Value="3" />--%>

                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">ผลการวิเคราะห์ตัวอย่าง</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                                    <tbody>
                                        <tr class="info" style="font-size: Small; height: 40px;">
                                            <th scope="col" style="width: 15%;">AC</th>
                                            <th scope="col" style="width: 15%;">Replication : 1</th>
                                            <th scope="col" style="width: 15%;">Replication : 2</th>
                                            <th scope="col" style="width: 15%;">average</th>

                                        </tr>


                                        <tr style="font-size: Small;">
                                            <td>fold dilution</td>
                                            <td>-</td>
                                            <td>-</td>
                                            <td>-</td>


                                        </tr>

                                        <tr style="font-size: Small;">
                                            <td>10 fold dilution</td>
                                            <td>15</td>
                                            <td>13</td>
                                            <td>14</td>
                                        </tr>

                                        <tr style="font-size: Small;">
                                            <td>100 fold dilution</td>
                                            <td>1</td>
                                            <td>1</td>
                                            <td>1</td>
                                        </tr>

                                        <tr style="font-size: Small;">
                                            <td>1000 fold dilution</td>
                                            <td>-</td>
                                            <td>-</td>
                                            <td>-</td>
                                        </tr>


                                    </tbody>
                                </table>


                                <%--<label class="col-sm-12 control-label">average value 14 * dilution rate 10 = numerical value of AC 140  CFU/g</label>--%>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>

            <asp:FormView ID="fvInsert_Result_LabOut" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <%-- <asp:HiddenField ID="hfNodeIDX" runat="server" Value="4" />
                    <asp:HiddenField ID="hfActorIDX" runat="server" Value="3" />
                    <asp:HiddenField ID="hfDocisionIDX" runat="server" Value="1" />
                    <asp:HiddenField ID="to_hfNodeIDX" runat="server" Value="5" />
                    <asp:HiddenField ID="to_hfActorIDX" runat="server" Value="3" />--%>

                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">Moisture(%)</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">

                                    <label class="col-sm-2 control-label">Moisture(%)</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_result" runat="server" CssClass="form-control" Text="2.45" Enabled="true"></asp:TextBox>
                                        <%--7G07001--%>
                                    </div>

                                    <label class="col-sm-6 control-label"></label>
                                    <%-- <div class="col-sm-4">
                                    <asp:TextBox ID="txt_tested_detail" runat="server" CssClass="form-control" Text='M1-1' Enabled="false" />
                                </div>--%>
                                </div>
                            </div>

                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>

            <asp:FormView ID="fvDocInsertAnalysis" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <asp:HiddenField ID="hfNodeIDX" runat="server" Value="4" />
                    <asp:HiddenField ID="hfActorIDX" runat="server" Value="3" />
                    <asp:HiddenField ID="hfDocisionIDX" runat="server" Value="1" />
                    <asp:HiddenField ID="to_hfNodeIDX" runat="server" Value="5" />
                    <asp:HiddenField ID="to_hfActorIDX" runat="server" Value="3" />

                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">รายละเอียดรายการรับงาน(Lab In รับงาน)</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Sample Code</label>
                                    <div class="col-sm-4">

                                        <asp:TextBox ID="txt_samplecode_show" runat="server" CssClass="form-control" Text="7G07002" Enabled="false"></asp:TextBox>

                                    </div>

                                    <label class="col-sm-2 control-label">Tested Detail</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_tested_detail" runat="server" CssClass="form-control" Text="Total Plate Count(CFU/g)" Enabled="false"></asp:TextBox>

                                    </div>


                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ตรวจสอบสภาพตัวอย่าง</label>
                                    <%--<div class="col-sm-4 control-label textleft">
                                        ปกติ
                                    </div>--%>

                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="ddl_approve_received" runat="server" AutoPostBack="true" CssClass="form-control" Enabled="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                            <%--<asp:ListItem Text="เลือกสถานะ" Value="0"></asp:ListItem>--%>
                                            <asp:ListItem Text="Acept" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Reject" Value="2"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <label class="col-sm-6 control-label"></label>

                                    <%-- <label class="col-sm-2 control-label">Comment</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_comment_receive_lab" runat="server" CssClass="form-control" placeHolder="กรอกความคิดเห็น ..." Enabled="true"></asp:TextBox>

                                    </div>--%>
                                </div>

                                <asp:Panel ID="Show_comment_noaccept" runat="server" Visible="false">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">เหตุผล</label>
                                        <div class="col-sm-4">
                                            <%--<asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" placeHolder="กรอกความคิดเห็น ..." Enabled="true"></asp:TextBox>--%>
                                            <asp:TextBox ID="txtcomment" CssClass="form-control" TextMode="MultiLine" Rows="3" placeHolder="มีการฉีกขาด" runat="server">
                                           
                                            </asp:TextBox>
                                        </div>

                                    </div>
                                </asp:Panel>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <asp:LinkButton ID="lbDocSave" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument="3,4,5,3,1"></asp:LinkButton>
                                        <asp:LinkButton ID="lbDocNoSave" CssClass="btn btn-success" Visible="false" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save1" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument="3,4,3,2,2"></asp:LinkButton>
                                        <asp:LinkButton ID="lbDocCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="Cancel" OnCommand="btnCommand" CommandName="cmdDocCancel"></asp:LinkButton>
                                    </div>
                                </div>

                                <%--<div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        
                                        <asp:LinkButton ID="lbNoCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="Cancel1" OnCommand="btnCommand" CommandName="cmdDocCancel"></asp:LinkButton>
                                    </div>
                                </div>--%>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>

            <asp:FormView ID="fvDocInsertAnalysisResult" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <asp:HiddenField ID="hfNodeIDX" runat="server" Value="5" />
                    <asp:HiddenField ID="hfActorIDX" runat="server" Value="3" />

                    <asp:HiddenField ID="to_hfNodeIDX" runat="server" Value="6" />
                    <asp:HiddenField ID="to_hfActorIDX" runat="server" Value="4" />

                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">กรอกผลการวิเคราะห์(In)</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">

                                    <label class="col-sm-2 control-label">Sample Code</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_samplecode_result" runat="server" CssClass="form-control" Text="7G07001" Enabled="false"></asp:TextBox>
                                        <%--7G07001--%>
                                    </div>

                                    <label class="col-sm-2 control-label">Tested Detail</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_tested_detail" runat="server" CssClass="form-control" Text='Total Plate Count(CFU/g)' Enabled="false" />
                                    </div>


                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Received Date</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_received_result" runat="server" CssClass="form-control" Text="07/07/2017" Enabled="false"></asp:TextBox>

                                    </div>

                                    <label class="col-sm-2 control-label">Tested Date</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_tested_result" runat="server" CssClass="form-control" Text="07/07/2017" Enabled="false"></asp:TextBox>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ผลการตรวจสอบ</label>
                                    <div class="col-sm-10">
                                        <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                                            <tbody>
                                                <tr class="info" style="font-size: Small; height: 40px;">
                                                    <th scope="col" style="width: 15%;">AC</th>
                                                    <th scope="col" style="width: 15%;">Replication : 1</th>
                                                    <th scope="col" style="width: 15%;">Replication : 2</th>
                                                    <th scope="col" style="width: 15%;">average</th>

                                                </tr>


                                                <tr style="font-size: Small;">
                                                    <td>fold dilution</td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox34" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox35" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox36" runat="server"></asp:TextBox></td>


                                                </tr>

                                                <tr style="font-size: Small;">
                                                    <td>10 fold dilution</td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox6" runat="server"></asp:TextBox></td>
                                                </tr>

                                                <tr style="font-size: Small;">
                                                    <td>100 fold dilution</td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox7" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox10" runat="server"></asp:TextBox></td>
                                                    <td>
                                                        <asp:TextBox ID="TextBox11" runat="server"></asp:TextBox></td>
                                                </tr>

                                                <tr style="font-size: Small;">
                                                    <td>1000 fold dilution</td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                </tr>


                                            </tbody>
                                        </table>
                                    </div>
                                </div>





                                <%--<div class="form-group">
                                    <label class="col-sm-2 control-label">ผลการตรวจสอบ</label>
                                   

                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_result" runat="server" CssClass="form-control" Text='ปกติ' Enabled="true" />
                                    </div>

                                    <label class="col-sm-2 control-label">Comment</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_comment_result" runat="server" CssClass="form-control" placeHolder="กรอกความคิดเห็น ..." Enabled="true"></asp:TextBox>

                                    </div>

                                   
                                </div>--%>

                                <div class="form-group">
                                    <%--<label class="col-sm-2 control-label"></label>--%>
                                    <div class="col-sm-12">
                                        <div class="pull-right">
                                            <asp:LinkButton ID="lbDocSave" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument="3,5,6,4,1"></asp:LinkButton>
                                            <asp:LinkButton ID="lbDocCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="Cancel" OnCommand="btnCommand" CommandName="cmdDocCancel"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>

            <asp:FormView ID="fvDocInsertAnalysisResultOut" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <asp:HiddenField ID="hfNodeIDX" runat="server" Value="5" />
                    <asp:HiddenField ID="hfActorIDX" runat="server" Value="3" />

                    <asp:HiddenField ID="to_hfNodeIDX" runat="server" Value="6" />
                    <asp:HiddenField ID="to_hfActorIDX" runat="server" Value="4" />

                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">กรอกผลการวิเคราะห์(Out)</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">


                                <div class="form-group">

                                    <label class="col-sm-2 control-label">Sample Code</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_samplecode_result" runat="server" CssClass="form-control" Text="7G07004" Enabled="false"></asp:TextBox>
                                        <%--7G07001--%>
                                    </div>

                                    <label class="col-sm-2 control-label">Tested Detail</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_tested_detail" runat="server" CssClass="form-control" Text='Moistue(%MC)' Enabled="false" />
                                    </div>


                                </div>

                                <div class="form-group">

                                    <label class="col-sm-2 control-label">Received Date</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_received_date" runat="server" CssClass="form-control" Enabled="false" placeHolder="07/07/2017" />
                                    </div>

                                    <label class="col-sm-6 control-label"></label>

                                    <%--<label class="col-sm-2 control-label">ผลการตรวจสอบ</label>--%>
                                    <%-- <div class="col-sm-4">
                                        <asp:TextBox ID="txt_comment_resultout" runat="server" CssClass="form-control" Enabled="true" placeHolder="กรอกผลการตรวจสอบ" />
                                    </div>--%>
                                </div>

                                <div class="form-group">

                                    <label class="col-sm-2 control-label">Moisture(%)</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_result" runat="server" CssClass="form-control" Text="2.45" Enabled="true"></asp:TextBox>
                                        <%--7G07001--%>
                                    </div>

                                    <label class="col-sm-6 control-label"></label>
                                    <%-- <div class="col-sm-4">
                                    <asp:TextBox ID="txt_tested_detail" runat="server" CssClass="form-control" Text='M1-1' Enabled="false" />
                                </div>--%>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <asp:LinkButton ID="lbDocSave" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument="4,6,10,2,1"></asp:LinkButton>
                                        <asp:LinkButton ID="lbDocCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="Cancel" OnCommand="btnCommand" CommandName="cmdDocCancel"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>

            <asp:FormView ID="fvDocInsertApprove" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <asp:HiddenField ID="hfNodeIDX" runat="server" Value="6" />
                    <asp:HiddenField ID="hfActorIDX" runat="server" Value="4" />
                    <asp:HiddenField ID="hfDocisionIDX" runat="server" Value="1" />
                    <asp:HiddenField ID="to_hfNodeIDX" runat="server" Value="10" />
                    <asp:HiddenField ID="to_hfActorIDX" runat="server" Value="2" />

                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">ตรวจสอบผล(Supervisor)</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Sample Code</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_samplecode_invest" runat="server" CssClass="form-control" Text="7G07001" Enabled="false"></asp:TextBox>
                                        <%-- 7G07001--%>
                                    </div>

                                    <label class="col-sm-2 control-label">Tested Detail</label>

                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_tested_detail" runat="server" CssClass="form-control" Text='Total Plate Count(CFU/g)' Enabled="false" />
                                    </div>


                                </div>

                                <div class="form-group">

                                    <label class="col-sm-2 control-label">Received Date</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_received_invest" runat="server" CssClass="form-control" Text="07/07/2017" Enabled="false"></asp:TextBox>

                                    </div>

                                    <label class="col-sm-2 control-label">Tested Date</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_datetest_invest" runat="server" CssClass="form-control" Text="11/07/2017" Enabled="false"></asp:TextBox>

                                    </div>

                                </div>

                                <%-- <div class="form-group">
                                    <label class="col-sm-2 control-label">ผลการตรวจสอบ</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_result_invest" runat="server" CssClass="form-control" Text="ปกติ" Enabled="false"></asp:TextBox>

                                    </div>

                                    <label class="col-sm-6 control-label"></label>

                                </div>--%>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ผลการอนุมัติ</label>
                                    <div class="col-sm-4">

                                        <asp:DropDownList ID="ddl_approve_invest" runat="server" AutoPostBack="true" CssClass="form-control" Enabled="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                            <asp:ListItem Text="อนุมัติ" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="ไม่อนุมัติ" Value="2"></asp:ListItem>
                                        </asp:DropDownList>



                                    </div>
                                    <label class="col-sm-2 control-label">Comment</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_comment_invest" runat="server" CssClass="form-control" placeHolder="กรอกความคิดเห็น ..." Enabled="true"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <asp:LinkButton ID="lbDocSave" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument="4,6,10,2,1"></asp:LinkButton>
                                        <asp:LinkButton ID="lbDocNoSave" CssClass="btn btn-success" Visible="false" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save1" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument="4,6,5,3,2"></asp:LinkButton>
                                        <asp:LinkButton ID="lbDocCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="Cancel" OnCommand="btnCommand" CommandName="cmdDocCancel"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>

            <asp:FormView ID="fvDocDetail12" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">รายละเอียดรายการ</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Document No</label>
                                    <div class="col-sm-4 control-label textleft">
                                        QA600005
                                    </div>
                                    <label class="col-sm-2 control-label">ผู้ทำรายการ</label>
                                    <div class="col-sm-4 control-label textleft">
                                        ทดสอบ ทดสอบ
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">องค์กร</label>
                                    <div class="col-sm-4 control-label textleft">
                                        ทดสอบ ทดสอบ
                                    </div>
                                    <label class="col-sm-2 control-label">ฝ่าย</label>
                                    <div class="col-sm-4 control-label textleft">
                                        ทดสอบ ทดสอบ
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">แผนก</label>
                                    <div class="col-sm-4 control-label textleft">
                                        ทดสอบ ทดสอบ
                                    </div>
                                    <label class="col-sm-2 control-label">ตำแหน่ง</label>
                                    <div class="col-sm-4 control-label textleft">
                                        ทดสอบ ทดสอบ
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">รายการที่สั่งตรวจ</label>
                                    <div class="col-sm-4 control-label textleft">
                                        ทดสอบ ทดสอบ
                                    </div>
                                    <label class="col-sm-6 control-label"></label>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <asp:LinkButton ID="lbDocCancel" CssClass="btn btn-default" runat="server" data-original-title="Back" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocCancel"><i class="fa fa-reply" aria-hidden="true"></i> Back</asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>

            <asp:FormView ID="fvDocDetail10" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">รายละเอียด Sample List</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Sample Code</label>
                                    <div class="col-sm-4 control-label textleft">
                                        7G07001
                                    </div>
                                    <label class="col-sm-2 control-label">Received Date</label>
                                    <div class="col-sm-4 control-label textleft">
                                        07/07/2017
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Tested Date</label>
                                    <div class="col-sm-4 control-label textleft">
                                        07/07/2017
                                    </div>
                                    <label class="col-sm-2 control-label">Lab</label>
                                    <div class="col-sm-4 control-label textleft">
                                        NPW
                                    </div>
                                </div>
                                <hr />
                                <%-- <div class="form-group">
                                    <label class="col-sm-2 control-label">Lab Name</label>
                                    <div class="col-sm-4 control-label textleft">
                                        Chemical
                                    </div>--%>
                                <%--<label class="col-sm-2 control-label">Unit</label>
                                    <div class="col-sm-4 control-label textleft">
                                       Testing
                                    </div>--%>
                                <%--</div>--%>

                                <%-- <div class="form-group">
                                    <label class="col-sm-2 control-label">Result</label>
                                    <div class="col-sm-4 control-label textleft">
                                        Completed
                                    </div>
                                    <label class="col-sm-2 control-label">Unit</label>
                                    <div class="col-sm-4 control-label textleft">
                                        Testing
                                    </div>
                                </div>--%>
                                <%--<hr />--%>
                                <%--<div class="form-group">
                                    <label class="col-sm-2 control-label">Lab Name</label>
                                    <div class="col-sm-4 control-label textleft">
                                        Microbiological
                                    </div>--%>
                                <%--<label class="col-sm-2 control-label">Unit</label>
                                    <div class="col-sm-4 control-label textleft">
                                       Testing
                                    </div>--%>
                                <%-- </div>--%>

                                <%--<div class="form-group">
                                    <label class="col-sm-2 control-label">Result</label>
                                    <div class="col-sm-4 control-label textleft">
                                        ok
                                    </div>
                                    <label class="col-sm-2 control-label">Unit</label>
                                    <div class="col-sm-4 control-label textleft">
                                        10 unit
                                    </div>
                                </div>--%>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>

            <asp:FormView ID="fvDocDetail12_report" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">รายละเอียดรายการ</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Sample Code</label>
                                    <div class="col-sm-4 control-label textleft">
                                        7G07005
                                    </div>
                                    <label class="col-sm-2 control-label">Received Date</label>
                                    <div class="col-sm-4 control-label textleft">
                                        10/07/2017
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Tested Date</label>
                                    <div class="col-sm-4 control-label textleft">
                                        11/07/2017
                                    </div>
                                    <label class="col-sm-2 control-label">Lab</label>
                                    <div class="col-sm-4 control-label textleft">
                                        NPW
                                    </div>
                                </div>
                                <hr />
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Lab Name</label>
                                    <div class="col-sm-4 control-label textleft">
                                        Chemical
                                    </div>
                                    <%--<label class="col-sm-2 control-label">Unit</label>
                                    <div class="col-sm-4 control-label textleft">
                                       Testing
                                    </div>--%>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Result</label>
                                    <div class="col-sm-4 control-label textleft">
                                        Completed
                                    </div>
                                    <label class="col-sm-2 control-label">Unit</label>
                                    <div class="col-sm-4 control-label textleft">
                                        Testing
                                    </div>
                                </div>
                                <hr />
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Lab Name</label>
                                    <div class="col-sm-4 control-label textleft">
                                        Microbiological
                                    </div>
                                    <%--<label class="col-sm-2 control-label">Unit</label>
                                    <div class="col-sm-4 control-label textleft">
                                       Testing
                                    </div>--%>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Result</label>
                                    <div class="col-sm-4 control-label textleft">
                                        ok
                                    </div>
                                    <label class="col-sm-2 control-label">Unit</label>
                                    <div class="col-sm-4 control-label textleft">
                                        10 unit
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>

            <!--document info-->

            <!--sample list-->
            <div class="panel panel-success" id="sampleList" runat="server">
                <div class="panel-heading">
                    <h3 class="panel-title">Sample list</h3>
                </div>
                <div class="panel-body">

                    <div class="form-horizontal" role="form">
                        <div class="col-sm-12">
                            <div class="form-group">


                                <a href='<%=ResolveUrl("~/print-samplecode-lab") %>' class="btn btn-default" target="_blank"><span style="font-size: 14px;" class="fa fa-print">Print</span></a>

                                <asp:LinkButton ID="export_all" runat="server" Font-Size="Small" class="btn btn-success"> <span style="font-size: 14px;" class="fa fa-file-excel-o"> Export</span></asp:LinkButton>

                            </div>
                        </div>
                    </div>

                    <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                        <tbody>
                            <tr class="info" style="font-size: Small; height: 40px;">
                                <th scope="col" style="width: 10%;">Sample Name</th>
                                <th scope="col" style="width: 10%;">Sample Code</th>
                                <th scope="col" style="width: 20%;">Tested Detail</th>
                                <th scope="col" style="width: 10%;">Received Date</th>
                                <th scope="col" style="width: 10%;">Tested Date</th>
                                <th scope="col" style="width: 10%;">Lab</th>
                                <th scope="col" style="width: 20%;">Status</th>
                                <th scope="col" style="width: 10%;">Action</th>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>BB 60g.</td>
                                <td>7G07001</td>
                                <td>Total Plate Count(CFU/g)</td>
                                <td>07/07/2017</td>
                                <td>07/07/2017</td>
                                <td>NPW</td>
                                <td>Ongoing</td>
                                <td>
                                    <asp:LinkButton ID="lbSampleResult" CssClass="btn-sm btn-default" runat="server" data-original-title="Sample Result" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument="3,5,6,4,0"><i class="fa fa-pencil" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>BB 60g.</td>
                                <td>7G07001</td>
                                <td>Moistue(%MC)</td>
                                <td>07/07/2017</td>
                                <td>10/07/2017</td>
                                <td>External Lab</td>
                                <td>Completed</td>
                                <td>
                                    <asp:LinkButton ID="lbSampleView" CssClass="btn-sm btn-default" runat="server" data-original-title="View" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument=""><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>BR 3.6g.</td>
                                <td>7G07002</td>
                                <td>Total Plate Count(CFU/g)</td>
                                <td>07/07/2017</td>
                                <td>-</td>
                                <td>-</td>
                                <td>Waiting Admin(In)</td>
                                <td>
                                    <asp:LinkButton ID="lbSampleReceive" CssClass="btn-sm btn-default" runat="server" data-original-title="Receive" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument="2,3,4,3,0"><i class="fa fa-sign-in" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>BR 3.6g.</td>
                                <td>7G07002</td>
                                <td>Moistue(%MC)</td>
                                <td>07/07/2017</td>
                                <td>-</td>
                                <td>-</td>
                                <td>Waiting Admin(Out)</td>
                                <td>
                                    <asp:LinkButton ID="lbSampleReceive_Out" CssClass="btn-sm btn-default" runat="server" data-original-title="Receive" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument="2,3,8,1,0"><i class="fa fa-sign-in" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!--sample list-->

            <!--sample Receive Internal Lab list-->
            <div class="panel panel-success" id="sampleBeforeRecieve" runat="server">
                <div class="panel-heading">
                    <h3 class="panel-title">Sample list</h3>
                </div>
                <div class="panel-body">

                    <div class="form-horizontal" role="form">
                        <div class="col-sm-12">
                            <div class="form-group">


                                <a href='<%=ResolveUrl("~/print-samplecode-lab") %>' class="btn btn-default" target="_blank"><span style="font-size: 14px;" class="fa fa-print">Print</span></a>
                                <asp:LinkButton ID="LinkButton45" runat="server" Font-Size="Small" class="btn btn-success"> <span style="font-size: 14px;" class="fa fa-file-excel-o"> Export</span></asp:LinkButton>
                            </div>
                        </div>
                    </div>


                    <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                        <tbody>
                            <tr class="info" style="font-size: Small; height: 40px;">
                                <th scope="col" style="width: 10%;">Sample Name</th>
                                <th scope="col" style="width: 10%;">Sample Code</th>
                                <th scope="col" style="width: 20%;">Tested Detail</th>
                                <th scope="col" style="width: 10%;">Received Date</th>
                                <th scope="col" style="width: 10%;">Tested Date</th>
                                <th scope="col" style="width: 10%;">Lab</th>
                                <th scope="col" style="width: 20%;">Status</th>
                                <th scope="col" style="width: 10%;">Action</th>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>BB 60g.</td>
                                <td>7G07001</td>
                                <td>Total Plate Count(CFU/g)</td>
                                <td>07/07/2017</td>
                                <td>07/07/2017</td>
                                <td>NPW</td>
                                <td>Ongoing</td>
                                <td>
                                    <asp:LinkButton ID="LinkButton10" CssClass="btn-sm btn-default" runat="server" data-original-title="Sample Result" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument="3,5,6,4,0"><i class="fa fa-pencil" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>BB 60g.</td>
                                <td>7G07001</td>
                                <td>Moistue(%MC)</td>
                                <td>07/07/2017</td>
                                <td>10/07/2017</td>
                                <td>External Lab</td>
                                <td>Completed</td>
                                <td>
                                    <asp:LinkButton ID="LinkButton11" CssClass="btn-sm btn-default" runat="server" data-original-title="View" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocEvent" CommandArgument=""><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>BR 3.6g.</td>
                                <td>7G07002</td>
                                <td>Total Plate Count(CFU/g)</td>
                                <td>07/07/2017</td>
                                <td>-</td>
                                <td>-</td>
                                <td>Waiting for Internal Lab</td>
                                <td>
                                    <asp:LinkButton ID="LinkButton12" CssClass="btn-sm btn-default" runat="server" data-original-title="Receive for Internal Lab" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocEvent" CommandArgument="5"><i class="fa fa-pencil" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>BR 3.6g.</td>
                                <td>7G07002</td>
                                <td>Moistue(%MC)</td>
                                <td>07/07/2017</td>
                                <td>-</td>
                                <td>-</td>
                                <td>Waiting Admin(Out)</td>
                                <td>
                                    <asp:LinkButton ID="LinkButton13" CssClass="btn-sm btn-default" runat="server" data-original-title="Receive" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument="2,3,8,1,0"><i class="fa fa-sign-in" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!--sample Receive Internal Lab list-->

            <!--sample Receive Internal Lab list-->
            <div class="panel panel-success" id="sampleRecieve" runat="server">
                <div class="panel-heading">
                    <h3 class="panel-title">Sample list</h3>
                </div>
                <div class="panel-body">

                    <div class="form-horizontal" role="form">
                        <div class="col-sm-12">
                            <div class="form-group">


                                <a href='<%=ResolveUrl("~/print-samplecode-lab") %>' class="btn btn-default" target="_blank"><span style="font-size: 14px;" class="fa fa-print">Print</span></a>
                                <asp:LinkButton ID="LinkButton46" runat="server" Font-Size="Small" class="btn btn-success"> <span style="font-size: 14px;" class="fa fa-file-excel-o"> Export</span></asp:LinkButton>
                            </div>
                        </div>
                    </div>
                    <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                        <tbody>
                            <tr class="info" style="font-size: Small; height: 40px;">
                                <th scope="col" style="width: 10%;">Sample Name</th>
                                <th scope="col" style="width: 10%;">Sample Code</th>
                                <th scope="col" style="width: 20%;">Tested Detail</th>
                                <th scope="col" style="width: 10%;">Received Date</th>
                                <th scope="col" style="width: 10%;">Tested Date</th>
                                <th scope="col" style="width: 10%;">Lab</th>
                                <th scope="col" style="width: 20%;">Status</th>
                                <th scope="col" style="width: 10%;">Action</th>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>BB 60g.</td>
                                <td>7G07001</td>
                                <td>Total Plate Count(CFU/g)</td>
                                <td>07/07/2017</td>
                                <td>07/07/2017</td>
                                <td>NPW</td>
                                <td>Ongoing</td>
                                <td>
                                    <asp:LinkButton ID="lbSampleReciveResult" CssClass="btn-sm btn-default" runat="server" data-original-title="Sample Result" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument="3,5,6,4,0"><i class="fa fa-pencil" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>BB 60g.</td>
                                <td>7G07001</td>
                                <td>Moistue(%MC)</td>
                                <td>07/07/2017</td>
                                <td>10/07/2017</td>
                                <td>External Lab</td>
                                <td>Completed</td>
                                <td>
                                    <asp:LinkButton ID="lbSampleReciveView" CssClass="btn-sm btn-default" runat="server" data-original-title="View" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocEvent" CommandArgument=""><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>BR 3.6g.</td>
                                <td>7G07002</td>
                                <td>Total Plate Count(CFU/g)</td>
                                <td>07/07/2017</td>
                                <td>11/07/2017</td>
                                <td>RJN</td>
                                <td>Ongoing</td>
                                <td>
                                    <asp:LinkButton ID="lbSampleReciveResult_1" CssClass="btn-sm btn-default" runat="server" data-original-title="Sample Result" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument="3,5,6,4,0"><i class="fa fa-pencil" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>BR 3.6g.</td>
                                <td>7G07002</td>
                                <td>Moistue(%MC)</td>
                                <td>07/07/2017</td>
                                <td>-</td>
                                <td>-</td>
                                <td>Waiting Admin(Out)</td>
                                <td>
                                    <asp:LinkButton ID="LinkButton24" CssClass="btn-sm btn-default" runat="server" data-original-title="Receive" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument="2,3,8,1,0"><i class="fa fa-sign-in" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!--sample Receive Internal Lab list-->

            <!--sample Sent To External Lab list-->
            <div class="panel panel-success" id="sampleRecieveDecisionUser" runat="server">
                <div class="panel-heading">
                    <h3 class="panel-title">Sample list</h3>
                </div>
                <div class="panel-body">

                    <div class="form-horizontal" role="form">
                        <div class="col-sm-12">
                            <div class="form-group">


                                <a href='<%=ResolveUrl("~/print-samplecode-lab") %>' class="btn btn-default" target="_blank"><span style="font-size: 14px;" class="fa fa-print">Print</span></a>
                                <asp:LinkButton ID="LinkButton47" runat="server" Font-Size="Small" class="btn btn-success"> <span style="font-size: 14px;" class="fa fa-file-excel-o"> Export</span></asp:LinkButton>
                            </div>
                        </div>
                    </div>

                    <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                        <tbody>
                            <tr class="info" style="font-size: Small; height: 40px;">
                                <th scope="col" style="width: 10%;">Sample Name</th>
                                <th scope="col" style="width: 10%;">Sample Code</th>
                                <th scope="col" style="width: 20%;">Tested Detail</th>
                                <th scope="col" style="width: 10%;">Received Date</th>
                                <th scope="col" style="width: 10%;">Tested Date</th>
                                <th scope="col" style="width: 10%;">Lab</th>
                                <th scope="col" style="width: 20%;">Status</th>
                                <th scope="col" style="width: 10%;">Action</th>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>BB 60g.</td>
                                <td>7G07001</td>
                                <td>Total Plate Count(CFU/g)</td>
                                <td>07/07/2017</td>
                                <td>07/07/2017</td>
                                <td>NPW</td>
                                <td>Ongoing</td>
                                <td>
                                    <asp:LinkButton ID="lbSampleReciveResultOut_1" CssClass="btn-sm btn-default" runat="server" data-original-title="Sample Result" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument="3,5,6,4,0"><i class="fa fa-pencil" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>BB 60g.</td>
                                <td>7G07001</td>
                                <td>Moistue(%MC)</td>
                                <td>07/07/2017</td>
                                <td>10/07/2017</td>
                                <td>External Lab</td>
                                <td>Completed</td>
                                <td>
                                    <asp:LinkButton ID="lbSampleOutView" CssClass="btn-sm btn-default" runat="server" data-original-title="View" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocEvent" CommandArgument=""><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>BR 3.6g.</td>
                                <td>7G07002</td>
                                <td>Total Plate Count(CFU/g)</td>
                                <td>07/07/2017</td>
                                <td>11/07/2017</td>
                                <td>RJN</td>
                                <td>Ongoing</td>
                                <td>
                                    <asp:LinkButton ID="lbSampleReciveResultOut_2" CssClass="btn-sm btn-default" runat="server" data-original-title="Sample Result" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocEvent" CommandArgument="5"><i class="fa fa-pencil" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>

                            <tr style="font-size: Small;">
                                <td>BR 3.6g.</td>
                                <td>7G07002</td>
                                <td>Moistue(%MC)</td>
                                <td>07/07/2017</td>
                                <td>-</td>
                                <td>-</td>
                                <td>Waiting for user out</td>
                                <td>
                                    <asp:LinkButton ID="lbSampleReciveUserOut" CssClass="btn-sm btn-default" runat="server" data-original-title="Receive for User" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument="1,8,9,2,0"><i class="fa fa-user" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
            <!--sample Sent To External Lab list-->

            <!--sample Sent To Admin Approve to External list-->
            <div class="panel panel-success" id="sampleRecieveforadmin" runat="server">
                <div class="panel-heading">
                    <h3 class="panel-title">Sample list</h3>
                </div>
                <div class="panel-body">
                    <div class="form-horizontal" role="form">
                        <div class="col-sm-12">
                            <div class="form-group">


                                <a href='<%=ResolveUrl("~/print-samplecode-lab") %>' class="btn btn-default" target="_blank"><span style="font-size: 14px;" class="fa fa-print">Print</span></a>
                                <asp:LinkButton ID="LinkButton48" runat="server" Font-Size="Small" class="btn btn-success"> <span style="font-size: 14px;" class="fa fa-file-excel-o"> Export</span></asp:LinkButton>
                            </div>
                        </div>
                    </div>

                    <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                        <tbody>
                            <tr class="info" style="font-size: Small; height: 40px;">
                                <th scope="col" style="width: 10%;">Sample Name</th>
                                <th scope="col" style="width: 10%;">Sample Code</th>
                                <th scope="col" style="width: 20%;">Tested Detail</th>
                                <th scope="col" style="width: 10%;">Received Date</th>
                                <th scope="col" style="width: 10%;">Tested Date</th>
                                <th scope="col" style="width: 10%;">Lab</th>
                                <th scope="col" style="width: 20%;">Status</th>
                                <th scope="col" style="width: 10%;">Action</th>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>BB 60g.</td>
                                <td>7G07001</td>
                                <td>Total Plate Count(CFU/g)</td>
                                <td>07/07/2017</td>
                                <td>07/07/2017</td>
                                <td>NPW</td>
                                <td>Waiting for Admin(In)</td>
                                <td>
                                    <asp:LinkButton ID="LinkButton1" CssClass="btn-sm btn-default" runat="server" data-original-title="Receive for Admin" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument="2,10,10,2,10"><i class="fa fa-user" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>BB 60g.</td>
                                <td>7G07001</td>
                                <td>Moistue(%MC)</td>
                                <td>07/07/2017</td>
                                <td>10/07/2017</td>
                                <td>External Lab</td>
                                <td>Completed</td>
                                <td>
                                    <asp:LinkButton ID="LinkButton4" CssClass="btn-sm btn-default" runat="server" data-original-title="View" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocEvent" CommandArgument=""><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>BR 3.6g.</td>
                                <td>7G07002</td>
                                <td>Total Plate Count(CFU/g)</td>
                                <td>07/07/2017</td>
                                <td>11/07/2017</td>
                                <td>RJN</td>
                                <td>Ongoing</td>
                                <td>
                                    <asp:LinkButton ID="LinkButton5" CssClass="btn-sm btn-default" runat="server" data-original-title="Sample Result" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocEvent" CommandArgument="5"><i class="fa fa-pencil" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>

                            <tr style="font-size: Small;">
                                <td>BR 3.6g.</td>
                                <td>7G07002</td>
                                <td>Moistue(%MC)</td>
                                <td>07/07/2017</td>
                                <td>-</td>
                                <td>-</td>
                                <td>Waiting for Admin(Out)</td>
                                <td>
                                    <asp:LinkButton ID="lbSampleAdminWaiting" CssClass="btn-sm btn-default" runat="server" data-original-title="Receive for Admin" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument="2,3,7,5,0"><i class="fa fa-user" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
            <!--sample Sent To Admin Approve to External list-->

            <!--sample Sent To Admin Approve to External list-->
            <div class="panel panel-success" id="sampleRecieveforLabExternal" runat="server">
                <div class="panel-heading">
                    <h3 class="panel-title">Sample list</h3>
                </div>
                <div class="panel-body">
                    <div class="form-horizontal" role="form">
                        <div class="col-sm-12">
                            <div class="form-group">


                                <a href='<%=ResolveUrl("~/print-samplecode-lab") %>' class="btn btn-default" target="_blank"><span style="font-size: 14px;" class="fa fa-print">Print</span></a>
                                <asp:LinkButton ID="LinkButton49" runat="server" Font-Size="Small" class="btn btn-success"> <span style="font-size: 14px;" class="fa fa-file-excel-o"> Export</span></asp:LinkButton>
                            </div>
                        </div>
                    </div>

                    <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                        <tbody>
                            <tr class="info" style="font-size: Small; height: 40px;">
                                <th scope="col" style="width: 10%;">Sample Name</th>
                                <th scope="col" style="width: 10%;">Sample Code</th>
                                <th scope="col" style="width: 20%;">Tested Detail</th>
                                <th scope="col" style="width: 10%;">Received Date</th>
                                <th scope="col" style="width: 10%;">Tested Date</th>
                                <th scope="col" style="width: 10%;">Lab</th>
                                <th scope="col" style="width: 20%;">Status</th>
                                <th scope="col" style="width: 10%;">Action</th>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>BB 60g.</td>
                                <td>7G07001</td>
                                <td>Total Plate Count(CFU/g)</td>
                                <td>07/07/2017</td>
                                <td>07/07/2017</td>
                                <td>NPW</td>
                                <td>Ongoing</td>
                                <td>
                                    <asp:LinkButton ID="LinkButton7" CssClass="btn-sm btn-default" runat="server" data-original-title="Sample Result" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument="3,5,6,4,0"><i class="fa fa-pencil" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>BB 60g.</td>
                                <td>7G07001</td>
                                <td>Moistue(%MC)</td>
                                <td>07/07/2017</td>
                                <td>10/07/2017</td>
                                <td>External Lab</td>
                                <td>Completed</td>
                                <td>
                                    <asp:LinkButton ID="LinkButton8" CssClass="btn-sm btn-default" runat="server" data-original-title="View" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocEvent" CommandArgument=""><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>BR 3.6g.</td>
                                <td>7G07002</td>
                                <td>Total Plate Count(CFU/g)</td>
                                <td>07/07/2017</td>
                                <td>11/07/2017</td>
                                <td>RJN</td>
                                <td>Ongoing</td>
                                <td>
                                    <asp:LinkButton ID="LinkButton9" CssClass="btn-sm btn-default" runat="server" data-original-title="Sample Result" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocEvent" CommandArgument="5"><i class="fa fa-pencil" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>

                            <tr style="font-size: Small;">
                                <td>BR 3.6g.</td>
                                <td>7G07002</td>
                                <td>Moistue(%MC)</td>
                                <td>07/07/2017</td>
                                <td>-</td>
                                <td>External Lab</td>
                                <td>Ongoing</td>
                                <td>
                                    <asp:LinkButton ID="lbRecieve" CssClass="btn-sm btn-default" runat="server" data-original-title="Sample Result External Lab" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument="2,11,10,2,20"><i class="fa fa-pencil" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
            <!--sample Sent To Admin Approve to External list-->

            <!--sample Waiting approve for sup  list-->
            <div class="panel panel-success" id="sampleWaitingSupervisor" runat="server">
                <div class="panel-heading">
                    <h3 class="panel-title">Sample list</h3>
                </div>
                <div class="panel-body">

                    <div class="form-horizontal" role="form">
                        <div class="col-sm-12">
                            <div class="form-group">


                                <a href='<%=ResolveUrl("~/print-samplecode-lab") %>' class="btn btn-default" target="_blank"><span style="font-size: 14px;" class="fa fa-print">Print</span></a>
                                <asp:LinkButton ID="LinkButton50" runat="server" Font-Size="Small" class="btn btn-success"> <span style="font-size: 14px;" class="fa fa-file-excel-o"> Export</span></asp:LinkButton>
                            </div>
                        </div>
                    </div>

                    <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                        <tbody>
                            <tr class="info" style="font-size: Small; height: 40px;">
                                <th scope="col" style="width: 10%;">Sample Name</th>
                                <th scope="col" style="width: 10%;">Sample Code</th>
                                <th scope="col" style="width: 20%;">Tested Detail</th>
                                <th scope="col" style="width: 10%;">Received Date</th>
                                <th scope="col" style="width: 10%;">Tested Date</th>
                                <th scope="col" style="width: 10%;">Lab</th>
                                <th scope="col" style="width: 20%;">Status</th>
                                <th scope="col" style="width: 10%;">Action</th>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>BB 60g.</td>
                                <td>7G07001</td>
                                <td>Total Plate Count(CFU/g)</td>
                                <td>07/07/2017</td>
                                <td>07/07/2017</td>
                                <td>NPW</td>
                                <td>Waiting for Supervisor</td>
                                <td>
                                    <asp:LinkButton ID="lbSampleWaitingApprove" CssClass="btn-sm btn-default" runat="server" data-original-title="Waiting Approve" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument="5,3,6,4,6"><i class="fa fa-user" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>BB 60g.</td>
                                <td>7G07001</td>
                                <td>Moistue(%MC)</td>
                                <td>07/07/2017</td>
                                <td>10/07/2017</td>
                                <td>External Lab</td>
                                <td>Completed</td>
                                <td>
                                    <asp:LinkButton ID="LinkButton2" CssClass="btn-sm btn-default" runat="server" data-original-title="View" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocEventViewResult" CommandArgument=""><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>BR 3.6g.</td>
                                <td>7G07002</td>
                                <td>Total Plate Count(CFU/g)</td>
                                <td>07/07/2017</td>
                                <td>11/07/2017</td>
                                <td>RJN</td>
                                <td>Ongoing</td>
                                <td>
                                    <asp:LinkButton ID="LinkButton3" CssClass="btn-sm btn-default" runat="server" data-original-title="Sample Result" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocEvent" CommandArgument="5"><i class="fa fa-pencil" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>

                            <tr style="font-size: Small;">
                                <td>BR 3.6g.</td>
                                <td>7G07002</td>
                                <td>Moistue(%MC)</td>
                                <td>07/07/2017</td>
                                <td>-</td>
                                <td>-</td>
                                <td>Waiting for Admin(Out)</td>
                                <td>
                                    <asp:LinkButton ID="LinkButton6" CssClass="btn-sm btn-default" runat="server" data-original-title="Receive for Admin" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument="2,3,8,1,8"><i class="fa fa-user" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <!--sample sample Waiting approve for sup  list list-->

            <!--sample Approve and Preview in Detail list-->
            <div class="panel panel-success" id="sampleApprove" runat="server">
                <div class="panel-heading">
                    <h3 class="panel-title">Sample list</h3>
                </div>
                <div class="panel-body">

                    <div class="form-horizontal" role="form">
                        <div class="col-sm-12">
                            <div class="form-group">


                                <a href='<%=ResolveUrl("~/print-samplecode-lab") %>' class="btn btn-default" target="_blank"><span style="font-size: 14px;" class="fa fa-print">Print</span></a>

                                <asp:LinkButton ID="export_excel" runat="server" Font-Size="Small" class="btn btn-success"> <span style="font-size: 14px;" class="fa fa-file-excel-o"> Export</span></asp:LinkButton>

                            </div>
                        </div>
                    </div>

                    <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                        <tbody>
                            <tr class="info" style="font-size: Small; height: 40px;">
                                <th scope="col" style="width: 10%;">Sample Name</th>
                                <th scope="col" style="width: 10%;">Sample Code</th>
                                <th scope="col" style="width: 20%;">Tested Detail</th>
                                <th scope="col" style="width: 10%;">Received Date</th>
                                <th scope="col" style="width: 10%;">Tested Date</th>
                                <th scope="col" style="width: 10%;">Lab</th>
                                <th scope="col" style="width: 20%;">Status</th>
                                <th scope="col" style="width: 10%;">Action</th>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>BB 60g.</td>
                                <td>7G07001</td>
                                <td>Total Plate Count(CFU/g)</td>
                                <td>07/07/2017</td>
                                <td>07/07/2017</td>
                                <td>NPW</td>
                                <td>Completed</td>
                                <td>
                                    <asp:LinkButton ID="lbSampleApproveView" CssClass="btn-sm btn-default" runat="server" data-original-title="View" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument="2,10,10,2,10"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>BB 60g.</td>
                                <td>7G07001</td>
                                <td>Moistue(%MC)</td>
                                <td>07/07/2017</td>
                                <td>10/07/2017</td>
                                <td>External Lab</td>
                                <td>Completed</td>
                                <td>
                                    <asp:LinkButton ID="lbSampleApproveView_2" CssClass="btn-sm btn-default" runat="server" data-original-title="View" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument=""><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>

                            <tr style="font-size: Small;">
                                <td>BR 3.6g.</td>
                                <td>7G07002</td>
                                <td>Total Plate Count(CFU/g)</td>
                                <td>07/07/2017</td>
                                <td>11/07/2017</td>
                                <td>RJN</td>
                                <td>Completed</td>
                                <td>
                                    <asp:LinkButton ID="lbSampleApproveView_4" CssClass="btn-sm btn-default" runat="server" data-original-title="View" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument=""><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>

                            <tr style="font-size: Small;">
                                <td>BR 3.6g.</td>
                                <td>7G07002</td>
                                <td>Moistue(%MC)</td>
                                <td>07/07/2017</td>
                                <td>11/07/2017</td>
                                <td>External Lab</td>
                                <td>Completed</td>
                                <td>
                                    <asp:LinkButton ID="lbSampleApproveView_3" CssClass="btn-sm btn-default" runat="server" data-original-title="View" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument=""><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <div class="pull-right">
                        <asp:LinkButton ID="lbDocSave" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument="2,10,12,1,12"></asp:LinkButton>
                        <asp:LinkButton ID="lbDocCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="Cancel" OnCommand="btnCommand" CommandName="cmdDocCancel"></asp:LinkButton>
                    </div>

                </div>
            </div>
            <!--sample Approve and Preview in Detail list-->

            <!--sample Preview list-->
            <div class="panel panel-success" id="samplePreview" runat="server">
                <div class="panel-heading">
                    <h3 class="panel-title">Sample list</h3>
                </div>
                <div class="panel-body">

                    <div class="form-horizontal" role="form">
                        <div class="col-sm-12">
                            <div class="form-group">


                                <a href='<%=ResolveUrl("~/print-samplecode-lab") %>' class="btn btn-default" target="_blank"><span style="font-size: 14px;" class="fa fa-print">Print</span></a>




                            </div>
                        </div>
                    </div>

                    <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                        <tbody>
                            <tr class="info" style="font-size: Small; height: 40px;">
                                <th scope="col" style="width: 15%;">Sample Code</th>
                                <th scope="col" style="width: 15%;">Received Date</th>
                                <th scope="col" style="width: 15%;">Tested Date</th>
                                <th scope="col" style="width: 10%;">Lab</th>
                                <th scope="col" style="width: 30%;">Status</th>
                                <%--<th scope="col" style="width: 15%;">Action</th>--%>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>7G07005</td>
                                <td>10/07/2017</td>
                                <td>11/07/2017</td>
                                <td>NPW</td>
                                <td>Completed</td>
                                <%--<td>
                                    <asp:LinkButton ID="LinkButton1" CssClass="btn-sm btn-default" runat="server" data-original-title="View" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocEvent" CommandArgument=""><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                </td>--%>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>7G07006</td>
                                <td>10/07/2017</td>
                                <td>11/07/2017</td>
                                <td>NPW</td>
                                <td>Completed</td>
                                <%--<td>
                                    <asp:LinkButton ID="LinkButton4" CssClass="btn-sm btn-default" runat="server" data-original-title="View" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocEvent" CommandArgument=""><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                </td>--%>
                            </tr>


                        </tbody>
                    </table>

                </div>
            </div>
            <!--sample Preview list-->

            <!--sample Preview Report list-->
            <div class="panel panel-success" id="samplePreviewReport" runat="server">
                <div class="panel-heading">
                    <h3 class="panel-title">Sample list</h3>
                </div>
                <div class="panel-body">
                    <div class="form-horizontal" role="form">
                        <div class="col-sm-12">
                            <div class="form-group">


                                <a href='<%=ResolveUrl("~/print-samplecode-lab") %>' class="btn btn-default" target="_blank"><span style="font-size: 14px;" class="fa fa-print">Print</span></a>

                            </div>
                        </div>
                    </div>

                    <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                        <tbody>
                            <tr class="info" style="font-size: Small; height: 40px;">
                                <th scope="col" style="width: 15%;">Sample Code</th>
                                <th scope="col" style="width: 15%;">Received Date</th>
                                <th scope="col" style="width: 15%;">Tested Date</th>
                                <th scope="col" style="width: 10%;">Lab</th>
                                <th scope="col" style="width: 30%;">Status</th>
                                <th scope="col" style="width: 15%;">Action</th>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>7G07005</td>
                                <td>10/07/2017</td>
                                <td>11/07/2017</td>
                                <td>NPW</td>
                                <td>Completed</td>
                                <td>
                                    <asp:LinkButton ID="lbSampleViewDetailReport" CssClass="btn-sm btn-default" runat="server" data-original-title="View" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocEventView_Report" CommandArgument="13"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>7G07006</td>
                                <td>10/07/2017</td>
                                <td>11/07/2017</td>
                                <td>NPW</td>
                                <td>Completed</td>
                                <td>
                                    <asp:LinkButton ID="lbSampleViewDetailReport_1" CssClass="btn-sm btn-default" runat="server" data-original-title="View" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocEventView_Report" CommandArgument="13"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                </td>
                            </tr>


                        </tbody>
                    </table>

                </div>
            </div>
            <!--sample Preview Report list-->

            <!--document log-->
            <div class="panel panel-default" id="logList" runat="server">
                <div class="panel-heading">
                    <h3 class="panel-title">รายละเอียดการดำเนินการ</h3>
                </div>
                <div class="panel-body">
                </div>
            </div>
            <!--document log-->

            <!-- document log detail preview -->
            <div class="panel panel-default" id="logListPreview" runat="server">
                <div class="panel-heading">
                    <h3 class="panel-title">รายละเอียดการดำเนินการ</h3>
                </div>
                <div class="panel-body">
                    <div class="form-horizontal" role="form">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">ดำเนินการโดย</label>
                            <div class="col-sm-4 control-label textleft">
                                ผู้สร้างรายการตรวจสอบ
                            </div>
                            <label class="col-sm-2 control-label">ผลการดำเนินการ</label>
                            <div class="col-sm-4 control-label textleft">
                                ดำเนินการ
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">โดย</label>
                            <div class="col-sm-4 control-label textleft">
                                ทดสอบ ทดสอบ
                            </div>
                            <label class="col-sm-2 control-label">วันที่</label>
                            <div class="col-sm-4 control-label textleft">
                                07/07/2017
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Comment</label>
                            <div class="col-sm-4 control-label textleft">
                                -
                            </div>
                            <%-- <label class="col-sm-2 control-label">ตำแหน่ง</label>
                                    <div class="col-sm-4 control-label textleft">
                                        ทดสอบ ทดสอบ
                                    </div>--%>
                        </div>
                        <hr />

                        <div class="form-group">
                            <label class="col-sm-2 control-label">ดำเนินการโดย</label>
                            <div class="col-sm-4 control-label textleft">
                                ผู้รับรายการตรวจสอบ
                            </div>
                            <label class="col-sm-2 control-label">ผลการดำเนินการ</label>
                            <div class="col-sm-4 control-label textleft">
                                รับตัวอย่าง
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">โดย</label>
                            <div class="col-sm-4 control-label textleft">
                                วราภรณ์ บำรุงกิจ
                            </div>
                            <label class="col-sm-2 control-label">วันที่</label>
                            <div class="col-sm-4 control-label textleft">
                                10/07/2017
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Comment</label>
                            <div class="col-sm-4 control-label textleft">
                                -
                            </div>
                            <%-- <label class="col-sm-2 control-label">ตำแหน่ง</label>
                                    <div class="col-sm-4 control-label textleft">
                                        ทดสอบ ทดสอบ
                                    </div>--%>
                        </div>
                        <hr />
                    </div>

                </div>
            </div>
            <!--document log detail preview-->

        </asp:View>
        <asp:View ID="tab2" runat="server">

            <div class="form-group">
                <asp:LinkButton ID="btnSearch" CssClass="btn btn-info" runat="server" data-original-title="Search" data-toggle="tooltip" CommandName="btnSearch" OnCommand="btnCommand"><span class="glyphicon glyphicon-search"></span> ค้นหา</asp:LinkButton>

            </div>

            <div class="form-group">
                <%--<div class="col-md-12">--%>
                <div id="fvBacktoIndex" class="row" runat="server" visible="false">
                    <%--<div class="col-md-12">--%>
                    <asp:LinkButton ID="btnSearchBack" CssClass="btn btn-info" runat="server" data-original-title="Back" data-toggle="tooltip" OnCommand="btnCommand" CommandName="btnSearchBack"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> กลับ</asp:LinkButton>
                    <%--</div>--%>
                </div>
                <%--</div>--%>
            </div>


            <!--*** START Search Index ***-->
            <div id="showsearch" runat="server" visible="false">

                <div class="panel panel-info">
                    <div class="panel-heading f-bold">ค้นหารายการส่งตรวจสอบ</div>
                    <div class="panel-body">
                        <div class="form-horizontal" role="form">

                            <%-------------- Document No, Sample Code --------------%>
                            <div class="form-group">
                                <asp:Label ID="lborg_idxsearch" CssClass="col-sm-2 control-label" runat="server" Text="Document No : " />
                                <div class="col-sm-4">
                                    <asp:TextBox ID="txt_document_nosearch" placeholder="Document No ..." runat="server" class="cls" CssClass="form-control">
                                    </asp:TextBox>

                                </div>

                                <asp:Label ID="lbrdept_idxsearch" CssClass="col-sm-2 control-label" runat="server" Text="Sample Code : " />
                                <div class="col-sm-4">
                                    <asp:TextBox ID="txt_sample_codesearch" AutoPostBack="true" placeholder="Sample Code ..." runat="server" CssClass="form-control">
                                              
                                    </asp:TextBox>

                                </div>

                            </div>
                            <%-------------- Document No, Sample Code --------------%>

                            <%-------------- Mat., เลขตู้  --------------%>
                            <div class="form-group">
                                <asp:Label ID="Label1" CssClass="col-sm-2 control-label" runat="server" Text="Mat : " />
                                <div class="col-sm-4">
                                    <asp:TextBox ID="txt_mat_search" AutoPostBack="true" placeholder="Mat ..." runat="server" CssClass="form-control">
                                    </asp:TextBox>

                                </div>

                                <asp:Label ID="Label2" CssClass="col-sm-2 control-label" runat="server" Text="เลขตู้ : " />
                                <div class="col-sm-4">
                                    <asp:TextBox ID="txt_too_search" AutoPostBack="true" placeholder="เลขตู้ ..." runat="server" CssClass="form-control">
                                              
                                    </asp:TextBox>

                                </div>

                            </div>
                            <%-------------- ชื่อลุกค้า, วันที่ส่ง --------------%>
                            <div class="form-group">
                                <asp:Label ID="Label4" CssClass="col-sm-2 control-label" runat="server" Text="ชื่อลูกค้า : " />
                                <div class="col-sm-4">
                                    <asp:TextBox ID="txt_customer_search" placeholder="name customer ..." runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                                <asp:Label ID="Label3" CssClass="col-sm-2 control-label" runat="server" Text="วันที่ส่งตรวจ : " />
                                <div class="col-sm-4">
                                    <asp:TextBox ID="txt_date_search" AutoPostBack="true" placeholder="Date ..." runat="server" CssClass="form-control">
                                    </asp:TextBox>

                                </div>
                            </div>
                            <%-------------- เวลา,กะ --------------%>
                            <div class="form-group">
                                <asp:Label ID="Label5" CssClass="col-sm-2 control-label" runat="server" Text="กะ : " />
                                <div class="col-sm-4">
                                    <asp:TextBox ID="txt_shift_search" AutoPostBack="true" placeholder="Time ..." runat="server" CssClass="form-control">
                                              
                                    </asp:TextBox>
                                </div>

                                <asp:Label ID="Label6" CssClass="col-sm-2 control-label" runat="server" Text="เวลา : " />
                                <div class="col-sm-4">
                                    <asp:TextBox ID="txt_time_search" AutoPostBack="true" placeholder="Time ..." runat="server" CssClass="form-control">
                                              
                                    </asp:TextBox>
                                </div>
                            </div>
                            <%-------------- btnsearch --------------%>
                            <div class="form-group">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-4">
                                    <div class="pull-left">
                                        <asp:LinkButton ID="btnSearchIndex1" CssClass="btn btn-primary" runat="server" ValidationGroup="SearchIndex" OnCommand="btnCommand" CommandName="btnSearchIndex1" data-toggle="tooltip" title="Search" OnClientClick="return confirm('คุณต้องการค้นหารายการนี้ใช่หรือไม่ ?')"><span class="glyphicon glyphicon-search"></span></asp:LinkButton>


                                        <asp:LinkButton ID="btnCancelIndex_First" CssClass="btn btn-default" runat="server" OnCommand="btnCommand" CommandName="cmdDocCancel" data-toggle="tooltip" title="Cancel"><i class="fa fa-times"></i></asp:LinkButton>
                                    </div>

                                </div>
                            </div>
                            <%-------------- btnsearch --------------%>
                        </div>
                    </div>

                </div>
            </div>
            <!--*** END Search Index ***-->

            <div class="form-horizontal" role="form" id="search_test_show" runat="server" visible="false">
                <div class="col-sm-12">
                    <div class="form-group">
                        <div class="input-group col-sm-3 pull-right">
                            <asp:TextBox ID="txtsearch_index" runat="server" placeholder="Document No/ Sample Code" CssClass="form-control" />
                            <div class="input-group-btn">
                                <asp:LinkButton ID="btnSearchIndex" CssClass="btn btn-primary" runat="server" data-original-title="Search.." data-toggle="tooltip" OnCommand="btnCommand" CommandName="btnSearchIndex"><i class="glyphicon glyphicon-search"></i></asp:LinkButton>
                                <asp:LinkButton ID="btnreset" CssClass="btn btn-default" runat="server" data-original-title="Reset.." data-toggle="tooltip" OnCommand="btnCommand" CommandArgument='0' CommandName="btnreset"><i class="fa fa-refresh" aria-hidden="true"></i></asp:LinkButton>
                            </div>
                        </div>

                        <label class="col-sm-9 control-label"></label>

                    </div>
                </div>
            </div>

            <div class="show_detail" runat="server">
                <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" id="show_data" runat="server" style="border-collapse: collapse;">
                    <tbody>
                        <tr class="info" style="font-size: Small; height: 40px;">
                            <th scope="col" style="width: 10%;">Document No</th>
                            <th scope="col" style="width: 15%;">Create Date</th>
                            <th scope="col" style="width: 15%;">Received Date</th>
                            <th scope="col" style="width: 30%;">Status</th>
                            <th scope="col" style="width: 15%;">Report(s)</th>
                            <th scope="col" style="width: 15%;">Action</th>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>QA600005</td>
                            <td>07/07/2017</td>
                            <td>10/07/2017</td>
                            <td>Completed</td>
                            <td>
                                <asp:LinkButton ID="lbDocView" CssClass="btn-sm btn-default" runat="server" data-original-title="Preview" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocEvent" CommandArgument="12"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="lbDocReport" CssClass="btn-sm btn-default" runat="server" data-original-title="Report" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocEvent" CommandArgument="13"><i class="fa fa-list-ul" aria-hidden="true"></i></asp:LinkButton>
                            </td>
                            <td></td>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>QA600004</td>
                            <td>07/07/2017</td>
                            <td>-</td>
                            <td>Cancelled</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>QA600003</td>
                            <td>06/07/2017</td>
                            <td>-</td>
                            <td>Waiting</td>
                            <td></td>
                            <td>
                                <asp:LinkButton ID="lbDocReceive" CssClass="btn-sm btn-default" runat="server" data-original-title="Receive" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocEvent" CommandArgument="2"><i class="fa fa-wpforms" aria-hidden="true"></i></asp:LinkButton>
                            </td>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>QA600002</td>
                            <td>06/07/2017</td>
                            <td>06/07/2017</td>
                            <td>Ongoing</td>
                            <td></td>
                            <td>
                                <asp:LinkButton ID="lbDocReceive_2" CssClass="btn-sm btn-default" runat="server" data-original-title="Receive" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocEvent" CommandArgument="2"><i class="fa fa-wpforms" aria-hidden="true"></i></asp:LinkButton>
                            </td>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>QA600001</td>
                            <td>06/07/2017</td>
                            <td>07/07/2017</td>
                            <td>Ongoing</td>
                            <td></td>
                            <td>
                                <asp:LinkButton ID="lbDocReceive_3" CssClass="btn-sm btn-default" runat="server" data-original-title="Receive" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocEvent" CommandArgument="2"><i class="fa fa-wpforms" aria-hidden="true"></i></asp:LinkButton>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div id="show_search_doc" runat="server">
                <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" id="search_data" runat="server" style="border-collapse: collapse;">
                    <tbody>
                        <tr class="info" style="font-size: Small; height: 40px;">
                            <th scope="col" style="width: 10%;">Document No</th>
                            <th scope="col" style="width: 15%;">Create Date</th>
                            <th scope="col" style="width: 15%;">Received Date</th>
                            <th scope="col" style="width: 30%;">Status</th>
                            <th scope="col" style="width: 15%;">Report(s)</th>
                            <th scope="col" style="width: 15%;">Action</th>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>QA600003</td>
                            <td>06/07/2017</td>
                            <td>-</td>
                            <td>Waiting</td>
                            <td></td>
                            <td>
                                <asp:LinkButton ID="LinkButton20" CssClass="btn-sm btn-default" runat="server" data-original-title="Receive" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocEvent" CommandArgument="2"><i class="fa fa-wpforms" aria-hidden="true"></i></asp:LinkButton>
                            </td>
                        </tr>

                    </tbody>
                </table>
            </div>

            <div id="show_search_sample" runat="server">
                <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" id="Table2" runat="server" style="border-collapse: collapse;">
                    <tbody>
                        <tr class="info" style="font-size: Small; height: 40px;">
                            <th scope="col" style="width: 10%;">Sample Name</th>
                            <th scope="col" style="width: 15%;">Sample Code</th>
                            <th scope="col" style="width: 15%;">Tested Detail</th>
                            <th scope="col" style="width: 15%;">Received Date</th>
                            <th scope="col" style="width: 15%;">Tested Date</th>
                            <th scope="col" style="width: 15%;">Lab</th>
                            <th scope="col" style="width: 30%;">Status</th>
                            <th scope="col" style="width: 15%;">Action</th>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>BR 3.6g.</td>
                            <td>7G07002</td>
                            <td>Total Plate Count(CFU/g)</td>
                            <td>07/07/2017</td>
                            <td>-</td>
                            <td>-</td>
                            <td>Waiting Admin(In)</td>
                            <td>
                                <asp:LinkButton ID="LinkButton22" CssClass="btn-sm btn-default" runat="server" data-original-title="Receive" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument="2,3,4,3,0"><i class="fa fa-wpforms" aria-hidden="true"></i></asp:LinkButton>
                            </td>
                        </tr>

                    </tbody>
                </table>
            </div>

            <div id="show_sucess" runat="server" visible="false">
                <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" id="ContentMain_gvDoingList" style="border-collapse: collapse;">
                    <tbody>
                        <tr class="info" style="font-size: Small; height: 40px;">
                            <th scope="col" style="width: 10%;">Document No</th>
                            <th scope="col" style="width: 15%;">Create Date</th>
                            <th scope="col" style="width: 15%;">Received Date</th>
                            <th scope="col" style="width: 30%;">Status</th>
                            <th scope="col" style="width: 15%;">Report(s)</th>
                            <th scope="col" style="width: 15%;">Action</th>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>QA600005</td>
                            <td>07/07/2017</td>
                            <td>10/07/2017</td>
                            <td>Completed</td>
                            <td>
                                <asp:LinkButton ID="LinkButton14" CssClass="btn-sm btn-default" runat="server" data-original-title="Preview" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocEvent" CommandArgument="12"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton15" CssClass="btn-sm btn-default" runat="server" data-original-title="Report" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocEvent" CommandArgument="13"><i class="fa fa-list-ul" aria-hidden="true"></i></asp:LinkButton>
                            </td>
                            <td></td>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>QA600004</td>
                            <td>07/07/2017</td>
                            <td>-</td>
                            <td>Cancelled</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>QA600003</td>
                            <td>06/07/2017</td>
                            <td>11/07/2017</td>
                            <td>Completed</td>
                            <td>
                                <asp:LinkButton ID="LinkButton16" CssClass="btn-sm btn-default" runat="server" data-original-title="Preview" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocEvent" CommandArgument="12"><i class="fa fa-file-text-o" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton19" CssClass="btn-sm btn-default" runat="server" data-original-title="Report" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocEvent" CommandArgument="13"><i class="fa fa-list-ul" aria-hidden="true"></i></asp:LinkButton>
                            </td>
                            <td></td>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>QA600002</td>
                            <td>06/07/2017</td>
                            <td>06/07/2017</td>
                            <td>Ongoing</td>
                            <td></td>
                            <td>
                                <asp:LinkButton ID="LinkButton17" CssClass="btn-sm btn-default" runat="server" data-original-title="Receive" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocEvent" CommandArgument="2"><i class="fa fa-wpforms" aria-hidden="true"></i></asp:LinkButton>
                            </td>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>QA600001</td>
                            <td>06/07/2017</td>
                            <td>07/07/2017</td>
                            <td>Ongoing</td>
                            <td></td>
                            <td>
                                <asp:LinkButton ID="LinkButton18" CssClass="btn-sm btn-default" runat="server" data-original-title="Receive" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocEvent" CommandArgument="2"><i class="fa fa-wpforms" aria-hidden="true"></i></asp:LinkButton>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </asp:View>
        <asp:View ID="tab3" runat="server">
            <div id="master_meterail" runat="server" visible="false">
                <div class="form-horizontal" role="form">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <asp:FileUpload ID="upload_file_material" runat="server" />
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <%--<div class="col-sm-12"></div>--%>
                            <div class="pull-left">
                                <asp:LinkButton ID="btnImportExcel" CssClass="btn btn-success" runat="server" ValidationGroup="Import" OnCommand="btnCommand" CommandName="btnImportExcel" data-toggle="tooltip" title="Import" OnClientClick="return confirm('คุณต้องการเพิ่มรายการนี้ใช่หรือไม่ ?')"><span class="glyphicon glyphicon-file"></span> Import</asp:LinkButton>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div id="show_detail_fileexcel" runat="server" visible="false">
                <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" id="Table1" runat="server" style="border-collapse: collapse;">
                    <tbody>
                        <tr class="info" style="font-size: Small; height: 40px;">
                            <th scope="col" style="width: 25%;">No.</th>
                            <th scope="col" style="width: 25%;">เลข Mat.</th>
                            <th scope="col" style="width: 25%;">Status</th>
                            <th scope="col" style="width: 25%;">Action</th>

                        </tr>
                        <tr style="font-size: Small;">
                            <td>1</td>
                            <td>1202884</td>
                            <td>online</td>
                            <td>
                                <asp:LinkButton ID="LinkButton21" CssClass="btn-sm btn-default" runat="server" data-original-title="Edit" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument=""><i class="glyphicon glyphicon-pencil" aria-hidden="true"></i></asp:LinkButton>

                                <asp:LinkButton ID="LinkButton28" CssClass="btn-sm btn-default" runat="server" data-original-title="Delete" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument=""><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></asp:LinkButton>

                            </td>

                        </tr>
                        <tr style="font-size: Small;">
                            <td>2</td>
                            <td>1202885</td>
                            <td>online</td>
                            <td>
                                <asp:LinkButton ID="LinkButton23" CssClass="btn-sm btn-default" runat="server" data-original-title="Edit" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument=""><i class="glyphicon glyphicon-pencil" aria-hidden="true"></i></asp:LinkButton>

                                <asp:LinkButton ID="LinkButton29" CssClass="btn-sm btn-default" runat="server" data-original-title="Delete" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument=""><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></asp:LinkButton>
                            </td>
                        </tr>


                    </tbody>
                </table>


            </div>

            <div id="master_data_microbiological" runat="server" visible="false">
                <div class="form-horizontal" role="form">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <%--<div class="col-sm-12"></div>--%>
                            <div class="pull-left">
                                <asp:LinkButton ID="btnInsertmasterlab" CssClass="btn btn-primary" runat="server" Font-Size="Small" ValidationGroup="Insert" OnCommand="btnCommand" CommandName="btnInsertmasterlab" data-toggle="tooltip" title="Insert" OnClientClick="return confirm('คุณต้องการเพิ่มรายการนี้ใช่หรือไม่ ?')"><span class="glyphicon glyphicon-plus"></span> เพิ่มข้อมูล</asp:LinkButton>
                            </div>

                        </div>
                    </div>
                </div>

                <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                    <tbody>
                        <tr class="info" style="font-size: Small; height: 40px;">
                            <th scope="col" style="width: 5%;">No.</th>
                            <th scope="col" style="width: 15%;">ประเภทปฏิบัติการ</th>
                            <%-- <th scope="col" style="width: 15%;">ชื่อการตรวจวิเคราะห์</th>--%>
                            <th scope="col" style="width: 20%;">รายละเอียด</th>
                            <th scope="col" style="width: 20%;">ระยะเวลาการวิเคราะห์</th>
                            <th scope="col" style="width: 10%;">สถานะ</th>
                            <th scope="col" style="width: 10%;">Action</th>

                        </tr>
                        <tr style="font-size: Small;">
                            <td>1.</td>
                            <td>ห้องปฏิบัติการจุลชีววิทยา</td>
                            <%--<td>M1-1</td>--%>
                            <td>Total Plate Count (CFU/g,CFU/Unit)</td>
                            <td>ระยะเวลาในการวิเคราะห์ 3 วันทำการ</td>
                            <td>online</td>
                            <td>
                                <asp:LinkButton ID="LinkButton30" CssClass="btn-sm btn-default" runat="server" data-original-title="Edit" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument=""><i class="glyphicon glyphicon-pencil" aria-hidden="true"></i></asp:LinkButton>

                                <asp:LinkButton ID="LinkButton31" CssClass="btn-sm btn-default" runat="server" data-original-title="Delete" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument=""><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></asp:LinkButton>
                            </td>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>2.</td>
                            <td>ห้องปฏิบัติการจุลชีววิทยา</td>
                            <%--<td>M1-2</td>--%>
                            <td>Total Plate Count (CFU/ml)</td>
                            <td>ระยะเวลาในการวิเคราะห์ 3 วันทำการ</td>
                            <td>online</td>
                            <td>
                                <asp:LinkButton ID="LinkButton32" CssClass="btn-sm btn-default" runat="server" data-original-title="Edit" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument=""><i class="glyphicon glyphicon-pencil" aria-hidden="true"></i></asp:LinkButton>

                                <asp:LinkButton ID="LinkButton33" CssClass="btn-sm btn-default" runat="server" data-original-title="Delete" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument=""><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></asp:LinkButton>
                            </td>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>3.</td>
                            <td>ห้องปฏิบัติการจุลชีววิทยา</td>
                            <%--<td>M2-1</td>--%>
                            <td>Yeast & Mold (CFU/g)</td>
                            <td>ระยะเวลาในการวิเคราะห์ 7 วันทำการ</td>
                            <td>online</td>
                            <td>
                                <asp:LinkButton ID="LinkButton34" CssClass="btn-sm btn-default" runat="server" data-original-title="Edit" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument=""><i class="glyphicon glyphicon-pencil" aria-hidden="true"></i></asp:LinkButton>

                                <asp:LinkButton ID="LinkButton35" CssClass="btn-sm btn-default" runat="server" data-original-title="Delete" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument=""><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></asp:LinkButton>
                            </td>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>4.</td>
                            <td>ห้องปฏิบัติการเคมี</td>
                            <%--<td>C-1</td>--%>
                            <td>Moisture (%MC)</td>
                            <td>ระยะเวลาในการวิเคราะห์ 2 วันทำการ</td>
                            <td>online</td>
                            <td>
                                <asp:LinkButton ID="LinkButton36" CssClass="btn-sm btn-default" runat="server" data-original-title="Edit" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument=""><i class="glyphicon glyphicon-pencil" aria-hidden="true"></i></asp:LinkButton>

                                <asp:LinkButton ID="LinkButton37" CssClass="btn-sm btn-default" runat="server" data-original-title="Delete" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument=""><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></asp:LinkButton>
                            </td>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>5.</td>
                            <td>ห้องปฏิบัติการเคมี</td>
                            <%--<td>C-2</td>--%>
                            <td>pH</td>
                            <td>ระยะเวลาในการวิเคราะห์ 2 วันทำการ</td>
                            <td>online</td>
                            <td>
                                <asp:LinkButton ID="LinkButton38" CssClass="btn-sm btn-default" runat="server" data-original-title="Edit" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument=""><i class="glyphicon glyphicon-pencil" aria-hidden="true"></i></asp:LinkButton>

                                <asp:LinkButton ID="LinkButton39" CssClass="btn-sm btn-default" runat="server" data-original-title="Delete" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument=""><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></asp:LinkButton>
                            </td>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>6.</td>
                            <td>ห้องปฏิบัติการเคมี</td>
                            <%-- <td>C-3</td>--%>
                            <td>Salt (%)</td>
                            <td>ระยะเวลาในการวิเคราะห์ 2 วันทำการ</td>
                            <td>online</td>
                            <td>
                                <asp:LinkButton ID="LinkButton40" CssClass="btn-sm btn-default" runat="server" data-original-title="Edit" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument=""><i class="glyphicon glyphicon-pencil" aria-hidden="true"></i></asp:LinkButton>

                                <asp:LinkButton ID="LinkButton41" CssClass="btn-sm btn-default" runat="server" data-original-title="Delete" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument=""><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></asp:LinkButton>
                            </td>
                        </tr>

                    </tbody>
                </table>
            </div>
            <div id="master_data_chemical" runat="server" visible="false">
                <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                    <tbody>
                        <tr class="info" style="font-size: Small; height: 40px;">
                            <th scope="col" style="width: 5%;">No.</th>
                            <%--<th scope="col" style="width: 20%;">ชื่อรายการตรวจสอบ</th>--%>
                            <th scope="col" style="width: 25%;">รายละเอียด</th>
                            <th scope="col" style="width: 25%;">ระยะเวลาการวิเคราะห์</th>
                            <th scope="col" style="width: 25%;">สถานะ</th>

                        </tr>
                        <tr style="font-size: Small;">
                            <td>1.</td>
                            <%--<td>C-1</td>--%>
                            <td>Moisture (%MC)</td>
                            <td>ระยะเวลาในการวิเคราะห์ 2 วันทำการ</td>
                            <td>online</td>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>2.</td>
                            <%--<td>C-2</td>--%>
                            <td>pH</td>
                            <td>ระยะเวลาในการวิเคราะห์ 2 วันทำการ</td>
                            <td>online</td>
                        </tr>
                        <tr style="font-size: Small;">
                            <td>3.</td>
                            <%--<td>C-3</td>--%>
                            <td>Salt (%)</td>
                            <td>ระยะเวลาในการวิเคราะห์ 2 วันทำการ</td>
                            <td>online</td>
                        </tr>

                    </tbody>
                </table>
            </div>
        </asp:View>

        <asp:View ID="tab4" runat="server">

            <div class="form-horizontal" role="form" id="show_search_lab" runat="server">

                <div class="col-sm-12">
                    <div class="form-group">
                        <div class="input-group col-sm-3 pull-right">
                            <asp:TextBox ID="TextBox3" runat="server" placeholder="Sample Name/ Sample Code" CssClass="form-control" />
                            <div class="input-group-btn">
                                <asp:LinkButton ID="LinkButton25" CssClass="btn btn-primary" runat="server" data-original-title="Search.." data-toggle="tooltip" OnCommand="btnCommand" CommandName="btnSearchIndex"><i class="glyphicon glyphicon-search"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton26" CssClass="btn btn-default" runat="server" data-original-title="Reset.." data-toggle="tooltip" OnCommand="btnCommand" CommandArgument='1' CommandName="btnreset"><i class="fa fa-refresh" aria-hidden="true"></i></asp:LinkButton>
                            </div>
                        </div>

                        <label class="col-sm-9 control-label"></label>

                    </div>
                </div>
            </div>

            <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                <tbody>
                    <tr class="info" style="font-size: Small; height: 40px;">
                        <th scope="col" style="width: 20%;">Sample Name</th>
                        <th scope="col" style="width: 20%;">Sample Code</th>
                        <th scope="col" style="width: 30%;">Tested Detail</th>
                        <%-- <th scope="col" style="width: 15%;">Received Date</th>
                        <th scope="col" style="width: 15%;">Tested Date</th>--%>
                        <th scope="col" style="width: 10%;">Lab</th>
                        <th scope="col" style="width: 20%;">Status</th>
                        <%--<th scope="col" style="width: 15%;">Action</th>--%>
                    </tr>


                    <tr style="font-size: Small;">
                        <td>BB 60g.</td>
                        <td>7G07001</td>
                        <td>Total Plate Count(CFU/g),Moistue(%MC)</td>
                        <%--<td>07/07/2017</td>
                        <td>07/07/2017</td>--%>
                        <td>NPW</td>
                        <td>Ongoing</td>
                        <%-- <td>
                            <asp:LinkButton ID="LinkButton25" CssClass="btn-sm btn-default" runat="server" data-original-title="Sample Result" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument="3,5,6,4,0"><i class="fa fa-pencil" aria-hidden="true"></i></asp:LinkButton>
                        </td>--%>
                    </tr>

                    <tr style="font-size: Small;">
                        <td>BR 3.6g.</td>
                        <td>7G07002</td>
                        <td>Total Plate Count(CFU/g),Moistue(%MC)</td>
                        <%--<td>07/07/2017</td>
                        <td>-</td>--%>
                        <td>-</td>
                        <td>Waiting for Internal Lab</td>
                        <%-- <td>
                            <asp:LinkButton ID="LinkButton27" CssClass="btn-sm btn-default" runat="server" data-original-title="Receive for Internal Lab" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocEvent" CommandArgument="5"><i class="fa fa-pencil" aria-hidden="true"></i></asp:LinkButton>
                        </td>--%>
                    </tr>

                    <%-- <tr style="font-size: Small;">
                        <td>BR 3.6g.</td>
                        <td>7G07004</td>
                        <td>Moistue(%MC)</td>--%>
                    <%--<td>07/07/2017</td>
                        <td>10/07/2017</td>--%>
                    <%-- <td>NPW</td>
                        <td>Ongoing</td>--%>
                    <%--<td>
                            <asp:LinkButton ID="LinkButton26" CssClass="btn-sm btn-default" runat="server" data-original-title="Sample Result" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument="3,5,6,4,0"><i class="fa fa-pencil" aria-hidden="true"></i></asp:LinkButton>
                        </td>--%>
                    <%-- </tr>--%>
                </tbody>
            </table>
        </asp:View>
        <asp:View ID="tab5" runat="server">
            <div class="form-horizontal" role="form" id="show_search_result" runat="server" visible="false">

                <div class="col-sm-12">
                    <div class="form-group">
                        <div class="input-group col-sm-3 pull-left">
                            <asp:TextBox ID="TextBox4" runat="server" placeholder="Sample Code" CssClass="form-control" />
                            <div class="input-group-btn">
                                <asp:LinkButton ID="LinkButton42" CssClass="btn btn-primary" runat="server" data-original-title="Search.." data-toggle="tooltip" OnCommand="btnCommand" CommandName="btnSearchIndex"><i class="glyphicon glyphicon-search"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton43" CssClass="btn btn-default" runat="server" data-original-title="Reset.." data-toggle="tooltip" OnCommand="btnCommand" CommandArgument='0' CommandName="btnreset"><i class="fa fa-refresh" aria-hidden="true"></i></asp:LinkButton>
                            </div>
                        </div>

                        <label class="col-sm-9 control-label"></label>

                    </div>
                </div>
            </div>

            <asp:FormView ID="Fv_Insert_Result" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <asp:HiddenField ID="hfNodeIDX" runat="server" Value="3" />
                    <asp:HiddenField ID="hfActorIDX" runat="server" Value="2" />
                    <asp:HiddenField ID="to_hfNodeIDX" runat="server" Value="4" />

                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">กรอกผลการวิเคราะห์ตัวอย่าง</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">

                                    <label class="col-sm-2 control-label">Sample Code</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_samplecode_result" runat="server" OnTextChanged="TextBoxChanged" AutoPostBack="true" CssClass="form-control" placeholder="ex.7G07001" Enabled="true"></asp:TextBox>
                                        <%--7G07001--%>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="pull-left">
                                            <asp:LinkButton ID="btnSearchIndex1" CssClass="btn btn-primary" runat="server" ValidationGroup="SearchIndex" OnCommand="btnCommand" Font-Size="Small" CommandName="btnSearchIndex1" data-toggle="tooltip" title="Search" OnClientClick="return confirm('คุณต้องการค้นหารายการนี้ใช่หรือไม่ ?')"><span class="glyphicon glyphicon-search"></span></asp:LinkButton>


                                            <asp:LinkButton ID="btnCancel" CssClass="btn btn-default" runat="server" OnCommand="btnCommand" CommandName="cmdDocCancelresult" data-toggle="tooltip" Font-Size="Small" title="Cancel"><i class="fa fa-times"></i></asp:LinkButton>
                                        </div>

                                        </label>
                                    </div>
                                    <label class="col-sm-4 control-label"></label>
                                </div>

                                <hr />

                                <asp:Panel ID="result_insert" runat="server" Visible="false">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">รายการที่สั่งตรวจ</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="TextBox5" runat="server" CssClass="form-control" Text='Total Plate Count(CFU/g),Moistue(%MC)' Enabled="false" />
                                        </div>

                                        <label class="col-sm-6 control-label"></label>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Standard Plate Count</label>
                                        <div class="col-sm-10">
                                            <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                                                <tbody>
                                                    <tr class="info" style="font-size: Small; height: 40px;">
                                                        <th scope="col" style="width: 15%;">AC</th>
                                                        <th scope="col" style="width: 15%;">Replication : 1</th>
                                                        <th scope="col" style="width: 15%;">Replication : 2</th>
                                                        <th scope="col" style="width: 15%;">average</th>

                                                    </tr>


                                                    <tr style="font-size: Small;">
                                                        <td>fold dilution</td>
                                                        <td>
                                                            <asp:TextBox ID="TextBox20" runat="server"></asp:TextBox></td>
                                                        <td>
                                                            <asp:TextBox ID="TextBox21" runat="server"></asp:TextBox></td>
                                                        <td>
                                                            <asp:TextBox ID="TextBox22" runat="server"></asp:TextBox></td>


                                                    </tr>

                                                    <tr style="font-size: Small;">
                                                        <td>10 fold dilution</td>
                                                        <td>
                                                            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox></td>
                                                        <td>
                                                            <asp:TextBox ID="TextBox12" runat="server"></asp:TextBox></td>
                                                        <td>
                                                            <asp:TextBox ID="TextBox13" runat="server"></asp:TextBox></td>
                                                    </tr>

                                                    <tr style="font-size: Small;">
                                                        <td>100 fold dilution</td>
                                                        <td>
                                                            <asp:TextBox ID="TextBox14" runat="server"></asp:TextBox></td>
                                                        <td>
                                                            <asp:TextBox ID="TextBox15" runat="server"></asp:TextBox></td>
                                                        <td>
                                                            <asp:TextBox ID="TextBox16" runat="server"></asp:TextBox></td>
                                                    </tr>

                                                    <tr style="font-size: Small;">
                                                        <td>1000 fold dilution</td>
                                                        <td>
                                                            <asp:TextBox ID="TextBox17" runat="server"></asp:TextBox></td>
                                                        <td>
                                                            <asp:TextBox ID="TextBox18" runat="server"></asp:TextBox></td>
                                                        <td>
                                                            <asp:TextBox ID="TextBox19" runat="server"></asp:TextBox></td>
                                                    </tr>


                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Moisture(%)</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txt_moisture" runat="server" CssClass="form-control" Text='2.45' Enabled="true" />
                                        </div>

                                        <label class="col-sm-6 control-label"></label>

                                    </div>

                                    <div class="form-group">
                                        <%--<label class="col-sm-2 control-label"></label>--%>
                                        <div class="col-sm-12">
                                            <div class="pull-right">
                                                <asp:LinkButton ID="lbDocSave" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument="3,5,6,4,21"></asp:LinkButton>
                                                <asp:LinkButton ID="lbDocCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="Cancel" OnCommand="btnCommand" CommandName="cmdDocCancel"></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>

                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>


        </asp:View>
        <asp:View ID="tab6" runat="server">

            <%-- <div class="pull-right">
                        <asp:LinkButton ID="LinkButton47" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument="1,1,2,2,0"></asp:LinkButton>
                        <asp:LinkButton ID="LinkButton48" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="Cancel" OnCommand="btnCommand" CommandName="cmdDocCancel"></asp:LinkButton>

                       
                    </div>--%>
            <asp:FormView ID="fv_supervisor_detail" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>

                    <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                        <tbody>

                            <tr class="info" style="font-size: Small; text-align: center; height: 40px;">
                                <th scope="col" rowspan="2" style="width: 6%; text-align: center;">Sample Code
                                </th>
                                <th scope="col" rowspan="2" style="width: 6%; text-align: center;">Tested Date</th>
                                <th scope="col" rowspan="2" style="width: 6%; text-align: center;">% MC
                                </th>
                                <th scope="col" rowspan="2" style="width: 6%; text-align: center;">TPC(CFU/g) 
                           <br />
                                    -
                                </th>
                                <th scope="col" rowspan="2" style="width: 6%; text-align: center;">Y&M(CFU/g) 
                            <br />
                                    < 100
                                </th>
                                <th scope="col" rowspan="2" style="width: 6%; text-align: center;">Coliform(MPN/100g) 
                            <br />
                                    < 30
                                </th>
                                <th scope="col" rowspan="2" style="width: 6%; text-align: center;">Coliform(MPN/g)
                            <br />
                                    < 3
                                </th>
                                <th scope="col" rowspan="2" style="width: 6%; text-align: center;">Coliform(CFU/g)
                            <br />
                                    < 10
                                </th>
                                <th scope="col" rowspan="2" style="width: 6%; text-align: center;">E. coli(MPN/100g)
                            <br />
                                    < 30
                                </th>
                                <th scope="col" rowspan="2" style="width: 6%; text-align: center;">E. coli(MPN/g)
                            <br />
                                    < 3
                                </th>
                                <th scope="col" rowspan="2" style="width: 6%; text-align: center;">E. coli(CFU/g)
                            <br />
                                    < 10
                                </th>
                                <th scope="col" rowspan="2" style="width: 6%; text-align: center;">S. aureus(MPN/g)(Per 0.1g)
                            <br />
                                    < 3.ND
                                </th>
                                <th scope="col" rowspan="2" style="width: 6%; text-align: center;">S. aureus(CFU/g)
                            <br />
                                    < 100
                                </th>
                                <th scope="col" rowspan="2" style="width: 6%; text-align: center;">Status
                                </th>
                                <th scope="col" rowspan="2" style="width: 16%; text-align: center;">อนุมัติ/ไม่อนุมัติ 
                                </th>
                            </tr>

                            <tr class="info" style="font-size: Small; text-align: center; height: 40px;">
                            </tr>

                            <tr style="font-size: Small; text-align: center; height: 40px;">
                                <td>7G07001</td>
                                <td>07/07/2017</td>
                                <td>2.32</td>
                                <td>50</td>
                                <td>30(Y=30)</td>
                                <td>< 30</td>
                                <td>-</td>
                                <td>-</td>
                                <td>< 30</td>
                                <td>-</td>
                                <td>-</td>
                                <td>< 3</td>
                                <td>-</td>
                                <td>Waiting for Supervisor</td>
                                <td>
                                    <asp:RadioButton ID="RadioButton5" runat="server" Width="50%" AutoPostBack="true" Text="อนุมัติ" GroupName="machine1" ValidationGroup="Saveinsertdetail" />
                                    <asp:RadioButton ID="RadioButton6" runat="server" Width="50%" AutoPostBack="true" Text="ไม่อนุมัติ" GroupName="machine1" ValidationGroup="Saveinsertdetail" />
                                </td>
                            </tr>
                            <tr style="font-size: Small; text-align: center; height: 40px;">
                                <td>7G07002</td>
                                <td>12/07/2017</td>
                                <td>2.25</td>
                                <td>100</td>
                                <td>< 10</td>
                                <td>36</td>
                                <td>-</td>
                                <td>-</td>
                                <td>< 30</td>
                                <td>-</td>
                                <td>-</td>
                                <td>< 3</td>
                                <td>-</td>
                                <td>Waiting for Supervisor</td>
                                <td>
                                    <asp:RadioButton ID="RadioButton1" runat="server" Width="50%" AutoPostBack="true" Text="อนุมัติ" GroupName="machine2" ValidationGroup="Saveinsertdetail" />
                                    <asp:RadioButton ID="RadioButton2" runat="server" Width="50%" AutoPostBack="true" Text="ไม่อนุมัติ" GroupName="machine2" ValidationGroup="Saveinsertdetail" />
                                </td>
                            </tr>
                            <tr style="font-size: Small; text-align: center; height: 40px;">
                                <td>7G07003</td>
                                <td>15/07/2017</td>
                                <td>2.38</td>
                                <td>120</td>
                                <td>30(M=10)</td>
                                <td>< 30</td>
                                <td>-</td>
                                <td>-</td>
                                <td>< 30</td>
                                <td>-</td>
                                <td>-</td>
                                <td>< 3</td>
                                <td>-</td>
                                <td>Waiting for Supervisor</td>
                                <td>
                                    <asp:RadioButton ID="RadioButton3" runat="server" Width="50%" AutoPostBack="true" Text="อนุมัติ" GroupName="machine3" ValidationGroup="Saveinsertdetail" />
                                    <asp:RadioButton ID="RadioButton4" runat="server" Width="50%" AutoPostBack="true" Text="ไม่อนุมัติ" GroupName="machine3" ValidationGroup="Saveinsertdetail" />
                                </td>
                            </tr>


                        </tbody>

                    </table>
                </InsertItemTemplate>
            </asp:FormView>
            <asp:FormView ID="fv_supervisor_detail1" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>

                    <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                        <tbody>

                            <tr class="info" style="font-size: Small; text-align: center; height: 40px;">
                                <th scope="col" rowspan="2" style="width: 6%; text-align: center;">Sample Code
                                </th>
                                <th scope="col" rowspan="2" style="width: 6%; text-align: center;">Tested Date</th>
                                <th scope="col" rowspan="2" style="width: 6%; text-align: center;">% MC
                                </th>
                                <th scope="col" rowspan="2" style="width: 6%; text-align: center;">TPC(CFU/g) 
                           <br />
                                    -
                                </th>
                                <th scope="col" rowspan="2" style="width: 6%; text-align: center;">Y&M(CFU/g) 
                            <br />
                                    < 100
                                </th>
                                <th scope="col" rowspan="2" style="width: 6%; text-align: center;">Coliform(MPN/100g) 
                            <br />
                                    < 30
                                </th>
                                <th scope="col" rowspan="2" style="width: 6%; text-align: center;">Coliform(MPN/g)
                            <br />
                                    < 3
                                </th>
                                <th scope="col" rowspan="2" style="width: 6%; text-align: center;">Coliform(CFU/g)
                            <br />
                                    < 10
                                </th>
                                <th scope="col" rowspan="2" style="width: 6%; text-align: center;">E. coli(MPN/100g)
                            <br />
                                    < 30
                                </th>
                                <th scope="col" rowspan="2" style="width: 6%; text-align: center;">E. coli(MPN/g)
                            <br />
                                    < 3
                                </th>
                                <th scope="col" rowspan="2" style="width: 6%; text-align: center;">E. coli(CFU/g)
                            <br />
                                    < 10
                                </th>
                                <th scope="col" rowspan="2" style="width: 6%; text-align: center;">S. aureus(MPN/g)(Per 0.1g)
                            <br />
                                    < 3.ND
                                </th>
                                <th scope="col" rowspan="2" style="width: 6%; text-align: center;">S. aureus(CFU/g)
                            <br />
                                    < 100
                                </th>
                                <th scope="col" rowspan="2" style="width: 6%; text-align: center;">Status
                                </th>
                                <th scope="col" rowspan="2" style="width: 16%; text-align: center;">อนุมัติ/ไม่อนุมัติ 
                                </th>
                            </tr>

                            <tr class="info" style="font-size: Small; text-align: center; height: 40px;">
                            </tr>
                            <tr style="font-size: Small; text-align: center; height: 40px;">
                                <td>7G07003</td>
                                <td>15/07/2017</td>
                                <td>2.38</td>
                                <td>120</td>
                                <td>30(M=10)</td>
                                <td>< 30</td>
                                <td>-</td>
                                <td>-</td>
                                <td>< 30</td>
                                <td>-</td>
                                <td>-</td>
                                <td>< 3</td>
                                <td>-</td>
                                <td>Waiting for Supervisor</td>
                                <td>
                                    <asp:RadioButton ID="RadioButton3" runat="server" Width="50%" AutoPostBack="true" Text="อนุมัติ" GroupName="machine3" ValidationGroup="Saveinsertdetail" />
                                    <asp:RadioButton ID="RadioButton4" runat="server" Width="50%" AutoPostBack="true" Text="ไม่อนุมัติ" GroupName="machine3" ValidationGroup="Saveinsertdetail" />
                                </td>
                            </tr>


                        </tbody>

                    </table>
                </InsertItemTemplate>
            </asp:FormView>

            <div class="pull-left">
                <asp:LinkButton ID="LinkButton4dd7" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="cmdDocSaveSup"></asp:LinkButton>
                <asp:LinkButton ID="LinkButddton48" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="Cancel" OnCommand="btnCommand" CommandName="cmdDocCancel"></asp:LinkButton>


            </div>

        </asp:View>

    </asp:MultiView>
    <!--multiview-->
</asp:Content>
