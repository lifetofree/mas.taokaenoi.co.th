﻿using DotNet.Highcharts;
using DotNet.Highcharts.Helpers;
using DotNet.Highcharts.Options;
////using Google.Apis.Auth.OAuth2;
////using Google.Apis.Calendar.v3;
////using Google.Apis.Calendar.v3.Data;
////using Google.Apis.Services;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Dynamic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class websystem_hr_hr_room_booking : System.Web.UI.Page
{

    #region initial function/data
    function_tool _funcTool = new function_tool();
    service_execute serviceexcute = new service_execute();
    data_employee _dataEmployee = new data_employee();
    data_roombooking _data_roombooking = new data_roombooking();


    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _keynetworkroombooking = ConfigurationManager.AppSettings["keynetworkroombooking"];
    string link_system_roombooking = "http://localhost/mas.taokaenoi.co.th/room-booking/";


    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    //-- employee --//

    // -- Room Booking
    static string _urlGetRbkm0Place = _serviceUrl + ConfigurationManager.AppSettings["urlGetRbkm0Place"];
    static string _urlGetRbkm0RoomDetailInPlace = _serviceUrl + ConfigurationManager.AppSettings["urlGetRbkm0RoomDetailInPlace"];
    static string _urlGetRbkSearchRommCreate = _serviceUrl + ConfigurationManager.AppSettings["urlGetRbkSearchRommCreate"];
    static string _urlGetCheckRoomInCreate = _serviceUrl + ConfigurationManager.AppSettings["urlGetCheckRoomInCreate"];
    static string _urlGetRbkm0ResultUse = _serviceUrl + ConfigurationManager.AppSettings["urlGetRbkm0ResultUse"];
    static string _urlSetRoomBooking = _serviceUrl + ConfigurationManager.AppSettings["urlSetRoomBooking"];
    static string _urlGetDetailRoomBooking = _serviceUrl + ConfigurationManager.AppSettings["urlGetDetailRoomBooking"];
    static string _urlGetViewDetailRoomBooking = _serviceUrl + ConfigurationManager.AppSettings["urlGetViewDetailRoomBooking"];
    static string _urlGetLogDetailRoomBooking = _serviceUrl + ConfigurationManager.AppSettings["urlGetLogDetailRoomBooking"];
    static string _urlGetWaitApproveRoomBooking = _serviceUrl + ConfigurationManager.AppSettings["urlGetWaitApproveRoomBooking"];
    static string _urlGetCountWaitApproveRoomBooking = _serviceUrl + ConfigurationManager.AppSettings["urlGetCountWaitApproveRoomBooking"];
    static string _urlGetViewWaitApproveRoomBooking = _serviceUrl + ConfigurationManager.AppSettings["urlGetViewWaitApproveRoomBooking"];
    static string _urlGetDecisionApprove = _serviceUrl + ConfigurationManager.AppSettings["urlGetDecisionApprove"];
    static string _urlSetApproveWaitRoomBooking = _serviceUrl + ConfigurationManager.AppSettings["urlSetApproveWaitRoomBooking"];
    static string _urlGetStatusSearchDetailRbk = _serviceUrl + ConfigurationManager.AppSettings["urlGetStatusSearchDetailRbk"];
    static string _urlGetSearchDetailRbk = _serviceUrl + ConfigurationManager.AppSettings["urlGetSearchDetailRbk"];
    static string _urlGetRbkm0Food = _serviceUrl + ConfigurationManager.AppSettings["urlGetRbkm0Food"];
    static string _urlGetViewFoodDetailRbk = _serviceUrl + ConfigurationManager.AppSettings["urlGetViewFoodDetailRbk"];
    static string _urlGetReportRoomTableRbk = _serviceUrl + ConfigurationManager.AppSettings["urlGetReportRoomTableRbk"];
    static string _urlGetReportRoomGraphRbk = _serviceUrl + ConfigurationManager.AppSettings["urlGetReportRoomGraphRbk"];
    static string _urlGetSearchDetailApproveRbk = _serviceUrl + ConfigurationManager.AppSettings["urlGetSearchDetailApproveRbk"];
    static string _urlGetDetailRoomShowSlider = _serviceUrl + ConfigurationManager.AppSettings["urlGetDetailRoomShowSlider"];
    static string _urlGetPermissionRoomBooking = _serviceUrl + ConfigurationManager.AppSettings["urlGetPermissionRoomBooking"];
    static string _urlGetRbkm0Conference = _serviceUrl + ConfigurationManager.AppSettings["urlGetRbkm0Conference_Devices"];

    // -- Room Booking


    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;
    //ViewState["vs_ViewWaitApproveRoomBooking"]

    String _keyfile;
    String _keyPassword;
    String _serviceAccountEmail;
    String _calendarId;

    #endregion initial function/data

    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());

        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];
        //check permission
        //foreach (int item in rdept_qmr)
        //{
        //    if (_dataEmployee.employee_list[0].rdept_idx == item)
        //    {
        //        _flag_qmr = true;
        //        break;
        //    }
        //}
        ViewState["rsec_permission"] = _dataEmployee.employee_list[0].rsec_idx;
        ViewState["rdept_permission"] = _dataEmployee.employee_list[0].rdept_idx;
        ViewState["rpos_permission"] = _dataEmployee.employee_list[0].rpos_idx;
        ViewState["joblevel_permission"] = _dataEmployee.employee_list[0].jobgrade_level;


        Session["PlaceSearch_Room"] = 0;
        Session["RoomIDXSearch_Room"] = 0;

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            initPage();
            initPageLoad();
            //getCountBookingReportGraph();
            string _key_u0_idx_room = "";// Page.RouteData.Values["link_u0_room_idx"].ToString().ToLower();


            string _u0_room_idx_comvert = "";
            _u0_room_idx_comvert = _funcTool.getDecryptRC4(_key_u0_idx_room.ToString(), _keynetworkroombooking);

            string _url_room_booking = link_system_roombooking + _u0_room_idx_comvert;
            //litDebug.Text = _u0_room_idx_comvert;

            //Uri myUri = new Uri(_url_room_booking);
            //string host = myUri.Host;  // host is "www.contoso.com"


            //litDebug.Text = link_system_roombooking;
            //Response.Redirect(host);
            if (_u0_room_idx_comvert.ToString() != "0")
            {
                if (ViewState["rpos_permission"].ToString() == "5911" || ViewState["rpos_permission"].ToString() == "5912" || ViewState["rpos_permission"].ToString() == "5882" || ViewState["rpos_permission"].ToString() == "5883" || ViewState["rpos_permission"].ToString() == "5896" || ViewState["rpos_permission"].ToString() == "5897" || ViewState["rpos_permission"].ToString() == "886" || ViewState["rpos_permission"].ToString() == "13" || ViewState["rsec_permission"].ToString() == "210" || ViewState["rpos_permission"].ToString() == "3543" || ViewState["rpos_permission"].ToString() == "5945" || ViewState["rpos_permission"].ToString() == "12220" || ViewState["rpos_permission"].ToString() == "13630" || ViewState["rsec_permission"].ToString() == "80" || ViewState["rsec_permission"].ToString() == "79" || ViewState["rpos_permission"].ToString() == "949" || ViewState["rpos_permission"].ToString() == "7114")
                {

                    //litDebug.Text = _u0_room_idx_comvert;
                    getCountWaitApprove();
                    //RemoveQueryStringByKey(link_system_roombooking, _u0_room_idx_comvert);
                }
                else
                {
                    setActiveTab("docDetail", 0, 0, 0, 0);
                    //ssss
                }
                // Response.Redirect(link_system_roombooking);
            }
            else
            {
                if (ViewState["rpos_permission"].ToString() == "5911" || ViewState["rpos_permission"].ToString() == "5912" || ViewState["rpos_permission"].ToString() == "5882" || ViewState["rpos_permission"].ToString() == "5883" || ViewState["rpos_permission"].ToString() == "5896" || ViewState["rpos_permission"].ToString() == "5897" || ViewState["rpos_permission"].ToString() == "886" || ViewState["rpos_permission"].ToString() == "13" || ViewState["rsec_permission"].ToString() == "210" || ViewState["rpos_permission"].ToString() == "3543" || ViewState["rpos_permission"].ToString() == "5945" || ViewState["rpos_permission"].ToString() == "12220" || ViewState["rpos_permission"].ToString() == "13630" || ViewState["rsec_permission"].ToString() == "80" || ViewState["rsec_permission"].ToString() == "79" || ViewState["rpos_permission"].ToString() == "949" || ViewState["rpos_permission"].ToString() == "7114")
                {
                    getCountWaitApprove();
                }
                // Response.Redirect(link_system_roombooking);
            }

        }
        else
        {
            //litDebug.Text = "54444";//_u0_room_idx_comvert;
            // Page.Response.Redirect(link_system_roombooking, true);
        }


    }


    #region set/get bind data

    //protected void getShowDetailRoomSlider()
    //{

    //    data_roombooking data_u0_roombooking_detail = new data_roombooking();
    //    rbk_u0_roombooking_detail m0_u0_roombooking_detail = new rbk_u0_roombooking_detail();
    //    data_u0_roombooking_detail.rbk_u0_roombooking_list = new rbk_u0_roombooking_detail[1];

    //    data_u0_roombooking_detail.rbk_u0_roombooking_list[0] = m0_u0_roombooking_detail;
    //    //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
    //    data_u0_roombooking_detail = callServicePostRoomBooking(_urlGetDetailRoomShowSlider, data_u0_roombooking_detail);

    //    if (data_u0_roombooking_detail.return_code == 0)
    //    {
    //        int i = 0;


    //        //   if (images.Text != null)
    //        // {

    //        foreach (var file in data_u0_roombooking_detail.rbk_u0_roombooking_list)
    //        {

    //            // string imagePath = _dataNews.u0_it_news_list[0].title_it_news;//ResolveUrl(getPath + "room-" + dataIBK.ibk_u0_reserve_action[0].u0_room_idx_ref + "/" + file.Name);
    //            indicators.Text += i == 0
    //            ? "<li data-target='#myCarousel' data-slide-to='" + i + "' class='active'></li>"
    //            : "<li data-target='#myCarousel' data-slide-to='" + i + "' class=''></li>";
    //            images.Text += i == 0 ? "<div class='item active'>" : "<div class='item'>";
    //            alertnews.Text = "<div style='margin:10px;' ></div>" + "&nbsp;&nbsp; " + "<i class=" + "'glyphicon glyphicon-globe'" + "></i>" + "<b>" + " รายการห้องประชุมวันนี้" + "</b>";
    //            // lbidxnews.Text += _dataNews.u0_it_news_list[i].u0_news_idx.ToString();
    //            //images.Text += "<br />" + "&nbsp;&nbsp; " + "<b>" + _dataNews.u0_it_news_list[i].title_it_news + "</b>"+ 
    //            //               "<br />" + "<a href =" + _dataNews.u0_it_news_list[i].u0_news_idx + "class=" + "'btn btn-default'" +"OnClick=" + "'Click'" + ">" + "</a>";


    //            images.Text += "<br />" + "&nbsp;&nbsp; " + "<b>" + data_u0_roombooking_detail.rbk_u0_roombooking_list[i].room_name_th + "</b>";
    //            //  "<br />" + "&nbsp;&nbsp; " + "<a href =" + _dataNews.u0_it_news_list[i].u0_news_idx +  ">" + "<button Visible='false'>" + "ดูรายละเอียด..." + "</ button >" + "</a>";


    //            images.Text += "</div>";

    //            i++;

    //        }



    //    }


    //}

    protected void getPlace(DropDownList ddlName, int _place_idx)
    {

        data_roombooking data_m0_place_detail = new data_roombooking();
        rbk_m0_place_detail m0_place_detail = new rbk_m0_place_detail();
        data_m0_place_detail.rbk_m0_place_list = new rbk_m0_place_detail[1];

        m0_place_detail.condition = 1;

        data_m0_place_detail.rbk_m0_place_list[0] = m0_place_detail;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        data_m0_place_detail = callServicePostRoomBooking(_urlGetRbkm0Place, data_m0_place_detail);

        setDdlData(ddlName, data_m0_place_detail.rbk_m0_place_list, "place_name", "place_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกสถานที่ ---", "0"));
        ddlName.SelectedValue = _place_idx.ToString();

    }

    protected void getRoom(DropDownList ddlName, int _place_idx)
    {

        data_roombooking data_m0_roomdetail = new data_roombooking();
        rbk_m0_room_detail m0_room_detail = new rbk_m0_room_detail();
        data_m0_roomdetail.rbk_m0_room_list = new rbk_m0_room_detail[1];

        m0_room_detail.place_idx = _place_idx;

        data_m0_roomdetail.rbk_m0_room_list[0] = m0_room_detail;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        data_m0_roomdetail = callServicePostRoomBooking(_urlGetRbkm0RoomDetailInPlace, data_m0_roomdetail);

        setDdlData(ddlName, data_m0_roomdetail.rbk_m0_room_list, "room_name_th", "m0_room_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกห้องประชุม ---", "0"));
        //ddlName.SelectedValue = _place_idx.ToString();

    }

    protected void getRoomEdit(DropDownList ddlName, int _roomedit_idx, int _place_edit_idx)
    {

        data_roombooking data_m0_roomdetail = new data_roombooking();
        rbk_m0_room_detail m0_room_detail = new rbk_m0_room_detail();
        data_m0_roomdetail.rbk_m0_room_list = new rbk_m0_room_detail[1];

        m0_room_detail.place_idx = _place_edit_idx;

        data_m0_roomdetail.rbk_m0_room_list[0] = m0_room_detail;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        data_m0_roomdetail = callServicePostRoomBooking(_urlGetRbkm0RoomDetailInPlace, data_m0_roomdetail);

        setDdlData(ddlName, data_m0_roomdetail.rbk_m0_room_list, "room_name_th", "m0_room_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกห้องประชุม ---", "0"));
        ddlName.SelectedValue = _roomedit_idx.ToString();

    }

    protected void getReSultUse(DropDownList ddlName, int _result_use)
    {

        data_roombooking data_m0_resultuse = new data_roombooking();
        rbk_m0_result_use_detail m0_result_use = new rbk_m0_result_use_detail();
        data_m0_resultuse.rbk_m0_result_use_list = new rbk_m0_result_use_detail[1];

        m0_result_use.result_use_idx = 0;
        m0_result_use.condition = 1;

        data_m0_resultuse.rbk_m0_result_use_list[0] = m0_result_use;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        data_m0_resultuse = callServicePostRoomBooking(_urlGetRbkm0ResultUse, data_m0_resultuse);

        setDdlData(ddlName, data_m0_resultuse.rbk_m0_result_use_list, "result_use_name", "result_use_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกเหตุผลการใช้ห้อง ---", "0"));
        ddlName.SelectedValue = _result_use.ToString();

    }

    protected void SelectFoodDetail()
    {

        data_roombooking data_m0_food_detail = new data_roombooking();
        rbk_m0_food_detail m0_food_detail = new rbk_m0_food_detail();
        data_m0_food_detail.rbk_m0_food_list = new rbk_m0_food_detail[1];

        m0_food_detail.condition = 1;

        data_m0_food_detail.rbk_m0_food_list[0] = m0_food_detail;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        data_m0_food_detail = callServicePostRoomBooking(_urlGetRbkm0Food, data_m0_food_detail);

        //ViewState["data_test_sample"] = data_m0_food_detail.rbk_m0_food_list;

        //CheckBoxList chk_TestDetail = (CheckBoxList)fvActor1Node1.FindControl("chk_TestDetail");
        chkFoodDetail.Items.Clear();
        chkFoodDetail.AppendDataBoundItems = true;
        chkFoodDetail.DataSource = data_m0_food_detail.rbk_m0_food_list;
        chkFoodDetail.DataTextField = "food_name";
        chkFoodDetail.DataValueField = "food_idx";
        chkFoodDetail.DataBind();

        //ViewState["data_test_sample"] = chk_TestDetail.SelectedValue;

        //setGridData(GvFoodInRoom, data_m0_food_detail.rbk_m0_food_list);

    }

    protected void getDetailRoomBooking()
    {

        data_roombooking data_u0_roomdetail = new data_roombooking();
        rbk_u0_roombooking_detail u0_roomdetail = new rbk_u0_roombooking_detail();
        data_u0_roomdetail.rbk_u0_roombooking_list = new rbk_u0_roombooking_detail[1];

        u0_roomdetail.cemp_idx = _emp_idx;
        u0_roomdetail.admin_idx = _emp_idx;
        u0_roomdetail.rpos_idx = int.Parse(ViewState["rpos_permission"].ToString());

        data_u0_roomdetail.rbk_u0_roombooking_list[0] = u0_roomdetail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_roomdetail));        
        data_u0_roomdetail = callServicePostRoomBooking(_urlGetDetailRoomBooking, data_u0_roomdetail);

        ViewState["vs_DetailRoomBooking"] = data_u0_roomdetail.rbk_u0_roombooking_list;
        setGridData(GvDetailRoomBooking, ViewState["vs_DetailRoomBooking"]);



    }

    protected void getCountWaitApprove()
    {

        data_roombooking data_u0_roomdetail_count = new data_roombooking();
        rbk_u0_roombooking_detail u0_roomdetail_count = new rbk_u0_roombooking_detail();
        data_u0_roomdetail_count.rbk_u0_roombooking_list = new rbk_u0_roombooking_detail[1];
        u0_roomdetail_count.admin_idx = _emp_idx;
        data_u0_roomdetail_count.rbk_u0_roombooking_list[0] = u0_roomdetail_count;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        data_u0_roomdetail_count = callServicePostRoomBooking(_urlGetCountWaitApproveRoomBooking, data_u0_roomdetail_count);

        if (data_u0_roomdetail_count.rbk_u0_roombooking_list[0].Count_WaitApprove > 0)
        {
            //litDebug.Text = "1";
            ViewState["vs_CountWaitApprove"] = data_u0_roomdetail_count.rbk_u0_roombooking_list[0].Count_WaitApprove;
            lbApprove.Text = "รายการ รออนุมัติ/ รอการเปลี่ยนแปลง <span class='badge progress-bar-danger' >" + Convert.ToString(ViewState["vs_CountWaitApprove"]) + "</span>";
            setActiveTab("docApprove", 0, 0, 0, 0);

        }
        else
        {
            //litDebug.Text = "2";
            ViewState["vs_CountWaitApprove"] = 0;
            lbApprove.Text = "รายการ รออนุมัติ/ รอการเปลี่ยนแปลง <span class='badge progress-bar-danger' >" + Convert.ToString(ViewState["vs_CountWaitApprove"]) + "</span>";
            setActiveTab("docDetailRoom", 0, 0, 0, 0);


        }

        //ViewState["vs_DetailRoomBooking"] = data_u0_roomdetail.rbk_u0_roombooking_list;
        //setGridData(GvDetailRoomBooking, ViewState["vs_DetailRoomBooking"]);

    }

    protected void getWaitApproveRoomBooking()
    {

        data_roombooking data_u0_roomdetail = new data_roombooking();
        rbk_u0_roombooking_detail u0_roomdetail = new rbk_u0_roombooking_detail();
        data_u0_roomdetail.rbk_u0_roombooking_list = new rbk_u0_roombooking_detail[1];
        u0_roomdetail.admin_idx = _emp_idx;

        data_u0_roomdetail.rbk_u0_roombooking_list[0] = u0_roomdetail;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        data_u0_roomdetail = callServicePostRoomBooking(_urlGetWaitApproveRoomBooking, data_u0_roomdetail);

        ViewState["vs_WaitApproveRoomBooking"] = data_u0_roomdetail.rbk_u0_roombooking_list;
        setGridData(GvWaitApprove, ViewState["vs_WaitApproveRoomBooking"]);

    }

    protected void getViewWaitApproveRoomBooking(int _u0_idx_viewapprove)
    {

        ViewState["vs_ViewWaitApproveRoomBooking"] = "";

        data_roombooking data_u0_waitapprove = new data_roombooking();
        rbk_u0_roombooking_detail u0_waitapprove = new rbk_u0_roombooking_detail();
        data_u0_waitapprove.rbk_u0_roombooking_list = new rbk_u0_roombooking_detail[1];

        u0_waitapprove.u0_document_idx = _u0_idx_viewapprove;

        data_u0_waitapprove.rbk_u0_roombooking_list[0] = u0_waitapprove;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        data_u0_waitapprove = callServicePostRoomBooking(_urlGetViewWaitApproveRoomBooking, data_u0_waitapprove);

        ViewState["vs_ViewWaitApproveRoomBooking"] = data_u0_waitapprove.rbk_u0_roombooking_list;
        FvWaitApproveDetail.Visible = true;
        setFormData(FvWaitApproveDetail, FormViewMode.ReadOnly, ViewState["vs_ViewWaitApproveRoomBooking"]);

    }

    protected void getViewDetailRoomBooking(int _u0_docidx)
    {

        data_roombooking data_u0_roomdetail_view = new data_roombooking();
        rbk_u0_roombooking_detail u0_roomdetail_view = new rbk_u0_roombooking_detail();
        data_u0_roomdetail_view.rbk_u0_roombooking_list = new rbk_u0_roombooking_detail[1];

        u0_roomdetail_view.u0_document_idx = _u0_docidx;

        data_u0_roomdetail_view.rbk_u0_roombooking_list[0] = u0_roomdetail_view;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        data_u0_roomdetail_view = callServicePostRoomBooking(_urlGetViewDetailRoomBooking, data_u0_roomdetail_view);

        ViewState["vs_ViewDetailRoomBooking"] = data_u0_roomdetail_view.rbk_u0_roombooking_list;
        fvDetailRoomBooking.Visible = true;
        setFormData(fvDetailRoomBooking, FormViewMode.ReadOnly, ViewState["vs_ViewDetailRoomBooking"]);


    }

    protected void getLogDetailRoomBooking(int _u0_docidx_log)
    {

        data_roombooking data_u0_roomdetail_log = new data_roombooking();
        rbk_u1_roombooking_detail u0_roomdetail_log = new rbk_u1_roombooking_detail();
        data_u0_roomdetail_log.rbk_u1_roombooking_list = new rbk_u1_roombooking_detail[1];

        u0_roomdetail_log.u0_document_idx = _u0_docidx_log;

        data_u0_roomdetail_log.rbk_u1_roombooking_list[0] = u0_roomdetail_log;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        data_u0_roomdetail_log = callServicePostRoomBooking(_urlGetLogDetailRoomBooking, data_u0_roomdetail_log);

        ViewState["vs_LogDetailRoomBooking"] = data_u0_roomdetail_log.rbk_u1_roombooking_list;
        setRepeaterData(rptLogRoomBooking, ViewState["vs_LogDetailRoomBooking"]);

        //setFormData(rptLogRoomBooking, FormViewMode.ReadOnly, ViewState["vs_LogDetailRoomBooking"]);


    }

    protected void getLogRoomBookingWaitApprove(int _u0_docidx_logapprove)
    {

        data_roombooking data_u0_roomdetail_logapprove = new data_roombooking();
        rbk_u1_roombooking_detail u0_roomdetail_logapprove = new rbk_u1_roombooking_detail();
        data_u0_roomdetail_logapprove.rbk_u1_roombooking_list = new rbk_u1_roombooking_detail[1];

        u0_roomdetail_logapprove.u0_document_idx = _u0_docidx_logapprove;

        data_u0_roomdetail_logapprove.rbk_u1_roombooking_list[0] = u0_roomdetail_logapprove;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        data_u0_roomdetail_logapprove = callServicePostRoomBooking(_urlGetLogDetailRoomBooking, data_u0_roomdetail_logapprove);

        ViewState["vs_LogDetailRoomBookingWaitApprove"] = data_u0_roomdetail_logapprove.rbk_u1_roombooking_list;
        setRepeaterData(rptLogRoomBookingWaitApprove, ViewState["vs_LogDetailRoomBookingWaitApprove"]);

    }

    protected void getDecision(DropDownList ddlName, int _node_)
    {

        data_roombooking data_decision = new data_roombooking();
        rbk_decision_detail m0_decision = new rbk_decision_detail();
        data_decision.rbk_decision_list = new rbk_decision_detail[1];

        m0_decision.noidx = _node_;

        data_decision.rbk_decision_list[0] = m0_decision;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        data_decision = callServicePostRoomBooking(_urlGetDecisionApprove, data_decision);

        setDdlData(ddlName, data_decision.rbk_decision_list, "decision_name", "decision_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกพิจารณารายการ ---", "0"));
        //ddlName.SelectedValue = _place_idx.ToString();

    }

    protected void getStatusSearch(DropDownList ddlName)
    {

        data_roombooking data_status_search = new data_roombooking();
        rbk_decision_detail m0_status_search = new rbk_decision_detail();
        data_status_search.rbk_decision_list = new rbk_decision_detail[1];

        data_status_search.rbk_decision_list[0] = m0_status_search;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        data_status_search = callServicePostRoomBooking(_urlGetStatusSearchDetailRbk, data_status_search);

        setDdlData(ddlName, data_status_search.rbk_decision_list, "status_name", "staidx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกสถานะเอกสาร ---", "0"));
        //ddlName.SelectedValue = _place_idx.ToString();

    }

    protected void getOrganizationList(DropDownList ddlName)
    {
        _dataEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dataEmployee.organization_list[0] = _orgList;

        _dataEmployee = callServicePostEmployee(_urlGetOrganizationList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกองค์กร ---", "0"));
    }

    protected void getDepartmentList(DropDownList ddlName, int _org_idx)
    {
        _dataEmployee.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _dataEmployee.department_list[0] = _deptList;

        _dataEmployee = callServicePostEmployee(_urlGetDepartmentList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกฝ่าย ---", "0"));
    }

    protected void SelectConferenceDetail(GridView gvName)
    {

        data_roombooking data_m0_con_detail = new data_roombooking();
        rbk_m0_conference_detail m0_con_detail = new rbk_m0_conference_detail();
        data_m0_con_detail.rbk_m0_conference_list = new rbk_m0_conference_detail[1];

        m0_con_detail.condition = 1;

        data_m0_con_detail.rbk_m0_conference_list[0] = m0_con_detail;

        //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0_food_detail));
        data_m0_con_detail = callServicePostRoomBooking(_urlGetRbkm0Conference, data_m0_con_detail);

        setGridData(gvName, data_m0_con_detail.rbk_m0_conference_list);

    }


    protected void getddlYear(DropDownList ddlName)
    {

        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Clear();

        var currentYear = DateTime.Today.Year;
        for (int i = 5; i >= 0; i--)
        {
            ddlName.Items.Add((currentYear - i).ToString());
        }

        ddlName.Items.FindByValue(Convert.ToString(currentYear)).Selected = true;
    }


    #endregion set/get bind data


    ////public class GoogleCalendarEventSmsNotifier
    ////{
    ////    String _keyfile;
    ////    String _keyPassword;
    ////    String _serviceAccountEmail;
    ////    String _calendarId;
    ////    public GoogleCalendarEventSmsNotifier(String keyFile, String keyPassword, String serviceAccountEmail, String calendarId)
    ////    {
    ////        _keyfile = keyFile;
    ////        _keyPassword = keyPassword;
    ////        _serviceAccountEmail = serviceAccountEmail;
    ////        _calendarId = calendarId;
    ////    }

    ////    public dynamic SendSms(dynamic request)
    ////    {
    ////        dynamic response = new ExpandoObject();
    ////        response.Status = true;
    ////        var certificate = new X509Certificate2(_keyfile, _keyPassword, X509KeyStorageFlags.Exportable);

    ////        ServiceAccountCredential credential = new ServiceAccountCredential(
    ////           new ServiceAccountCredential.Initializer(_serviceAccountEmail)
    ////           {
    ////               Scopes = new[] { CalendarService.Scope.Calendar }
    ////           }.FromCertificate(certificate));

    ////        // Create the service.
    ////        var service = new CalendarService(new BaseClientService.Initializer()
    ////        {
    ////            HttpClientInitializer = credential,
    ////            ApplicationName = "Calendar Api",
    ////            ApiKey = ""
    ////        });


    ////        Google.Apis.Calendar.v3.Data.Event newEvent = new Google.Apis.Calendar.v3.Data.Event();
    ////        newEvent.Kind = "calendar#event";


    ////        EventDateTime EStart = new EventDateTime();
    ////        EStart.DateTime = request.eventStart;
    ////        newEvent.Start = EStart;
    ////        EventDateTime EEnd = new EventDateTime();
    ////        EEnd.DateTime = request.eventEnd;
    ////        newEvent.End = EEnd;


    ////        newEvent.Reminders = new Event.RemindersData();
    ////        newEvent.Reminders.UseDefault = false;
    ////        EventReminder ER = new EventReminder();
    ////        ER.Method = "sms";
    ////        ER.Minutes = request.eventReminderMinutes;
    ////        newEvent.Reminders.Overrides = new List<EventReminder>();
    ////        newEvent.Reminders.Overrides.Add(ER);


    ////        newEvent.Summary = request.eventSummary;
    ////        newEvent.Description = request.eventDescription;
    ////        newEvent.Organizer = new Event.OrganizerData() { DisplayName = request.eventOrganizerName, Email = request.eventOrganizerEmail, Self = true };
    ////        newEvent.Creator = new Event.CreatorData() { DisplayName = request.eventCreatorName, Email = request.eventCreatorEmail, Self = true };


    ////        EventAttendee attendee = new EventAttendee();
    ////        attendee.Email = request.eventAttendeeEmail;
    ////        attendee.DisplayName = request.eventAttendeeName;
    ////        newEvent.Location = request.eventLocation;

    ////        IList<EventAttendee> myAttendees = new List<EventAttendee>();
    ////        myAttendees.Add(attendee);
    ////        newEvent.Attendees = myAttendees;


    ////        try
    ////        {
    ////            //service.Calendars.Insert(newCal).Execute();                
    ////            EventsResource.InsertRequest insertRequest = service.Events.Insert(newEvent, _calendarId);
    ////            insertRequest.SendNotifications = true;
    ////            response.Result = insertRequest.Execute();
    ////        }
    ////        catch (Exception e)
    ////        {
    ////            response.Status = false;
    ////            response.Exception = e;
    ////            response.Message = e.Message;
    ////        }
    ////        response.Message = "SMS has been sent successfully";
    ////        return response;

    ////    }

    ////}


    #region bind graph
    public void getCountBookingReportGraph()
    {

        data_roombooking data_report_countbooking = new data_roombooking();
        rbk_search_report_room_detail search_report_countbooking = new rbk_search_report_room_detail();
        data_report_countbooking.rbk_search_report_room_list = new rbk_search_report_room_detail[1];

        search_report_countbooking.cemp_idx = _emp_idx;
        search_report_countbooking.DC_YEAR = ddlyear.SelectedValue.ToString();
        search_report_countbooking.DC_Mount = ddlmonth.SelectedValue.ToString();
        search_report_countbooking.staidx = int.Parse(ddlStatusBooking.SelectedValue);
        search_report_countbooking.condition = 0;

        data_report_countbooking.rbk_search_report_room_list[0] = search_report_countbooking;

        //_local_xml = serviceexcute.actionExec("conn_mas", "data_roombooking", "service_roombooking", data_report_countbooking, 261);
        //data_report_countbooking = (data_roombooking)_funcTool.convertXmlToObject(typeof(data_roombooking), _local_xml);

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_report_countbooking));

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_report_countbooking));
        data_report_countbooking = callServicePostRoomBooking(_urlGetReportRoomGraphRbk, data_report_countbooking);

        ViewState["Vs_ReportCountBookingDept"] = data_report_countbooking.rbk_search_report_room_list;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_report_countbooking));

        if (data_report_countbooking.return_code == 0)
        {

            //litDebug.Text = "3333";
            int count = data_report_countbooking.rbk_search_report_room_list.Length;
            string[] Dept_name = new string[count];
            object[] Count_booking = new object[count];
            int i = 0;
            foreach (var data in data_report_countbooking.rbk_search_report_room_list)
            {
                Dept_name[i] = data.dept_name_th.ToString();
                Count_booking[i] = data.count_booking.ToString();
                i++;
            }
            Highcharts chart = new Highcharts("chart");
            chart.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "Number of times" } } });
            chart.SetXAxis(new XAxis { Categories = Dept_name });
            chart.SetSeries(
                new DotNet.Highcharts.Options.Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                    Name = "Count of Booking",
                    Data = new Data(Count_booking)
                }
            );
            Update_PanelGraph.Visible = true;
            litReportChart.Text = chart.ToHtmlString();
            // -- -- //

            // -- pie chart --//
            var caseclose = new List<object>();

            foreach (var data in data_report_countbooking.rbk_search_report_room_list)
            {
                caseclose.Add(new object[] { data.dept_name_th.ToString(), data.count_booking.ToString() });
            }

            Highcharts chart1 = new Highcharts("chart1");
            chart1.SetTitle(new DotNet.Highcharts.Options.Title { Text = "Department / Counts" });
            chart1.InitChart(new DotNet.Highcharts.Options.Chart { DefaultSeriesType = DotNet.Highcharts.Enums.ChartTypes.Pie });

            chart1.SetPlotOptions(new PlotOptions
            {
                Pie = new PlotOptionsPie
                {
                    ShowInLegend = true,
                    AllowPointSelect = true,
                    DataLabels = new PlotOptionsPieDataLabels
                    {
                        Enabled = true,
                        // Formatter = "function() { return '" + this. + ' : '" + lv1count + "';}" //  this.percentage.toFixed(2)+' % '; }"
                        //Color = ColorTranslator.FromHtml("#000000"),
                        //ConnectorColor = ColorTranslator.FromHtml("#000000"),
                        Formatter = "function() { return this.point.name +' : '+  this.y.toFixed(0)+' '; }"
                    }
                }
            });

            chart1.SetTooltip(new Tooltip
            {
                Enabled = true,
                Formatter = "function() { return this.point.name +' : '+  this.percentage.toFixed(2)+' % '; }"

            });
            chart1.SetSeries(new DotNet.Highcharts.Options.Series[]
                 {
                        new DotNet.Highcharts.Options.Series()
                        {
                            Data = new Data (caseclose.ToArray())
                        }
                 });


            litReportChart1.Text = chart1.ToHtmlString();
            // -- pie chart --//
        }
        else
        {
            Update_PanelGraph.Visible = false;
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- ไม่พบข้อมูล ---');", true);

            //litReportChart.Text = "<p class='bg-danger'>No result.</p>";
        }
    }

    public void getCountCancelBookingReportGraph()
    {

        data_roombooking data_report_countcancel = new data_roombooking();
        rbk_search_report_room_detail search_report_countcancel = new rbk_search_report_room_detail();
        data_report_countcancel.rbk_search_report_room_list = new rbk_search_report_room_detail[1];

        search_report_countcancel.cemp_idx = _emp_idx;
        search_report_countcancel.DC_YEAR = ddlyear.SelectedValue.ToString();
        search_report_countcancel.DC_Mount = ddlmonth.SelectedValue.ToString();
        search_report_countcancel.staidx = int.Parse(ddlStatusBooking.SelectedValue);
        search_report_countcancel.condition = 1;

        data_report_countcancel.rbk_search_report_room_list[0] = search_report_countcancel;

        //_local_xml = serviceexcute.actionExec("conn_mas", "data_roombooking", "service_roombooking", data_report_countbooking, 261);
        //data_report_countbooking = (data_roombooking)_funcTool.convertXmlToObject(typeof(data_roombooking), _local_xml);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_report_countbooking));
        data_report_countcancel = callServicePostRoomBooking(_urlGetReportRoomGraphRbk, data_report_countcancel);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_report_countcancel));
        //ViewState["Vs_ReportCountBookingDept"] = data_report_countbooking.rbk_search_report_room_list;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_report_countbooking));

        if (data_report_countcancel.return_code == 0)
        {

            //litDebug.Text = "3333";
            int count = data_report_countcancel.rbk_search_report_room_list.Length;
            string[] Dept_name = new string[count];
            object[] Count_booking = new object[count];
            int i = 0;
            foreach (var data in data_report_countcancel.rbk_search_report_room_list)
            {
                Dept_name[i] = data.dept_name_th.ToString();
                Count_booking[i] = data.count_booking.ToString();
                i++;
            }
            Highcharts chart = new Highcharts("chart");
            chart.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "Number of times" } } });
            chart.SetXAxis(new XAxis { Categories = Dept_name });
            chart.SetSeries(
                new DotNet.Highcharts.Options.Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                    Name = "Count of Booking",
                    Data = new Data(Count_booking)
                }
            );
            Update_PanelGraph.Visible = true;
            litReportChart.Text = chart.ToHtmlString();

            // -- pie chart --//
            var caseclose = new List<object>();

            foreach (var data in data_report_countcancel.rbk_search_report_room_list)
            {
                caseclose.Add(new object[] { data.dept_name_th.ToString(), data.count_booking.ToString() });
            }

            Highcharts chart1 = new Highcharts("chart1");
            chart1.SetTitle(new DotNet.Highcharts.Options.Title { Text = "Department / Counts" });
            chart1.InitChart(new DotNet.Highcharts.Options.Chart { DefaultSeriesType = DotNet.Highcharts.Enums.ChartTypes.Pie });

            chart1.SetPlotOptions(new PlotOptions
            {
                Pie = new PlotOptionsPie
                {
                    ShowInLegend = true,
                    AllowPointSelect = true,
                    DataLabels = new PlotOptionsPieDataLabels
                    {
                        Enabled = true,
                        // Formatter = "function() { return '" + this. + ' : '" + lv1count + "';}" //  this.percentage.toFixed(2)+' % '; }"
                        //Color = ColorTranslator.FromHtml("#000000"),
                        //ConnectorColor = ColorTranslator.FromHtml("#000000"),
                        Formatter = "function() { return this.point.name +' : '+  this.y.toFixed(0)+' '; }"
                    }
                }
            });

            chart1.SetTooltip(new Tooltip
            {
                Enabled = true,
                Formatter = "function() { return this.point.name +' : '+  this.percentage.toFixed(2)+' % '; }"

            });
            chart1.SetSeries(new DotNet.Highcharts.Options.Series[]
                 {
                        new DotNet.Highcharts.Options.Series()
                        {
                            Data = new Data (caseclose.ToArray())
                        }
                 });


            litReportChart1.Text = chart1.ToHtmlString();
            // -- pie chart --//

        }
        else
        {
            Update_PanelGraph.Visible = false;
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- ไม่พบข้อมูล ---');", true);

            //litReportChart.Text = "<p class='bg-danger'>No result.</p>";
        }
    }

    #endregion end graph

    #region event command
    protected void navCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        setActiveTab(cmdArg, 0, 0, 0, 0);

    }

    protected void ClickPrint(object sender, EventArgs e)
    {
        //  litDebug.Text = "lals";
    }

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        //linkBtnTrigger(lbDocSaveImport);

        switch (cmdName)
        {

            case "cmdSearchRoomCreate":

                //setActiveTab("docCreate", 0, 0, 0, 0);
                DropDownList ddlTypeRecive = (DropDownList)FvInsertRoomBooking.FindControl("ddlTypeRecive");
                DropDownList ddlPlace = (DropDownList)FvInsertRoomBooking.FindControl("ddlPlace");
                DropDownList ddlRoom = (DropDownList)FvInsertRoomBooking.FindControl("ddlRoom");
                TextBox txtDateStart_create = (TextBox)FvInsertRoomBooking.FindControl("txtDateStart_create");
                TextBox txt_timestart_create = (TextBox)FvInsertRoomBooking.FindControl("txt_timestart_create");
                TextBox txtDateEnd_create = (TextBox)FvInsertRoomBooking.FindControl("txtDateEnd_create");
                TextBox txt_timeend_create = (TextBox)FvInsertRoomBooking.FindControl("txt_timeend_create");


                ViewState["va_TypeText_Insert"] = ddlTypeRecive.SelectedItem.ToString();
                ViewState["va_TypeIDX_Insert"] = ddlTypeRecive.SelectedValue;
                ViewState["va_PlaceText_Insert"] = ddlPlace.SelectedItem.ToString();
                ViewState["va_PlaceIDX_Insert"] = ddlPlace.SelectedValue;
                ViewState["va_RoomText_Insert"] = ddlRoom.SelectedItem.ToString();
                ViewState["va_RoomIDX_Insert"] = ddlRoom.SelectedValue;
                ViewState["va_DateStart_Insert"] = txtDateStart_create.Text;
                ViewState["va_TimeStart_Insert"] = txt_timestart_create.Text;
                ViewState["va_DateEnd_Insert"] = txtDateEnd_create.Text;
                ViewState["va_TimeEnd_Insert"] = txt_timeend_create.Text;

                //litDebug.Text = txtSearchToHidden.Value;

                //
                ViewState["va_DateStart_Insert_CheckInsertDate"] = txtDateStart_create.Text + " " + txt_timestart_create.Text;
                //ViewState["va_TimeStart_Insert"] = txt_timestart_create.Text;
                ViewState["va_DateEnd_Insert_CheckInsertDate"] = txtDateEnd_create.Text + " " + txt_timeend_create.Text;

                //


                //DateTime starting = new DateTime();
                DateTime Datestarting = DateTime.ParseExact(txtDateStart_create.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                //DateTime ending = new DateTime();
                DateTime Dateending = DateTime.ParseExact(txtDateEnd_create.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                DateTime tempStartTime = DateTime.ParseExact(txt_timestart_create.Text, "HH:mm", CultureInfo.InvariantCulture);
                DateTime tempEndTime = DateTime.ParseExact(txt_timeend_create.Text, "HH:mm", CultureInfo.InvariantCulture);

                //// all room detail online ////
                // ---cmdSave
                IFormatProvider culture = new CultureInfo("en-US", true);
                DateTime DateToday = DateTime.ParseExact(DateTime.Now.ToString("dd/MM/yyyy HH:mm"), "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                DateTime dateVal_start = DateTime.ParseExact(ViewState["va_DateStart_Insert_CheckInsertDate"].ToString(), "dd/MM/yyyy HH:mm", culture);
                DateTime dateVal_end = DateTime.ParseExact(ViewState["va_DateEnd_Insert_CheckInsertDate"].ToString(), "dd/MM/yyyy HH:mm", culture);

                // --- Result Date Time In Food Detail Roombooking -- //
                ViewState["Vs_DateTimeCheckFooddetail"] = (dateVal_start - DateToday).TotalDays;
                // --- Result Date Time In Food Detail Roombooking -- //

                //litDebug.Text = (dateVal_start - DateToday).TotalDays.ToString();//dateVal_end.ToString();

                // check time before insert room booking
                if (dateVal_start >= DateToday && dateVal_start < dateVal_end)
                {
                    //ddlSelectedIndexChangeded Rooom Booking
                    data_roombooking data_roomcheck_detail = new data_roombooking();
                    rbk_check_room_detail roomcheck_detail = new rbk_check_room_detail();
                    data_roomcheck_detail.rbk_check_room_list = new rbk_check_room_detail[1];
                    roomcheck_detail.condition = 0;
                    data_roomcheck_detail.rbk_check_room_list[0] = roomcheck_detail;
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_roomcheck_detail));

                    data_roomcheck_detail = callServicePostRoomBooking(_urlGetCheckRoomInCreate, data_roomcheck_detail);
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_roomcheck_detail));

                    //// all room detail online ////

                    //// search room busy ////
                    data_roombooking data_search_detail = new data_roombooking();
                    rbk_search_room_detail search_detail = new rbk_search_room_detail();
                    data_search_detail.rbk_search_room_list = new rbk_search_room_detail[1];

                    //search_detail.condition = 1;
                    search_detail.condition = int.Parse(ddlTypeRecive.SelectedValue); // condition type create room
                    search_detail.place_idx = int.Parse(ddlPlace.SelectedValue);

                    if (ddlRoom.SelectedValue != "0")
                    {
                        search_detail.m0_room_idx = int.Parse(ddlRoom.SelectedValue);
                    }
                    else
                    {
                        search_detail.m0_room_idx = 0;
                    }

                    search_detail.date_start = txtDateStart_create.Text + " " + txt_timestart_create.Text;
                    search_detail.time_start = txt_timestart_create.Text;
                    search_detail.date_end = txtDateEnd_create.Text + " " + txt_timeend_create.Text;
                    search_detail.time_end = txt_timeend_create.Text;
                    search_detail.cemp_idx = _emp_idx;


                    data_search_detail.rbk_search_room_list[0] = search_detail;
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_search_detail));
                    data_search_detail = callServicePostRoomBooking(_urlGetRbkSearchRommCreate, data_search_detail);
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_search_detail));

                    // search room busy ////
                    if (data_search_detail.return_code == 1)// ไม่เร่งด่วน ว่างหมด
                    {
                        //litDebug.Text = "1";
                        ViewState["data_check_Room"] = "";
                        if (int.Parse(ddlRoom.SelectedValue) == 0) //ไม่ได้ search ห้อง
                        {
                            var linqRoomDetail = from _dt_RoomDetail in data_roomcheck_detail.rbk_check_room_list
                                                 where
                                                 _dt_RoomDetail.place_idx == int.Parse(ddlPlace.SelectedValue)
                                                 || _dt_RoomDetail.m0_room_idx == int.Parse(ddlRoom.SelectedValue)
                                                 select _dt_RoomDetail;

                            Update_DetailRoomSearch.Visible = true;
                            ViewState["vs_DetailRoomInSearch"] = linqRoomDetail.ToList();
                            setGridData(GvDetailRoomSearch, ViewState["vs_DetailRoomInSearch"]);
                        }
                        else // เลือกห้อง search
                        {
                            var linqRoomDetail = from _dt_RoomDetail in data_roomcheck_detail.rbk_check_room_list
                                                 where
                                                 _dt_RoomDetail.place_idx == int.Parse(ddlPlace.SelectedValue)
                                                 && _dt_RoomDetail.m0_room_idx == int.Parse(ddlRoom.SelectedValue)
                                                 select _dt_RoomDetail;

                            Update_DetailRoomSearch.Visible = true;
                            ViewState["vs_DetailRoomInSearch"] = linqRoomDetail.ToList();
                            setGridData(GvDetailRoomSearch, ViewState["vs_DetailRoomInSearch"]);
                        }

                    }
                    else if (data_search_detail.return_code == 0) // ไม่เร่งด่วน เจอบางส่วนที่ไม่ว่าง
                    {
                        //litDebug.Text = "2";
                        ViewState["data_check_Room"] = "";
                        ViewState["data_check_Room_Not"] = data_search_detail.rbk_search_room_list;

                        rbk_search_room_detail[] _templist_IdSetTest = (rbk_search_room_detail[])ViewState["data_check_Room_Not"];

                        var linqRoomDetailCheck = (from _dt_RoomDetailCheck in _templist_IdSetTest

                                                   select _dt_RoomDetailCheck).ToList();

                        string SetTestId = "";
                        string listSetTestId = "";

                        for (int idset = 0; idset < linqRoomDetailCheck.Count(); idset++)
                        {
                            SetTestId = linqRoomDetailCheck[idset].m0_room_idx.ToString();
                            listSetTestId += "," + SetTestId;

                        }

                        //Split comma frist posision
                        string resultString = listSetTestId.IndexOf(',') > -1
                        ? listSetTestId.Substring(listSetTestId.IndexOf(',') + 1)
                        : listSetTestId;

                        //litDebug.Text = resultString;

                        data_roombooking data_roomcheck_detail_check = new data_roombooking();
                        rbk_check_room_detail roomcheck_detail_check = new rbk_check_room_detail();
                        data_roomcheck_detail_check.rbk_check_room_list = new rbk_check_room_detail[1];

                        if (ddlRoom.SelectedValue != "0")
                        {
                            roomcheck_detail_check.m0_room_idx = int.Parse(ddlRoom.SelectedValue); //เลือกห้อง search
                        }
                        else
                        {

                            roomcheck_detail_check.m0_room_idx = 0;
                        }
                        roomcheck_detail_check.condition = 1;


                        roomcheck_detail_check.m0_room_idx_check = resultString;
                        roomcheck_detail_check.place_idx = int.Parse(ddlPlace.SelectedValue);
                        roomcheck_detail_check.date_start = txtDateStart_create.Text;
                        roomcheck_detail_check.time_start = txt_timestart_create.Text;
                        roomcheck_detail_check.date_end = txtDateEnd_create.Text;
                        roomcheck_detail_check.time_end = txt_timeend_create.Text;

                        data_roomcheck_detail_check.rbk_check_room_list[0] = roomcheck_detail_check;
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_roomcheck_detail_check));

                        data_roomcheck_detail_check = callServicePostRoomBooking(_urlGetCheckRoomInCreate, data_roomcheck_detail_check);
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_roomcheck_detail_check));

                        Update_DetailRoomSearch.Visible = true;
                        if (data_roomcheck_detail_check.return_code == 0)
                        {
                            //litDebug.Text = "28888";
                            ViewState["vs_DetailRoomInSearch"] = data_roomcheck_detail_check.rbk_check_room_list;
                            setGridData(GvDetailRoomSearch, ViewState["vs_DetailRoomInSearch"]);
                        }
                        else
                        {
                            //litDebug.Text = "999";
                            ViewState["vs_DetailRoomInSearch"] = null;
                            setGridData(GvDetailRoomSearch, ViewState["vs_DetailRoomInSearch"]);
                        }

                    }
                    else if (data_search_detail.return_code == 3)// เร่งด่วน จองได้หมด ไม่ว่างบางส่วน
                    {
                        //litDebug.Text = "3";
                        ViewState["data_check_Room"] = data_search_detail.rbk_search_room_list;


                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_search_detail));aaa

                        //ViewState["data_check_Room_Not"] = data_search_detail.rbk_search_room_list;

                        data_roombooking data_roomcheck_detail_check = new data_roombooking();
                        rbk_check_room_detail roomcheck_detail_check = new rbk_check_room_detail();
                        data_roomcheck_detail_check.rbk_check_room_list = new rbk_check_room_detail[1];

                        roomcheck_detail_check.condition = 2;
                        roomcheck_detail_check.m0_room_idx = int.Parse(ddlRoom.SelectedValue);
                        roomcheck_detail_check.place_idx = int.Parse(ddlPlace.SelectedValue);
                        roomcheck_detail_check.date_start = txtDateStart_create.Text;
                        roomcheck_detail_check.time_start = txt_timestart_create.Text;
                        roomcheck_detail_check.date_end = txtDateEnd_create.Text;
                        roomcheck_detail_check.time_end = txt_timeend_create.Text;
                        roomcheck_detail_check.cemp_idx = _emp_idx;


                        data_roomcheck_detail_check.rbk_check_room_list[0] = roomcheck_detail_check;
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_roomcheck_detail_check));

                        data_roomcheck_detail_check = callServicePostRoomBooking(_urlGetCheckRoomInCreate, data_roomcheck_detail_check);
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_roomcheck_detail_check));

                        Update_DetailRoomSearch.Visible = true;
                        ViewState["vs_DetailRoomInSearch"] = data_roomcheck_detail_check.rbk_check_room_list;
                        setGridData(GvDetailRoomSearch, ViewState["vs_DetailRoomInSearch"]);


                        ////
                        ////litDebug.Text = "3";
                        //Update_DetailRoomSearch.Visible = true;
                        //ViewState["vs_DetailRoomInSearch"] = data_search_detail.rbk_search_room_list;
                        //setGridData(GvDetailRoomSearch, ViewState["vs_DetailRoomInSearch"]);
                    }
                    else
                    {
                        //litDebug.Text = "4"; เร่งด่วน
                        ViewState["data_check_Room"] = "";

                        data_roombooking data_roomcheck_detail_check = new data_roombooking();
                        rbk_check_room_detail roomcheck_detail_check = new rbk_check_room_detail();
                        data_roomcheck_detail_check.rbk_check_room_list = new rbk_check_room_detail[1];

                        roomcheck_detail_check.condition = 2;
                        roomcheck_detail_check.m0_room_idx = int.Parse(ddlRoom.SelectedValue);
                        roomcheck_detail_check.place_idx = int.Parse(ddlPlace.SelectedValue);

                        data_roomcheck_detail_check.rbk_check_room_list[0] = roomcheck_detail_check;
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_roomcheck_detail_check));

                        data_roomcheck_detail_check = callServicePostRoomBooking(_urlGetCheckRoomInCreate, data_roomcheck_detail_check);
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_roomcheck_detail_check));

                        Update_DetailRoomSearch.Visible = true;
                        ViewState["vs_DetailRoomInSearch"] = data_roomcheck_detail_check.rbk_check_room_list;
                        setGridData(GvDetailRoomSearch, ViewState["vs_DetailRoomInSearch"]);
                    }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ตรวจสอบวันที่และเวลา ก่อนทำการบันทึก');", true);
                    ViewState["vs_DetailRoomInSearch"] = null;
                    setGridData(GvDetailRoomSearch, ViewState["vs_DetailRoomInSearch"]);
                }


                break;
            case "cmdAddGoogleCalendar":

                string[] arg_calendar = new string[16];
                arg_calendar = e.CommandArgument.ToString().Split(';');
                int _u0_idx_calendar = int.Parse(arg_calendar[0]);
                int _m0_room_idx_calendar = int.Parse(arg_calendar[1]);
                int _type_booking_idx_calendar = int.Parse(arg_calendar[2]);
                int _m0_node_idx_calendar = int.Parse(arg_calendar[3]);
                int _m0_actor_idx_calendar = int.Parse(arg_calendar[4]);
                int _staidx_calendar = int.Parse(arg_calendar[5]);
                int _cemp_idx_calendar = int.Parse(arg_calendar[6]);
                int _place_idx_calendar = int.Parse(arg_calendar[7]);
                string _date_start_calendar = arg_calendar[8];
                string _time_start_calendar = arg_calendar[9];
                string _date_end_calendar = arg_calendar[10];
                string _time_end_calendar = arg_calendar[11];
                string _detail_booking_calendar = arg_calendar[12];
                string _room_name_th_calendar = arg_calendar[13];
                string _place_name_calendar = arg_calendar[14];
                string _topic_booking_calendar = arg_calendar[15];
                int _value_calendar = int.Parse(arg_calendar[16]);


                IFormatProvider culture_ = new CultureInfo("en-US", true);
                //DateTime DateNow = DateTime.ParseExact(DateTime.Now.ToString("yyyyMMddTHHmmssZ"), "yyyyMMddTHHmmssZ", CultureInfo.InvariantCulture);
                string DateNow_start = _date_start_calendar.ToString() + " " + _time_start_calendar.ToString();//"12/02/2019 10:00";
                string DateNow_end = _date_end_calendar.ToString() + " " + _time_end_calendar.ToString();//"12/02/2019 10:00";

                DateTime Datestart = DateTime.ParseExact(DateNow_start, "dd/MM/yyyy HH:mm", culture_);
                DateTime Dateend = DateTime.ParseExact(DateNow_end, "dd/MM/yyyy HH:mm", culture_);
                //litDebug1.Text = TimeZoneInfo.ConvertTimeToUtc(Datestarting).ToString("yyyyMMddTHHmmssZ");

                string _url = "http://www.google.com/calendar/event";
                string TEMPLATE = "";
                if (_value_calendar == 1)//add calendar
                {
                    TEMPLATE = "TEMPLATE";
                }
                else //view calendar
                {
                    TEMPLATE = "VIEW";
                }

                //string VIEW = "VIEW";
                string text = _topic_booking_calendar.ToString();
                string dates = TimeZoneInfo.ConvertTimeToUtc(Datestart).ToString("yyyyMMddTHHmmssZ") + "/" + TimeZoneInfo.ConvertTimeToUtc(Dateend).ToString("yyyyMMddTHHmmssZ");//"20131124T010000Z/20131124T020000Z";
                string details = _detail_booking_calendar.ToString();
                string location = _room_name_th_calendar.ToString() + " " + _place_name_calendar.ToString();

                string url_ = _url + "?action=" + TEMPLATE + "&text=" + text + "&dates=" + dates + "&details=" + details + "&location=" + location;

                Response.Write("<script>window.open('" + url_ + "','_blank');</script>");


                //litDebug.Text = "222222";

                break;

            case "cmdAddRoomDetail":

                string[] arg1 = new string[2];
                arg1 = e.CommandArgument.ToString().Split(';');
                int _m0_room_idx = int.Parse(arg1[0]);
                string _room_name_th = arg1[1];

                setActiveTab("docCreate", 0, 0, 0, 1);

                //GridView GvDetailRoomSearch = (GridView)
                //Label lbl_room_detail_notbusy = (Label)GvDetailRoomSearch.FindControl("lbl_room_detail_notbusy");


                UpdatePanel_InsertDetailRoom.Visible = true;
                txt_StatusIDX_insert.Text = ViewState["va_TypeIDX_Insert"].ToString();
                txt_StatusText_insert.Text = ViewState["va_TypeText_Insert"].ToString();
                txt_PlaceIDX_insert.Text = ViewState["va_PlaceIDX_Insert"].ToString();
                txt_PlaceText_insert.Text = ViewState["va_PlaceText_Insert"].ToString();
                //txt_RoomIDX_insert.Text = ViewState["va_RoomIDX_Insert"].ToString();
                //txt_RoomText_insert.Text = ViewState["va_RoomText_Insert"].ToString();
                txtDateStart_insert.Text = ViewState["va_DateStart_Insert"].ToString();
                txt_timestart_insert.Text = ViewState["va_TimeStart_Insert"].ToString();
                txtDateEnd_insert.Text = ViewState["va_DateEnd_Insert"].ToString();
                txt_timeend_insert.Text = ViewState["va_TimeEnd_Insert"].ToString();
                //

                var lb = (LinkButton)sender;
                GridViewRow row = (GridViewRow)lb.NamingContainer;
                Label lbl_room_detail_statusroom = (Label)row.FindControl("lbl_room_detail_statusroom");
                txt_StatusRoomBooking.Text = lbl_room_detail_statusroom.Text;//ViewState["vs_StatusRoomBooking"].ToString();

                // Check Food Detail In RoomBooking
                Decimal totalDays = Convert.ToDecimal(ViewState["Vs_DateTimeCheckFooddetail"]);

                if (totalDays > 3) // > 3 day
                {
                    chkFoodDetail.Enabled = true;
                    txt_fooddetail_room.Enabled = false;
                }
                else // < 3 day
                {
                    chkFoodDetail.Enabled = false;
                    txt_fooddetail_room.Enabled = true;
                }

                // Check Food Detail In RoomBooking


                //file Pictue
                var btnViewFileRoomDetailCreate = (HyperLink)UpdatePanel_InsertDetailRoom.FindControl("btnViewFileRoomDetailCreate");

                string filePath_View_Create = ConfigurationManager.AppSettings["path_flie_roombooking"];
                string directoryName_ViewCreate = _m0_room_idx.ToString();//lbl_place_name_edit.Text.Trim() + lbl_room_name_en_edit.Text + lblm0_room_idx_edit.Text;

                //litDebug.Text = _m0_room_idx.ToString();
                if (Directory.Exists(Server.MapPath(filePath_View_Create + directoryName_ViewCreate)))
                {
                    string[] filesPath = Directory.GetFiles(Server.MapPath(filePath_View_Create + directoryName_ViewCreate));
                    List<ListItem> files = new List<ListItem>();
                    foreach (string path in filesPath)
                    {
                        string getfiles = "";
                        getfiles = Path.GetFileName(path);
                        btnViewFileRoomDetailCreate.NavigateUrl = filePath_View_Create + directoryName_ViewCreate + "/" + getfiles;

                        HtmlImage imgecreate = new HtmlImage();
                        imgecreate.Src = filePath_View_Create + directoryName_ViewCreate + "/" + getfiles;
                        imgecreate.Height = 200;
                        btnViewFileRoomDetailCreate.Controls.Add(imgecreate);


                    }
                    btnViewFileRoomDetailCreate.Visible = true;
                    txt_AlertDetailPicture.Visible = false;
                }
                else
                {
                    btnViewFileRoomDetailCreate.Visible = false;
                    txt_AlertDetailPicture.Visible = true;
                    //btnViewFileRoomDetailCreate.Text = "ไม่มีรูปภาพ";
                    //btnViewFileRoomDetailCreate.Visible = false;
                }

                //set Emptry in TextBox
                txt_TopicName.Text = String.Empty;
                txt_CountPeople.Text = String.Empty;
                txt_CommentCreate.Text = String.Empty;
                txt_fooddetail_room.Text = String.Empty;


                getReSultUse(ddlResultUse, 0);
                SelectConferenceDetail(GvConferenceInRoom);
                SelectFoodDetail();

                if(ViewState["va_PlaceIDX_Insert"].ToString() == "8")
                {
                    GvConferenceInRoom.Visible = true;
                }
                else
                {
                    GvConferenceInRoom.Visible = false;
                }


                //
                if (ViewState["va_RoomIDX_Insert"].ToString() != "0")
                {
                    txt_RoomIDX_insert.Text = ViewState["va_RoomIDX_Insert"].ToString();
                    txt_RoomText_insert.Text = ViewState["va_RoomText_Insert"].ToString();
                }
                else
                {
                    txt_RoomIDX_insert.Text = _m0_room_idx.ToString();
                    txt_RoomText_insert.Text = _room_name_th.ToString();
                }

                break;
            case "cmdCancelToSerach":
                setActiveTab("docCreate", 0, 0, 0, 0);
                break;

            case "cmdCancel":
                setActiveTab("docCreate", 0, 0, 0, 0);
                break;

            case "cmdSave":

                //insert Room Booking
                data_roombooking data_room_booking = new data_roombooking();
                rbk_u0_roombooking_detail room_booking = new rbk_u0_roombooking_detail();
                data_room_booking.rbk_u0_roombooking_list = new rbk_u0_roombooking_detail[1];

                room_booking.type_booking_idx = int.Parse(txt_StatusIDX_insert.Text);
                room_booking.place_idx = int.Parse(txt_PlaceIDX_insert.Text);
                room_booking.m0_room_idx = int.Parse(txt_RoomIDX_insert.Text);
                room_booking.date_start = txtDateStart_insert.Text + " " + txt_timestart_insert.Text;
                //search_detail.date_start = txtDateStart_create.Text + " " + txt_timestart_create.Text;
                room_booking.date_end = txtDateEnd_insert.Text + " " + txt_timeend_insert.Text;
                room_booking.time_start = txt_timestart_insert.Text;
                room_booking.time_end = txt_timeend_insert.Text;
                room_booking.result_use_idx = int.Parse(ddlResultUse.SelectedValue);
                room_booking.cemp_idx = _emp_idx;
                room_booking.topic_booking = txt_TopicName.Text;
                room_booking.count_people = int.Parse(txt_CountPeople.Text);
                room_booking.detail_booking = txt_CommentCreate.Text;
                room_booking.m0_actor_idx = 1;
                room_booking.m0_node_idx = 1;
                room_booking.decision = int.Parse(txt_StatusIDX_insert.Text);
                room_booking.status_booking = txt_StatusRoomBooking.Text;

                foreach (GridViewRow row_con in GvConferenceInRoom.Rows)
                {
                    if (row_con.RowType == DataControlRowType.DataRow)
                    {
                        RadioButton rdoconference = (RadioButton)row_con.FindControl("rdoconference");
                        Label lbl_m0_conidx = (Label)row_con.FindControl("lbl_m0_conidx");

                        if (rdoconference.Checked)
                        {
                            room_booking.m0_conidx = int.Parse(lbl_m0_conidx.Text);

                        }
                    }
                }

                if (int.Parse(txt_PlaceIDX_insert.Text) != 8 && int.Parse(txt_PlaceIDX_insert.Text) != 9)
                {
                    room_booking.type_room = 1;
                }
                else
                {
                    room_booking.type_room = 2;
                }
                // Detail Food Room Booking //

                var rbk_u2_roombooking = new rbk_u2_roombooking_detail[chkFoodDetail.Items.Count];
                int sum_u2_roombooking = 0;
                int i_u2_roombooking = 0;

                if (txt_fooddetail_room.Text == "") //check detail food roombooking
                {

                    List<String> Add_FoodDetailRoom = new List<string>();
                    foreach (ListItem chkList_FoodDetail in chkFoodDetail.Items)
                    {
                        if (chkList_FoodDetail.Selected)
                        {
                            rbk_u2_roombooking[i_u2_roombooking] = new rbk_u2_roombooking_detail();
                            rbk_u2_roombooking[i_u2_roombooking].food_idx = int.Parse(chkList_FoodDetail.Value);
                            //u2_doc_data[i].material_code = u1_doc_insert.material_code;
                            //u2_doc_data.count_chktest = i + 1;

                            sum_u2_roombooking = sum_u2_roombooking + 1;

                            i_u2_roombooking++;
                        }
                    }
                }
                else
                {
                    room_booking.detail_foodbooking = txt_fooddetail_room.Text;
                }

                // Detail Food Room Booking //

                data_room_booking.rbk_u0_roombooking_list[0] = room_booking;
                data_room_booking.rbk_u2_roombooking_list = rbk_u2_roombooking;
                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_room_booking));// vs_StatusRoomBooking

                data_room_booking = callServicePostRoomBooking(_urlSetRoomBooking, data_room_booking);
                //

                if (data_room_booking.return_code == 7)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- มีข้อมูลนี้แล้ว!!! ---');", true);
                    break;
                }
                else// (data_room_booking.return_code == 0)
                {
                    //Check Wait Approve
                    data_roombooking data_u0_roomdetail_count_ = new data_roombooking();
                    rbk_u0_roombooking_detail u0_roomdetail_count_ = new rbk_u0_roombooking_detail();
                    data_u0_roomdetail_count_.rbk_u0_roombooking_list = new rbk_u0_roombooking_detail[1]; ;
                    u0_roomdetail_count_.admin_idx = _emp_idx;
                    data_u0_roomdetail_count_.rbk_u0_roombooking_list[0] = u0_roomdetail_count_;
                    //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
                    data_u0_roomdetail_count_ = callServicePostRoomBooking(_urlGetCountWaitApproveRoomBooking, data_u0_roomdetail_count_);

                    if (data_u0_roomdetail_count_.rbk_u0_roombooking_list[0].Count_WaitApprove > 0)
                    {
                        //litDebug.Text = "1";
                        ViewState["vs_CountWaitApprove"] = data_u0_roomdetail_count_.rbk_u0_roombooking_list[0].Count_WaitApprove;
                        lbApprove.Text = "รายการ รออนุมัติ/ รอการเปลี่ยนแปลง <span class='badge progress-bar-danger' >" + Convert.ToString(ViewState["vs_CountWaitApprove"]) + "</span>";
                        setActiveTab("docDetail", 0, 0, 0, 0);
                        //setActiveTab("docApprove", 0, 0, 0, 0);

                    }
                    else
                    {
                        //litDebug.Text = "2";
                        ViewState["vs_CountWaitApprove"] = 0;
                        lbApprove.Text = "รายการ รออนุมัติ/ รอการเปลี่ยนแปลง <span class='badge progress-bar-danger' >" + Convert.ToString(ViewState["vs_CountWaitApprove"]) + "</span>";
                        setActiveTab("docDetail", 0, 0, 0, 0);
                        //setActiveTab("docDetailRoom", 0, 0, 0, 0);


                    }
                }


                break;

            case "cmdViewRoomDetail":

                string[] arg2 = new string[7];
                arg2 = e.CommandArgument.ToString().Split(';');
                int _u0_document_idx_view = int.Parse(arg2[0]);
                int _m0_room_idx_view = int.Parse(arg2[1]);
                string _type_booking_idx_view = arg2[2];
                int _m0_node_idx_view = int.Parse(arg2[3]);
                int _m0_actor_idx_view = int.Parse(arg2[4]);
                int _staidx_view = int.Parse(arg2[5]);
                int _cemp_idx_view = int.Parse(arg2[6]);
                int _place_view = int.Parse(arg2[7]);

                ViewState["Vs_m0_node_idx_view"] = _m0_node_idx_view;
                ViewState["Vs_m0_actor_idx_view"] = _m0_actor_idx_view;
                ViewState["Vs_staidx_view"] = _staidx_view;
                ViewState["Vs_type_booking_idx_view"] = _type_booking_idx_view;
                ViewState["Vs_cemp_idx_view"] = _cemp_idx_view;
                ViewState["Vs_place_view"] = _place_view;

                //litDebug.Text = ViewState["Vs_m0_node_idx_view"].ToString();
                setActiveTab("docDetail", _u0_document_idx_view, 0, int.Parse(ViewState["Vs_staidx_view"].ToString()), 1);

                //litDebug.Text = ViewState["Vs_staidx_view"].ToString();
                break;

            case "cmdCancelToDetail":

                //Check Wait Approve
                data_roombooking data_u0_roomdetail_count_de = new data_roombooking();
                rbk_u0_roombooking_detail u0_roomdetail_count_de = new rbk_u0_roombooking_detail();
                data_u0_roomdetail_count_de.rbk_u0_roombooking_list = new rbk_u0_roombooking_detail[1];
                u0_roomdetail_count_de.admin_idx = _emp_idx;
                data_u0_roomdetail_count_de.rbk_u0_roombooking_list[0] = u0_roomdetail_count_de;
                //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
                data_u0_roomdetail_count_de = callServicePostRoomBooking(_urlGetCountWaitApproveRoomBooking, data_u0_roomdetail_count_de);

                if (data_u0_roomdetail_count_de.rbk_u0_roombooking_list[0].Count_WaitApprove > 0)
                {
                    //litDebug.Text = "1";
                    ViewState["vs_CountWaitApprove"] = data_u0_roomdetail_count_de.rbk_u0_roombooking_list[0].Count_WaitApprove;
                    lbApprove.Text = "รายการ รออนุมัติ/ รอการเปลี่ยนแปลง <span class='badge progress-bar-danger' >" + Convert.ToString(ViewState["vs_CountWaitApprove"]) + "</span>";
                    setActiveTab("docDetail", 0, 0, 0, 0);
                    //setActiveTab("docApprove", 0, 0, 0, 0);

                }
                else
                {
                    //litDebug.Text = "2";
                    ViewState["vs_CountWaitApprove"] = 0;
                    lbApprove.Text = "รายการ รออนุมัติ/ รอการเปลี่ยนแปลง <span class='badge progress-bar-danger' >" + Convert.ToString(ViewState["vs_CountWaitApprove"]) + "</span>";
                    setActiveTab("docDetail", 0, 0, 0, 0);
                    //setActiveTab("docDetailRoom", 0, 0, 0, 0);

                }

                break;
            case "cmdCancelToWaitApprove":

                setActiveTab("docApprove", 0, 0, 0, 0);

                break;

            case "cmdCancelBookingRoom": //cancel Booking Room

                ////ViewState["Vs_m0_node_idx_view"] = _m0_node_idx_view;
                ////ViewState["Vs_m0_actor_idx_view"] = _m0_actor_idx_view;
                ////ViewState["Vs_staidx_view"] = _staidx_view;

                TextBox tb_u0_document_idx = (TextBox)fvDetailRoomBooking.FindControl("tb_u0_document_idx");
                TextBox tbplace_idx = (TextBox)fvDetailRoomBooking.FindControl("tbplace_idx");
                Label tb_checkDateTime = (Label)fvDetailRoomBooking.FindControl("tb_checkDateTime"); //date check set button form database
                TextBox tbActorCempIDX = (TextBox)fvDetailRoomBooking.FindControl("tbActorCempIDX"); //date check set button form database

                //IFormatProvider culture_cancel = new CultureInfo("en-US", true);
                //DateTime DateToday_Set = DateTime.ParseExact(DateTime.Now.ToString("dd/MM/yyyy HH:mm"), "dd/MM/yyyy HH:mm", culture_cancel); //Date Time Today Incheck
                //DateTime DateTime_DBSet = DateTime.ParseExact(tb_checkDateTime.Text, "dd/MM/yyyy HH:mm", culture_cancel);

                //var result_time = DateTime_DBSet.Subtract(DateToday_Set).TotalMinutes; // result time chrck > 30 min because cancel in room
                //                                                                       //litDebug.Text = DateToday_Set.ToString();


                //litDebug.Text = ViewState["Vs_m0_actor_idx_view"].ToString();

                //approve Room Booking
                data_roombooking data_cancel_room = new data_roombooking();
                rbk_u0_roombooking_detail cancel_booking = new rbk_u0_roombooking_detail();
                data_cancel_room.rbk_u0_roombooking_list = new rbk_u0_roombooking_detail[1];

                cancel_booking.u0_document_idx = int.Parse(tb_u0_document_idx.Text);

                ////if(ViewState["Vs_m0_node_idx_view"].ToString() == "2") 
                ////{
                ////    cancel_booking.m0_actor_idx = 1;//int.Parse(ViewState["Vs_m0_actor_idx_view"].ToString());
                ////}
                ////else
                ////{
                ////    cancel_booking.m0_actor_idx = 2;//int.Parse(ViewState["Vs_m0_actor_idx_view"].ToString());
                ////}

                if (_emp_idx == int.Parse(tbActorCempIDX.Text))
                {
                    cancel_booking.m0_actor_idx = 1;

                    if (int.Parse(tbplace_idx.Text) != 8 && int.Parse(tbplace_idx.Text) != 9)
                    {
                        cancel_booking.type_room = 1;

                    }
                    else
                    {
                        cancel_booking.type_room = 2;
                    }
                }
                else
                {
                    if (int.Parse(tbplace_idx.Text) != 8 && int.Parse(tbplace_idx.Text) != 9)
                    {
                        cancel_booking.type_room = 1;
                        cancel_booking.m0_actor_idx = 2;

                    }
                    else
                    {
                        cancel_booking.type_room = 2;
                        cancel_booking.m0_actor_idx = 3;

                    }
                }


                cancel_booking.m0_node_idx = 4;//int.Parse(ViewState["Vs_staidx_view"].ToString());
                cancel_booking.decision = 6;//int.Parse(ddlDecisionApprove.SelectedValue);
                ////cancel_booking.comment = txt_CommentApprove.Text;
                cancel_booking.cemp_idx = _emp_idx;

                data_cancel_room.rbk_u0_roombooking_list[0] = cancel_booking;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_cancel_room));
                data_cancel_room = callServicePostRoomBooking(_urlSetApproveWaitRoomBooking, data_cancel_room);


                data_roombooking data_u0_roomdetail_count_cancel = new data_roombooking();
                rbk_u0_roombooking_detail u0_roomdetail_count_cancel = new rbk_u0_roombooking_detail();
                data_u0_roomdetail_count_cancel.rbk_u0_roombooking_list = new rbk_u0_roombooking_detail[1];
                u0_roomdetail_count_cancel.admin_idx = _emp_idx;
                data_u0_roomdetail_count_cancel.rbk_u0_roombooking_list[0] = u0_roomdetail_count_cancel;
                //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
                data_u0_roomdetail_count_cancel = callServicePostRoomBooking(_urlGetCountWaitApproveRoomBooking, data_u0_roomdetail_count_cancel);

                if (data_u0_roomdetail_count_cancel.rbk_u0_roombooking_list[0].Count_WaitApprove > 0)
                {
                    //litDebug.Text = "1";
                    ViewState["vs_CountWaitApprove"] = data_u0_roomdetail_count_cancel.rbk_u0_roombooking_list[0].Count_WaitApprove;
                    lbApprove.Text = "รายการ รออนุมัติ/ รอการเปลี่ยนแปลง <span class='badge progress-bar-danger' >" + Convert.ToString(ViewState["vs_CountWaitApprove"]) + "</span>";
                    setActiveTab("docDetail", 0, 0, 0, 0);
                    //setActiveTab("docApprove", 0, 0, 0, 0);

                }
                else
                {
                    //litDebug.Text = "2";
                    ViewState["vs_CountWaitApprove"] = 0;
                    lbApprove.Text = "รายการ รออนุมัติ/ รอการเปลี่ยนแปลง <span class='badge progress-bar-danger' >" + Convert.ToString(ViewState["vs_CountWaitApprove"]) + "</span>";
                    setActiveTab("docDetail", 0, 0, 0, 0);
                    //setActiveTab("docDetailRoom", 0, 0, 0, 0);


                }

                break;

            case "cmdViewWaitApprove":

                string[] arg3 = new string[5];
                arg3 = e.CommandArgument.ToString().Split(';');
                int _u0_document_idx_approve = int.Parse(arg3[0]);
                int _m0_room_idx_approve = int.Parse(arg3[1]);
                string _room_name_th_approve = arg3[2];
                int _m0_node_idx_approve = int.Parse(arg3[3]);
                int _m0_actor_idx_approve = int.Parse(arg3[4]);
                int _staidx_approve = int.Parse(arg3[5]);

                ViewState["vs_u0_document_idx_approve"] = _u0_document_idx_approve;
                ViewState["vs_m0_node_idx_approve"] = _m0_node_idx_approve;
                ViewState["vs_m0_actor_idx_approve"] = _m0_actor_idx_approve;
                ViewState["vs_staidx_approve"] = _staidx_approve;

                setActiveTab("docApprove", _u0_document_idx_approve, 0, _m0_node_idx_approve, 1);
                break;

            case "cmdSaveApprove":

                //int node_idx_approve = int.Parse(ViewState["vs_m0_node_idx_approve"].ToString());
                //int actor_idx_approve = int.Parse(ViewState["vs_m0_actor_idx_approve"].ToString());
                //int status_idx_approve = int.Parse(ViewState["vs_staidx_approve"].ToString());
                DropDownList ddlDecisionApprove = (DropDownList)FvDetailApproveHR.FindControl("ddlDecisionApprove");
                TextBox txt_CommentApprove = (TextBox)FvDetailApproveHR.FindControl("txt_CommentApprove");

                TextBox tb_date_start_approve = (TextBox)FvWaitApproveDetail.FindControl("tb_date_start_approve");
                TextBox tb_time_start_approve = (TextBox)FvWaitApproveDetail.FindControl("tb_time_start_approve");

                TextBox tb_date_end_approve = (TextBox)FvWaitApproveDetail.FindControl("tb_date_end_approve");
                TextBox tb_time_end_approve = (TextBox)FvWaitApproveDetail.FindControl("tb_time_end_approve");
                TextBox tb_place_idx_approve = (TextBox)FvWaitApproveDetail.FindControl("tb_place_idx_approve");
                TextBox tb_m0_room_idx_approve = (TextBox)FvWaitApproveDetail.FindControl("tb_m0_room_idx_approve");

                //approve Room Booking
                data_roombooking data_hr_approve = new data_roombooking();
                rbk_u0_roombooking_detail hr_approve = new rbk_u0_roombooking_detail();
                data_hr_approve.rbk_u0_roombooking_list = new rbk_u0_roombooking_detail[1];

                hr_approve.u0_document_idx = int.Parse(ViewState["vs_u0_document_idx_approve"].ToString());
                hr_approve.m0_actor_idx = int.Parse(ViewState["vs_m0_actor_idx_approve"].ToString());
                hr_approve.m0_node_idx = int.Parse(ViewState["vs_m0_node_idx_approve"].ToString());
                hr_approve.decision = int.Parse(ddlDecisionApprove.SelectedValue);
                hr_approve.comment = txt_CommentApprove.Text;
                hr_approve.cemp_idx = _emp_idx;
                //room_booking_edit.date_start = txtDateStart_insert.Text + " " + txt_timestart_insert.Text;
                hr_approve.date_start = tb_date_start_approve.Text + " " + tb_time_start_approve.Text;
                hr_approve.date_end = tb_date_end_approve.Text + " " + tb_time_end_approve.Text;
                hr_approve.place_idx = int.Parse(tb_place_idx_approve.Text);
                hr_approve.m0_room_idx = int.Parse(tb_m0_room_idx_approve.Text);

                if (int.Parse(tb_place_idx_approve.Text) != 8 && int.Parse(tb_place_idx_approve.Text) != 9)
                {
                    hr_approve.type_room = 1;
                }
                else
                {
                    hr_approve.type_room = 2;
                }

                data_hr_approve.rbk_u0_roombooking_list[0] = hr_approve;
                //
                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_hr_approve));
                data_hr_approve = callServicePostRoomBooking(_urlSetApproveWaitRoomBooking, data_hr_approve);
                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_hr_approve)); //9999

                if (data_hr_approve.return_code == 0)
                {
                    //litDebug.Text = "33333";
                    //Check Count Detail Wait Approve
                    data_roombooking data_u0_roomdetail_count = new data_roombooking();
                    rbk_u0_roombooking_detail u0_roomdetail_count = new rbk_u0_roombooking_detail();
                    data_u0_roomdetail_count.rbk_u0_roombooking_list = new rbk_u0_roombooking_detail[1];
                    u0_roomdetail_count.admin_idx = _emp_idx;
                    data_u0_roomdetail_count.rbk_u0_roombooking_list[0] = u0_roomdetail_count;
                    //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));
                    data_u0_roomdetail_count = callServicePostRoomBooking(_urlGetCountWaitApproveRoomBooking, data_u0_roomdetail_count);
                    if (data_u0_roomdetail_count.return_code == 0)
                    {
                        ViewState["vs_CountWaitApprove"] = data_u0_roomdetail_count.rbk_u0_roombooking_list[0].Count_WaitApprove;

                        if (int.Parse(ViewState["vs_CountWaitApprove"].ToString()) > 0)
                        {
                            lbApprove.Text = "รายการ รออนุมัติ/ รอการเปลี่ยนแปลง <span class='badge progress-bar-danger' >" + Convert.ToString(ViewState["vs_CountWaitApprove"]) + "</span>";
                            setActiveTab("docApprove", 0, 0, 0, 0);
                        }
                        else
                        {
                            ViewState["vs_CountWaitApprove"] = 0;
                            lbApprove.Text = "รายการ รออนุมัติ/ รอการเปลี่ยนแปลง <span class='badge progress-bar-danger' >" + Convert.ToString(ViewState["vs_CountWaitApprove"]) + "</span>";
                            setActiveTab("docDetailRoom", 0, 0, 0, 0);
                        }


                    }

                }

                break;

            case "cmdEditDetailBooking":

                fvDetailRoomBooking.Visible = true;
                setFormData(fvDetailRoomBooking, FormViewMode.Edit, ViewState["vs_ViewDetailRoomBooking"]);

                DropDownList ddlPlace_edit = (DropDownList)fvDetailRoomBooking.FindControl("ddlPlace_edit");
                TextBox tb_place_idx_edit = (TextBox)fvDetailRoomBooking.FindControl("tb_place_idx");


                DropDownList ddlRoom_edit = (DropDownList)fvDetailRoomBooking.FindControl("ddlRoom_edit");
                TextBox tb_m0_room_idx_edit = (TextBox)fvDetailRoomBooking.FindControl("tb_m0_room_idx");

                TextBox tb_result_use_idx = (TextBox)fvDetailRoomBooking.FindControl("tb_result_use_idx");
                DropDownList ddlResultUse_edit = (DropDownList)fvDetailRoomBooking.FindControl("ddlResultUse_edit");
                LinkButton btnSaveEditDetail = (LinkButton)fvDetailRoomBooking.FindControl("btnSaveEditDetail");

                //////btnSaveEditDetail.Visible = true;
                //ViewState["vs_StatusRoomBooking_edit"] = "";

                if (ViewState["Vs_Status_ProcessCheck_edit"].ToString() == "6")
                {
                    btnSaveEditDetail.Visible = false;
                }
                else
                {
                    btnSaveEditDetail.Visible = true;
                }


                getPlace(ddlPlace_edit, int.Parse(tb_place_idx_edit.Text));
                getRoomEdit(ddlRoom_edit, int.Parse(tb_m0_room_idx_edit.Text), int.Parse(tb_place_idx_edit.Text));

                Update_EditRoomBookingButton.Visible = false;

                getReSultUse(ddlResultUse_edit, int.Parse(tb_result_use_idx.Text));


                break;

            case "cmdBackDetailBeforeEdit":
                //litDebug.Text = ViewState["Vs_m0_node_idx_view"].ToString();
                //setActiveTab("docDetail", int.Parse(ViewState["Vs_u0_document_idx_view"].ToString()), 0, 0, 1);
                setActiveTab("docDetail", int.Parse(ViewState["Vs_u0_document_idx_view"].ToString()), 0, int.Parse(ViewState["Vs_staidx_view"].ToString()), 1);
                break;
            case "cmdBackDetailBeforeEdit_approve":

                setActiveTab("docApprove", int.Parse(ViewState["vs_u0_document_idx_approve"].ToString()), 0, int.Parse(ViewState["vs_m0_node_idx_approve"].ToString()), 1);
                break;

            case "cmdSaveEdit":

                TextBox tb_u0_document_idx_edit = (TextBox)fvDetailRoomBooking.FindControl("tb_u0_document_idx");
                TextBox tb_type_booking_idx_edit = (TextBox)fvDetailRoomBooking.FindControl("tb_type_booking_idx");
                DropDownList ddlRoom_edit_ = (DropDownList)fvDetailRoomBooking.FindControl("ddlRoom_edit");
                TextBox tb_topic_booking_edit = (TextBox)fvDetailRoomBooking.FindControl("tb_topic_booking");
                DropDownList ddlResultUse_edit_ = (DropDownList)fvDetailRoomBooking.FindControl("ddlResultUse_edit");
                TextBox tb_count_people_edit = (TextBox)fvDetailRoomBooking.FindControl("tb_count_people");
                TextBox tb_detail_booking_edit = (TextBox)fvDetailRoomBooking.FindControl("tb_detail_booking");
                TextBox tb_status_booking_edit = (TextBox)fvDetailRoomBooking.FindControl("tb_status_booking");
                TextBox tbActorCempIDX_edit = (TextBox)fvDetailRoomBooking.FindControl("tbActorCempIDX");
                TextBox tb_place_idx_approve_edit = (TextBox)fvDetailRoomBooking.FindControl("tb_place_idx");
                GridView GvConferenceInRoom_DetailEdit = (GridView)fvDetailRoomBooking.FindControl("GvConferenceInRoom_DetailEdit");
                //litDebug.Text = ViewState["Vs_m0_node_idx_view"].ToString();

                //insert Room Booking
                data_roombooking data_room_booking_edit = new data_roombooking();
                rbk_u0_roombooking_detail room_booking_edit = new rbk_u0_roombooking_detail();
                data_room_booking_edit.rbk_u0_roombooking_list = new rbk_u0_roombooking_detail[1];

                room_booking_edit.u0_document_idx = int.Parse(tb_u0_document_idx_edit.Text);
                room_booking_edit.type_booking_idx = int.Parse(tb_type_booking_idx_edit.Text);
                room_booking_edit.m0_room_idx = int.Parse(ddlRoom_edit_.SelectedValue);
                room_booking_edit.topic_booking = tb_topic_booking_edit.Text;
                room_booking_edit.result_use_idx = int.Parse(ddlResultUse_edit_.SelectedValue);
                room_booking_edit.count_people = int.Parse(tb_count_people_edit.Text);
                room_booking_edit.detail_booking = tb_detail_booking_edit.Text;
                room_booking_edit.cemp_idx = _emp_idx;

                foreach (GridViewRow row_con in GvConferenceInRoom_DetailEdit.Rows)
                {
                    if (row_con.RowType == DataControlRowType.DataRow)
                    {
                        RadioButton rdoconference_edit = (RadioButton)row_con.FindControl("rdoconference_edit");
                        Label lbl_m0_conidx = (Label)row_con.FindControl("lbl_m0_conidx");

                        if (rdoconference_edit.Checked)
                        {
                            room_booking_edit.m0_conidx = int.Parse(lbl_m0_conidx.Text);

                        }
                    }
                }



                if (ViewState["Vs_staidx_view"].ToString() == "1" && int.Parse(tbActorCempIDX_edit.Text) == _emp_idx)
                {
                    room_booking_edit.m0_actor_idx = 1;
                    room_booking_edit.m0_node_idx = 1;
                    room_booking_edit.decision = int.Parse(tb_type_booking_idx_edit.Text);

                    if (int.Parse(tb_place_idx_approve_edit.Text) != 8 && int.Parse(tb_place_idx_approve_edit.Text) != 9)
                    {
                        room_booking_edit.type_room = 1;

                    }
                    else
                    {
                        room_booking_edit.type_room = 2;

                    }
                }
                else
                {
                    if (int.Parse(tb_place_idx_approve_edit.Text) != 8 && int.Parse(tb_place_idx_approve_edit.Text) != 9)
                    {
                        room_booking_edit.type_room = 1;
                        room_booking_edit.m0_actor_idx = 2;

                    }
                    else
                    {
                        room_booking_edit.type_room = 2;
                        room_booking_edit.m0_actor_idx = 3;

                    }
                    room_booking_edit.m0_node_idx = 5;
                    room_booking_edit.decision = 7;//int.Parse(tb_type_booking_idx_edit.Text); 
                }


                room_booking_edit.status_booking = "ว่าง";//tb_status_booking_edit.Text;

                data_room_booking_edit.rbk_u0_roombooking_list[0] = room_booking_edit;
                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_room_booking_edit));// vs_StatusRoomBooking

                data_room_booking_edit = callServicePostRoomBooking(_urlSetRoomBooking, data_room_booking_edit);
                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_room_booking_edit));// vs_StatusRoomBooking
                //Check Wait Approve
                data_roombooking data_u0_roomdetail_count_edit = new data_roombooking();
                rbk_u0_roombooking_detail u0_roomdetail_count_edit = new rbk_u0_roombooking_detail();
                data_u0_roomdetail_count_edit.rbk_u0_roombooking_list = new rbk_u0_roombooking_detail[1];
                u0_roomdetail_count_edit.admin_idx = _emp_idx;
                data_u0_roomdetail_count_edit.rbk_u0_roombooking_list[0] = u0_roomdetail_count_edit;
                //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
                data_u0_roomdetail_count_edit = callServicePostRoomBooking(_urlGetCountWaitApproveRoomBooking, data_u0_roomdetail_count_edit);

                if (data_u0_roomdetail_count_edit.rbk_u0_roombooking_list[0].Count_WaitApprove > 0)
                {
                    //litDebug.Text = "1";
                    ViewState["vs_CountWaitApprove"] = data_u0_roomdetail_count_edit.rbk_u0_roombooking_list[0].Count_WaitApprove;
                    lbApprove.Text = "รายการ รออนุมัติ/ รอการเปลี่ยนแปลง <span class='badge progress-bar-danger' >" + Convert.ToString(ViewState["vs_CountWaitApprove"]) + "</span>";

                    setActiveTab("docDetail", 0, 0, 0, 0);


                }
                else
                {
                    //litDebug.Text = "2";
                    ViewState["vs_CountWaitApprove"] = 0;
                    lbApprove.Text = "รายการ รออนุมัติ/ รอการเปลี่ยนแปลง <span class='badge progress-bar-danger' >" + Convert.ToString(ViewState["vs_CountWaitApprove"]) + "</span>";

                    setActiveTab("docDetail", 0, 0, 0, 0);

                }
                break;

            case "cmdSearchIndex":

                div_searchDetail.Visible = false;
                Update_BackToDetailRoomBooking.Visible = true;
                setFormData(FvSearchDetailRoom, FormViewMode.Insert, null);

                DropDownList ddlStatusSearchDetail = (DropDownList)FvSearchDetailRoom.FindControl("ddlStatusSearchDetail");
                getStatusSearch(ddlStatusSearchDetail);
                getPlace(ddlPlaceSearchDetail, 0);

                break;

            case "cmdSearchRoomDetail":

                DropDownList ddlTypeReciveSearchDetail_search = (DropDownList)FvSearchDetailRoom.FindControl("ddlTypeReciveSearchDetail");
                DropDownList ddlStatusSearchDetail_search = (DropDownList)FvSearchDetailRoom.FindControl("ddlStatusSearchDetail");
                DropDownList ddlPlaceSearchDetail_search = (DropDownList)FvSearchDetailRoom.FindControl("ddlPlaceSearchDetail");
                DropDownList ddlRoomSearchDetail_search = (DropDownList)FvSearchDetailRoom.FindControl("ddlRoomSearchDetail");
                TextBox txtDateStart_SearchDetail_search = (TextBox)FvSearchDetailRoom.FindControl("txtDateStart_SearchDetail");
                TextBox txtDateEnd_SearchDetail_search = (TextBox)FvSearchDetailRoom.FindControl("txtDateEnd_SearchDetail");
                TextBox AddStartdate = (TextBox)FvSearchDetailRoom.FindControl("AddStartdate");
                TextBox AddEndDate = (TextBox)FvSearchDetailRoom.FindControl("AddEndDate");
                DropDownList ddlSearchDate = (DropDownList)FvSearchDetailRoom.FindControl("ddlSearchDate");


                // --- Check Date Search Between //
                IFormatProvider culture_search = new CultureInfo("en-US", true);
                //DateTime DateToday = DateTime.ParseExact(DateTime.Now.ToString("dd/MM/yyyy HH:mm"), "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                DateTime dateVal_startserach = DateTime.ParseExact(AddStartdate.Text, "dd/MM/yyyy", culture_search);
                if (AddEndDate.Text != "")
                {
                    DateTime dateVal_endsearch = DateTime.ParseExact(AddEndDate.Text, "dd/MM/yyyy", culture_search);
                    ViewState["Vs_CheckDateSearch"] = (dateVal_endsearch - dateVal_startserach).TotalDays.ToString();

                    //litDebug.Text = ViewState["Vs_CheckDateSearch"].ToString();
                }

                // --- Check Date Search Between //

                //// search room busy ////
                data_roombooking data_search_detail_booking = new data_roombooking();
                rbk_search_room_detail search_detail_booking = new rbk_search_room_detail();
                data_search_detail_booking.rbk_search_room_list = new rbk_search_room_detail[1];

                search_detail_booking.type_booking_idx = int.Parse(ddlTypeReciveSearchDetail_search.SelectedValue);
                search_detail_booking.staidx = int.Parse(ddlStatusSearchDetail_search.SelectedValue);
                search_detail_booking.place_idx = int.Parse(ddlPlaceSearchDetail_search.SelectedValue);
                search_detail_booking.m0_room_idx = int.Parse(ddlRoomSearchDetail_search.SelectedValue);
                search_detail_booking.date_start = AddStartdate.Text;
                search_detail_booking.date_end = AddEndDate.Text;
                search_detail_booking.IFSearchbetween = int.Parse(ddlSearchDate.SelectedValue);
                search_detail_booking.cemp_idx = _emp_idx;
                search_detail_booking.rpos_idx = int.Parse(ViewState["rpos_permission"].ToString());


                //search_detail_booking.date_start = txtDateStart_SearchDetail_search.Text;
                //search_detail_booking.date_end = txtDateEnd_SearchDetail_search.Text;

                data_search_detail_booking.rbk_search_room_list[0] = search_detail_booking;
                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_search_detail_booking));

                if (int.Parse(ddlSearchDate.SelectedValue) == 3 && int.Parse(ViewState["Vs_CheckDateSearch"].ToString()) <= 0) //between date start and date end
                {

                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- กรุณาตรวจสอบวันที่ก่อนทำการการค้นหา ---');", true);
                    ViewState["vs_DetailRoomBooking"] = null;
                    setGridData(GvDetailRoomBooking, ViewState["vs_DetailRoomBooking"]);
                    break;
                }
                else
                {
                    data_search_detail_booking = callServicePostRoomBooking(_urlGetSearchDetailRbk, data_search_detail_booking);

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_search_detail));

                    ViewState["vs_DetailRoomBooking"] = data_search_detail_booking.rbk_u0_roombooking_list;
                    setGridData(GvDetailRoomBooking, ViewState["vs_DetailRoomBooking"]);

                }

                break;
            case "cmdEditDetailBookingHR":

                FvWaitApproveDetail.Visible = true;
                setFormData(FvWaitApproveDetail, FormViewMode.Edit, ViewState["vs_ViewWaitApproveRoomBooking"]);

                DropDownList ddlPlace_edit_approve = (DropDownList)FvWaitApproveDetail.FindControl("ddlPlace_edit_approve");
                TextBox tb_place_idx_approve_ = (TextBox)FvWaitApproveDetail.FindControl("tb_place_idx_approve");

                DropDownList ddlRoom_edit_approve = (DropDownList)FvWaitApproveDetail.FindControl("ddlRoom_edit_approve");
                TextBox tb_m0_room_idx_approve_ = (TextBox)FvWaitApproveDetail.FindControl("tb_m0_room_idx_approve");

                TextBox tb_result_use_idx_approve = (TextBox)FvWaitApproveDetail.FindControl("tb_result_use_idx_approve");
                DropDownList ddlResultUse_edit_approve = (DropDownList)FvWaitApproveDetail.FindControl("ddlResultUse_edit_approve");
                LinkButton btnSaveEditDetail_approve = (LinkButton)FvWaitApproveDetail.FindControl("btnSaveEditDetail_approve");

                //btnSaveEditDetail_approve.Visible = true;
                //ViewState["vs_StatusRoomBooking_edit"] = "";
                
                getPlace(ddlPlace_edit_approve, int.Parse(tb_place_idx_approve_.Text));
                getRoomEdit(ddlRoom_edit_approve, int.Parse(tb_m0_room_idx_approve_.Text), int.Parse(tb_place_idx_approve_.Text));
                Update_EditRoomBookingButtonHR.Visible = false;

                getReSultUse(ddlResultUse_edit_approve, int.Parse(tb_result_use_idx_approve.Text));

                break;
            case "cmdSaveEditReplaceRoom":

                TextBox tb_u0_document_idx_approve = (TextBox)FvWaitApproveDetail.FindControl("tb_u0_document_idx_approve");
                TextBox tb_type_booking_idx_approve = (TextBox)FvWaitApproveDetail.FindControl("tb_type_booking_idx_approve");
                DropDownList ddlRoom_edit_approve_ = (DropDownList)FvWaitApproveDetail.FindControl("ddlRoom_edit_approve");
                TextBox tb_topic_booking_approve = (TextBox)FvWaitApproveDetail.FindControl("tb_topic_booking_approve");
                DropDownList ddlResultUse_edit_approve_ = (DropDownList)FvWaitApproveDetail.FindControl("ddlResultUse_edit_approve");
                TextBox tb_count_people_approve = (TextBox)FvWaitApproveDetail.FindControl("tb_count_people_approve");
                TextBox tb_detail_booking_approve = (TextBox)FvWaitApproveDetail.FindControl("tb_detail_booking_approve");
                TextBox tb_status_booking_approve = (TextBox)FvWaitApproveDetail.FindControl("tb_status_booking_approve");
                TextBox tb_place_idx_approve_edit_ = (TextBox)FvWaitApproveDetail.FindControl("tb_place_idx_approve");
                GridView GvConferenceInRoom_DetailApproveEdit = (GridView)FvWaitApproveDetail.FindControl("GvConferenceInRoom_DetailApproveEdit");

                //litDebug.Text = ViewState["Vs_m0_node_idx_view"].ToString();

                //insert Room Booking
                data_roombooking data_room_booking_edit_approve = new data_roombooking();
                rbk_u0_roombooking_detail room_booking_edit_approve = new rbk_u0_roombooking_detail();
                data_room_booking_edit_approve.rbk_u0_roombooking_list = new rbk_u0_roombooking_detail[1];

                room_booking_edit_approve.u0_document_idx = int.Parse(tb_u0_document_idx_approve.Text);
                room_booking_edit_approve.type_booking_idx = int.Parse(tb_type_booking_idx_approve.Text);
                room_booking_edit_approve.m0_room_idx = int.Parse(ddlRoom_edit_approve_.SelectedValue);
                room_booking_edit_approve.topic_booking = tb_topic_booking_approve.Text;
                room_booking_edit_approve.result_use_idx = int.Parse(ddlResultUse_edit_approve_.SelectedValue);
                room_booking_edit_approve.count_people = int.Parse(tb_count_people_approve.Text);
                room_booking_edit_approve.detail_booking = tb_detail_booking_approve.Text;
                room_booking_edit_approve.cemp_idx = _emp_idx;


                room_booking_edit_approve.m0_node_idx = 5;
                room_booking_edit_approve.decision = 7;//int.Parse(tb_type_booking_idx_edit.Text); 

                if (int.Parse(tb_place_idx_approve_edit_.Text) != 8 && int.Parse(tb_place_idx_approve_edit_.Text) != 9)
                {
                    room_booking_edit_approve.type_room = 1;
                    room_booking_edit_approve.m0_actor_idx = 2;

                }
                else
                {
                    room_booking_edit_approve.type_room = 2;
                    room_booking_edit_approve.m0_actor_idx = 3;

                }

                foreach (GridViewRow row_con in GvConferenceInRoom_DetailApproveEdit.Rows)
                {
                    if (row_con.RowType == DataControlRowType.DataRow)
                    {
                        RadioButton rdoconference_appedit = (RadioButton)row_con.FindControl("rdoconference_appedit");
                        Label lbl_m0_conidx = (Label)row_con.FindControl("lbl_m0_conidx");

                        if (rdoconference_appedit.Checked)
                        {
                            room_booking_edit_approve.m0_conidx = int.Parse(lbl_m0_conidx.Text);

                        }
                    }
                }
                room_booking_edit_approve.status_booking = "ว่าง";//tb_status_booking_edit.Text;
                data_room_booking_edit_approve.rbk_u0_roombooking_list[0] = room_booking_edit_approve;
                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_room_booking_edit_approve));// vs_StatusRoomBooking

                data_room_booking_edit_approve = callServicePostRoomBooking(_urlSetRoomBooking, data_room_booking_edit_approve);
                getCountWaitApprove();
                break;

            case "cmdCancelBookingRoomHR": //cancel Booking Room

                TextBox tb_u0_document_idx_approve_ = (TextBox)FvWaitApproveDetail.FindControl("tb_u0_document_idx_approve");
                TextBox tb_place_idx_approve_cancel = (TextBox)FvWaitApproveDetail.FindControl("tb_place_idx_approve");


                //litDebug.Text = ViewState["Vs_m0_actor_idx_view"].ToString();

                //approve Room Booking
                data_roombooking data_cancel_room_approve = new data_roombooking();
                rbk_u0_roombooking_detail cancel_booking_approve = new rbk_u0_roombooking_detail();
                data_cancel_room_approve.rbk_u0_roombooking_list = new rbk_u0_roombooking_detail[1];

                cancel_booking_approve.u0_document_idx = int.Parse(tb_u0_document_idx_approve_.Text);

                //int.Parse(ViewState["Vs_m0_actor_idx_view"].ToString());
                //if (ViewState["Vs_m0_node_idx_view"].ToString() == "2")
                //{
                //    cancel_booking.m0_actor_idx = 1;//int.Parse(ViewState["Vs_m0_actor_idx_view"].ToString());
                //}
                //else
                //{
                //    cancel_booking.m0_actor_idx = 2;//int.Parse(ViewState["Vs_m0_actor_idx_view"].ToString());
                //}

                cancel_booking_approve.m0_node_idx = 4;//int.Parse(ViewState["Vs_staidx_view"].ToString());
                cancel_booking_approve.decision = 6;//int.Parse(ddlDecisionApprove.SelectedValue);
                ////cancel_booking.comment = txt_CommentApprove.Text;
                cancel_booking_approve.cemp_idx = _emp_idx;

                if (int.Parse(tb_place_idx_approve_cancel.Text) != 8 && int.Parse(tb_place_idx_approve_cancel.Text) != 9)
                {
                    cancel_booking_approve.type_room = 1;
                    cancel_booking_approve.m0_actor_idx = 2;

                }
                else
                {
                    cancel_booking_approve.type_room = 2;
                    cancel_booking_approve.m0_actor_idx = 3;

                }

                data_cancel_room_approve.rbk_u0_roombooking_list[0] = cancel_booking_approve;
                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_cancel_room_approve));// vs_StatusRoomBooking
                data_cancel_room_approve = callServicePostRoomBooking(_urlSetApproveWaitRoomBooking, data_cancel_room_approve);



                data_roombooking data_u0_count_cancel_approve = new data_roombooking();
                rbk_u0_roombooking_detail u0_count_cancel_approve = new rbk_u0_roombooking_detail();
                data_u0_count_cancel_approve.rbk_u0_roombooking_list = new rbk_u0_roombooking_detail[1];
                u0_count_cancel_approve.admin_idx = _emp_idx;
                data_u0_count_cancel_approve.rbk_u0_roombooking_list[0] = u0_count_cancel_approve;
                //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
                data_u0_count_cancel_approve = callServicePostRoomBooking(_urlGetCountWaitApproveRoomBooking, data_u0_count_cancel_approve);
                getCountWaitApprove();

                break;

            case "cmdSearchReportRoomDetail":

                data_roombooking data_report_roombooking = new data_roombooking();
                rbk_search_report_room_detail search_report_room = new rbk_search_report_room_detail();
                data_report_roombooking.rbk_search_report_room_list = new rbk_search_report_room_detail[1];
                search_report_room.condition = 0;
                search_report_room.cemp_idx = _emp_idx;
                search_report_room.staidx = int.Parse(ddlStatusRoomBooking.SelectedValue);
                search_report_room.IFSearchbetween = int.Parse(ddlSearchDateReport.SelectedValue);
                search_report_room.date_start = txtAddStartdateReport.Text;
                search_report_room.date_end = txtAddEndDateReport.Text;
                search_report_room.org_idx = int.Parse(ddlorgidx.SelectedValue);
                search_report_room.rdept_idx = int.Parse(ddlrdeptidx.SelectedValue);
                search_report_room.place_idx = int.Parse(ddlPlaceReportSearchDetail.SelectedValue);
                search_report_room.m0_room_idx = int.Parse(ddlRoomReportSearchDetail.SelectedValue);

                data_report_roombooking.rbk_search_report_room_list[0] = search_report_room;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_report_roombooking));


                if (txtAddEndDateReport.Text != "")
                {

                    IFormatProvider culture_report = new CultureInfo("en-US", true);
                    //DateTime DateToday_Set = DateTime.ParseExact(DateTime.Now.ToString("dd/MM/yyyy HH:mm"), "dd/MM/yyyy HH:mm", culture); //Date Time Today Incheck
                    DateTime DateTime_Start_Check = DateTime.ParseExact(txtAddStartdateReport.Text, "dd/MM/yyyy", culture_report);
                    DateTime DateTime_End_Check = DateTime.ParseExact(txtAddEndDateReport.Text, "dd/MM/yyyy", culture_report);

                    var result_day = DateTime_End_Check.Subtract(DateTime_Start_Check).TotalDays; // result time chrck > 30 min because cancel in room

                    //litDebug.Text = result_day.ToString();
                    if (result_day > 0)
                    {
                        data_report_roombooking = callServicePostRoomBooking(_urlGetReportRoomTableRbk, data_report_roombooking);
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_report_roombooking));

                        Update_PanelReportTable.Visible = true;
                        ViewState["Vs_DetailReportRoomTable"] = data_report_roombooking.rbk_search_report_room_list;
                        setGridData(GvDetailTableReport, ViewState["Vs_DetailReportRoomTable"]);

                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- กรุณาเช็ควันที่ค้นก่อนที่การค้นหา!!! ---');", true);
                        break;
                    }

                }
                else
                {
                    data_report_roombooking = callServicePostRoomBooking(_urlGetReportRoomTableRbk, data_report_roombooking);
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_report_roombooking));

                    Update_PanelReportTable.Visible = true;
                    ViewState["Vs_DetailReportRoomTable"] = data_report_roombooking.rbk_search_report_room_list;
                    setGridData(GvDetailTableReport, ViewState["Vs_DetailReportRoomTable"]);
                }
                break;
            case "cmdExportReportRoomDetail":

                data_roombooking data_report_roombooking_export = new data_roombooking();
                rbk_search_report_room_detail search_report_room_export = new rbk_search_report_room_detail();
                data_report_roombooking_export.rbk_search_report_room_list = new rbk_search_report_room_detail[1];
                search_report_room_export.condition = 0;
                search_report_room_export.cemp_idx = _emp_idx;
                search_report_room_export.staidx = int.Parse(ddlStatusRoomBooking.SelectedValue);
                search_report_room_export.IFSearchbetween = int.Parse(ddlSearchDateReport.SelectedValue);
                search_report_room_export.date_start = txtAddStartdateReport.Text;
                search_report_room_export.date_end = txtAddEndDateReport.Text;
                search_report_room_export.org_idx = int.Parse(ddlorgidx.SelectedValue);
                search_report_room_export.rdept_idx = int.Parse(ddlrdeptidx.SelectedValue);
                search_report_room_export.place_idx = int.Parse(ddlPlaceReportSearchDetail.SelectedValue);
                search_report_room_export.m0_room_idx = int.Parse(ddlRoomReportSearchDetail.SelectedValue);

                data_report_roombooking_export.rbk_search_report_room_list[0] = search_report_room_export;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_report_roombooking_export));
                data_report_roombooking_export = callServicePostRoomBooking(_urlGetReportRoomTableRbk, data_report_roombooking_export);
                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_report_roombooking));

                //Update_PanelReportTable.Visible = true;
                ViewState["Vs_DetailReportRoomTable_Export"] = data_report_roombooking_export.rbk_search_report_room_list;
                //setGridData(GvDetailTableReport, ViewState["Vs_DetailReportRoomTable"]);

                int count_ = 0;

                if (data_report_roombooking_export.return_code == 0)
                {
                    DataTable tableRoomBooking = new DataTable();
                    tableRoomBooking.Columns.Add("ชื่อห้อง", typeof(String));
                    tableRoomBooking.Columns.Add("ฝ่าย", typeof(String));
                    tableRoomBooking.Columns.Add("จำนวน", typeof(String));
                    //tableRoomBooking.Columns.Add("จำนวน", typeof(String));
                    //tableRoomBooking.Columns.Add("หน่วยนับ", typeof(String));

                    foreach (var row_report in data_report_roombooking_export.rbk_search_report_room_list)
                    {
                        DataRow addMRoomBookingRow = tableRoomBooking.NewRow();

                        addMRoomBookingRow[0] = row_report.room_name_th.ToString();
                        addMRoomBookingRow[1] = row_report.dept_name_th.ToString();
                        addMRoomBookingRow[2] = row_report.count_booking.ToString();
                        //addMaterialLogRow[2] = getTypeMaterialStockLog((string)materialLog.type_stock.ToString(), false);
                        //addMaterialLogRow[3] = string.Format("{0:n2}", materialLog.amount_material_log.ToString());
                        //addMaterialLogRow[4] = materialLog.unit_name_log.ToString();

                        tableRoomBooking.Rows.InsertAt(addMRoomBookingRow, count_++);
                    }
                    WriteExcelWithNPOI(tableRoomBooking, "xls", "report-roombooking");
                    //WriteExcelWithNPOI(tableRoomBooking, "xls", "report-roombooking", "Export To Excel");
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- ไม่พบข้อมูล ---');", true);
                    break;
                }

                break;
            case "cmdViewDetailReportTable":

                Update_PnSearchReport.Visible = false;
                Update_PanelReportTable.Visible = false;


                string[] arg8 = new string[2];
                arg8 = e.CommandArgument.ToString().Split(';');
                int _m0_room_idx_report = int.Parse(arg8[0]);
                int _rdept_idx_report = int.Parse(arg8[1]);

                data_roombooking data_report_view = new data_roombooking();
                rbk_search_report_room_detail search_report_view = new rbk_search_report_room_detail();
                data_report_view.rbk_search_report_room_list = new rbk_search_report_room_detail[1];
                search_report_view.condition = 1;
                search_report_view.cemp_idx = _emp_idx;
                search_report_view.staidx = int.Parse(ddlStatusRoomBooking.SelectedValue);
                search_report_view.IFSearchbetween = int.Parse(ddlSearchDateReport.SelectedValue);
                search_report_view.date_start = txtAddStartdateReport.Text;
                search_report_view.date_end = txtAddEndDateReport.Text;
                search_report_view.org_idx = int.Parse(ddlorgidx.SelectedValue);
                search_report_view.rdept_idx = _rdept_idx_report;
                search_report_view.place_idx = int.Parse(ddlPlaceReportSearchDetail.SelectedValue);
                search_report_view.m0_room_idx = _m0_room_idx_report;

                data_report_view.rbk_search_report_room_list[0] = search_report_view;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_report_view));
                data_report_view = callServicePostRoomBooking(_urlGetReportRoomTableRbk, data_report_view);

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_report_view));
                Update_PanelDetailReportTable.Visible = true;
                Update_PanelBackToSearch.Visible = true;

                ViewState["Vs_ViewDetailReportRoomTable"] = data_report_view.rbk_search_report_room_list;
                setGridData(GvViewDetailReport, ViewState["Vs_ViewDetailReportRoomTable"]);

                //litDebug.Text = ddlStatusRoomBooking.SelectedValue.ToString();
                break;

            case "cmdBackToSearchReport":
                Update_PanelDetailReportTable.Visible = false;
                Update_PanelBackToSearch.Visible = false;

                Update_PnSearchReport.Visible = true;
                Update_PanelReportTable.Visible = true;


                setGridData(GvDetailTableReport, ViewState["Vs_DetailReportRoomTable"]);


                break;

            case "cmdExportDetailReportRoomDetail":

                int count_detail_ = 0;
                if (ViewState["Vs_ViewDetailReportRoomTable"] != null)
                {
                    DataTable tableRoomBooking = new DataTable();
                    tableRoomBooking.Columns.Add("สถานที่", typeof(String));
                    tableRoomBooking.Columns.Add("ชื่อห้อง", typeof(String));
                    tableRoomBooking.Columns.Add("ฝ่าย", typeof(String));
                    tableRoomBooking.Columns.Add("วันและเวลาที่เริ่มจอง", typeof(String));
                    tableRoomBooking.Columns.Add("วันและเวลาที่สิ้นสุด", typeof(String));
                    //tableRoomBooking.Columns.Add("จำนวน", typeof(String));
                    //tableRoomBooking.Columns.Add("หน่วยนับ", typeof(String));

                    rbk_search_report_room_detail[] _item_listexport_detail = (rbk_search_report_room_detail[])ViewState["Vs_ViewDetailReportRoomTable"];

                    foreach (var row_report_detail in _item_listexport_detail)
                    {
                        DataRow addMRoomBookingRow_Detail = tableRoomBooking.NewRow();

                        addMRoomBookingRow_Detail[0] = row_report_detail.place_name.ToString();
                        addMRoomBookingRow_Detail[1] = row_report_detail.room_name_th.ToString();
                        addMRoomBookingRow_Detail[2] = row_report_detail.dept_name_th.ToString();
                        addMRoomBookingRow_Detail[3] = "ตั้งแต่วันที่ : " + " " + row_report_detail.date_start.ToString() + " " + "เวลา : " + " " + row_report_detail.time_start.ToString();
                        addMRoomBookingRow_Detail[4] = "ถึงวันที่ : " + " " + row_report_detail.date_end.ToString() + " " + "เวลา : " + " " + row_report_detail.time_end.ToString();
                        //addMaterialLogRow[2] = getTypeMaterialStockLog((string)materialLog.type_stock.ToString(), false);
                        //addMaterialLogRow[3] = string.Format("{0:n2}", materialLog.amount_material_log.ToString());
                        //addMaterialLogRow[4] = materialLog.unit_name_log.ToString();

                        tableRoomBooking.Rows.InsertAt(addMRoomBookingRow_Detail, count_detail_++);
                    }
                    WriteExcelWithNPOI(tableRoomBooking, "xls", "report-roombooking");
                    //WriteExcelWithNPOI(tableRoomBooking, "xls", "report-roombooking", "Export To Excel");
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- ไม่พบข้อมูล ---');", true);
                    break;
                }

                break;

            case "cmdSearchReportGraph":

                switch (ddlTypeGraph.SelectedValue)
                {
                    case "1": //จำนวนการจอง/หน่วยงาน
                        getCountBookingReportGraph();
                        break;

                    case "2": //จำนวนการยกเลิก/หน่วยงาน
                        getCountCancelBookingReportGraph();
                        break;
                }

                break;

            case "cmdSearchRoomApprove":

                // --- Check Date Search Between //
                IFormatProvider culture_searchapprove = new CultureInfo("en-US", true);
                //DateTime DateToday = DateTime.ParseExact(DateTime.Now.ToString("dd/MM/yyyy HH:mm"), "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                DateTime dateVal_start_serachapprove = DateTime.ParseExact(AddStartdateApprove.Text, "dd/MM/yyyy", culture_searchapprove);
                if (AddEndDateApprove.Text != "")
                {
                    DateTime dateVal_end_searchapprove = DateTime.ParseExact(AddEndDateApprove.Text, "dd/MM/yyyy", culture_searchapprove);
                    ViewState["Vs_CheckDateSearch_Approve"] = (dateVal_end_searchapprove - dateVal_start_serachapprove).TotalDays.ToString();

                    //litDebug.Text = ViewState["Vs_CheckDateSearch"].ToString();
                }

                // --- Check Date Search Between //



                //// search room busy ////
                data_roombooking data_search_approve_booking = new data_roombooking();
                rbk_search_room_detail search_approve_booking = new rbk_search_room_detail();
                data_search_approve_booking.rbk_search_room_list = new rbk_search_room_detail[1];

                search_approve_booking.date_start = AddStartdateApprove.Text;
                search_approve_booking.date_end = AddEndDateApprove.Text;
                search_approve_booking.IFSearchbetween = int.Parse(ddlSearchDateApprove.SelectedValue);
                search_approve_booking.place_idx = int.Parse(ddlPlaceSearchApprove.SelectedValue);
                search_approve_booking.m0_room_idx = int.Parse(ddlRoomSearchApprove.SelectedValue);
                search_approve_booking.cemp_idx = _emp_idx;
                search_approve_booking.rpos_idx = int.Parse(ViewState["rpos_permission"].ToString());

                data_search_approve_booking.rbk_search_room_list[0] = search_approve_booking;


                if (int.Parse(ddlSearchDateApprove.SelectedValue) == 3 && int.Parse(ViewState["Vs_CheckDateSearch_Approve"].ToString()) <= 0) //between date start and date end
                {

                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- กรุณาตรวจสอบวันที่ก่อนทำการการค้นหา ---');", true);
                    ViewState["vs_WaitApproveRoomBooking"] = null;
                    setGridData(GvWaitApprove, ViewState["vs_WaitApproveRoomBooking"]);
                    break;
                }
                else
                {
                    data_search_approve_booking = callServicePostRoomBooking(_urlGetSearchDetailApproveRbk, data_search_approve_booking);
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_search_approve_booking));

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_search_detail));

                    ViewState["vs_WaitApproveRoomBooking"] = data_search_approve_booking.rbk_u0_roombooking_list;
                    setGridData(GvWaitApprove, ViewState["vs_WaitApproveRoomBooking"]);

                }

                break;


        }

    }
    //endbtn

    #endregion event command

    #region selected Changed
    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        if (sender is DropDownList)
        {
            DropDownList ddlName = (DropDownList)sender;

            switch (ddlName.ID)
            {
                case "ddlPlace":

                    DropDownList ddlPlace = (DropDownList)FvInsertRoomBooking.FindControl("ddlPlace");
                    DropDownList ddlRoom = (DropDownList)FvInsertRoomBooking.FindControl("ddlRoom");

                    getRoom(ddlRoom, int.Parse(ddlPlace.SelectedValue));
                    setGridData(GvDetailRoomSearch, ViewState["vs_DetailRoomInSearch"]);

                    break;
                case "ddlPlace_edit":

                    DropDownList ddlPlace_edit = (DropDownList)fvDetailRoomBooking.FindControl("ddlPlace_edit");
                    DropDownList ddlRoom_edit = (DropDownList)fvDetailRoomBooking.FindControl("ddlRoom_edit");
                    getRoom(ddlRoom_edit, int.Parse(ddlPlace_edit.SelectedValue));
                    setGridData(GvDetailRoomSearch_Edit, ViewState["vs_DetailRoomInSearch"]);

                    break;
                case "ddlPlaceSearch":

                    DropDownList ddlPlaceSearch = (DropDownList)FvDetailUseRoomSearch.FindControl("ddlPlaceSearch");
                    DropDownList ddlRoomSearch = (DropDownList)FvDetailUseRoomSearch.FindControl("ddlRoomSearch");

                    Session["PlaceSearch_Room"] = ddlPlaceSearch.SelectedValue;
                    getRoom(ddlRoomSearch, int.Parse(ddlPlaceSearch.SelectedValue));

                    break;
                case "ddlRoomSearch":

                    DropDownList ddlPlaceSearch_value = (DropDownList)FvDetailUseRoomSearch.FindControl("ddlPlaceSearch");
                    DropDownList ddlRoomSearch_value = (DropDownList)FvDetailUseRoomSearch.FindControl("ddlRoomSearch");

                    if (ddlPlaceSearch_value.SelectedValue != "0")
                    {
                        Session["PlaceSearch_Room"] = ddlPlaceSearch_value.SelectedValue;
                        Session["RoomIDXSearch_Room"] = ddlRoomSearch_value.SelectedValue;
                        //litDebug.Text = Session["RoomIDXSearch_Room"].ToString();
                    }
                    //getRoom(ddlRoomSearch, int.Parse(ddlPlaceSearch.SelectedValue));

                    break;
                case "ddlRoom_edit":

                    //DropDownList ddlPlace_edit = (DropDownList)fvDetailRoomBooking.FindControl("ddlPlace_edit");
                    DropDownList ddlRoom_edit_search = (DropDownList)fvDetailRoomBooking.FindControl("ddlRoom_edit");
                    DropDownList ddlPlace_edit_search = (DropDownList)fvDetailRoomBooking.FindControl("ddlPlace_edit");
                    TextBox tb_type_booking_idx = (TextBox)fvDetailRoomBooking.FindControl("tb_type_booking_idx");
                    TextBox tb_date_start_edit = (TextBox)fvDetailRoomBooking.FindControl("tb_date_start");
                    TextBox tb_time_start_edit = (TextBox)fvDetailRoomBooking.FindControl("tb_time_start");
                    TextBox tb_date_end_edit = (TextBox)fvDetailRoomBooking.FindControl("tb_date_end");
                    TextBox tb_time_end_edit = (TextBox)fvDetailRoomBooking.FindControl("tb_time_end");
                    UpdatePanel Update_DetailRoomSearch_Edit = (UpdatePanel)fvDetailRoomBooking.FindControl("Update_DetailRoomSearch_Edit");
                    TextBox tb_m0_room_idx = (TextBox)fvDetailRoomBooking.FindControl("tb_m0_room_idx");
                    LinkButton btnSaveEditDetail = (LinkButton)fvDetailRoomBooking.FindControl("btnSaveEditDetail");


                    if (ddlRoom_edit_search.SelectedValue != "0" && tb_m0_room_idx.Text != ddlRoom_edit_search.SelectedValue) // เปลี่ยนห้แง
                    {
                        //selected Rooom Booking
                        data_roombooking data_roomcheck_detail = new data_roombooking();
                        rbk_check_room_detail roomcheck_detail = new rbk_check_room_detail();
                        data_roomcheck_detail.rbk_check_room_list = new rbk_check_room_detail[1];
                        roomcheck_detail.condition = 0;
                        data_roomcheck_detail.rbk_check_room_list[0] = roomcheck_detail;
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_roomcheck_detail));

                        data_roomcheck_detail = callServicePostRoomBooking(_urlGetCheckRoomInCreate, data_roomcheck_detail);
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_roomcheck_detail));

                        //// all room detail online ////

                        //// search room busy ////
                        data_roombooking data_search_detail = new data_roombooking();
                        rbk_search_room_detail search_detail = new rbk_search_room_detail();
                        data_search_detail.rbk_search_room_list = new rbk_search_room_detail[1];

                        //search_detail.condition = 1;
                        search_detail.condition = int.Parse(tb_type_booking_idx.Text); // condition type create room 
                        search_detail.place_idx = int.Parse(ddlPlace_edit_search.SelectedValue);

                        search_detail.m0_room_idx = int.Parse(ddlRoom_edit_search.SelectedValue);


                        //if (ddlRoom.SelectedValue != "0")
                        //{
                        //    search_detail.m0_room_idx = int.Parse(ddlRoom.SelectedValue);
                        //}
                        //else
                        //{
                        //    search_detail.m0_room_idx = 0;
                        //}
                        search_detail.date_start = tb_date_start_edit.Text + " " + tb_time_start_edit.Text;
                        search_detail.time_start = tb_time_start_edit.Text;
                        search_detail.date_end = tb_date_end_edit.Text + " " + tb_time_end_edit.Text;
                        search_detail.time_end = tb_time_end_edit.Text;
                        search_detail.cemp_idx = _emp_idx;


                        data_search_detail.rbk_search_room_list[0] = search_detail;
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_search_detail));
                        data_search_detail = callServicePostRoomBooking(_urlGetRbkSearchRommCreate, data_search_detail);
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_search_detail));

                        //////// search room busy ////
                        if (data_search_detail.return_code == 1)// ไม่เร่งด่วน ว่างหมด
                        {
                            btnSaveEditDetail.Visible = false;
                            //litDebug.Text = "1";
                            ViewState["data_check_Room"] = "";
                            if (int.Parse(ddlRoom_edit_search.SelectedValue) == 0)
                            {
                                var linqRoomDetail = from _dt_RoomDetail in data_roomcheck_detail.rbk_check_room_list
                                                     where
                                                     _dt_RoomDetail.place_idx == int.Parse(ddlPlace_edit_search.SelectedValue)
                                                     //|| _dt_RoomDetail.m0_room_idx == int.Parse(ddlRoom.SelectedValue)
                                                     select _dt_RoomDetail;

                                Update_DetailRoomSearch_Edit.Visible = true;
                                ViewState["vs_DetailRoomInSearch"] = linqRoomDetail.ToList();
                                setGridData(GvDetailRoomSearch_Edit, ViewState["vs_DetailRoomInSearch"]);
                            }
                            else
                            {
                                var linqRoomDetail = from _dt_RoomDetail in data_roomcheck_detail.rbk_check_room_list
                                                     where
                                                     _dt_RoomDetail.place_idx == int.Parse(ddlPlace_edit_search.SelectedValue)
                                                     && _dt_RoomDetail.m0_room_idx == int.Parse(ddlRoom_edit_search.SelectedValue)
                                                     select _dt_RoomDetail;

                                Update_DetailRoomSearch_Edit.Visible = true;
                                ViewState["vs_DetailRoomInSearch"] = linqRoomDetail.ToList();
                                setGridData(GvDetailRoomSearch_Edit, ViewState["vs_DetailRoomInSearch"]);
                            }

                        }
                        else if (data_search_detail.return_code == 0) // ไม่เร่งด่วน เจอบางส่วน
                        {
                            btnSaveEditDetail.Visible = false;
                            //litDebug.Text = "2";
                            ViewState["data_check_Room"] = "";
                            ViewState["data_check_Room_Not"] = data_search_detail.rbk_search_room_list;

                            rbk_search_room_detail[] _templist_IdSetTest = (rbk_search_room_detail[])ViewState["data_check_Room_Not"];

                            var linqRoomDetailCheck = (from _dt_RoomDetailCheck in _templist_IdSetTest

                                                       select _dt_RoomDetailCheck).ToList(); ;

                            string SetTestId = "";
                            string listSetTestId = "";

                            for (int idset = 0; idset < linqRoomDetailCheck.Count(); idset++)
                            {
                                SetTestId = linqRoomDetailCheck[idset].m0_room_idx.ToString();
                                listSetTestId += "," + SetTestId;

                            }

                            //Split comma frist posision
                            string resultString = listSetTestId.IndexOf(',') > -1
                            ? listSetTestId.Substring(listSetTestId.IndexOf(',') + 1)
                            : listSetTestId;

                            //litDebug.Text = resultString;
                            data_roombooking data_roomcheck_detail_check = new data_roombooking();
                            rbk_check_room_detail roomcheck_detail_check = new rbk_check_room_detail();
                            data_roomcheck_detail_check.rbk_check_room_list = new rbk_check_room_detail[1];

                            if (ddlRoom_edit_search.SelectedValue != "0")
                            {
                                roomcheck_detail_check.m0_room_idx = int.Parse(ddlRoom_edit_search.SelectedValue);
                            }
                            else
                            {

                                roomcheck_detail_check.m0_room_idx = 0;
                            }
                            roomcheck_detail_check.condition = 1;


                            roomcheck_detail_check.m0_room_idx_check = resultString;
                            roomcheck_detail_check.place_idx = int.Parse(ddlPlace_edit_search.SelectedValue);

                            ////roomcheck_detail_check.date_start = tb_date_start_edit.Text;
                            ////roomcheck_detail_check.time_start = tb_time_start_edit.Text;
                            ////roomcheck_detail_check.date_end = tb_date_end_edit.Text;
                            ////roomcheck_detail_check.time_end = tb_time_end_edit.Text;

                            data_roomcheck_detail_check.rbk_check_room_list[0] = roomcheck_detail_check;
                            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_roomcheck_detail_check));

                            data_roomcheck_detail_check = callServicePostRoomBooking(_urlGetCheckRoomInCreate, data_roomcheck_detail_check);
                            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_roomcheck_detail_check));


                            if (data_roomcheck_detail_check.return_code == 0)
                            {
                                Update_DetailRoomSearch_Edit.Visible = true;
                                //litDebug.Text = "28888";
                                ViewState["vs_DetailRoomInSearch"] = data_roomcheck_detail_check.rbk_check_room_list;
                                setGridData(GvDetailRoomSearch_Edit, ViewState["vs_DetailRoomInSearch"]);
                            }
                            else
                            {
                                //litDebug.Text = "999";
                                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ห้องไม่ว่าง');", true);

                                Update_DetailRoomSearch_Edit.Visible = false;
                                ViewState["vs_DetailRoomInSearch"] = null;
                                setGridData(GvDetailRoomSearch_Edit, ViewState["vs_DetailRoomInSearch"]);
                            }


                        }
                        else if (data_search_detail.return_code == 3)// เร่งด่วน จองได้หมด ไม่ว่างบางส่วน
                        {
                            btnSaveEditDetail.Visible = false;
                            //litDebug.Text = "3";
                            ViewState["data_check_Room"] = data_search_detail.rbk_search_room_list;

                            data_roombooking data_roomcheck_detail_check = new data_roombooking();
                            rbk_check_room_detail roomcheck_detail_check = new rbk_check_room_detail();
                            data_roomcheck_detail_check.rbk_check_room_list = new rbk_check_room_detail[1];

                            roomcheck_detail_check.condition = 2;
                            roomcheck_detail_check.m0_room_idx = int.Parse(ddlRoom_edit_search.SelectedValue);
                            roomcheck_detail_check.place_idx = int.Parse(ddlPlace_edit_search.SelectedValue);
                            roomcheck_detail_check.cemp_idx = _emp_idx;

                            ////roomcheck_detail_check.date_start = txtDateStart_create.Text + txt_timestart_create.Text;
                            ////roomcheck_detail_check.time_start = txt_timestart_create.Text;
                            ////roomcheck_detail_check.date_end = txtDateEnd_create.Text + txt_timeend_create.Text;
                            ////roomcheck_detail_check.time_end = txt_timeend_create.Text;

                            data_roomcheck_detail_check.rbk_check_room_list[0] = roomcheck_detail_check;
                            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_roomcheck_detail_check));

                            data_roomcheck_detail_check = callServicePostRoomBooking(_urlGetCheckRoomInCreate, data_roomcheck_detail_check);
                            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_roomcheck_detail_check));

                            Update_DetailRoomSearch_Edit.Visible = true;
                            ViewState["vs_DetailRoomInSearch"] = data_roomcheck_detail_check.rbk_check_room_list;
                            setGridData(GvDetailRoomSearch_Edit, ViewState["vs_DetailRoomInSearch"]);

                        }
                        else
                        {
                            btnSaveEditDetail.Visible = false;
                            //litDebug.Text = "54";
                            ViewState["data_check_Room"] = "";

                            data_roombooking data_roomcheck_detail_check = new data_roombooking();
                            rbk_check_room_detail roomcheck_detail_check = new rbk_check_room_detail();
                            data_roomcheck_detail_check.rbk_check_room_list = new rbk_check_room_detail[1];

                            roomcheck_detail_check.condition = 2;
                            roomcheck_detail_check.m0_room_idx = int.Parse(ddlRoom_edit_search.SelectedValue);
                            roomcheck_detail_check.place_idx = int.Parse(ddlPlace_edit_search.SelectedValue);

                            data_roomcheck_detail_check.rbk_check_room_list[0] = roomcheck_detail_check;
                            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_roomcheck_detail_check));

                            data_roomcheck_detail_check = callServicePostRoomBooking(_urlGetCheckRoomInCreate, data_roomcheck_detail_check);
                            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_roomcheck_detail_check));

                            Update_DetailRoomSearch_Edit.Visible = true;
                            ViewState["vs_DetailRoomInSearch"] = data_roomcheck_detail_check.rbk_check_room_list;
                            setGridData(GvDetailRoomSearch_Edit, ViewState["vs_DetailRoomInSearch"]);
                        }
                        //getRoom(ddlRoom_edit, int.Parse(ddlPlace_edit.SelectedValue));


                    }
                    else
                    {

                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ห้องซ้ำ กรุณาเลือกห้องที่ต้องการเปลี่ยนแปลง');", true);

                        Update_DetailRoomSearch_Edit.Visible = false;
                        if (ViewState["Vs_Status_ProcessCheck_edit"].ToString() == "6")
                        {
                            btnSaveEditDetail.Visible = false;
                        }
                        else
                        {
                            btnSaveEditDetail.Visible = true;
                        }
                        //btnSaveEditDetail.Visible = true;

                        ViewState["vs_DetailRoomInSearch"] = null;
                        setGridData(GvDetailRoomSearch_Edit, ViewState["vs_DetailRoomInSearch"]);
                    }

                    break;
                case "ddlPlaceSearchDetail":

                    DropDownList ddlPlaceSearchDetail = (DropDownList)FvSearchDetailRoom.FindControl("ddlPlaceSearchDetail");
                    DropDownList ddlRoomSearchDetail = (DropDownList)FvSearchDetailRoom.FindControl("ddlRoomSearchDetail");
                    getRoom(ddlRoomSearchDetail, int.Parse(ddlPlaceSearchDetail.SelectedValue));

                    break;

                case "ddlSearchDate":

                    if (int.Parse(ddlSearchDate.SelectedValue) == 3)
                    {
                        AddEndDate.Enabled = true;
                    }
                    else
                    {
                        AddEndDate.Enabled = false;
                        AddEndDate.Text = string.Empty;
                    }

                    break;

                case "ddlRoom_edit_approve":

                    //DropDownList ddlPlace_edit = (DropDownList)fvDetailRoomBooking.FindControl("ddlPlace_edit");
                    DropDownList ddlRoom_edit_approve = (DropDownList)FvWaitApproveDetail.FindControl("ddlRoom_edit_approve");
                    DropDownList ddlPlace_edit_approve = (DropDownList)FvWaitApproveDetail.FindControl("ddlPlace_edit_approve");
                    TextBox tb_type_booking_idx_approve = (TextBox)FvWaitApproveDetail.FindControl("tb_type_booking_idx_approve");
                    TextBox tb_date_start_approve = (TextBox)FvWaitApproveDetail.FindControl("tb_date_start_approve");
                    TextBox tb_time_start_approve = (TextBox)FvWaitApproveDetail.FindControl("tb_time_start_approve");
                    TextBox tb_date_end_approve = (TextBox)FvWaitApproveDetail.FindControl("tb_date_end_approve");
                    TextBox tb_time_end_approve = (TextBox)FvWaitApproveDetail.FindControl("tb_time_end_approve");
                    UpdatePanel Update_DetailRoomSearch_EditApprove = (UpdatePanel)FvWaitApproveDetail.FindControl("Update_DetailRoomSearch_EditApprove");
                    TextBox tb_m0_room_idx_approve = (TextBox)FvWaitApproveDetail.FindControl("tb_m0_room_idx_approve");
                    //LinkButton btnSaveEditDetail = (LinkButton)FvWaitApproveDetail.FindControl("btnSaveEditDetail");


                    if (tb_m0_room_idx_approve.Text != ddlRoom_edit_approve.SelectedValue) // เปลี่ยนห้แง
                    {
                        //selected Rooom Booking
                        data_roombooking data_roomcheck_detail_approve = new data_roombooking();
                        rbk_check_room_detail roomcheck_detail_approve = new rbk_check_room_detail();
                        data_roomcheck_detail_approve.rbk_check_room_list = new rbk_check_room_detail[1];
                        roomcheck_detail_approve.condition = 0;
                        data_roomcheck_detail_approve.rbk_check_room_list[0] = roomcheck_detail_approve;
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_roomcheck_detail));

                        data_roomcheck_detail_approve = callServicePostRoomBooking(_urlGetCheckRoomInCreate, data_roomcheck_detail_approve);
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_roomcheck_detail));

                        //// all room detail online ////

                        //// search room busy ////
                        data_roombooking data_search_detail_approve = new data_roombooking();
                        rbk_search_room_detail search_detail_approve = new rbk_search_room_detail();
                        data_search_detail_approve.rbk_search_room_list = new rbk_search_room_detail[1];

                        //search_detail.condition = 1;
                        search_detail_approve.condition = int.Parse(tb_type_booking_idx_approve.Text); // condition type create room 
                        search_detail_approve.place_idx = int.Parse(ddlPlace_edit_approve.SelectedValue);

                        search_detail_approve.m0_room_idx = int.Parse(ddlRoom_edit_approve.SelectedValue);


                        //if (ddlRoom.SelectedValue != "0")
                        //{
                        //    search_detail.m0_room_idx = int.Parse(ddlRoom.SelectedValue);
                        //}
                        //else
                        //{
                        //    search_detail.m0_room_idx = 0;
                        //}
                        search_detail_approve.date_start = tb_date_start_approve.Text + " " + tb_time_start_approve.Text;
                        search_detail_approve.time_start = tb_time_start_approve.Text;
                        search_detail_approve.date_end = tb_date_end_approve.Text + " " + tb_time_end_approve.Text;
                        search_detail_approve.time_end = tb_time_end_approve.Text;
                        search_detail_approve.cemp_idx = _emp_idx;


                        data_search_detail_approve.rbk_search_room_list[0] = search_detail_approve;
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_search_detail));
                        data_search_detail_approve = callServicePostRoomBooking(_urlGetRbkSearchRommCreate, data_search_detail_approve);
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_search_detail_approve));

                        //////////// search room busy ////
                        if (data_search_detail_approve.return_code == 1)// ไม่เร่งด่วน ว่างหมด
                        {
                            //btnSaveEditDetail.Visible = false;
                            //litDebug.Text = "1";
                            ViewState["data_check_RoomApprove"] = "";
                            if (int.Parse(ddlRoom_edit_approve.SelectedValue) == 0)
                            {
                                var linqRoomDetail = from _dt_RoomDetail in data_roomcheck_detail_approve.rbk_check_room_list
                                                     where
                                                     _dt_RoomDetail.place_idx == int.Parse(ddlPlace_edit_approve.SelectedValue)
                                                     //|| _dt_RoomDetail.m0_room_idx == int.Parse(ddlRoom.SelectedValue)
                                                     select _dt_RoomDetail;

                                Update_DetailRoomSearch_EditApprove.Visible = true;
                                ViewState["vs_DetailRoomInSearchApprove"] = linqRoomDetail.ToList();
                                setGridData(GvDetailRoomSearch_EditApprove, ViewState["vs_DetailRoomInSearchApprove"]);
                            }
                            else
                            {
                                var linqRoomDetail = from _dt_RoomDetail in data_roomcheck_detail_approve.rbk_check_room_list
                                                     where
                                                     _dt_RoomDetail.place_idx == int.Parse(ddlPlace_edit_approve.SelectedValue)
                                                     && _dt_RoomDetail.m0_room_idx == int.Parse(ddlRoom_edit_approve.SelectedValue)
                                                     select _dt_RoomDetail;

                                Update_DetailRoomSearch_EditApprove.Visible = true;
                                ViewState["vs_DetailRoomInSearchApprove"] = linqRoomDetail.ToList();
                                setGridData(GvDetailRoomSearch_EditApprove, ViewState["vs_DetailRoomInSearchApprove"]);
                            }

                        }
                        else if (data_search_detail_approve.return_code == 0) // ไม่เร่งด่วน เจอบางส่วน
                        {
                            ////btnSaveEditDetail.Visible = false;
                            //litDebug.Text = "2";
                            ViewState["data_check_RoomApprove"] = "";
                            ViewState["data_check_Room_Not_Approve"] = data_search_detail_approve.rbk_search_room_list;

                            rbk_search_room_detail[] _templist_IdSetTest = (rbk_search_room_detail[])ViewState["data_check_Room_Not_Approve"];

                            var linqRoomDetailCheck = (from _dt_RoomDetailCheck in _templist_IdSetTest

                                                       select _dt_RoomDetailCheck).ToList(); ;

                            string SetTestId = "";
                            string listSetTestId = "";

                            for (int idset = 0; idset < linqRoomDetailCheck.Count(); idset++)
                            {
                                SetTestId = linqRoomDetailCheck[idset].m0_room_idx.ToString();
                                listSetTestId += "," + SetTestId;

                            }

                            //Split comma frist posision
                            string resultString = listSetTestId.IndexOf(',') > -1
                            ? listSetTestId.Substring(listSetTestId.IndexOf(',') + 1)
                            : listSetTestId;

                            //litDebug.Text = resultString;
                            data_roombooking data_roomcheck_detail_check = new data_roombooking();
                            rbk_check_room_detail roomcheck_detail_check = new rbk_check_room_detail();
                            data_roomcheck_detail_check.rbk_check_room_list = new rbk_check_room_detail[1];

                            if (ddlRoom_edit_approve.SelectedValue != "0")
                            {
                                roomcheck_detail_check.m0_room_idx = int.Parse(ddlRoom_edit_approve.SelectedValue);
                            }
                            else
                            {

                                roomcheck_detail_check.m0_room_idx = 0;
                            }
                            roomcheck_detail_check.condition = 1;


                            roomcheck_detail_check.m0_room_idx_check = resultString;
                            roomcheck_detail_check.place_idx = int.Parse(ddlPlace_edit_approve.SelectedValue);

                            ////roomcheck_detail_check.date_start = tb_date_start_edit.Text;
                            ////roomcheck_detail_check.time_start = tb_time_start_edit.Text;
                            ////roomcheck_detail_check.date_end = tb_date_end_edit.Text;
                            ////roomcheck_detail_check.time_end = tb_time_end_edit.Text;

                            data_roomcheck_detail_check.rbk_check_room_list[0] = roomcheck_detail_check;
                            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_roomcheck_detail_check));

                            data_roomcheck_detail_check = callServicePostRoomBooking(_urlGetCheckRoomInCreate, data_roomcheck_detail_check);
                            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_roomcheck_detail_check));


                            if (data_roomcheck_detail_check.return_code == 0)
                            {
                                Update_DetailRoomSearch_EditApprove.Visible = true;
                                //litDebug.Text = "28888";
                                ViewState["vs_DetailRoomInSearchApprove"] = data_roomcheck_detail_check.rbk_check_room_list;
                                setGridData(GvDetailRoomSearch_EditApprove, ViewState["vs_DetailRoomInSearchApprove"]);
                            }
                            else
                            {
                                //litDebug.Text = "999";
                                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ห้องไม่ว่าง');", true);

                                Update_DetailRoomSearch_EditApprove.Visible = false;
                                ViewState["vs_DetailRoomInSearchApprove"] = null;
                                setGridData(GvDetailRoomSearch_EditApprove, ViewState["vs_DetailRoomInSearchApprove"]);
                            }


                        }
                        else if (data_search_detail_approve.return_code == 3)// เร่งด่วน จองได้หมด ไม่ว่างบางส่วน
                        {
                            ////btnSaveEditDetail.Visible = false;
                            //litDebug.Text = "3";
                            ViewState["data_check_RoomApprove"] = data_search_detail_approve.rbk_search_room_list;

                            data_roombooking data_roomcheck_detail_check = new data_roombooking();
                            rbk_check_room_detail roomcheck_detail_check = new rbk_check_room_detail();
                            data_roomcheck_detail_check.rbk_check_room_list = new rbk_check_room_detail[1];

                            roomcheck_detail_check.condition = 2;
                            roomcheck_detail_check.m0_room_idx = int.Parse(ddlRoom_edit_approve.SelectedValue);
                            roomcheck_detail_check.place_idx = int.Parse(ddlPlace_edit_approve.SelectedValue);
                            roomcheck_detail_check.cemp_idx = _emp_idx;

                            ////roomcheck_detail_check.date_start = txtDateStart_create.Text + txt_timestart_create.Text;
                            ////roomcheck_detail_check.time_start = txt_timestart_create.Text;
                            ////roomcheck_detail_check.date_end = txtDateEnd_create.Text + txt_timeend_create.Text;
                            ////roomcheck_detail_check.time_end = txt_timeend_create.Text;

                            data_roomcheck_detail_check.rbk_check_room_list[0] = roomcheck_detail_check;
                            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_roomcheck_detail_check));

                            data_roomcheck_detail_check = callServicePostRoomBooking(_urlGetCheckRoomInCreate, data_roomcheck_detail_check);
                            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_roomcheck_detail_check));

                            Update_DetailRoomSearch_EditApprove.Visible = true;
                            ViewState["vs_DetailRoomInSearchApprove"] = data_roomcheck_detail_check.rbk_check_room_list;
                            setGridData(GvDetailRoomSearch_EditApprove, ViewState["vs_DetailRoomInSearchApprove"]);

                        }
                        else
                        {
                            ////btnSaveEditDetail.Visible = false;
                            //litDebug.Text = "4";
                            ViewState["data_check_RoomApprove"] = "";

                            data_roombooking data_roomcheck_detail_check = new data_roombooking();
                            rbk_check_room_detail roomcheck_detail_check = new rbk_check_room_detail();
                            data_roomcheck_detail_check.rbk_check_room_list = new rbk_check_room_detail[1];

                            roomcheck_detail_check.condition = 2;
                            roomcheck_detail_check.m0_room_idx = int.Parse(ddlRoom_edit_approve.SelectedValue);
                            roomcheck_detail_check.place_idx = int.Parse(ddlPlace_edit_approve.SelectedValue);

                            data_roomcheck_detail_check.rbk_check_room_list[0] = roomcheck_detail_check;
                            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_roomcheck_detail_check));

                            data_roomcheck_detail_check = callServicePostRoomBooking(_urlGetCheckRoomInCreate, data_roomcheck_detail_check);
                            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_roomcheck_detail_check));

                            Update_DetailRoomSearch_EditApprove.Visible = true;
                            ViewState["vs_DetailRoomInSearchApprove"] = data_roomcheck_detail_check.rbk_check_room_list;
                            setGridData(GvDetailRoomSearch_EditApprove, ViewState["vs_DetailRoomInSearchApprove"]);
                        }
                        //////getRoom(ddlRoom_edit, int.Parse(ddlPlace_edit.SelectedValue));
                        ///
                        GridView GvConferenceInRoom_DetailApproveEdit = (GridView)FvWaitApproveDetail.FindControl("GvConferenceInRoom_DetailApproveEdit");


                        SelectConferenceDetail(GvConferenceInRoom_DetailApproveEdit);
                       

                    }
                    else
                    {

                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ห้องซ้ำ กรุณาเลือกห้องที่ต้องการแทนที่');", true);

                        Update_DetailRoomSearch_EditApprove.Visible = false;
                        //btnSaveEditDetail.Visible = true;

                        ViewState["vs_DetailRoomInSearchApprove"] = null;
                        setGridData(GvDetailRoomSearch_EditApprove, ViewState["vs_DetailRoomInSearchApprove"]);
                    }

                    break;

                case "ddlTypeReport":

                    //set Panel In Report

                    Update_PanelReportTable.Visible = false;

                    Update_PnTableReportDetail.Visible = false;
                    Update_PnGraphReportDetail.Visible = false;
                    ddlStatusRoomBooking.ClearSelection();
                    ddlSearchDateReport.ClearSelection();
                    txtAddEndDateReport.Enabled = false;
                    txtAddStartdateReport.Text = string.Empty;
                    txtAddEndDateReport.Text = string.Empty;

                    ddlrdeptidx.Items.Clear();
                    ddlrdeptidx.Items.Insert(0, new ListItem("--- เลือกฝ่าย ---", "0"));

                    ddlRoomReportSearchDetail.Items.Clear();
                    ddlRoomReportSearchDetail.Items.Insert(0, new ListItem("--- เลือกห้องประชุม ---", "0"));

                    //Clear Panel Graph

                    ddlTypeGraph.ClearSelection();
                    ddlStatusBooking.ClearSelection();
                    ddlmonth.ClearSelection();
                    Update_PanelGraph.Visible = false;

                    //Clear Panel Graph


                    switch (ddlTypeReport.SelectedValue)
                    {
                        case "1": //ตาราง
                            Update_PnTableReportDetail.Visible = true;
                            getOrganizationList(ddlorgidx);
                            getPlace(ddlPlaceReportSearchDetail, 0);
                            //setActiveTab(string activeTab, int uidx, int u1idx, int staidx, int _chk_tab)
                            //setActiveTab("docReport", 0, 0, 0, int.Parse(ddlTypeReport.SelectedValue));

                            break;
                        case "2": //กราฟ

                            Update_PnGraphReportDetail.Visible = true;

                            getddlYear(ddlyear);
                            //getddlDay(ddlDay);

                            //Update_PnTableReportDetail.Visible = false;
                            //setActiveTab("docReport", 0, 0, 0, int.Parse(ddlTypeReport.SelectedValue));
                            break;

                    }

                    break;
                case "ddlSearchDateReport":
                    if (int.Parse(ddlSearchDateReport.SelectedValue) == 3) //between
                    {
                        txtAddEndDateReport.Enabled = true;
                    }

                    break;

                case "ddlorgidx":
                    getDepartmentList(ddlrdeptidx, int.Parse(ddlorgidx.SelectedValue));
                    break;
                case "ddlPlaceReportSearchDetail":

                    getRoom(ddlRoomReportSearchDetail, int.Parse(ddlPlaceReportSearchDetail.SelectedValue));

                    break;

                case "ddlTypeGraph":

                    if (ddlTypeGraph.SelectedValue == "1") //Count booking/Dept
                    {
                        status_report.Visible = true;
                        ddlStatusBooking.Visible = true;

                        ddlStatusBooking.ClearSelection();

                        // Panel Graph 
                        Update_PanelGraph.Visible = false;

                        ////if(ViewState["Vs_ReportCountBookingDept"] != null)
                        ////{
                        ////    getCountBookingReportGraph();
                        ////}

                    }
                    else //Count Cancel/Dept
                    {
                        // Panel Graph 
                        Update_PanelGraph.Visible = false;

                        status_report.Visible = false;
                        ddlStatusBooking.Visible = false;
                        //getCountCancelBookingReportGraph();
                    }
                    setOntop.Focus();
                    break;

                case "ddlPlaceSearchApprove":

                    DropDownList ddlPlaceSearchApprove = (DropDownList)FvSearch_Approve.FindControl("ddlPlaceSearchApprove");
                    DropDownList ddlRoomSearchApprove = (DropDownList)FvSearch_Approve.FindControl("ddlRoomSearchApprove");
                    getRoom(ddlRoomSearchApprove, int.Parse(ddlPlaceSearchApprove.SelectedValue));

                    break;

                case "ddlSearchDateApprove":

                    if (int.Parse(ddlSearchDateApprove.SelectedValue) == 3)
                    {
                        AddEndDateApprove.Enabled = true;
                    }
                    else
                    {
                        AddEndDateApprove.Enabled = false;
                        AddEndDateApprove.Text = string.Empty;
                    }


                    break;


            }
        }
        else if (sender is RadioButton)
        {
            RadioButton rdoName = (RadioButton)sender;

            switch (rdoName.ID)
            {
                case "rdoconference":

                    foreach (GridViewRow gvrSelectedRow in GvConferenceInRoom.Rows)
                    {
                        ((RadioButton)gvrSelectedRow.FindControl("rdoconference")).Checked = false;
                        HyperLink btnViewFileConference = (HyperLink)gvrSelectedRow.FindControl("btnViewFileConference");
                        TextBox txt_AlertDetailPicture_Con = (TextBox)gvrSelectedRow.FindControl("txt_AlertDetailPicture_Con");
                        Label lbl_m0_conidx = (Label)gvrSelectedRow.FindControl("lbl_m0_conidx");

                        GetPathFile(lbl_m0_conidx.Text, btnViewFileConference, txt_AlertDetailPicture);

                    }
                    RadioButton rbButton = (RadioButton)sender;
                    GridViewRow gvRow = (GridViewRow)rbButton.NamingContainer;
                    ((RadioButton)gvRow.FindControl("rdoconference")).Checked = true;

                    break;

                case "rdoconference_edit":
                    GridView GvConferenceInRoom_DetailEdit = (GridView)fvDetailRoomBooking.FindControl("GvConferenceInRoom_DetailEdit");

                    foreach (GridViewRow gvrSelectedRow_edit in GvConferenceInRoom_DetailEdit.Rows)
                    {
                        ((RadioButton)gvrSelectedRow_edit.FindControl("rdoconference_edit")).Checked = false;
                        HyperLink btnViewFileConference_edit = (HyperLink)gvrSelectedRow_edit.FindControl("btnViewFileConference_edit");
                        TextBox txt_AlertDetailPicture_Conedit = (TextBox)gvrSelectedRow_edit.FindControl("txt_AlertDetailPicture_Conedit");
                        Label lbl_m0_conidx = (Label)gvrSelectedRow_edit.FindControl("lbl_m0_conidx");

                        GetPathFile(lbl_m0_conidx.Text, btnViewFileConference_edit, txt_AlertDetailPicture);

                    }
                    RadioButton rbButton_edit = (RadioButton)sender;
                    GridViewRow gvRow_edit = (GridViewRow)rbButton_edit.NamingContainer;
                    ((RadioButton)gvRow_edit.FindControl("rdoconference_edit")).Checked = true;

                    break;

                case "rdoconference_appedit":
                    GridView GvConferenceInRoom_DetailApproveEdit = (GridView)FvWaitApproveDetail.FindControl("GvConferenceInRoom_DetailApproveEdit");

                    foreach (GridViewRow gvrSelectedRow_editapp in GvConferenceInRoom_DetailApproveEdit.Rows)
                    {
                        ((RadioButton)gvrSelectedRow_editapp.FindControl("rdoconference_appedit")).Checked = false;
                        HyperLink btnViewFileConference_Appedit = (HyperLink)gvrSelectedRow_editapp.FindControl("btnViewFileConference_Appedit");
                        TextBox txt_AlertDetailPicture_ConAppEdit = (TextBox)gvrSelectedRow_editapp.FindControl("txt_AlertDetailPicture_ConAppEdit");
                        Label lbl_m0_conidx = (Label)gvrSelectedRow_editapp.FindControl("lbl_m0_conidx");

                        GetPathFile(lbl_m0_conidx.Text, btnViewFileConference_Appedit, txt_AlertDetailPicture_ConAppEdit);

                    }
                    RadioButton rbButton_editapp = (RadioButton)sender;
                    GridViewRow gvRow_editapp = (GridViewRow)rbButton_editapp.NamingContainer;
                    ((RadioButton)gvRow_editapp.FindControl("rdoconference_appedit")).Checked = true;

                    break;
            }
        }
    }

    #endregion selected Changed


    #region PathFile
    protected void GetPathFile(string m0_conidx, HyperLink hyperlinkname, TextBox Textboxname)
    {
        string getPathConference = ConfigurationSettings.AppSettings["path_room_conference"];
        string directoryName_ViewConference = m0_conidx;


        if (Directory.Exists(Server.MapPath(getPathConference + directoryName_ViewConference)))
        {
            string[] filesPath = Directory.GetFiles(Server.MapPath(getPathConference + directoryName_ViewConference));
            List<ListItem> files = new List<ListItem>();
            foreach (string path in filesPath)
            {
                string getfiles = "";
                getfiles = Path.GetFileName(path);
                hyperlinkname.NavigateUrl = getPathConference + directoryName_ViewConference + "/" + getfiles;

                HtmlImage imgecreate = new HtmlImage();
                imgecreate.Src = getPathConference + directoryName_ViewConference + "/" + getfiles;
                imgecreate.Height = 150;
                hyperlinkname.Controls.Add(imgecreate);


            }
            hyperlinkname.Visible = true;
            Textboxname.Visible = false;
        }
        else
        {
            hyperlinkname.Visible = false;
            Textboxname.Visible = true;
        }
    }
    #endregion

    #region gridview
    protected void gvPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView gvName = (GridView)sender;
        gvName.PageIndex = e.NewPageIndex;

        switch (gvName.ID)
        {

            case "gvRoomCreate":
                //setGridData(gvRoomCreate, ViewState["EmpSearchList_report"]);
                setOntop.Focus();
                break;

            case "GvDetailRoomBooking":

                if (ViewState["vs_DetailRoomBooking"].ToString() != "")
                {
                    setGridData(GvDetailRoomBooking, ViewState["vs_DetailRoomBooking"]);
                }
                setOntop.Focus();
                //else if(ViewState["vs_SearchDetailRoomBooking"].ToString() != "")
                //{
                //    setGridData(GvDetailRoomBooking, ViewState["vs_SearchDetailRoomBooking"]);
                //}

                setOntop.Focus();
                break;
            case "GvWaitApprove":

                if (ViewState["vs_WaitApproveRoomBooking"].ToString() != "")
                {
                    setGridData(GvWaitApprove, ViewState["vs_WaitApproveRoomBooking"]);
                }
                //else if(ViewState["vs_SearchDetailRoomBooking"].ToString() != "")
                //{
                //    setGridData(GvDetailRoomBooking, ViewState["vs_SearchDetailRoomBooking"]);
                //}

                setOntop.Focus();
                break;
            case "GvViewDetailReport":

                setGridData(GvViewDetailReport, ViewState["Vs_ViewDetailReportRoomTable"]);
                setOntop.Focus();
                break;
        }
    }

    protected void gvRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvDetailRoomBooking":

                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    Label lbl_type_booking_idx_detail = (Label)e.Row.FindControl("lbl_type_booking_idx_detail");
                    Label type_booking_name_detail_on = (Label)e.Row.FindControl("type_booking_name_detail_on");
                    Label type_booking_name_detail_off = (Label)e.Row.FindControl("type_booking_name_detail_off");
                    Label lbl_staidx_detail = (Label)e.Row.FindControl("lbl_staidx_detail");
                    //LinkButton btnCancelRoomDetail = (LinkButton)e.Row.FindControl("btnCancelRoomDetail");
                    //LinkButton btnChangeRoomDetail = (LinkButton)e.Row.FindControl("btnChangeRoomDetail");

                    Label lbl_date_start_detail = (Label)e.Row.FindControl("lbl_date_start_detail");
                    Label lbl_time_start_detail = (Label)e.Row.FindControl("lbl_time_start_detail");
                    Label lbl_checkDateTime = (Label)e.Row.FindControl("lbl_checkDateTime");

                    Label lbl_cemp_idx_detail = (Label)e.Row.FindControl("lbl_cemp_idx_detail");
                    LinkButton btnAddGoogleCalendar = (LinkButton)e.Row.FindControl("btnAddGoogleCalendar");
                    UpdatePanel PnAddGoogleCalendar = (UpdatePanel)e.Row.FindControl("PnAddGoogleCalendar");
                    UpdatePanel PnViewGoogleCalendar = (UpdatePanel)e.Row.FindControl("PnViewGoogleCalendar");

                    PnAddGoogleCalendar.Visible = false;
                    PnViewGoogleCalendar.Visible = false;

                    ViewState["vs_TypeBooking"] = lbl_type_booking_idx_detail.Text;


                    if (ViewState["vs_TypeBooking"].ToString() == "0")
                    {
                        type_booking_name_detail_on.Visible = true;
                    }
                    else if (ViewState["vs_TypeBooking"].ToString() == "1")
                    {
                        type_booking_name_detail_off.Visible = true;
                    }

                    string datetime_startcheck = lbl_date_start_detail.Text + " " + lbl_time_start_detail.Text;

                    //check visible add or view calendar
                    IFormatProvider culture = new CultureInfo("en-US", true);
                    DateTime DateToday_Set = DateTime.ParseExact(DateTime.Now.ToString("dd/MM/yyyy HH:mm"), "dd/MM/yyyy HH:mm", culture); //Date Time Today Incheck
                    DateTime DateTime_DBSet = DateTime.ParseExact(datetime_startcheck.ToString(), "dd/MM/yyyy HH:mm", culture);

                    var result_timeAddCalendar = DateTime_DBSet.Subtract(DateToday_Set).TotalMinutes; // result time chrck > 30 min because cancel in room
                    //litDebug.Text += result_timeAddCalendar.ToString() + "|";

                    if (_emp_idx == int.Parse(lbl_cemp_idx_detail.Text) && lbl_staidx_detail.Text == "3")
                    {
                        if (result_timeAddCalendar > 360)//can add google calendar 6 hours
                        {
                            PnAddGoogleCalendar.Visible = true;

                        }
                        else
                        {
                            PnViewGoogleCalendar.Visible = true;
                        }

                    }
                    else if (lbl_staidx_detail.Text == "3")
                    {

                        PnViewGoogleCalendar.Visible = true;
                    }

                    ////IFormatProvider culture = new CultureInfo("en-US", true);
                    ////DateTime DateToday_Set = DateTime.ParseExact(DateTime.Now.ToString("dd/MM/yyyy HH:mm"), "dd/MM/yyyy HH:mm", culture);

                    //////string timeString = "11/12/2009 13:30:00";

                    ////DateTime dateVal = DateTime.ParseExact(lbl_checkDateTime.Text, "dd/MM/yyyy HH:mm", culture);

                    ////if ((lbl_staidx_detail.Text == "5") || DateToday_Set > dateVal) // ไม่อนุมัติ
                    ////{
                    ////    btnCancelRoomDetail.Visible = false;
                    ////    btnChangeRoomDetail.Visible = false;
                    ////}
                    ////else
                    ////{
                    ////    btnCancelRoomDetail.Visible = true;
                    ////    btnChangeRoomDetail.Visible = true;
                    ////}
                    //set buttin edit/ remove

                }

                break;

            case "GvDetailRoomSearch":
                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    //ViewState["data_check_Room"] = data_search_detail.rbk_search_room_list;

                    Label lbl_m0_room_idx_check = (Label)e.Row.FindControl("lbl_m0_room_idx");
                    Label lbl_room_detail_statusroom = (Label)e.Row.FindControl("lbl_room_detail_statusroom");


                    var btnViewFileRoom_ = (HyperLink)e.Row.FindControl("btnViewFileRoom");
                    //var btnViewFileRoom_ = (Image)e.Row.FindControl("btnViewFileRoom");

                    string filePath_View = ConfigurationManager.AppSettings["path_flie_roombooking"];
                    string directoryName = lbl_m0_room_idx_check.Text;//lbl_place_name_file.Text.Trim() + lbl_room_name_en_view_file.Text + lbl_m0_room_idx_file.Text;


                    //litDebug.Text = directoryName.ToString();
                    if (Directory.Exists(Server.MapPath(filePath_View + directoryName)))
                    {
                        string[] filesPath = Directory.GetFiles(Server.MapPath(filePath_View + directoryName));
                        List<ListItem> files = new List<ListItem>();
                        foreach (string path in filesPath)
                        {
                            string getfiles = "";
                            getfiles = Path.GetFileName(path);
                            btnViewFileRoom_.NavigateUrl = filePath_View + directoryName + "/" + getfiles;
                            //btnViewFileRoom_.ImageUrl = filePath_View + directoryName + "/" + getfiles;

                            HtmlImage img = new HtmlImage();
                            img.Src = filePath_View + directoryName + "/" + getfiles;
                            img.Height = 100;
                            btnViewFileRoom_.Controls.Add(img);



                        }
                        btnViewFileRoom_.Visible = true;
                    }
                    else
                    {
                        btnViewFileRoom_.Visible = false;
                        //btnViewFileRoom_.Text = "-";
                    }


                    if (ViewState["data_check_Room"].ToString() != "")
                    {

                        //litDebug1.Text = "2";
                        //not busy
                        rbk_search_room_detail[] _templist_IdSetTest_ = (rbk_search_room_detail[])ViewState["data_check_Room"];

                        var linqRoomDetailCheck_ = (from _dt_RoomDetailCheck in _templist_IdSetTest_

                                                    select _dt_RoomDetailCheck).ToList();

                        string SetTestId = "";
                        string listSetTestId = "";


                        for (int idset = 0; idset < linqRoomDetailCheck_.Count(); idset++)
                        {
                            SetTestId = linqRoomDetailCheck_[idset].m0_room_idx.ToString();
                            listSetTestId += "," + SetTestId;
                        }
                        //Split comma frist posision
                        string resultString = listSetTestId.IndexOf(',') > -1
                        ? listSetTestId.Substring(listSetTestId.IndexOf(',') + 1)
                        : listSetTestId;

                        //litDebug1.Text = resultString.ToString();
                        string[] setTestChkOnly = resultString.Split(',');

                        for (int i = 0; i < setTestChkOnly.Length; i++)
                        {
                            if (setTestChkOnly[i] == lbl_m0_room_idx_check.Text)
                            {
                                //return i;
                                ////lbl_room_detail_notbusy.Visible = true;
                                ////lbl_room_detail_busy.Visible = false;
                                //ViewState["vs_StatusRoomBooking"] = "ไม่ว่าง";
                                lbl_room_detail_statusroom.Text = "ไม่ว่าง";
                                //litDebug1.Text = "555";
                                break;
                            }
                            else
                            {
                                ////lbl_room_detail_busy.Visible = true;
                                ////lbl_room_detail_notbusy.Visible = false;
                                //ViewState["vs_StatusRoomBooking"] = "ว่าง";
                                lbl_room_detail_statusroom.Text = "ว่าง";
                                //litDebug1.Text = "444";
                            }


                        }


                    }
                    else
                    {
                        //litDebug1.Text = "1111";
                        lbl_room_detail_statusroom.Text = "ว่าง";
                        //lbl_room_detail_busy.Visible = true;
                        //ViewState["vs_StatusRoomBooking"] = "ว่าง";
                    }

                }
                break;

            case "GvWaitApprove":

                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    Label lbl_type_booking_idx_approve = (Label)e.Row.FindControl("lbl_type_booking_idx_approve");
                    Label type_booking_name_detail_on_approve = (Label)e.Row.FindControl("type_booking_name_detail_on_approve");
                    Label type_booking_name_detail_off_approve = (Label)e.Row.FindControl("type_booking_name_detail_off_approve");

                    ViewState["vs_WaitApproveTypeBooking"] = lbl_type_booking_idx_approve.Text;


                    if (ViewState["vs_WaitApproveTypeBooking"].ToString() == "0")
                    {
                        type_booking_name_detail_on_approve.Visible = true;
                    }
                    else if (ViewState["vs_WaitApproveTypeBooking"].ToString() == "1")
                    {
                        type_booking_name_detail_off_approve.Visible = true;
                    }
                }

                break;

            case "GvDetailRoomSearch_Edit":
                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    //ViewState["data_check_Room"] = data_search_detail.rbk_search_room_list;

                    Label lbl_m0_room_idx_edit_check = (Label)e.Row.FindControl("lbl_m0_room_idx_edit");
                    Label lbl_room_detail_status_edit = (Label)e.Row.FindControl("lbl_room_detail_status_edit");
                    //Label lbl_room_detail_notbusy_edit = (Label)e.Row.FindControl("lbl_room_detail_notbusy_edit");
                    //Label lbl_room_detail_busy_edit = (Label)e.Row.FindControl("lbl_room_detail_busy_edit");

                    var btnViewFileRoomEdit_ = (HyperLink)e.Row.FindControl("btnViewFileRoomEdit");
                    //var btnViewFileRoom_ = (Image)e.Row.FindControl("btnViewFileRoom");

                    string filePath_View_edit = ConfigurationManager.AppSettings["path_flie_roombooking"];
                    string directoryName_edit = lbl_m0_room_idx_edit_check.Text;//lbl_place_name_file.Text.Trim() + lbl_room_name_en_view_file.Text + lbl_m0_room_idx_file.Text;


                    //litDebug.Text = directoryName.ToString();
                    if (Directory.Exists(Server.MapPath(filePath_View_edit + directoryName_edit)))
                    {
                        string[] filesPath = Directory.GetFiles(Server.MapPath(filePath_View_edit + directoryName_edit));
                        List<ListItem> files = new List<ListItem>();
                        foreach (string path in filesPath)
                        {
                            string getfiles = "";
                            getfiles = Path.GetFileName(path);
                            btnViewFileRoomEdit_.NavigateUrl = filePath_View_edit + directoryName_edit + "/" + getfiles;
                            //btnViewFileRoom_.ImageUrl = filePath_View + directoryName + "/" + getfiles;

                            HtmlImage img = new HtmlImage();
                            img.Src = filePath_View_edit + directoryName_edit + "/" + getfiles;
                            img.Height = 100;
                            btnViewFileRoomEdit_.Controls.Add(img);

                        }
                        btnViewFileRoomEdit_.Visible = true;
                    }
                    else
                    {
                        btnViewFileRoomEdit_.Visible = false;
                        //btnViewFileRoom_.Text = "-";
                    }

                    UpdatePanel Update_DetailRoomSearch_Edit = (UpdatePanel)fvDetailRoomBooking.FindControl("Update_DetailRoomSearch_Edit");
                    //litDebug1.Text = ViewState["data_check_Room"].ToString();

                    if (ViewState["data_check_Room"].ToString() != "")
                    {
                        //litDebug.Text = "8"; 
                        rbk_search_room_detail[] _templist_IdSetTest_ = (rbk_search_room_detail[])ViewState["data_check_Room"];

                        var linqRoomDetailCheck_ = (from _dt_RoomDetailCheck in _templist_IdSetTest_

                                                    select _dt_RoomDetailCheck).ToList(); ;
                        for (int idset = 0; idset < linqRoomDetailCheck_.Count(); idset++)
                        {
                            ////SetTestId = linqRoomDetailCheck[idset].m0_room_idx.ToString();
                            ////listSetTestId += "," + SetTestId;

                            if (lbl_m0_room_idx_edit_check.Text == linqRoomDetailCheck_[idset].m0_room_idx.ToString()) // not
                            {
                                //lbl_room_detail_notbusy_edit.Visible = true;
                                //lbl_room_detail_busy_edit.Visible = false;
                                lbl_room_detail_status_edit.Text = "ไม่ว่าง";
                                //ViewState["vs_StatusRoomBooking_edit"] = "ไม่ว่าง";

                                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ห้องไม่ว่าง');", true);
                                Update_DetailRoomSearch_Edit.Visible = false;


                            }
                            else
                            {
                                //lbl_room_detail_busy_edit.Visible = true;
                                //lbl_room_detail_notbusy_edit.Visible = false;
                                //ViewState["vs_StatusRoomBooking_edit"] = "ว่าง";
                                lbl_room_detail_status_edit.Text = "ว่าง";
                                Update_DetailRoomSearch_Edit.Visible = true;
                            }

                        }
                    }
                    else
                    {
                        lbl_room_detail_status_edit.Text = "ว่าง";
                        //litDebug1.Text = "9";
                        //lbl_room_detail_busy_edit.Visible = true;
                        //ViewState["vs_StatusRoomBooking_edit"] = "ว่าง";
                    }

                }
                break;
            case "GvDetailRoomSearch_EditApprove":
                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    //ViewState["data_check_Room"] = data_search_detail.rbk_search_room_list;

                    Label lbl_m0_room_idx_edit_approve = (Label)e.Row.FindControl("lbl_m0_room_idx_edit_approve");
                    Label lbl_room_detail_status_edit_approve = (Label)e.Row.FindControl("lbl_room_detail_status_edit_approve");
                    //Label lbl_room_detail_notbusy_edit = (Label)e.Row.FindControl("lbl_room_detail_notbusy_edit");
                    //Label lbl_room_detail_busy_edit = (Label)e.Row.FindControl("lbl_room_detail_busy_edit");

                    var btnViewFileRoomEditApprove = (HyperLink)e.Row.FindControl("btnViewFileRoomEditApprove");
                    //var btnViewFileRoom_ = (Image)e.Row.FindControl("btnViewFileRoom");

                    string filePath_View_edit = ConfigurationManager.AppSettings["path_flie_roombooking"];
                    string directoryName_edit = lbl_m0_room_idx_edit_approve.Text;//lbl_place_name_file.Text.Trim() + lbl_room_name_en_view_file.Text + lbl_m0_room_idx_file.Text;


                    //litDebug.Text = directoryName.ToString();
                    if (Directory.Exists(Server.MapPath(filePath_View_edit + directoryName_edit)))
                    {
                        string[] filesPath = Directory.GetFiles(Server.MapPath(filePath_View_edit + directoryName_edit));
                        List<ListItem> files = new List<ListItem>();
                        foreach (string path in filesPath)
                        {
                            string getfiles = "";
                            getfiles = Path.GetFileName(path);
                            btnViewFileRoomEditApprove.NavigateUrl = filePath_View_edit + directoryName_edit + "/" + getfiles;
                            //btnViewFileRoom_.ImageUrl = filePath_View + directoryName + "/" + getfiles;

                            HtmlImage img = new HtmlImage();
                            img.Src = filePath_View_edit + directoryName_edit + "/" + getfiles;
                            img.Height = 100;
                            btnViewFileRoomEditApprove.Controls.Add(img);

                        }
                        btnViewFileRoomEditApprove.Visible = true;
                    }
                    else
                    {
                        btnViewFileRoomEditApprove.Visible = false;
                        //btnViewFileRoom_.Text = "-";
                    }

                    UpdatePanel Update_DetailRoomSearch_EditApprove = (UpdatePanel)FvWaitApproveDetail.FindControl("Update_DetailRoomSearch_EditApprove");
                    //litDebug1.Text = ViewState["data_check_Room"].ToString();

                    if (ViewState["data_check_RoomApprove"].ToString() != "")
                    {
                        //litDebug1.Text = "8";
                        rbk_search_room_detail[] _templist_IdSetTest_ = (rbk_search_room_detail[])ViewState["data_check_RoomApprove"];

                        var linqRoomDetailCheck_ = (from _dt_RoomDetailCheck in _templist_IdSetTest_

                                                    select _dt_RoomDetailCheck).ToList(); ;
                        for (int idset = 0; idset < linqRoomDetailCheck_.Count(); idset++)
                        {
                            ////SetTestId = linqRoomDetailCheck[idset].m0_room_idx.ToString();
                            ////listSetTestId += "," + SetTestId;

                            if (lbl_m0_room_idx_edit_approve.Text == linqRoomDetailCheck_[idset].m0_room_idx.ToString()) // not
                            {
                                //lbl_room_detail_notbusy_edit.Visible = true;
                                //lbl_room_detail_busy_edit.Visible = false;
                                lbl_room_detail_status_edit_approve.Text = "ไม่ว่าง";
                                //ViewState["vs_StatusRoomBooking_edit"] = "ไม่ว่าง";

                                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ห้องไม่ว่าง');", true);
                                Update_DetailRoomSearch_EditApprove.Visible = false;


                            }
                            else
                            {
                                //lbl_room_detail_busy_edit.Visible = true;
                                //lbl_room_detail_notbusy_edit.Visible = false;
                                //ViewState["vs_StatusRoomBooking_edit"] = "ว่าง";
                                lbl_room_detail_status_edit_approve.Text = "ว่าง";
                                Update_DetailRoomSearch_EditApprove.Visible = true;
                            }

                        }
                    }
                    else
                    {
                        lbl_room_detail_status_edit_approve.Text = "ว่าง";
                        //litDebug1.Text = "9";
                        //lbl_room_detail_busy_edit.Visible = true;
                        //ViewState["vs_StatusRoomBooking_edit"] = "ว่าง";
                    }

                }
                break;

            case "GvConferenceInRoom":

                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    HyperLink btnViewFileConference = (HyperLink)e.Row.FindControl("btnViewFileConference");
                    TextBox txt_AlertDetailPicture_Con = (TextBox)e.Row.FindControl("txt_AlertDetailPicture_Con");
                    Label lbl_m0_conidx = (Label)e.Row.FindControl("lbl_m0_conidx");
                    Label lbl_pic_check = (Label)e.Row.FindControl("lbl_pic_check");
                    Label lbl_sound_check = (Label)e.Row.FindControl("lbl_sound_check");
                    CheckBox chkpic_show = (CheckBox)e.Row.FindControl("chkpic_show");
                    CheckBox chksound_show = (CheckBox)e.Row.FindControl("chksound_show");

                    if (lbl_pic_check.Text == "1")
                    {
                        chkpic_show.Checked = true;
                    }
                    else
                    {
                        chkpic_show.Checked = false;
                    }

                    if (lbl_sound_check.Text == "1")
                    {
                        chksound_show.Checked = true;
                    }
                    else
                    {
                        chksound_show.Checked = false;
                    }

                    GetPathFile(lbl_m0_conidx.Text, btnViewFileConference, txt_AlertDetailPicture);


                    //string getPathConference = ConfigurationSettings.AppSettings["path_room_conference"];
                    //string directoryName_ViewConference = lbl_m0_conidx.Text;


                    //if (Directory.Exists(Server.MapPath(getPathConference + directoryName_ViewConference)))
                    //{
                    //    string[] filesPath = Directory.GetFiles(Server.MapPath(getPathConference + directoryName_ViewConference));
                    //    List<ListItem> files = new List<ListItem>();
                    //    foreach (string path in filesPath)
                    //    {
                    //        string getfiles = "";
                    //        getfiles = Path.GetFileName(path);
                    //        btnViewFileConference.NavigateUrl = getPathConference + directoryName_ViewConference + "/" + getfiles;

                    //        HtmlImage imgecreate = new HtmlImage();
                    //        imgecreate.Src = getPathConference + directoryName_ViewConference + "/" + getfiles;
                    //        imgecreate.Height = 150;
                    //        btnViewFileConference.Controls.Add(imgecreate);


                    //    }
                    //    btnViewFileConference.Visible = true;
                    //    txt_AlertDetailPicture_Con.Visible = false;
                    //}
                    //else
                    //{
                    //    btnViewFileConference.Visible = false;
                    //    txt_AlertDetailPicture_Con.Visible = true;
                    //}

                }

                break;

            case "GvConferenceInRoom_Detail":
                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    HyperLink btnViewFileConference_Detail = (HyperLink)e.Row.FindControl("btnViewFileConference_Detail");
                    TextBox txt_AlertDetailPicture_ConDetail = (TextBox)e.Row.FindControl("txt_AlertDetailPicture_ConDetail");
                    Label lbl_m0_conidx = (Label)e.Row.FindControl("lbl_m0_conidx");
                    Label lbl_pic_check = (Label)e.Row.FindControl("lbl_pic_check");
                    Label lbl_sound_check = (Label)e.Row.FindControl("lbl_sound_check");
                    CheckBox chkpic_show = (CheckBox)e.Row.FindControl("chkpic_show");
                    CheckBox chksound_show = (CheckBox)e.Row.FindControl("chksound_show");
                    TextBox txtm0_conidx = (TextBox)fvDetailRoomBooking.FindControl("txtm0_conidx");
                    RadioButton rdoconference_detail = (RadioButton)e.Row.FindControl("rdoconference_detail");

                    if (lbl_pic_check.Text == "1")
                    {
                        chkpic_show.Checked = true;
                    }
                    else
                    {
                        chkpic_show.Checked = false;
                    }

                    if (lbl_sound_check.Text == "1")
                    {
                        chksound_show.Checked = true;
                    }
                    else
                    {
                        chksound_show.Checked = false;
                    }

                    GetPathFile(lbl_m0_conidx.Text, btnViewFileConference_Detail, txt_AlertDetailPicture_ConDetail);

                    if (lbl_m0_conidx.Text == txtm0_conidx.Text)
                    {
                        rdoconference_detail.Checked = true;
                    }
                    else
                    {
                        rdoconference_detail.Checked = false;

                    }
                }

                break;

            case "GvConferenceInRoom_ApproveDetail":
                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    HyperLink btnViewFileConference_Approve = (HyperLink)e.Row.FindControl("btnViewFileConference_Approve");
                    TextBox txt_AlertDetailPicture_ConApprove = (TextBox)e.Row.FindControl("txt_AlertDetailPicture_ConApprove");
                    Label lbl_m0_conidx = (Label)e.Row.FindControl("lbl_m0_conidx");
                    Label lbl_pic_check = (Label)e.Row.FindControl("lbl_pic_check");
                    Label lbl_sound_check = (Label)e.Row.FindControl("lbl_sound_check");
                    CheckBox chkpic_show = (CheckBox)e.Row.FindControl("chkpic_show");
                    CheckBox chksound_show = (CheckBox)e.Row.FindControl("chksound_show");
                    TextBox txtm0_conidx = (TextBox)fvDetailRoomBooking.FindControl("txtm0_conidx");
                    RadioButton rdoconference_detailapprove = (RadioButton)e.Row.FindControl("rdoconference_detailapprove");
                    TextBox txtm0_conidx_approve = (TextBox)FvWaitApproveDetail.FindControl("txtm0_conidx_approve");

                    if (lbl_pic_check.Text == "1")
                    {
                        chkpic_show.Checked = true;
                    }
                    else
                    {
                        chkpic_show.Checked = false;
                    }

                    if (lbl_sound_check.Text == "1")
                    {
                        chksound_show.Checked = true;
                    }
                    else
                    {
                        chksound_show.Checked = false;
                    }

                    GetPathFile(lbl_m0_conidx.Text, btnViewFileConference_Approve, txt_AlertDetailPicture_ConApprove);

                    if (lbl_m0_conidx.Text == txtm0_conidx_approve.Text)
                    {
                        rdoconference_detailapprove.Checked = true;
                    }
                    else
                    {
                        rdoconference_detailapprove.Checked = false;

                    }
                }

                break;

            case "GvConferenceInRoom_DetailEdit":
                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    HyperLink btnViewFileConference_edit = (HyperLink)e.Row.FindControl("btnViewFileConference_edit");
                    TextBox txt_AlertDetailPicture_Conedit = (TextBox)e.Row.FindControl("txt_AlertDetailPicture_Conedit");
                    Label lbl_m0_conidx = (Label)e.Row.FindControl("lbl_m0_conidx");
                    Label lbl_pic_check = (Label)e.Row.FindControl("lbl_pic_check");
                    Label lbl_sound_check = (Label)e.Row.FindControl("lbl_sound_check");
                    CheckBox chkpic_show = (CheckBox)e.Row.FindControl("chkpic_show");
                    CheckBox chksound_show = (CheckBox)e.Row.FindControl("chksound_show");
                    TextBox txtm0_conidx = (TextBox)fvDetailRoomBooking.FindControl("txtm0_conidx");
                    RadioButton rdoconference_edit = (RadioButton)e.Row.FindControl("rdoconference_edit");
                    TextBox txtm0_conidx_edit = (TextBox)fvDetailRoomBooking.FindControl("txtm0_conidx_edit");

                    if (lbl_pic_check.Text == "1")
                    {
                        chkpic_show.Checked = true;
                    }
                    else
                    {
                        chkpic_show.Checked = false;
                    }

                    if (lbl_sound_check.Text == "1")
                    {
                        chksound_show.Checked = true;
                    }
                    else
                    {
                        chksound_show.Checked = false;
                    }

                    GetPathFile(lbl_m0_conidx.Text, btnViewFileConference_edit, txt_AlertDetailPicture_Conedit);

                    if (lbl_m0_conidx.Text == txtm0_conidx_edit.Text)
                    {
                        rdoconference_edit.Checked = true;
                    }
                    else
                    {
                        rdoconference_edit.Checked = false;

                    }
                }

                break;

            case "GvConferenceInRoom_DetailApproveEdit":
                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    HyperLink btnViewFileConference_Appedit = (HyperLink)e.Row.FindControl("btnViewFileConference_Appedit");
                    TextBox txt_AlertDetailPicture_ConAppEdit = (TextBox)e.Row.FindControl("txt_AlertDetailPicture_ConAppEdit");
                    Label lbl_m0_conidx = (Label)e.Row.FindControl("lbl_m0_conidx");
                    Label lbl_pic_check = (Label)e.Row.FindControl("lbl_pic_check");
                    Label lbl_sound_check = (Label)e.Row.FindControl("lbl_sound_check");
                    CheckBox chkpic_show = (CheckBox)e.Row.FindControl("chkpic_show");
                    CheckBox chksound_show = (CheckBox)e.Row.FindControl("chksound_show");
                    TextBox txtm0_conidx_appedit = (TextBox)FvWaitApproveDetail.FindControl("txtm0_conidx_appedit");
                    RadioButton rdoconference_appedit = (RadioButton)e.Row.FindControl("rdoconference_appedit");
                    TextBox txtm0_conidx_edit = (TextBox)fvDetailRoomBooking.FindControl("txtm0_conidx_edit");

                    if (lbl_pic_check.Text == "1")
                    {
                        chkpic_show.Checked = true;
                    }
                    else
                    {
                        chkpic_show.Checked = false;
                    }

                    if (lbl_sound_check.Text == "1")
                    {
                        chksound_show.Checked = true;
                    }
                    else
                    {
                        chksound_show.Checked = false;
                    }

                    GetPathFile(lbl_m0_conidx.Text, btnViewFileConference_Appedit, txt_AlertDetailPicture_ConAppEdit);

                    if (lbl_m0_conidx.Text == txtm0_conidx_appedit.Text)
                    {
                        rdoconference_appedit.Checked = true;
                    }
                    else
                    {
                        rdoconference_appedit.Checked = false;

                    }
                }

                break;
        }
    }

    #endregion gridview

    #region Formview Databind
    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        if (sender is FormView)
        {
            FormView FvName = (FormView)sender;

            switch (FvName.ID)
            {
                case "FvWaitApproveDetail":
                    if (FvName.CurrentMode == FormViewMode.ReadOnly)
                    {
                        TextBox tb_u0_document_idx_approve = (TextBox)FvName.FindControl("tb_u0_document_idx_approve");
                        TextBox tb_m0_room_idx_approve = (TextBox)FvName.FindControl("tb_m0_room_idx_approve");
                        TextBox txt_AlertDetailPictureApprove = (TextBox)FvName.FindControl("txt_AlertDetailPictureApprove");
                        var btnViewFileRoomDetailApprove = (HyperLink)FvName.FindControl("btnViewFileRoomDetailApprove");
                        CheckBoxList chkFoodDetail_approve = (CheckBoxList)FvName.FindControl("chkFoodDetail_approve");
                        GridView GvConferenceInRoom_ApproveDetail = (GridView)FvName.FindControl("GvConferenceInRoom_ApproveDetail");
                        TextBox tb_status_booking = (TextBox)FvName.FindControl("tb_status_booking");
                        TextBox tb_place_idx_approve = (TextBox)FvName.FindControl("tb_place_idx_approve");

                        if (tb_status_booking.Text == "ว่าง")
                        {

                            //tb_status_booking.Attributes["style"] = "color:green; font-weight:bold;";
                            //tb_status_booking.Style.Add("background-color", "#32CD32");
                            tb_status_booking.Style.Add("color", "#32CD32");
                            tb_status_booking.Style["font-weight"] = "bold";


                        }
                        else
                        {
                            //tb_status_booking.Attributes["style"] = "color:red; font-weight:bold;";
                            //tb_status_booking.Style.Add("background-color", "#FF6600");
                            tb_status_booking.Style.Add("color", "#FF0000");
                            tb_status_booking.Style["font-weight"] = "bold";

                        }

                        SelectConferenceDetail(GvConferenceInRoom_ApproveDetail);

                        if(tb_place_idx_approve.Text == "8")
                        {
                            GvConferenceInRoom_ApproveDetail.Visible = true;
                        }
                        else
                        {
                            GvConferenceInRoom_ApproveDetail.Visible = false;
                        }

                        string filePath_View_approve = ConfigurationManager.AppSettings["path_flie_roombooking"];
                        string directoryName_View_approve = tb_m0_room_idx_approve.Text;//lbl_place_name_edit.Text.Trim() + lbl_room_name_en_edit.Text + lblm0_room_idx_edit.Text; dddd
                        //litDebug.Text = tb_m0_room_idx_approve.Text;
                        if (Directory.Exists(Server.MapPath(filePath_View_approve + directoryName_View_approve)))
                        {
                            string[] filesPath = Directory.GetFiles(Server.MapPath(filePath_View_approve + directoryName_View_approve));
                            List<ListItem> files = new List<ListItem>();
                            foreach (string path in filesPath)
                            {
                                string getfiles = "";
                                getfiles = Path.GetFileName(path);
                                btnViewFileRoomDetailApprove.NavigateUrl = filePath_View_approve + directoryName_View_approve + "/" + getfiles;

                                HtmlImage imgedit = new HtmlImage();
                                imgedit.Src = filePath_View_approve + directoryName_View_approve + "/" + getfiles;
                                imgedit.Height = 200;
                                btnViewFileRoomDetailApprove.Controls.Add(imgedit);


                            }
                            btnViewFileRoomDetailApprove.Visible = true;
                            txt_AlertDetailPictureApprove.Visible = false;
                        }
                        else
                        {
                            btnViewFileRoomDetailApprove.Visible = false;
                            txt_AlertDetailPictureApprove.Visible = true;
                        }

                        //Check Detail Food In Room Detail
                        //lectFoodDetail(); eqtqt
                        data_roombooking data_m0_food_detail = new data_roombooking();
                        rbk_m0_food_detail m0_food_detail = new rbk_m0_food_detail();
                        data_m0_food_detail.rbk_m0_food_list = new rbk_m0_food_detail[1];

                        m0_food_detail.condition = 1;

                        data_m0_food_detail.rbk_m0_food_list[0] = m0_food_detail;
                        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
                        data_m0_food_detail = callServicePostRoomBooking(_urlGetRbkm0Food, data_m0_food_detail);

                        //ViewState["data_test_sample"] = data_m0_food_detail.rbk_m0_food_list;

                        //CheckBoxList chk_TestDetail = (CheckBoxList)fvActor1Node1.FindControl("chk_TestDetail");
                        //chkFoodDetail_approve.Items.Clear();
                        chkFoodDetail_approve.AppendDataBoundItems = true;
                        chkFoodDetail_approve.DataSource = data_m0_food_detail.rbk_m0_food_list;
                        chkFoodDetail_approve.DataTextField = "food_name";
                        chkFoodDetail_approve.DataValueField = "food_idx";
                        chkFoodDetail_approve.DataBind();


                        // bind value in detail view approve food detail
                        data_roombooking data_m0_food_detail_view = new data_roombooking();
                        rbk_u2_roombooking_detail m0_food_detail_view = new rbk_u2_roombooking_detail();
                        data_m0_food_detail_view.rbk_u2_roombooking_list = new rbk_u2_roombooking_detail[1];

                        m0_food_detail_view.u0_document_idx = int.Parse(tb_u0_document_idx_approve.Text);

                        data_m0_food_detail_view.rbk_u2_roombooking_list[0] = m0_food_detail_view;
                        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
                        data_m0_food_detail_view = callServicePostRoomBooking(_urlGetViewFoodDetailRbk, data_m0_food_detail_view);

                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_food_detail_view));

                        if (data_m0_food_detail_view.return_code == 0)
                        {
                            ViewState["Vs_DetailFoodView_Check"] = data_m0_food_detail_view.rbk_u2_roombooking_list;


                            ViewState["Vs_DetailFoodView_Check"] = data_m0_food_detail_view.rbk_u2_roombooking_list;

                            rbk_u2_roombooking_detail[] _itemDetailFoodRoom = (rbk_u2_roombooking_detail[])ViewState["Vs_DetailFoodView_Check"];

                            string[] _value_chk;
                            List<String> _value = new List<string>();
                            var _linqFoodDetail = (from data in _itemDetailFoodRoom //_itemTestDetail
                                                   select new
                                                   {
                                                       //data.root_detail,
                                                       data.food_idx,

                                                   }).ToList();



                            for (int z = 0; z < _linqFoodDetail.Count(); z++)
                            {
                                _value.Add(_linqFoodDetail[z].food_idx.ToString());

                            }

                            _value_chk = _value.ToArray();
                            //ViewState[""] = 
                            //  litDebug.Text = _value;

                            foreach (ListItem chkListSet in chkFoodDetail_approve.Items)
                            {

                                if (Array.IndexOf(_value_chk, chkListSet.Value) > -1)
                                {
                                    chkListSet.Selected = true;

                                }
                                else
                                {
                                    chkListSet.Selected = false;
                                    //item.Value = "0";

                                }

                            }


                        }

                    }

                    if (FvName.CurrentMode == FormViewMode.Edit)
                    {
                        TextBox tb_u0_document_idx_approve = (TextBox)FvName.FindControl("tb_u0_document_idx_approve");
                        CheckBoxList chkFoodDetail_approve = (CheckBoxList)FvName.FindControl("chkFoodDetail_approve");
                        TextBox tb_m0_room_idx_approve = (TextBox)FvName.FindControl("tb_m0_room_idx_approve");
                        TextBox txt_AlertDetailPictureApprove = (TextBox)FvName.FindControl("txt_AlertDetailPictureApprove");
                        var btnViewFileRoomDetailApprove = (HyperLink)FvName.FindControl("btnViewFileRoomDetailApprove");
                        GridView GvConferenceInRoom_DetailApproveEdit = (GridView)FvName.FindControl("GvConferenceInRoom_DetailApproveEdit");
                        TextBox tb_status_booking_approve = (TextBox)FvName.FindControl("tb_status_booking_approve");
                        TextBox tb_place_idx_approve = (TextBox)FvName.FindControl("tb_place_idx_approve");


                        if (tb_status_booking_approve.Text == "ว่าง")
                        {

                            //tb_status_booking.Attributes["style"] = "color:green; font-weight:bold;";
                            //tb_status_booking.Style.Add("background-color", "#32CD32");
                            tb_status_booking_approve.Style.Add("color", "#32CD32");
                            tb_status_booking_approve.Style["font-weight"] = "bold";


                        }
                        else
                        {
                            //tb_status_booking.Attributes["style"] = "color:red; font-weight:bold;";
                            //tb_status_booking.Style.Add("background-color", "#FF6600");
                            tb_status_booking_approve.Style.Add("color", "#FF0000");
                            tb_status_booking_approve.Style["font-weight"] = "bold";

                        }

                        SelectConferenceDetail(GvConferenceInRoom_DetailApproveEdit);
                        if (tb_place_idx_approve.Text == "8")
                        {
                            GvConferenceInRoom_DetailApproveEdit.Visible = true;
                        }
                        else
                        {
                            GvConferenceInRoom_DetailApproveEdit.Visible = false;
                        }


                        string filePath_View_approve = ConfigurationManager.AppSettings["path_flie_roombooking"];
                        string directoryName_View_approve = tb_m0_room_idx_approve.Text;//lbl_place_name_edit.Text.Trim() + lbl_room_name_en_edit.Text + lblm0_room_idx_edit.Text; dddd
                        //litDebug.Text = tb_m0_room_idx_approve.Text;
                        if (Directory.Exists(Server.MapPath(filePath_View_approve + directoryName_View_approve)))
                        {
                            string[] filesPath = Directory.GetFiles(Server.MapPath(filePath_View_approve + directoryName_View_approve));
                            List<ListItem> files = new List<ListItem>();
                            foreach (string path in filesPath)
                            {
                                string getfiles = "";
                                getfiles = Path.GetFileName(path);
                                btnViewFileRoomDetailApprove.NavigateUrl = filePath_View_approve + directoryName_View_approve + "/" + getfiles;

                                HtmlImage imgedit = new HtmlImage();
                                imgedit.Src = filePath_View_approve + directoryName_View_approve + "/" + getfiles;
                                imgedit.Height = 200;
                                btnViewFileRoomDetailApprove.Controls.Add(imgedit);


                            }
                            btnViewFileRoomDetailApprove.Visible = true;
                            txt_AlertDetailPictureApprove.Visible = false;
                        }
                        else
                        {
                            btnViewFileRoomDetailApprove.Visible = false;
                            txt_AlertDetailPictureApprove.Visible = true;
                        }


                        //Check Detail Food In Room Detail
                        //lectFoodDetail(); eqtqt
                        data_roombooking data_m0_food_detail = new data_roombooking();
                        rbk_m0_food_detail m0_food_detail = new rbk_m0_food_detail();
                        data_m0_food_detail.rbk_m0_food_list = new rbk_m0_food_detail[1];

                        m0_food_detail.condition = 1;

                        data_m0_food_detail.rbk_m0_food_list[0] = m0_food_detail;
                        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
                        data_m0_food_detail = callServicePostRoomBooking(_urlGetRbkm0Food, data_m0_food_detail);

                        //ViewState["data_test_sample"] = data_m0_food_detail.rbk_m0_food_list;

                        //CheckBoxList chk_TestDetail = (CheckBoxList)fvActor1Node1.FindControl("chk_TestDetail");
                        //chkFoodDetail_approve.Items.Clear();
                        chkFoodDetail_approve.AppendDataBoundItems = true;
                        chkFoodDetail_approve.DataSource = data_m0_food_detail.rbk_m0_food_list;
                        chkFoodDetail_approve.DataTextField = "food_name";
                        chkFoodDetail_approve.DataValueField = "food_idx";
                        chkFoodDetail_approve.DataBind();


                        // bind value in detail view approve food detail
                        data_roombooking data_m0_food_detail_view = new data_roombooking();
                        rbk_u2_roombooking_detail m0_food_detail_view = new rbk_u2_roombooking_detail();
                        data_m0_food_detail_view.rbk_u2_roombooking_list = new rbk_u2_roombooking_detail[1];

                        m0_food_detail_view.u0_document_idx = int.Parse(tb_u0_document_idx_approve.Text);

                        data_m0_food_detail_view.rbk_u2_roombooking_list[0] = m0_food_detail_view;
                        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
                        data_m0_food_detail_view = callServicePostRoomBooking(_urlGetViewFoodDetailRbk, data_m0_food_detail_view);

                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_food_detail_view));

                        if (data_m0_food_detail_view.return_code == 0)
                        {
                            ViewState["Vs_DetailFoodView_Check"] = data_m0_food_detail_view.rbk_u2_roombooking_list;


                            ViewState["Vs_DetailFoodView_Check"] = data_m0_food_detail_view.rbk_u2_roombooking_list;

                            rbk_u2_roombooking_detail[] _itemDetailFoodRoom = (rbk_u2_roombooking_detail[])ViewState["Vs_DetailFoodView_Check"];

                            string[] _value_chk;
                            List<String> _value = new List<string>();
                            var _linqFoodDetail = (from data in _itemDetailFoodRoom //_itemTestDetail
                                                   select new
                                                   {
                                                       //data.root_detail,
                                                       data.food_idx,

                                                   }).ToList();



                            for (int z = 0; z < _linqFoodDetail.Count(); z++)
                            {
                                _value.Add(_linqFoodDetail[z].food_idx.ToString());

                            }

                            _value_chk = _value.ToArray();
                            //ViewState[""] = 
                            //  litDebug.Text = _value;

                            foreach (ListItem chkListSet in chkFoodDetail_approve.Items)
                            {

                                if (Array.IndexOf(_value_chk, chkListSet.Value) > -1)
                                {
                                    chkListSet.Selected = true;

                                }
                                else
                                {
                                    chkListSet.Selected = false;
                                    //item.Value = "0";

                                }

                            }


                        }
                        

                    }
                    break;
                case "fvDetailRoomBooking":
                    if (FvName.CurrentMode == FormViewMode.ReadOnly)
                    {

                        TextBox tb_m0_room_idx = (TextBox)FvName.FindControl("tb_m0_room_idx");
                        TextBox tb_u0_document_idx = (TextBox)FvName.FindControl("tb_u0_document_idx");
                        TextBox txt_AlertDetailPictureView = (TextBox)FvName.FindControl("txt_AlertDetailPictureView");
                        var btnViewFileRoomDetail = (HyperLink)FvName.FindControl("btnViewFileRoomDetail");
                        CheckBoxList chkFoodDetail_RoomDetail = (CheckBoxList)FvName.FindControl("chkFoodDetail_RoomDetail");
                        GridView GvConferenceInRoom_Detail = (GridView)FvName.FindControl("GvConferenceInRoom_Detail");
                        TextBox txtm0_conidx = (TextBox)FvName.FindControl("txtm0_conidx");
                        TextBox tbplace_idx = (TextBox)FvName.FindControl("tbplace_idx");
                        TextBox tb_status_booking = (TextBox)FvName.FindControl("tb_status_booking");
                        if (tb_status_booking.Text == "ว่าง")
                        {

                            //tb_status_booking.Attributes["style"] = "color:green; font-weight:bold;";
                            //tb_status_booking.Style.Add("background-color", "#32CD32");
                            tb_status_booking.Style.Add("color", "#32CD32");
                            tb_status_booking.Style["font-weight"] = "bold";


                        }
                        else
                        {
                            //tb_status_booking.Attributes["style"] = "color:red; font-weight:bold;";
                            //tb_status_booking.Style.Add("background-color", "#FF6600");
                            tb_status_booking.Style.Add("color", "#FF0000");
                            tb_status_booking.Style["font-weight"] = "bold";

                        }

                        SelectConferenceDetail(GvConferenceInRoom_Detail);

                        if (tbplace_idx.Text == "8")
                        {
                            GvConferenceInRoom_Detail.Visible = true;
                        }
                        else
                        {
                            GvConferenceInRoom_Detail.Visible = false;
                        }


                        //Check Detail Food In Room Detail
                        //lectFoodDetail(); eqtqt
                        data_roombooking data_m0_food_detail = new data_roombooking();
                        rbk_m0_food_detail m0_food_detail = new rbk_m0_food_detail();
                        data_m0_food_detail.rbk_m0_food_list = new rbk_m0_food_detail[1];

                        m0_food_detail.condition = 1;

                        data_m0_food_detail.rbk_m0_food_list[0] = m0_food_detail;
                        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
                        data_m0_food_detail = callServicePostRoomBooking(_urlGetRbkm0Food, data_m0_food_detail);

                        //ViewState["data_test_sample"] = data_m0_food_detail.rbk_m0_food_list;

                        //CheckBoxList chk_TestDetail = (CheckBoxList)fvActor1Node1.FindControl("chk_TestDetail");
                        //chkFoodDetail_approve.Items.Clear();
                        chkFoodDetail_RoomDetail.AppendDataBoundItems = true;
                        chkFoodDetail_RoomDetail.DataSource = data_m0_food_detail.rbk_m0_food_list;
                        chkFoodDetail_RoomDetail.DataTextField = "food_name";
                        chkFoodDetail_RoomDetail.DataValueField = "food_idx";
                        chkFoodDetail_RoomDetail.DataBind();


                        // bind value in detail view approve food detail
                        data_roombooking data_m0_food_detail_view = new data_roombooking();
                        rbk_u2_roombooking_detail m0_food_detail_view = new rbk_u2_roombooking_detail();
                        data_m0_food_detail_view.rbk_u2_roombooking_list = new rbk_u2_roombooking_detail[1];

                        m0_food_detail_view.u0_document_idx = int.Parse(tb_u0_document_idx.Text);

                        data_m0_food_detail_view.rbk_u2_roombooking_list[0] = m0_food_detail_view;
                        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
                        data_m0_food_detail_view = callServicePostRoomBooking(_urlGetViewFoodDetailRbk, data_m0_food_detail_view);

                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_food_detail_view));

                        if (data_m0_food_detail_view.return_code == 0)
                        {
                            ViewState["Vs_DetailFoodView_Check"] = data_m0_food_detail_view.rbk_u2_roombooking_list;


                            ViewState["Vs_DetailFoodView_Check"] = data_m0_food_detail_view.rbk_u2_roombooking_list;

                            rbk_u2_roombooking_detail[] _itemDetailFoodRoom = (rbk_u2_roombooking_detail[])ViewState["Vs_DetailFoodView_Check"];

                            string[] _value_chk;
                            List<String> _value = new List<string>();
                            var _linqFoodDetail = (from data in _itemDetailFoodRoom //_itemTestDetail
                                                   select new
                                                   {
                                                       //data.root_detail,
                                                       data.food_idx,

                                                   }).ToList();



                            for (int z = 0; z < _linqFoodDetail.Count(); z++)
                            {
                                _value.Add(_linqFoodDetail[z].food_idx.ToString());

                            }

                            _value_chk = _value.ToArray();
                            //ViewState[""] = 
                            //  litDebug.Text = _value;

                            foreach (ListItem chkListSet in chkFoodDetail_RoomDetail.Items)
                            {

                                if (Array.IndexOf(_value_chk, chkListSet.Value) > -1)
                                {
                                    chkListSet.Selected = true;

                                }
                                else
                                {
                                    chkListSet.Selected = false;
                                    //item.Value = "0";

                                }

                            }


                        }

                        string filePath_View_detail = ConfigurationManager.AppSettings["path_flie_roombooking"];
                        string directoryName_View_detail = tb_m0_room_idx.Text;//lbl_place_name_edit.Text.Trim() + lbl_room_name_en_edit.Text + lblm0_room_idx_edit.Text; dddd
                        //litDebug.Text = tb_m0_room_idx_approve.Text;
                        if (Directory.Exists(Server.MapPath(filePath_View_detail + directoryName_View_detail)))
                        {
                            string[] filesPath = Directory.GetFiles(Server.MapPath(filePath_View_detail + directoryName_View_detail));
                            List<ListItem> files = new List<ListItem>();
                            foreach (string path in filesPath)
                            {
                                string getfiles = "";
                                getfiles = Path.GetFileName(path);
                                btnViewFileRoomDetail.NavigateUrl = filePath_View_detail + directoryName_View_detail + "/" + getfiles;

                                HtmlImage imgedit = new HtmlImage();
                                imgedit.Src = filePath_View_detail + directoryName_View_detail + "/" + getfiles;
                                imgedit.Height = 200;
                                btnViewFileRoomDetail.Controls.Add(imgedit);


                            }
                            btnViewFileRoomDetail.Visible = true;
                            txt_AlertDetailPictureView.Visible = false;
                        }
                        else
                        {
                            btnViewFileRoomDetail.Visible = false;
                            txt_AlertDetailPictureView.Visible = true;
                        }

                    }

                    if (FvName.CurrentMode == FormViewMode.Edit)
                    {
                        TextBox tb_m0_room_idx = (TextBox)FvName.FindControl("tb_m0_room_idx");
                        TextBox tb_u0_document_idx = (TextBox)FvName.FindControl("tb_u0_document_idx");
                        TextBox txt_AlertDetailPictureView = (TextBox)FvName.FindControl("txt_AlertDetailPictureView");
                        var btnViewFileRoomDetail = (HyperLink)FvName.FindControl("btnViewFileRoomDetail");
                        CheckBoxList chkFoodDetail_RoomDetail = (CheckBoxList)FvName.FindControl("chkFoodDetail_RoomDetail");
                        GridView GvConferenceInRoom_DetailEdit = (GridView)FvName.FindControl("GvConferenceInRoom_DetailEdit");
                        TextBox tb_place_idx = (TextBox)FvName.FindControl("tb_place_idx");
                        TextBox tb_status_booking = (TextBox)FvName.FindControl("tb_status_booking");

                        if (tb_status_booking.Text == "ว่าง")
                        {

                            //tb_status_booking.Attributes["style"] = "color:green; font-weight:bold;";
                            //tb_status_booking.Style.Add("background-color", "#32CD32");
                            tb_status_booking.Style.Add("color", "#32CD32");
                            tb_status_booking.Style["font-weight"] = "bold";


                        }
                        else
                        {
                            //tb_status_booking.Attributes["style"] = "color:red; font-weight:bold;";
                            //tb_status_booking.Style.Add("background-color", "#FF6600");
                            tb_status_booking.Style.Add("color", "#FF0000");
                            tb_status_booking.Style["font-weight"] = "bold";

                        }

                        //Check Detail Food In Room Detail
                        //lectFoodDetail(); eqtqt

                        SelectConferenceDetail(GvConferenceInRoom_DetailEdit);
                        if (tb_place_idx.Text == "8")
                        {
                            GvConferenceInRoom_DetailEdit.Visible = true;
                        }
                        else
                        {
                            GvConferenceInRoom_DetailEdit.Visible = false;
                        }

                        data_roombooking data_m0_food_detail = new data_roombooking();
                        rbk_m0_food_detail m0_food_detail = new rbk_m0_food_detail();
                        data_m0_food_detail.rbk_m0_food_list = new rbk_m0_food_detail[1];

                        m0_food_detail.condition = 1;

                        data_m0_food_detail.rbk_m0_food_list[0] = m0_food_detail;
                        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
                        data_m0_food_detail = callServicePostRoomBooking(_urlGetRbkm0Food, data_m0_food_detail);

                        //ViewState["data_test_sample"] = data_m0_food_detail.rbk_m0_food_list;

                        //CheckBoxList chk_TestDetail = (CheckBoxList)fvActor1Node1.FindControl("chk_TestDetail");
                        //chkFoodDetail_approve.Items.Clear();
                        chkFoodDetail_RoomDetail.AppendDataBoundItems = true;
                        chkFoodDetail_RoomDetail.DataSource = data_m0_food_detail.rbk_m0_food_list;
                        chkFoodDetail_RoomDetail.DataTextField = "food_name";
                        chkFoodDetail_RoomDetail.DataValueField = "food_idx";
                        chkFoodDetail_RoomDetail.DataBind();


                        // bind value in detail view approve food detail
                        data_roombooking data_m0_food_detail_view = new data_roombooking();
                        rbk_u2_roombooking_detail m0_food_detail_view = new rbk_u2_roombooking_detail();
                        data_m0_food_detail_view.rbk_u2_roombooking_list = new rbk_u2_roombooking_detail[1];

                        m0_food_detail_view.u0_document_idx = int.Parse(tb_u0_document_idx.Text);

                        data_m0_food_detail_view.rbk_u2_roombooking_list[0] = m0_food_detail_view;
                        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
                        data_m0_food_detail_view = callServicePostRoomBooking(_urlGetViewFoodDetailRbk, data_m0_food_detail_view);

                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_food_detail_view));

                        if (data_m0_food_detail_view.return_code == 0)
                        {
                            ViewState["Vs_DetailFoodView_Check"] = data_m0_food_detail_view.rbk_u2_roombooking_list;


                            ViewState["Vs_DetailFoodView_Check"] = data_m0_food_detail_view.rbk_u2_roombooking_list;

                            rbk_u2_roombooking_detail[] _itemDetailFoodRoom = (rbk_u2_roombooking_detail[])ViewState["Vs_DetailFoodView_Check"];

                            string[] _value_chk;
                            List<String> _value = new List<string>();
                            var _linqFoodDetail = (from data in _itemDetailFoodRoom //_itemTestDetail
                                                   select new
                                                   {
                                                       //data.root_detail,
                                                       data.food_idx,

                                                   }).ToList();



                            for (int z = 0; z < _linqFoodDetail.Count(); z++)
                            {
                                _value.Add(_linqFoodDetail[z].food_idx.ToString());

                            }

                            _value_chk = _value.ToArray();
                            //ViewState[""] = 
                            //  litDebug.Text = _value;

                            foreach (ListItem chkListSet in chkFoodDetail_RoomDetail.Items)
                            {

                                if (Array.IndexOf(_value_chk, chkListSet.Value) > -1)
                                {
                                    chkListSet.Selected = true;

                                }
                                else
                                {
                                    chkListSet.Selected = false;
                                    //item.Value = "0";

                                }

                            }


                        }

                        string filePath_View_detail = ConfigurationManager.AppSettings["path_flie_roombooking"];
                        string directoryName_View_detail = tb_m0_room_idx.Text;//lbl_place_name_edit.Text.Trim() + lbl_room_name_en_edit.Text + lblm0_room_idx_edit.Text; dddd
                        //litDebug.Text = tb_m0_room_idx_approve.Text;
                        if (Directory.Exists(Server.MapPath(filePath_View_detail + directoryName_View_detail)))
                        {
                            string[] filesPath = Directory.GetFiles(Server.MapPath(filePath_View_detail + directoryName_View_detail));
                            List<ListItem> files = new List<ListItem>();
                            foreach (string path in filesPath)
                            {
                                string getfiles = "";
                                getfiles = Path.GetFileName(path);
                                btnViewFileRoomDetail.NavigateUrl = filePath_View_detail + directoryName_View_detail + "/" + getfiles;

                                HtmlImage imgedit = new HtmlImage();
                                imgedit.Src = filePath_View_detail + directoryName_View_detail + "/" + getfiles;
                                imgedit.Height = 200;
                                btnViewFileRoomDetail.Controls.Add(imgedit);


                            }
                            btnViewFileRoomDetail.Visible = true;
                            txt_AlertDetailPictureView.Visible = false;
                        }
                        else
                        {
                            btnViewFileRoomDetail.Visible = false;
                            txt_AlertDetailPictureView.Visible = true;
                        }
                    }


                    break;

            }
        }
    }
    #endregion Formview Databind

    #region reuse

    protected void initPageLoad()
    {

    }

    protected void initPage()
    {
        setActiveTab("docDetailRoom", 0, 0, 0, 0);
    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }

    protected void setDir(string dirName)
    {
        if (!Directory.Exists(Server.MapPath(dirName)))
        {
            Directory.CreateDirectory(Server.MapPath(dirName));
        }
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void setRepeaterData(Repeater rptName, Object obj)
    {
        rptName.DataSource = obj;
        rptName.DataBind();
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_qa callServicePostMasterQA(string _cmdUrl, data_qa _data_qa)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_qa);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_qa = (data_qa)_funcTool.convertJsonToObject(typeof(data_qa), _localJson);

        return _data_qa;
    }

    protected data_qa callServicePostQA(string _cmdUrl, data_qa _data_qa)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_qa);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_qa = (data_qa)_funcTool.convertJsonToObject(typeof(data_qa), _localJson);

        return _data_qa;
    }

    protected data_roombooking callServicePostRoomBooking(string _cmdUrl, data_roombooking _data_roombooking)
    {
        _localJson = _funcTool.convertObjectToJson(_data_roombooking);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        _data_roombooking = (data_roombooking)_funcTool.convertJsonToObject(typeof(data_roombooking), _localJson);


        return _data_roombooking;
    }

    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }

    protected void setActiveView(string activeTab, int uidx, int u1idx, int staidx, int _chk_tab)
    {
        mvSystem.SetActiveView((View)mvSystem.FindControl(activeTab));

        //set tab create
        setFormData(fvEmpDetail, FormViewMode.ReadOnly, null);
        setFormData(FvInsertRoomBooking, FormViewMode.ReadOnly, null);
        setGridData(GvDetailRoomSearch, null);
        Update_BackToSearch.Visible = false;
        Update_DetailRoomSearch.Visible = false;
        UpdatePanel_InsertDetailRoom.Visible = false;

        //set tab detail
        div_searchDetail.Visible = false;
        Update_BackToDetailRoomBooking.Visible = false;
        GvDetailRoomBooking.Visible = false;
        setGridData(GvDetailRoomBooking, null);

        fvDetailRoomBooking.Visible = false;
        setFormData(fvDetailRoomBooking, FormViewMode.Insert, null);

        div_LogViewDetail.Visible = false;
        setRepeaterData(rptLogRoomBooking, null);
        Update_EditRoomBookingButton.Visible = false;
        Panel_SearchDetail.Visible = false;
        setFormData(FvSearchDetailRoom, FormViewMode.ReadOnly, null);

        //set tab Approve
        GvWaitApprove.Visible = false;
        setGridData(GvWaitApprove, null);
        Panel_BackToWaitApprove.Visible = false;

        FvWaitApproveDetail.Visible = false;
        setFormData(FvWaitApproveDetail, FormViewMode.Insert, null);

        div_LogViewDetailWaitApprove.Visible = false;
        setRepeaterData(rptLogRoomBookingWaitApprove, null);
        setFormData(FvDetailApproveHR, FormViewMode.ReadOnly, null);
        Update_EditRoomBookingButtonHR.Visible = false;

        //set tab Room
        setFormData(FvDetailUseRoomSearch, FormViewMode.ReadOnly, null);
        //div_calendar.Visible = false;

        //buttin edit and cancel room booking //
        btnEditDetailBooking.Visible = false;
        btnCancelBookingRoom.Visible = false;
        //buttin edit and cancel room booking //

        //set tab Report

        Update_PnSearchReport.Visible = false;
        Update_PnTableReportDetail.Visible = false;
        ddlTypeReport.ClearSelection();
        ddlStatusRoomBooking.ClearSelection();
        ddlSearchDateReport.ClearSelection();

        txtAddEndDateReport.Enabled = false;
        txtAddStartdateReport.Text = string.Empty;
        txtAddEndDateReport.Text = string.Empty;

        Update_PanelReportTable.Visible = false;
        Update_PanelDetailReportTable.Visible = false;
        Update_PanelBackToSearch.Visible = false;


        Update_PnGraphReportDetail.Visible = false;
        Update_PanelGraph.Visible = false;
        //set tab Report

        //set tab Approve
        setFormData(FvSearch_Approve, FormViewMode.ReadOnly, null);
        //set tab Approve


        switch (activeTab)
        {
            case "docDetailRoom":

                switch (_chk_tab)
                {
                    case 0:
                        setFormData(FvDetailUseRoomSearch, FormViewMode.Insert, null);
                        DropDownList ddlPlaceSearch = (DropDownList)FvDetailUseRoomSearch.FindControl("ddlPlaceSearch");
                        getPlace(ddlPlaceSearch, 0);

                        //getShowDetailRoomSlider();

                        break;

                }
                setOntop.Focus();
                break;
            case "docDetail":
                //Page.Response.Redirect(Page.Request.Url.ToString(), true);

                //litDebug.Text = staidx + "," + "2222";
                switch (_chk_tab)
                {
                    case 0:
                        div_searchDetail.Visible = true;
                        GvDetailRoomBooking.Visible = true;
                        getDetailRoomBooking();
                        setOntop.Focus();
                        break;
                    case 1:

                        Update_BackToDetailRoomBooking.Visible = true;
                        getViewDetailRoomBooking(uidx);
                        div_LogViewDetail.Visible = true;
                        getLogDetailRoomBooking(uidx);

                        //Update_EditRoomBookingButton.Visible = false;

                        //ViewState["Vs_m0_node_idx_view"] = _m0_node_idx_view;
                        //ViewState["Vs_m0_actor_idx_view"] = _m0_actor_idx_view;
                        //ViewState["Vs_staidx_view"] = _staidx_view;
                        //ViewState["Vs_type_booking_idx_view"] = _type_booking_idx_view;

                        //set button edit and cancel booking //
                        ViewState["Vs_u0_document_idx_view"] = uidx;
                        ViewState["Vs_Status_ProcessCheck_edit"] = staidx;

                        Label tb_checkDateTime = (Label)fvDetailRoomBooking.FindControl("tb_checkDateTime"); //date check set button form database
                        TextBox tb_staidx = (TextBox)fvDetailRoomBooking.FindControl("tb_staidx"); //date check set button form database
                        TextBox tbActorCempIDX = (TextBox)fvDetailRoomBooking.FindControl("tbActorCempIDX"); //date check set button form database


                        IFormatProvider culture = new CultureInfo("en-US", true);
                        DateTime DateToday_Set = DateTime.ParseExact(DateTime.Now.ToString("dd/MM/yyyy HH:mm"), "dd/MM/yyyy HH:mm", culture); //Date Time Today Incheck
                        DateTime DateTime_DBSet = DateTime.ParseExact(tb_checkDateTime.Text, "dd/MM/yyyy HH:mm", culture);

                        var result_time = DateTime_DBSet.Subtract(DateToday_Set).TotalMinutes; // result time chrck > 30 min because cancel in room
                        //litDebug.Text = DateToday_Set.ToString();

                        switch (staidx)
                        {
                            case 1: // user edit cancel

                                if ((DateToday_Set <= DateTime_DBSet) && (result_time > 30) && (_emp_idx == int.Parse(tbActorCempIDX.Text)))
                                {
                                    Update_EditRoomBookingButton.Visible = true;
                                    btnEditDetailBooking.Visible = true;
                                    btnCancelBookingRoom.Visible = true;
                                    ////if(result > 30)
                                    ////{
                                    ////    btnCancelBookingRoom.Visible = true;
                                    ////    litDebug.Text = result.ToString();
                                    ////}
                                    ////else
                                    ////{
                                    ////    btnCancelBookingRoom.Visible = false;
                                    ////    //litDebug.Text = result.ToString();
                                    ////}
                                }
                                else
                                {

                                }

                                break;
                            case 3: // hr edit and cancel
                            case 4:

                                //if ((DateToday_Set < DateTime_DBSet) && (result_time > 30) && (ViewState["rpos_permission"].ToString() == "5911" || ViewState["rpos_permission"].ToString() == "5912" || ViewState["rpos_permission"].ToString() == "5883" || ViewState["rpos_permission"].ToString() == "5896" || ViewState["rpos_permission"].ToString() == "5897" || ViewState["rpos_permission"].ToString() == "1019" || _emp_idx == 1413 || _emp_idx == 23069))
                                //{
                                //    Update_EditRoomBookingButton.Visible = true;
                                //    btnEditDetailBooking.Visible = true;
                                //    btnCancelBookingRoom.Visible = true;
                                //    //litDebug.Text = "22222";
                                //    //litDebug.Text = ViewState["Vs_type_booking_idx_view"].ToString();
                                //}

                                ////if ((DateToday_Set <= DateTime_DBSet) && (result_time > 30) && (_emp_idx == int.Parse(tbActorCempIDX.Text))) 
                                ////{

                                ////    data_roombooking data_u0_roomdetail_per = new data_roombooking();
                                ////    rbk_u0_roombooking_detail u0_roomdetail_per = new rbk_u0_roombooking_detail();
                                ////    data_u0_roomdetail_per.rbk_u0_roombooking_list = new rbk_u0_roombooking_detail[1]; 

                                ////    u0_roomdetail_per.cemp_idx = _emp_idx;
                                ////    u0_roomdetail_per.admin_idx = _emp_idx;
                                ////    u0_roomdetail_per.rpos_idx = int.Parse(ViewState["rpos_permission"].ToString());
                                ////    u0_roomdetail_per.place_idx = int.Parse(ViewState["Vs_place_view"].ToString());
                                ////    data_u0_roomdetail_per.rbk_u0_roombooking_list[0] = u0_roomdetail_per;


                                ////    litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_roomdetail_per));     

                                ////    data_u0_roomdetail_per = callServicePostRoomBooking(_urlGetPermissionRoomBooking, data_u0_roomdetail_per);
                                ////    //ViewState["Vs_place_view"] = _place_view;
                                ////    if (data_u0_roomdetail_per.return_code == 0)
                                ////    {
                                ////        Update_EditRoomBookingButton.Visible = true;
                                ////        //btnEditDetailBooking.Visible = true;
                                ////        btnCancelBookingRoom.Visible = true;
                                ////    }

                                ////    //litDebug.Text = "22222";
                                ////    //litDebug.Text = ViewState["Vs_type_booking_idx_view"].ToString();
                                ////}

                                data_roombooking data_u0_roomdetail_per = new data_roombooking();
                                rbk_u0_roombooking_detail u0_roomdetail_per = new rbk_u0_roombooking_detail();
                                data_u0_roomdetail_per.rbk_u0_roombooking_list = new rbk_u0_roombooking_detail[1];

                                u0_roomdetail_per.cemp_idx = _emp_idx;
                                u0_roomdetail_per.admin_idx = _emp_idx;
                                u0_roomdetail_per.rpos_idx = int.Parse(ViewState["rpos_permission"].ToString());
                                u0_roomdetail_per.place_idx = int.Parse(ViewState["Vs_place_view"].ToString());
                                data_u0_roomdetail_per.rbk_u0_roombooking_list[0] = u0_roomdetail_per;


                                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_roomdetail_per));

                                data_u0_roomdetail_per = callServicePostRoomBooking(_urlGetPermissionRoomBooking, data_u0_roomdetail_per);

                                if ((DateToday_Set <= DateTime_DBSet) && (result_time > 30) && (_emp_idx == int.Parse(tbActorCempIDX.Text)) && data_u0_roomdetail_per.return_code != 0)
                                {
                                    Update_EditRoomBookingButton.Visible = true;
                                    //btnEditDetailBooking.Visible = true;
                                    btnCancelBookingRoom.Visible = true;


                                }
                                else if ((DateToday_Set <= DateTime_DBSet) && (result_time > 30) && (_emp_idx == int.Parse(tbActorCempIDX.Text)) && data_u0_roomdetail_per.return_code == 0)
                                {
                                    Update_EditRoomBookingButton.Visible = true;
                                    btnEditDetailBooking.Visible = true;
                                    btnCancelBookingRoom.Visible = true;
                                }
                                else if ((DateToday_Set <= DateTime_DBSet) && (result_time > 30) && data_u0_roomdetail_per.return_code == 0)
                                {
                                    Update_EditRoomBookingButton.Visible = true;
                                    btnEditDetailBooking.Visible = true;
                                    btnCancelBookingRoom.Visible = true;
                                }

                                break;
                            case 6:

                                data_roombooking data_u0_roomdetail_per_ = new data_roombooking();
                                rbk_u0_roombooking_detail u0_roomdetail_per_ = new rbk_u0_roombooking_detail();
                                data_u0_roomdetail_per_.rbk_u0_roombooking_list = new rbk_u0_roombooking_detail[1];

                                u0_roomdetail_per_.cemp_idx = _emp_idx;
                                u0_roomdetail_per_.admin_idx = _emp_idx;
                                u0_roomdetail_per_.rpos_idx = int.Parse(ViewState["rpos_permission"].ToString());
                                u0_roomdetail_per_.place_idx = int.Parse(ViewState["Vs_place_view"].ToString());
                                data_u0_roomdetail_per_.rbk_u0_roombooking_list[0] = u0_roomdetail_per_;


                                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_roomdetail_per));

                                data_u0_roomdetail_per_ = callServicePostRoomBooking(_urlGetPermissionRoomBooking, data_u0_roomdetail_per_);

                                //if ((DateToday_Set <= DateTime_DBSet) && (result_time > 30) && (_emp_idx == int.Parse(tbActorCempIDX.Text)) && data_u0_roomdetail_per_.return_code != 0)
                                //{
                                //    Update_EditRoomBookingButton.Visible = true;
                                //    //btnEditDetailBooking.Visible = true;
                                //    btnCancelBookingRoom.Visible = true;


                                //}
                                if ((DateToday_Set <= DateTime_DBSet) && (result_time > 30) && data_u0_roomdetail_per_.return_code == 0 && (_emp_idx == int.Parse(tbActorCempIDX.Text)))
                                {
                                    Update_EditRoomBookingButton.Visible = true;
                                    btnEditDetailBooking.Visible = true;
                                    btnCancelBookingRoom.Visible = true;
                                }
                                else if ((DateToday_Set <= DateTime_DBSet) && (result_time > 30) && data_u0_roomdetail_per_.return_code == 0)
                                {
                                    Update_EditRoomBookingButton.Visible = true;
                                    btnEditDetailBooking.Visible = true;
                                    btnCancelBookingRoom.Visible = true;
                                }
                                else if ((DateToday_Set <= DateTime_DBSet) && (result_time > 30) && (_emp_idx == int.Parse(tbActorCempIDX.Text)) && data_u0_roomdetail_per_.return_code != 0)
                                {
                                    Update_EditRoomBookingButton.Visible = true;
                                    //btnEditDetailBooking.Visible = true;
                                    btnCancelBookingRoom.Visible = true;
                                }


                                //if ((DateToday_Set <= DateTime_DBSet) && (result_time > 30) && (_emp_idx != int.Parse(tbActorCempIDX.Text)))
                                //{

                                //    data_roombooking data_u0_roomdetail_per_ = new data_roombooking();
                                //    rbk_u0_roombooking_detail u0_roomdetail_per_ = new rbk_u0_roombooking_detail();
                                //    data_u0_roomdetail_per_.rbk_u0_roombooking_list = new rbk_u0_roombooking_detail[1];

                                //    u0_roomdetail_per_.cemp_idx = _emp_idx;
                                //    u0_roomdetail_per_.admin_idx = _emp_idx;
                                //    u0_roomdetail_per_.rpos_idx = int.Parse(ViewState["rpos_permission"].ToString());

                                //    data_u0_roomdetail_per_.rbk_u0_roombooking_list[0] = u0_roomdetail_per_;


                                //    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_roomdetail));        
                                //    data_u0_roomdetail_per_ = callServicePostRoomBooking(_urlGetDetailRoomBooking, data_u0_roomdetail_per_);
                                //    if (data_u0_roomdetail_per_.return_code == 0)
                                //    {
                                //        Update_EditRoomBookingButton.Visible = true;
                                //        btnEditDetailBooking.Visible = true;
                                //        btnCancelBookingRoom.Visible = true;
                                //    }

                                //    //litDebug.Text = "22222";
                                //    //litDebug.Text = ViewState["Vs_type_booking_idx_view"].ToString();
                                //}



                                break;


                        }

                        //set button edit and cancel booking //
                        setOntop.Focus();
                        break;
                }
                break;
            case "docCreate":

                switch (_chk_tab)
                {
                    case 0:
                        setFormData(fvEmpDetail, FormViewMode.ReadOnly, ((data_employee)ViewState["vsEmpProfile"]).employee_list);
                        setFormData(FvInsertRoomBooking, FormViewMode.Insert, null);
                        DropDownList ddlPlace = (DropDownList)FvInsertRoomBooking.FindControl("ddlPlace");
                        getPlace(ddlPlace, 0);

                        break;
                    case 1:
                        Update_BackToSearch.Visible = true;
                        break;
                }

                break;
            case "docApprove":

                switch (_chk_tab)
                {
                    case 0:

                        setFormData(FvSearch_Approve, FormViewMode.Insert, null);
                        getPlace(ddlPlaceSearchApprove, 0);

                        GvWaitApprove.Visible = true;
                        getWaitApproveRoomBooking();

                        setOntop.Focus();
                        break;
                    case 1:
                        int u0_document_idx_approve = int.Parse(ViewState["vs_u0_document_idx_approve"].ToString());
                        int node_idx_approve = int.Parse(ViewState["vs_m0_node_idx_approve"].ToString());
                        int actor_idx_approve = int.Parse(ViewState["vs_m0_actor_idx_approve"].ToString());
                        int status_idx_approve = int.Parse(ViewState["vs_staidx_approve"].ToString());


                        ////Label tb_checkDateTime = (Label)fvDetailRoomBooking.FindControl("tb_checkDateTime"); //date check set button form database

                        ////IFormatProvider culture = new CultureInfo("en-US", true);
                        ////DateTime DateToday_Set = DateTime.ParseExact(DateTime.Now.ToString("dd/MM/yyyy HH:mm"), "dd/MM/yyyy HH:mm", culture); //Date Time Today Incheck
                        ////DateTime DateTime_DBSet = DateTime.ParseExact(tb_checkDateTime.Text, "dd/MM/yyyy HH:mm", culture);

                        ////var result_time = DateTime_DBSet.Subtract(DateToday_Set).TotalMinutes; // result time chrck > 30 min because cancel in room

                        //litDebug.Text = node_idx_approve.ToString();
                        Panel_BackToWaitApprove.Visible = true;
                        getViewWaitApproveRoomBooking(uidx);
                        div_LogViewDetailWaitApprove.Visible = true;
                        getLogRoomBookingWaitApprove(uidx);
                        Update_EditRoomBookingButtonHR.Visible = true;
                        if (staidx != 6)
                        {
                            setFormData(FvDetailApproveHR, FormViewMode.Insert, null);
                            DropDownList ddlDecisionApprove = (DropDownList)FvDetailApproveHR.FindControl("ddlDecisionApprove");
                            getDecision(ddlDecisionApprove, staidx);
                            Update_EditRoomBookingButtonHR.Visible = false;
                            setOntop.Focus();
                        }

                        break;
                }

                //Response.Redirect(link_system_roombooking);
                break;

            case "docReport":
                Update_PnSearchReport.Visible = true;
                //ddlTypeReport.ClearSelection();
                //switch (_chk_tab)
                //{
                //    case 1: // ตาราง
                //        Update_PnTableReportDetail.Visible = true;
                //        break;

                //    case 2: // กราฟ

                //        break;
                //}

                break;
        }
    }

    protected void setActiveTab(string activeTab, int uidx, int u1idx, int staidx, int _chk_tab)
    {
        setActiveView(activeTab, uidx, u1idx, staidx, _chk_tab);
        switch (activeTab)
        {
            case "docDetailRoom":
                li0.Attributes.Add("class", "active");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "");

                //li3.Attributes.Add("class", "");
                break;
            case "docDetail":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "active");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "");

                //li3.Attributes.Add("class", "");
                break;
            case "docCreate":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "active");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "");

                break;
            case "docApprove":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "active");
                li4.Attributes.Add("class", "");

                break;
            case "docReport":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "active");

                break;

        }
    }

    protected string getOnlyDate(string _dateIn)
    {
        return _funcTool.getOnlyDate(_dateIn);
    }

    protected string getStatus(int status)
    {
        if (status == 0)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Certificate'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='No Certificate'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        // Confirms that an HtmlForm control is rendered for the
        //specified ASP.NET server control at run time.
    }

    protected void rptOnRowDataBound(Object Sender, RepeaterItemEventArgs e)
    {
        var rptName = (Repeater)Sender;
        switch (rptName.ID)
        {
            case "rp_place":

                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {

                    //var chk_coler = (Label)e.Item.FindControl("lbcheck_coler");
                    //var btnPlaceLab = (LinkButton)e.Item.FindControl("btnPlaceLab");

                    //for (int k = 0; k <= rp_place.Items.Count; k++)
                    //{
                    //    btnPlaceLab.CssClass = ConfigureColors(k);
                    //    //Console.WriteLine(i);
                    //}


                }

                break;
        }
    }

    #endregion reuse


    protected void WriteExcelWithNPOI(DataTable dt, String extension, String fileName)
    {
        IWorkbook workbook;
        if (extension == "xlsx")
        {
            workbook = new XSSFWorkbook();
        }
        else if (extension == "xls")
        {
            workbook = new HSSFWorkbook();
        }
        else
        {
            throw new Exception("This format is not supported");
        }
        ISheet sheet1 = workbook.CreateSheet("Sheet 1");
        IRow row1 = sheet1.CreateRow(0);
        for (int j = 0; j < dt.Columns.Count; j++)
        {
            ICell cell = row1.CreateCell(j);
            String columnName = dt.Columns[j].ToString();
            cell.SetCellValue(columnName);
        }
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            IRow row = sheet1.CreateRow(i + 1);
            for (int j = 0; j < dt.Columns.Count; j++)
            {
                ICell cell = row.CreateCell(j);
                String columnName = dt.Columns[j].ToString();
                cell.SetCellValue(dt.Rows[i][columnName].ToString());
            }
        }
        using (var exportData = new MemoryStream())
        {
            Response.Clear();
            workbook.Write(exportData);
            if (extension == "xlsx")
            {
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xlsx"));
                Response.BinaryWrite(exportData.ToArray());
            }
            else if (extension == "xls")
            {
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xls"));
                Response.BinaryWrite(exportData.GetBuffer());
            }
            Response.End();
        }
    }

    //protected void WriteExcelWithNPOI(DataTable dt, String extension, String fileName, string ATitle)
    //protected void WriteExcelWithNPOI(DataTable dt, String extension, String fileName)
    //{

    //    IWorkbook workbook;
    //    if (extension == "xlsx")
    //    {
    //        workbook = new XSSFWorkbook();
    //    }
    //    else if (extension == "xls")
    //    {
    //        workbook = new HSSFWorkbook();
    //    }
    //    else
    //    {
    //        throw new Exception("This format is not supported");
    //    }
    //    ISheet sheet1 = workbook.CreateSheet("Sheet 1");

    //    IRow row1 = sheet1.CreateRow(0);

    //    ICell cellTitle = row1.CreateCell(0);
    //    String columnNameTitle = ATitle;
    //    cellTitle.SetCellValue(columnNameTitle);


    //    row1 = sheet1.CreateRow(1);
    //    cellTitle = row1.CreateCell(0);
    //    columnNameTitle = "Print Date : " + DateTime.Now.ToString("dd/MM/yyyy", CultureInfo.InstalledUICulture);
    //    cellTitle.SetCellValue(columnNameTitle);

    //    row1 = sheet1.CreateRow(3);
    //    for (int j = 0; j < dt.Columns.Count; j++)
    //    {
    //        ICell cell = row1.CreateCell(j);
    //        String columnName = dt.Columns[j].ToString();
    //        cell.SetCellValue(columnName);
    //    }
    //    for (int i = 0; i < dt.Rows.Count; i++)
    //    {
    //        IRow row = sheet1.CreateRow(i + 4);
    //        for (int j = 0; j < dt.Columns.Count; j++)
    //        {
    //            ICell cell = row.CreateCell(j);
    //            String columnName = dt.Columns[j].ToString();
    //            cell.SetCellValue(dt.Rows[i][columnName].ToString());
    //        }
    //    }
    //    using (var exportData = new MemoryStream())
    //    {
    //        Response.Clear();
    //        workbook.Write(exportData);
    //        if (extension == "xlsx")
    //        {
    //            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    //            Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xlsx"));
    //            Response.BinaryWrite(exportData.ToArray());
    //        }
    //        else if (extension == "xls")
    //        {
    //            Response.ContentType = "application/vnd.ms-excel";
    //            Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xls"));
    //            Response.BinaryWrite(exportData.GetBuffer());
    //        }
    //        Response.End();
    //    }
    //}
}