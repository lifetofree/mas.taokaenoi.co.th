﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="reference-price.aspx.cs" Inherits="websystem_reference_price_reference_price" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:HyperLink ID="SETFOCUS" runat="server"></asp:HyperLink>
    <!--tab menu-->
    <nav class="navbar navbar-default" id="divBtnHead" runat="server" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-bar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="sub-navbar">
                    <a class="navbar-brand" href="#"><b>Menu</b></a>
                </div>
            </div>


            <!--Collect the nav links, forms, and other content for toggling-->
            <div class="collapse navbar-collapse" id="menu-bar">
                <ul class="nav navbar-nav" id="uiNav" runat="server">
                    <li id="li0" runat="server">
                        <asp:LinkButton ID="lbtab1" runat="server" CommandName="cmdtab2" OnCommand="navCommand" CommandArgument="tab1"> Computer & Software</asp:LinkButton>
                    </li>
                    <li id="li1" runat="server">
                        <asp:LinkButton ID="lbtab2" runat="server" CommandName="cmdtab2" OnCommand="navCommand" CommandArgument="tab2"> SAP,BI,TM1</asp:LinkButton>
                    </li>
                    <li id="li2" runat="server">
                        <asp:LinkButton ID="lbtab3" runat="server" CommandName="cmdtab3" OnCommand="navCommand" CommandArgument="tab3"> Network</asp:LinkButton>
                    </li>
                    <li id="li3" runat="server">
                        <asp:LinkButton ID="lbtab4" runat="server" CommandName="cmdtab4" OnCommand="navCommand" CommandArgument="tab4"> Domain & Hosting</asp:LinkButton>
                    </li>

                </ul>
            </div>
        </div>
    </nav>
    <!--tab menu-->

    <!--multiview-->
    <asp:MultiView ID="mvReferencePrice" runat="server">
        <asp:View ID="tab1" runat="server">

            <div class="alert-message alert-message-success">


                <blockquote class="danger" style="font-size: small; background-color: lightgoldenrodyellow;">
                    <h4><b>Notice:</b></h4>
                    <p>*ดุลพินิจในการเลือก Hardware และ Software ขึ้นอยู่กับฝ่ายสารสนเทศ ในการตัดสินใจว่าเหมาะสมหรือไม่</p>
                </blockquote>

            </div>
            <blockquote class="danger" style="font-size: small; background-color: powderblue;">
                <h4><b>ประเภท Computer And Hardware</b></h4>

            </blockquote>

            <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                <tbody>
                    <tr class="alert-info info" style="font-size: Small;">
                        <th scope="col" colspan="3" style="width: 100%;">ประเภท Notebook</th>
                    </tr>
                </tbody>
            </table>

            <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                <tbody>

                    <tr class="alert-success success" style="font-size: Small; height: 30px;">
                        <th scope="col" style="width: 20%;">ระดับ</th>
                        <th scope="col" style="width: 10%;">ราคา</th>
                        <th scope="col" style="width: 70%;">หมายเหตุ</th>
                    </tr>
                    <tr style="font-size: Small;">
                        <td>Super Special</td>
                        <td>32,000</td>
                        <td>เจ้าหน้าที่ที่ต้องการเครื่องที่มีการประมวลผลสูงๆ</td>
                    </tr>
                    <tr style="font-size: Small;">
                        <td>Special</td>
                        <td>25,000</td>
                        <td>ระดับ Director หรือ พนักงานที่มีการประมวลผลของเครื่องในระบบสูง</td>
                    </tr>
                    <tr style="font-size: Small;">
                        <td>High</td>
                        <td>20,000</td>
                        <td>เหมาะสำหรับ Assistant Manager, Manager</td>
                    </tr>
                    <tr style="font-size: Small;">
                        <td>Medium</td>
                        <td>18,000</td>
                        <td>ระดับเจ้าหน้าที่ ที่ใช้งานระบบ SAP, MS, Office, Google</td>
                    </tr>
                    <tr style="font-size: Small;">
                        <td>Low</td>
                        <td>14,000</td>
                        <td>ระดับเจ้าหน้าที่ ทำเอกสารทั่วไป ที่ไม่ต้องการความเร็วมาก</td>
                    </tr>
                </tbody>
            </table>

            <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                <tbody>
                    <tr class="alert-info info" style="font-size: Small;">
                        <th scope="col" colspan="3" style="width: 100%;">ประเภท PC</th>
                    </tr>
                </tbody>
            </table>

            <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                <tbody>

                    <tr class="alert-success success" style="font-size: Small; height: 30px;">
                        <th scope="col" style="width: 20%;">ระดับ</th>
                        <th scope="col" style="width: 10%;">ราคา</th>
                        <th scope="col" style="width: 70%;">หมายเหตุ</th>
                    </tr>
                    <tr style="font-size: Small;">
                        <td>Special Graphic</td>
                        <td>60,000</td>
                        <td>เฉพาะงานด้านกราฟฟิค *ราคาอาจมีการปรับเปลี่ยนตามสเปคที่ผู้ใช้เสนอมา</td>
                    </tr>
                    <tr style="font-size: Small;">
                        <td>Special</td>
                        <td>23,000</td>
                        <td>ระดับ Director หรือ พนักงานที่มีการประมวลผลของเครื่องในระบบสูง</td>
                    </tr>
                    <tr style="font-size: Small;">
                        <td>High</td>
                        <td>19,000</td>
                        <td>เหมาะสำหรับ Assistant Manager, Manager</td>
                    </tr>
                    <tr style="font-size: Small;">
                        <td>Medium</td>
                        <td>17,000</td>
                        <td>ระดับเจ้าหน้าที่ ที่ใช้งานระบบ SAP, MS, Office, Google</td>
                    </tr>
                    <tr style="font-size: Small;">
                        <td>Low</td>
                        <td>15,000</td>
                        <td>ระดับเจ้าหน้าที่ ทำเอกสารทั่วไป ที่ไม่ต้องการความเร็วมาก</td>
                    </tr>
                    <tr style="font-size: Small;">
                        <td>Monitor IPS</td>
                        <td>4,500</td>
                        <td>19 " เฉพาะงานด้านกราฟฟิค</td>
                    </tr>
                    <tr style="font-size: Small;">
                        <td>Monitor</td>
                        <td>3,000</td>
                        <td>19 " เหมาะกับงานทั่วไป</td>
                    </tr>
                </tbody>
            </table>

            <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                <tbody>
                    <tr class="alert-info info" style="font-size: Small;">
                        <th scope="col" colspan="9" style="width: 100%;">ประเภท Printer</th>
                    </tr>
                </tbody>
            </table>


            <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                <tbody>
                    <%--<tr class="alert-success success" style="font-size: Small;">
                        <th scope="col" colspan="9" style="width: 100%;">ประเภท Printer</th>
                    </tr>--%>
                    <tr class="alert-success success" style="font-size: Small; height: 30px;">
                        <th scope="col" style="width: 5%;">ยี่ห้อ</th>
                        <th scope="col" style="width: 5%;">ประเภท</th>
                        <th scope="col" style="width: 10%;">รุ่น</th>
                        <th scope="col" style="width: 5%;">ขนาด</th>
                        <th scope="col" style="width: 8%;">ราคาเช่า/เดือน</th>
                        <th scope="col" style="width: 10%;">ราคาปริ้นขาวดำ/แผ่น</th>
                        <th scope="col" style="width: 10%;">ราคาปริ้นสี/แผ่น</th>
                        <th scope="col" style="width: 10%;">ปริมาณการปริ้น/เดือน</th>
                        <th scope="col" style="width: 10%;">หมายเหตุ</th>
                    </tr>
                    <tr style="font-size: Small;">
                        <td>Muratec</td>
                        <td>ขาวดำ</td>
                        <td>22MFX2835</td>
                        <td>กลาง</td>
                        <td>1,000</td>
                        <td>0.2</td>
                        <td>-</td>
                        <td>10,000</td>
                        <td rowspan="4">หากต้องการใช้งาน FAX หรือ ปริ้น A3 ให้ระบุมาใน Memo ในการขอเช่าเครื่องปริ้น</td>
                    </tr>
                    <tr style="font-size: Small;">
                        <td>Ricoh</td>
                        <td>ขาวดำ</td>
                        <td>31MMP3055</td>
                        <td>ใหญ่</td>
                        <td>2,860</td>
                        <td>0.2</td>
                        <td>-</td>
                        <td>20,000</td>

                    </tr>
                    <tr style="font-size: Small;">
                        <td>Ricoh</td>
                        <td>สี</td>
                        <td>31MMPC306ZSP</td>
                        <td>เล็ก</td>
                        <td>2,170</td>
                        <td>0.2</td>
                        <td>2.5</td>
                        <td>8,000</td>

                    </tr>
                    <tr style="font-size: Small;">
                        <td>Ricoh</td>
                        <td>สี</td>
                        <td>31MMPC2004ZSP</td>
                        <td>ใหญ่</td>
                        <td>2,580</td>
                        <td>0.2</td>
                        <td>2.5</td>
                        <td>20,000</td>

                    </tr>

                </tbody>
            </table>
              <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                <tbody>
                    <tr class="alert-info info" style="font-size: Small;">
                        <th scope="col" colspan="9" style="width: 100%;">ประเภท External HDD</th>
                    </tr>
                </tbody>
            </table>

                <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                <tbody>
                    <%--<tr class="alert-success success" style="font-size: Small;">
                        <th scope="col" colspan="9" style="width: 100%;">ประเภท Printer</th>
                    </tr>--%>
                    <tr class="alert-success success" style="font-size: Small; height: 30px;">
                        <th scope="col" style="width: 50%;">ชื่อ</th>
                        <th scope="col" style="width: 50%;">ราคา (บาท)</th>
                    </tr>
                    <tr style="font-size: Small;">
                        <td>External HDD 1 TB</td>
                        <td>2,300</td>
                       
                    </tr>
                    <tr style="font-size: Small;">
                        <td>External HDD 2 TB</td>
                        <td>3,000</td>
                    </tr>
                </tbody>
            </table>
            <blockquote class="danger" style="font-size: small; background-color: powderblue;">
                <h4><b>ประเภท Software</b></h4>

            </blockquote>
            <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                <tbody>
                    <tr class="alert-info info" style="font-size: Small;">
                        <th scope="col" colspan="9" style="width: 100%;">ประเภท Windows</th>
                    </tr>
                </tbody>
            </table>
            <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                <tbody>
                    <%-- <tr class="alert-success success" style="font-size: Small;">
                        <th scope="col" colspan="9" style="width: 100%;">ประเภท Gmail</th>
                    </tr>--%>
                    <tr class="alert-success success" style="font-size: Small; height: 30px;">
                        <th scope="col" style="width: 25%;">ชื่อ</th>
                        <th scope="col" style="width: 10%;">อายุการใช้งาน</th>
                        <th scope="col" style="width: 5%;">ราคา</th>
                        <th scope="col" style="width: 60%;">หมายเหตุ</th>

                    </tr>
                    <tr style="font-size: Small;">
                        <td>Microsoft Windows 10 Pro 32/64 Bit (Open)</td>
                        <td>ถาวร</td>
                        <td>9,000</td>
                        <td>-</td>

                    </tr>

                </tbody>
            </table>
            <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                <tbody>
                    <tr class="alert-info info" style="font-size: Small;">
                        <th scope="col" colspan="9" style="width: 100%;">ประเภท Microsoft Office</th>
                    </tr>
                </tbody>
            </table>

            <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                <tbody>
                    <%--   <tr class="alert-success success" style="font-size: Small;">
                        <th scope="col" colspan="9" style="width: 100%;">ประเภท Microsoft Office</th>
                    </tr>--%>
                    <tr class="alert-success success" style="font-size: Small; height: 30px;">
                        <th scope="col" style="width: 25%;">ชื่อ</th>
                        <th scope="col" style="width: 10%;">อายุการใช้งาน</th>
                        <th scope="col" style="width: 5%;">ราคา</th>
                        <th scope="col" style="width: 60%;">หมายเหตุ</th>

                    </tr>
                    <tr style="font-size: Small;">
                        <td>Microsoft Office Standard 2016</td>
                        <td>ถาวร</td>
                        <td>15,000</td>
                        <td>Word, Excel, PowerPoint, Onenote, Outlook, Publisher (เฉพาะผู้ที่มีความจำเป็นต้องใช้งาน)</td>

                    </tr>
                    <tr style="font-size: Small;">
                        <td>Microsoft Office Professional 2016</td>
                        <td>ถาวร</td>
                        <td>18,000</td>
                        <td>Word, Excel, PowerPoint, Onenote, Outlook, Publisher, Access, InfoPath, Visio (เฉพาะผู้ที่มีความจำเป็นต้องใช้งาน)</td>

                    </tr>
                    <tr style="font-size: Small;">
                        <td>Microsoft Office Standard 2016 for mac book</td>
                        <td>ถาวร</td>
                        <td>15,000</td>
                        <td>Word, Excel, PowerPoint, Onenote, Outlook, Publisher (เฉพาะผู้ที่มีความจำเป็นต้องใช้งาน)</td>


                    </tr>

                </tbody>
            </table>
            <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                <tbody>
                    <tr class="alert-info info" style="font-size: Small;">
                        <th scope="col" colspan="9" style="width: 100%;">ประเภท Adobe</th>
                    </tr>
                </tbody>
            </table>
            <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                <tbody>
                    <%-- <tr class="alert-success success" style="font-size: Small;">
                        <th scope="col" colspan="9" style="width: 100%;">ประเภท Adobe</th>
                    </tr>--%>
                    <tr class="alert-success success" style="font-size: Small; height: 30px;">
                        <th scope="col" style="width: 25%;">ชื่อ</th>
                        <th scope="col" style="width: 10%;">อายุการใช้งาน</th>
                        <th scope="col" style="width: 5%;">ราคา</th>
                        <th scope="col" style="width: 60%;">หมายเหตุ</th>

                    </tr>
                    <tr style="font-size: Small;">
                        <td>AdobePDF Pro DC</td>
                        <td>1 Year</td>
                        <td>7,000</td>
                        <td>สำหรับหน่วยงานที่จำเป็นต้องใช้ ในการแก้ไขไฟล์ PDF</td>

                    </tr>
                    <tr style="font-size: Small;">
                        <td>Adobe Creative Cloud All App</td>
                        <td>1 Year</td>
                        <td>35,000</td>
                        <td>เฉพาะ Web Programmer, Graphic หรือ หน่วยงานอื่นๆ ที่จำเป็นต้องใช้</td>

                    </tr>
                    <tr style="font-size: Small;">
                        <td>Adobe Photoshop CC for Windows</td>
                        <td>1 Year</td>
                        <td>15,000</td>
                        <td>เฉพาะ Web Programmer, Graphic หรือ หน่วยงานอื่นๆ ที่จำเป็นต้องใช้</td>

                    </tr>
                    <tr style="font-size: Small;">
                        <td>Adobe illustrator CC for Windows</td>
                        <td>1 Year</td>
                        <td>15,000</td>
                        <td>เฉพาะ Web Programmer, Graphic หรือ หน่วยงานอื่นๆ ที่จำเป็นต้องใช้</td>

                    </tr>
                    <tr style="font-size: Small;">
                        <td>Adobe Photoshop CC for mac book</td>
                        <td>1 Year</td>
                        <td>15,000</td>
                        <td>เฉพาะ Web Programmer, Graphic หรือ หน่วยงานอื่นๆ ที่จำเป็นต้องใช้</td>

                    </tr>
                    <tr style="font-size: Small;">
                        <td>Adobe illustrator CC for mac book</td>
                        <td>1 Year</td>
                        <td>15,000</td>
                        <td>เฉพาะ Web Programmer, Graphic หรือ หน่วยงานอื่นๆ ที่จำเป็นต้องใช้</td>

                    </tr>


                </tbody>
            </table>

            <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                <tbody>
                    <tr class="alert-info info" style="font-size: Small;">
                        <th scope="col" colspan="9" style="width: 100%;">ประเภท Gmail</th>
                    </tr>
                </tbody>
            </table>
            <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                <tbody>
                    <%-- <tr class="alert-success success" style="font-size: Small;">
                        <th scope="col" colspan="9" style="width: 100%;">ประเภท Gmail</th>
                    </tr>--%>
                    <tr class="alert-success success" style="font-size: Small; height: 30px;">
                        <th scope="col" style="width: 25%;">ชื่อ</th>
                        <th scope="col" style="width: 10%;">อายุการใช้งาน</th>
                        <th scope="col" style="width: 5%;">ราคา</th>
                        <th scope="col" style="width: 60%;">หมายเหตุ</th>

                    </tr>
                    <tr style="font-size: Small;">
                        <td>Gmail License</td>
                        <td>1 Year</td>
                        <td>1,200</td>
                        <td>แพลนตามจำนวนที่ใช้งานอยู่ในปัจจุบัน ถ้าหน่วยงานใดต้องการจะเพิ่ม ให้แพลนเพิ่มไปด้วย</td>

                    </tr>

                </tbody>
            </table>

            <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                <tbody>
                    <tr class="alert-info info" style="font-size: Small;">
                        <th scope="col" colspan="9" style="width: 100%;">ประเภท ราคาเช่าพื้นที่ Gmail</th>
                    </tr>
                </tbody>
            </table>

            <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                <tbody>
                    <%-- <tr class="alert-success success" style="font-size: Small;">
                        <th scope="col" colspan="9" style="width: 100%;">ประเภท Gmail</th>
                    </tr>--%>
                    <tr class="alert-success success" style="font-size: Small; height: 30px;">
                        <th scope="col" style="width: 40%;">ขนาด</th>
                        <th scope="col" style="width: 30%;">ราคา/เดือน (บาท)</th>
                        <th scope="col" style="width: 30%;">ราคา/ปี (บาท)</th>
                    </tr>

                    <tr style="font-size: Small;">
                        <td>100 GB</td>
                        <td>70</td>
                        <td>700</td>
                    </tr>

                      <tr style="font-size: Small;">
                        <td>1 TB</td>
                        <td>350</td>
                        <td>3,500</td>
                    </tr>
                    <%--      <tr class="alert-success success" style="font-size: Small; height: 30px;">
                        <th scope="col" style="width: 20%;">License Size</th>
                        <th scope="col" style="width: 20%;">Monthly Price (USD)</th>
                        <th scope="col"  style="width: 60%;">หมายเหตุ</th>
                    </tr>

                    <tr style="font-size: Small;">
                        <td>20 GB</td>
                        <td>$ 4.00</td>
                        <td rowspan="9" style="padding-top: 130px;">ราคาที่ใส่เป็นแบบรายเดือนและเป็นสกุลเงินแบบดอลลาร์สหรัฐ หากจะเช่าพื้นที่ต้องคูณตามค่าเงินบาทปัจจุบัน</td>


                    </tr>
                    <tr style="font-size: Small;">
                        <td>50 GB</td>
                        <td>$ 7.50</td>


                    </tr>
                    <tr style="font-size: Small;">
                        <td>200 GB</td>
                        <td>$ 17.50</td>

                    </tr>
                    <tr style="font-size: Small;">
                        <td>400 GB</td>
                        <td>$ 35.00</td>

                    </tr>
                    <tr style="font-size: Small;">
                        <td>1 TB</td>
                        <td>$ 89.00</td>

                    </tr>
                    <tr style="font-size: Small;">
                        <td>2 TB</td>
                        <td>$ 179.00</td>

                    </tr>
                    <tr style="font-size: Small;">
                        <td>4 TB</td>
                        <td>$ 358.00</td>

                    </tr>
                    <tr style="font-size: Small;">
                        <td>8 TB</td>
                        <td>$ 716.00</td>
                    </tr>
                    <tr style="font-size: Small;">
                        <td>16 TB</td>
                        <td>$ 1,430.00</td>

                    </tr>--%>
                </tbody>
            </table>


            <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                <tbody>
                    <tr class="alert-info info" style="font-size: Small;">
                        <th scope="col" colspan="9" style="width: 100%;">ประเภท Anti-Virus</th>
                    </tr>
                </tbody>
            </table>
            <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                <tbody>
                    <%--   <tr class="alert-success success" style="font-size: Small;">
                        <th scope="col" colspan="9" style="width: 100%;">ประเภท Anti-Virus</th>
                    </tr>--%>
                    <tr class="alert-success success" style="font-size: Small; height: 30px;">
                        <th scope="col" style="width: 25%;">ชื่อ</th>
                        <th scope="col" style="width: 10%;">อายุการใช้งาน</th>
                        <th scope="col" style="width: 5%;">ราคา</th>
                        <th scope="col" style="width: 60%;">หมายเหตุ</th>

                    </tr>
                    <tr style="font-size: Small;">
                        <td>Symantec</td>
                        <td>3 Year</td>
                        <td>1,400</td>
                        <td>แพลนตามจำนวนที่ใช้งานอยู่ในปัจจุบัน ถ้าหน่วยงานใดที่วางแพลนซื้อเครื่องคอมพิวเตอร์ในปีหน้า ให้แพลนเพิ่มตามจำนวนเครื่องที่แพลนไว้เข้าไปด้วย</td>

                    </tr>

                </tbody>
            </table>
            <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                <tbody>
                    <tr class="alert-info info" style="font-size: Small;">
                        <th scope="col" colspan="9" style="width: 100%;">ประเภท Autodesk</th>
                    </tr>
                </tbody>
            </table>
            <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                <tbody>
                    <%--  <tr class="alert-success success" style="font-size: Small;">
                        <th scope="col" colspan="9" style="width: 100%;">ประเภท Autodesk</th>
                    </tr>--%>
                    <tr class="alert-success success" style="font-size: Small; height: 30px;">
                        <th scope="col" style="width: 25%;">ชื่อ</th>
                        <th scope="col" style="width: 10%;">อายุการใช้งาน</th>
                        <th scope="col" style="width: 5%;">ราคา</th>
                        <th scope="col" style="width: 60%;">หมายเหตุ</th>

                    </tr>
                    <tr style="font-size: Small;">
                        <td>Autodesk Autocad 2018 (2D&3D)</td>
                        <td>1 Year</td>
                        <td>47,000</td>
                        <td>เฉพาะ Engineering หรือหน่วยงานอื่นๆ ที่จำเป็นต้องใช้</td>

                    </tr>
                    <tr style="font-size: Small;">
                        <td>Autodesk AutoCAD 2018 LT (2D)</td>
                        <td>1 Year</td>
                        <td>13,500</td>
                        <td>เฉพาะ Engineering หรือหน่วยงานอื่นๆ ที่จำเป็นต้องใช้</td>

                    </tr>
                </tbody>
            </table>



        </asp:View>
        <asp:View ID="tab2" runat="server">
            <blockquote class="danger" style="font-size: small; background-color: powderblue;">
                <h4><b>ประเภท SAP</b></h4>
            </blockquote>
            <%--  <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                <tbody>
                      <tr class="alert-info info" style="font-size: Small;">
                        <th scope="col" colspan="9" style="width: 100%;">ประเภท SAP</th>
                    </tr>
                </tbody>
            </table>--%>
            <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                <tbody>
                    <%--   <tr class="alert-success success" style="font-size: Small;">
                        <th scope="col" colspan="9" style="width: 100%;">ประเภท SAP</th>
                    </tr>--%>
                    <tr class="alert-success success" style="font-size: Small; height: 30px;">
                        <th scope="col" style="width: 5%;">ประเภทสินทรัพย์</th>
                        <th scope="col" style="width: 2%;"></th>
                        <th scope="col" style="width: 5%;">ประเภท License</th>
                        <th scope="col" style="width: 30%;">ชื่อ ประเภท License</th>
                        <th scope="col" style="width: 15%;">เพิ่มเติม</th>
                        <th scope="col" style="width: 30%;">เงื่อนไข</th>
                        <th scope="col" style="width: 7%;">ราคา/หน่วย</th>
                        <th scope="col" style="width: 5%;">ค่า MA/ปี</th>

                    </tr>
                    <tr style="font-size: Small;">
                        <td>License</td>
                        <td>SAP</td>
                        <td>FN</td>
                        <td>SAP Logistics User (Handheld)</td>
                        <td>สำหรับผู้ใช้งาน WM</td>
                        <td>License SAP ขั้นต่ำ 10 License รวมกัน</td>
                        <td>35,000</td>
                        <td>7,700</td>
                    </tr>
                    <tr style="font-size: Small;">
                        <td>License</td>
                        <td>SAP</td>
                        <td>CB</td>
                        <td>SAP Application Professional</td>
                        <td>สำหรับ User ใช้งานทั่วไป</td>
                        <td>License SAP ขั้นต่ำ 10 License รวมกัน</td>
                        <td>90,000</td>
                        <td>19,800</td>
                    </tr>
                    <tr style="font-size: Small;">
                        <td>License</td>
                        <td>BI</td>
                        <td>BI</td>
                        <td>SAP BusinessObjects (BI)</td>
                        <td>สำหรับ User ใช้งานทั่วไป</td>
                        <td>License SAP ขั้นต่ำ 5 License รวมกัน</td>
                        <td>100,000</td>
                        <td>22,000</td>
                    </tr>
                     <tr style="font-size: Small;">
                        <td>License</td>
                        <td>TM1</td>
                        <td>USER</td>
                        <td>Planning TM1 User Include MA 1 Year</td>
                        <td>สำหรับ User ใช้งานทั่วไป</td>
                        <td>License  TM1 ขั้นต่ำ 5 License <span style="color:red">รวมกัน  MA +10% ต่อปี</span> </td>
                        <td>80,500</td>
                        <td>-</td>
                    </tr>
                      <tr style="font-size: Small;">
                        <td>License</td>
                        <td>Tableau</td>
                        <td>USER</td>
                        <td>Web Client - Interactor License Include MA 1 Year </td>
                        <td>สำหรับ User ใช้งานทั่วไป</td>
                        <td>License  TM1 ขั้นต่ำ 5 License <span style="color:red">รวมกัน  MA +10% ต่อปี</span> </td>
                        <td>36,000</td>
                        <td>-</td>
                    </tr>
                     <%-- <tr style="font-size: Small;">
                        <td>License</td>
                        <td>-</td>
                        <td>-</td>
                        <td>Windows 10 Pro</td>
                        <td>สำหรับ User ใช้งานทั่วไป</td>
                        <td>ซื้อถาวร</td>
                        <td>9,000</td>
                        <td>-</td>
                    </tr>
                     <tr style="font-size: Small;">
                        <td>License</td>
                        <td>-</td>
                        <td>-</td>
                        <td>Microsoft Office 2016 (Standard)</td>
                        <td>สำหรับ User ใช้งานทั่วไป</td>
                        <td>ซื้อถาวร</td>
                        <td>15,000</td>
                        <td>-</td>
                    </tr>
                     <tr style="font-size: Small;">
                        <td>License</td>
                        <td>-</td>
                        <td>-</td>
                        <td>Microsoft Office 2016 (Professional)</td>
                        <td>สำหรับ User ใช้งานทั่วไป</td>
                        <td>ซื้อถาวร</td>
                        <td>18,000</td>
                        <td>-</td>
                    </tr>
                      <tr style="font-size: Small;">
                        <td>License</td>
                        <td>-</td>
                        <td>-</td>
                        <td>Microsoft Office 2016 (Standard) of Mac</td>
                        <td>สำหรับ User ใช้งานทั่วไป</td>
                        <td>ซื้อถาวร</td>
                        <td>15,000</td>
                        <td>-</td>
                    </tr>
                        <tr style="font-size: Small;">
                        <td>License</td>
                        <td>-</td>
                        <td>-</td>
                        <td>Gmail</td>
                        <td>สำหรับ User ใช้งานทั่วไป</td>
                        <td>ราคาต่อปี</td>
                        <td>1,200</td>
                        <td>-</td>
                    </tr>
                     <tr style="font-size: Small;">
                        <td>License</td>
                        <td>-</td>
                        <td>-</td>
                        <td>Symantec</td>
                        <td>สำหรับ User ใช้งานทั่วไป</td>
                        <td>ราคาต่อปี</td>
                        <td>1,400</td>
                        <td>-</td>
                    </tr>--%>
                </tbody>
            </table>


        </asp:View>
        <asp:View ID="tab3" runat="server">

            <blockquote class="danger" style="font-size: small; background-color: powderblue;">
                <h4><b>ประเภท Network</b></h4>
            </blockquote>

            <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                <tbody>
                    <%--  <tr class="alert-success success" style="font-size: Small;">
                        <th scope="col" colspan="9" style="width: 100%;">ประเภท NetWork </th>
                    </tr>--%>
                    <tr class="alert-success success" style="font-size: Small; height: 30px;">
                        <th scope="col" style="width: 25%;">ประเภทสินทรัพย์</th>
                        <th scope="col" style="width: 25%;">ชื่อ ประเภท License</th>
                        <th scope="col" style="width: 25%;">เงื่อนไข</th>
                        <th scope="col" style="width: 25%;">ราคา/หน่วย</th>


                    </tr>
                    <tr style="font-size: Small;">
                        <td>License</td>
                        <td>Windows Server 2012, 2016</td>
                        <td>ราคา/เดือน</td>
                        <td>800</td>
                    </tr>
                    <tr style="font-size: Small;">
                        <td>License</td>
                        <td>SQL Server 2012 (2 Core Pack)</td>
                        <td>ราคา/เดือน</td>
                        <td>6,000</td>
                    </tr>

                </tbody>
            </table>

        </asp:View>
        <asp:View ID="tab4" runat="server">

            <blockquote class="danger" style="font-size: small; background-color: powderblue;">
                <h4><b>ประเภท Domain</b></h4>
            </blockquote>
            <%--    <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                <tbody>
                      <tr class="alert-info info" style="font-size: Small;">
                        <th scope="col" colspan="9" style="width: 100%;">ประเภท Domain / ราคาโดยประมาณ </th>
                    </tr>
                </tbody>
            </table>--%>
            <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                <tbody>
                    <%--  <tr class="alert-success success" style="font-size: Small;">
                        <th scope="col" colspan="5" style="width: 100%;">ประเภท Domain / ราคาโดยประมาณ </th>
                    </tr>--%>
                    <tr class="alert-success success" style="font-size: Small; height: 30px;">
                        <th scope="col" style="width: 20%;">ชื่อ - นามสกุล</th>
                        <th scope="col" style="width: 10%;">1 ปี</th>
                        <th scope="col" style="width: 10%;">2 ปี</th>
                        <th scope="col" style="width: 10%;">3 ปี</th>
                        <th scope="col" style="width: 50%;">เอกสารเพิ่มเติม</th>


                    </tr>
                    <tr style="font-size: Small;">
                        <td>.com</td>
                        <td>495</td>
                        <td>979</td>
                        <td>1,452</td>
                        <td>แจ้งจดในนาม, ระยะเวลา</td>
                    </tr>
                    <tr style="font-size: Small;">
                        <td>.co.th</td>
                        <td>880</td>
                        <td>1,705</td>
                        <td>2,530</td>
                        <td>แจ้งจดในนาม, ระยะเวลา, หนังสือรับรองบริษัท พร้อมเซ็นกำกับ</td>
                    </tr>
                    <tr style="font-size: Small;">
                        <td>.co</td>
                        <td>1,045</td>
                        <td>2,035</td>
                        <td>3,025</td>
                        <td>แจ้งจดในนาม, ระยะเวลา</td>
                    </tr>
                    <tr style="font-size: Small;">
                        <td>.me</td>
                        <td>1,045</td>
                        <td>2,035</td>
                        <td>3,025</td>
                        <td>แจ้งจดในนาม, ระยะเวลา</td>
                    </tr>
                    <tr style="font-size: Small;">
                        <td>.in.th</td>
                        <td>440</td>
                        <td>875</td>
                        <td>1,309</td>
                        <td>แจ้งจดในนาม, ระยะเวลา</td>
                    </tr>
                    <tr class="alert-danger danger" style="font-size: Small;">
                        <th scope="col" colspan="5" style="width: 100%;">** Domain อื่นๆ สามารถสอบถามเพิ่มเติมได้ครับ / ทาง MIS จะดำเนินการประสานงานให้ครับ  </th>
                    </tr>
                </tbody>
            </table>

            <blockquote class="danger" style="font-size: small; background-color: powderblue;">
                <h4><b>ประเภท Hosting</b></h4>
            </blockquote>
            <%-- <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                <tbody>
                      <tr class="alert-info info" style="font-size: Small;">
                        <th scope="col" colspan="9" style="width: 100%;">ประเภท Hosting / ราคาโดยประมาณ </th>
                    </tr>
                </tbody>
                  </table>--%>
            <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
                <tbody>
                    <%--  <tr class="alert-success success" style="font-size: Small;">
                        <th scope="col" colspan="6" style="width: 100%;">ประเภท Hosting / ราคาโดยประมาณ </th>
                    </tr>--%>
                    <tr class="alert-success success" style="font-size: Small; height: 30px;">
                        <th scope="col" style="width: 20%;">รายละเอียด</th>
                        <th scope="col" style="width: 10%;">Pack 1</th>
                        <th scope="col" style="width: 10%;">Pack 2</th>
                        <th scope="col" style="width: 10%;">Pack 3</th>
                        <th scope="col" style="width: 10%;">Pack 4</th>
                        <th scope="col" style="width: 10%;">Pack 5</th>


                    </tr>
                    <tr style="font-size: Small;">
                        <td>ค่าบริการ / เดือน</td>
                        <td>495</td>
                        <td>935</td>
                        <td>1,375</td>
                        <td>2,145</td>
                        <td>3,245</td>
                    </tr>
                    <tr style="font-size: Small;">
                        <td>พื้นที่ใช้งาน</td>
                        <td>10 GB</td>
                        <td>30 GB</td>
                        <td>80 GB</td>
                        <td>200 GB</td>
                        <td>500 GB</td>
                    </tr>
                    <tr class="alert-danger danger" style="font-size: Small;">
                        <th scope="col" colspan="6" style="width: 100%;">** Hosting อื่นๆ สามารถสอบถามเพิ่มเติมได้ครับ / ทาง MIS จะดำเนินการประสานงานให้ครับ  </th>
                    </tr>
                </tbody>
            </table>



        </asp:View>
    </asp:MultiView>

</asp:Content>

