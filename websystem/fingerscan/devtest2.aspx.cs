using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Net;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using System.Xml.Linq;
using System.Xml.Serialization;

using Suprema;
using System.Runtime.InteropServices;

public partial class websystem_fingerscan_devtest2 : System.Web.UI.Page
{
    #region initial function/data
    int emp_idx = 0;
    int defaultInt = 0;
    IPAddress defaultIP = new IPAddress(0);

    // static string Server = "172.16.11.214";
    // static string Username = "admin";
    // static string Password = "1qaz@WSX";
    // static string Port = "4590";
    // static string BaseUri = "WebSdk";
    // static string ApplicationId = "KxsD11z743Hf5Gq9mv3+5ekxzemlCiUXkTFY5ba1NOGcLCmGstt2n0zYE9NsNimv";
    // static string Host = Server + ":" + Port;

    // /// <summary>
    // /// The guid of the Admin in the Directory
    // /// </summary>
    // private static readonly string AdminGuid = "00000000-0000-0000-0000-000000000003";

    // private static readonly int HighestPort = 65535;

    // private static readonly Regex Numeric = new Regex(@"^[0-9]+$", RegexOptions.Compiled | RegexOptions.IgnoreCase);

    // /// <summary>
    // /// The Guid of the SstemConfiguration in the Directory
    // /// </summary>
    // private static readonly string SystemConfigurationGuid = "00000000-0000-0000-0000-000000000007";

    // public bool bIsConnected = false;//the boolean value identifies whether the device is connected

    function_tool _funcTool = new function_tool();

    // Biostar2 SDK
    IntPtr context = IntPtr.Zero;
    BS2ErrorCode result;
    UInt16 port = BS2Environment.BS2_TCP_DEVICE_PORT_DEFAULT;
    string deviceIpAddress = "";
    UInt32 deviceId = 0;
    BS2SimpleDeviceInfo deviceInfo;
    UInt32 numUid = 0;
    API.IsAcceptableUserID ptrIsAcceptableUserID = null; // we don't need to user id filtering
    IntPtr uidsObjs = IntPtr.Zero;
    BS2UserBlob[] userBlob;
    BS2UserBlobEx[] userBlobEx;
    // BS2UserSmallBlobEx[] userSmallBlobEx;
    IntPtr uids = IntPtr.Zero;
    UInt32 uidCount = 0;
    // BS2_USER_MASK userMask;
    BS2SystemConfig systemConfig;
    BS2Configs configs;
    IntPtr logsObj = IntPtr.Zero;
    UInt32 numLog = 0;
    UInt32 gmtTime = 0;

    private const int USER_PAGE_SIZE = 1024;
    #endregion initial function/data

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!(Session["emp_idx"] != null && (Session["emp_idx"].ToString() != "172" || Session["emp_idx"].ToString() != "23069")))
        {
            string path = HttpContext.Current.Request.Url.AbsolutePath;
            Response.Redirect(ResolveUrl("~/"));
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        emp_idx = int.Parse(Session["emp_idx"].ToString());
    }

    #region button command
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {
            case "cmdGetVersion":
                IntPtr versionPtr = API.BS2_Version();
                litResult.Text += String.Format("SDK version : {0}", Marshal.PtrToStringAnsi(versionPtr)) + setNewline();
                break;
            case "cmdAllocate":
                context = API.BS2_AllocateContext();
                if (context == IntPtr.Zero)
                {
                    litResult.Text += "BS2_AllocateContext failed." + setNewline();
                }
                else
                {
                    litResult.Text += context.ToString() + setNewline();
                }
                break;
            case "cmdRelease":
                API.BS2_ReleaseContext(context);
                litResult.Text += "BS2_ReleaseContext success" + setNewline();
                break;
            case "cmdInitialize":
                context = API.BS2_AllocateContext();
                result = (BS2ErrorCode)API.BS2_Initialize(context);
                if (result != BS2ErrorCode.BS_SDK_SUCCESS)
                {
                    litResult.Text += String.Format("BS2_Initialize failed. error : {0}", result) + setNewline();
                    API.BS2_ReleaseContext(context);
                }
                else
                {
                    litResult.Text += "BS2_Initialize success." + setNewline();
                }
                break;
            case "cmdConnect":
                deviceIpAddress = tbIp.Text.Trim();
                // allocate context
                context = API.BS2_AllocateContext();
                // initialize
                result = (BS2ErrorCode)API.BS2_Initialize(context);
                if (result != BS2ErrorCode.BS_SDK_SUCCESS)
                {
                    litResult.Text += String.Format("BS2_Initialize failed. error : {0}", result) + setNewline();
                    API.BS2_ReleaseContext(context);
                }
                else
                {
                    litResult.Text += "BS2_Initialize success." + setNewline();
                    // connect device
                    result = (BS2ErrorCode)API.BS2_ConnectDeviceViaIP(context, deviceIpAddress, port, out deviceId);
                    if (result != BS2ErrorCode.BS_SDK_SUCCESS)
                    {
                        litResult.Text += String.Format("BS2_ConnectDeviceViaIP failed. error : {0}", result) + setNewline();
                        API.BS2_ReleaseContext(context);
                    }
                    else
                    {
                        litResult.Text += "BS2_ConnectDeviceViaIP success. Device id is " + deviceId.ToString() + setNewline();

                        // ****************************** start of process ******************************//
                        try
                        {
                            // get device info
                            result = (BS2ErrorCode)API.BS2_GetDeviceInfo(context, deviceId, out deviceInfo);
                            if (result != BS2ErrorCode.BS_SDK_SUCCESS)
                            {
                                litResult.Text += String.Format("BS2_GetDeviceInfo failed. error : {0}", result) + setNewline();
                            }
                            else
                            {
                                litResult.Text += "BS2_GetDeviceInfo success. Device id is " + deviceId.ToString() + setNewline();
                                litResult.Text += "BS2_GetDeviceInfo success. Device maximum no of user is " + deviceInfo.maxNumOfUser.ToString() + setNewline();
                                litResult.Text += "BS2_GetDeviceInfo success. Device type is " + API.productNameDictionary[(BS2DeviceTypeEnum)deviceInfo.type].ToString() + setNewline();
                            }

                            // reset config ******************* danger *******************
                            // result = (BS2ErrorCode)API.BS2_ResetConfig(context, deviceId, true);
                            // if (result != BS2ErrorCode.BS_SDK_SUCCESS)
                            // {
                            //     litResult.Text += String.Format("BS2_ResetConfig failed. error : {0}", result) + setNewline();
                            // }
                            // else
                            // {
                            //     litResult.Text += "BS2_ResetConfig success."+ setNewline();
                            // }
                            // reset config ******************* danger *******************

                            // get user list then use for API.BS2_ReleaseObject(uidsObjs); release object
                            result = (BS2ErrorCode)API.BS2_GetUserList(context, deviceId, out uidsObjs, out numUid, ptrIsAcceptableUserID);
                            if (result != BS2ErrorCode.BS_SDK_SUCCESS)
                            {
                                litResult.Text += String.Format("BS2_GetUserList failed. error : {0}", result) + setNewline();
                            }
                            else
                            {
                                litResult.Text += "BS2_GetUserList success." + setNewline();
                                litResult.Text += String.Format("numUid is : {0}", numUid.ToString()) + setNewline();
                                litResult.Text += String.Format("uidsObjs is : {0}", uidsObjs.ToString()) + setNewline();

                                if (numUid > 0)
                                {
                                    IntPtr curUidObbjs = uidsObjs;
                                    BS2UserBlob[] userBlobs = new BS2UserBlob[USER_PAGE_SIZE];

                                    for (UInt32 idx = 0; idx < numUid; idx++)
                                    {
                                        UInt32 available = numUid - idx;
                                        if (available > USER_PAGE_SIZE)
                                        {
                                            available = USER_PAGE_SIZE;
                                        }

                                        result = (BS2ErrorCode)API.BS2_GetUserDatas(context, deviceId, curUidObbjs, available, userBlob, (UInt32)BS2UserMaskEnum.ALL);

                                        if (result != BS2ErrorCode.BS_SDK_SUCCESS)
                                        {
                                            litResult.Text += String.Format("BS2_GetUserDatas failed. error : {0}", result) + setNewline();
                                        }
                                        else
                                        {
                                            for (UInt32 loop = 0; loop < available; loop++)
                                            {
                                                litResult.Text += String.Format("BS2_GetUserDatas data : {0}", userBlobs[loop].user) + setNewline();
                                                // don't need to release cardObjs, fingerObjs, faceObjs because we get only BS2User
                                                if (userBlob[loop].cardObjs != IntPtr.Zero)
                                                    API.BS2_ReleaseObject(userBlob[loop].cardObjs);
                                                if (userBlob[loop].fingerObjs != IntPtr.Zero)
                                                    API.BS2_ReleaseObject(userBlob[loop].fingerObjs);
                                                if (userBlob[loop].faceObjs != IntPtr.Zero)
                                                    API.BS2_ReleaseObject(userBlob[loop].faceObjs);
                                            }

                                            idx += available;
                                            curUidObbjs += (int)available*BS2Environment.BS2_USER_ID_SIZE;
                                        }
                                    }
                                }
                            }

                            // // set data
                            // uids = new IntPtr(00000020);
                            // uidCount = numUid;

                            // // get user infos
                            // result = (BS2ErrorCode)API.BS2_GetUserInfos(context, deviceId, uids, uidCount, userBlob);
                            // // result = (BS2ErrorCode)API.BS2_GetUserInfosEx(context, deviceId, uids, uidCount, userBlobEx);
                            // if (result != BS2ErrorCode.BS_SDK_SUCCESS)
                            // {
                            //     litResult.Text += String.Format("BS2_GetUserInfos failed. error : {0}", result) + setNewline();
                            // }
                            // else
                            // {
                            //     litResult.Text += "BS2_GetUserInfos success." + setNewline();
                            // }

                            // remove all user ******************* danger *******************
                            // result = (BS2ErrorCode)API.BS2_RemoveAllUser(context, deviceId);
                            // if (result != BS2ErrorCode.BS_SDK_SUCCESS)
                            // {
                            //     litResult.Text += String.Format("BS2_RemoveAllUser failed. error : {0}", result) + setNewline();
                            // }
                            // else
                            // {
                            //     litResult.Text += "BS2_RemoveAllUser success." + setNewline();
                            // }
                            // remove all user ******************* danger *******************

                            // clear log ******************* danger *******************
                            // result = (BS2ErrorCode)API.BS2_ClearLog(context, deviceId);
                            // if (result != BS2ErrorCode.BS_SDK_SUCCESS)
                            // {
                            //     litResult.Text += String.Format("BS2_ClearLog failed. error : {0}", result) + setNewline();
                            // }
                            // else
                            // {
                            //     litResult.Text += "BS2_ClearLog success." + setNewline();
                            // }
                            // clear log ******************* danger *******************

                            // // remove user
                            // result = (BS2ErrorCode)API.BS2_RemoveUser(context, deviceId, uids, uidCount);
                            // if (result != BS2ErrorCode.BS_SDK_SUCCESS)
                            // {
                            //     litResult.Text += String.Format("BS2_RemoveUser failed. error : {0}", result) + setNewline();
                            // }
                            // else
                            // {
                            //     litResult.Text += "BS2_RemoveUser success." + setNewline();
                            // }

                            // // get all log
                            // result = (BS2ErrorCode)API.BS2_GetLog(context, deviceId, 0, 0, out logsObj, out numLog);
                            // if (result != BS2ErrorCode.BS_SDK_SUCCESS)
                            // {
                            //     litResult.Text += String.Format("BS2_GetLog failed. error : {0}", result) + setNewline();
                            // }
                            // else
                            // {
                            //     litResult.Text += "BS2_GetLog success." + setNewline();
                            //     for (int i = 0; i < numLog; i++)
                            //     {
                            //         litResult.Text += logsObj.ToString() + " | " 
                            //                         //+ logsObj[i].dateTime.ToString()
                            //                         + setNewline();
                            //     }
                            // }
                            // API.BS2_ReleaseObject(logsObj);

                            // // enroll user
                            // BS2User _userInfo = new BS2User();
                            // _userInfo.userID = Encoding.ASCII.GetBytes("5");;
                            // _userInfo.flag = 0x01;
                            // B2UserSetting _userSetting = new B2UserSetting();
                            // _userSetting.startTime = 0;
                            // _userSetting.endTime = 0;
                            // _userSetting.fingerSuthMode = 1;// Uses fingerprint and PIN authentication
                            // _userSetting.securityLevel = 3;// Normal security level

                            // userBlob = new BS2UserBlobEx[1];
                            // userBlob[0].user = _userInfo;
                            // // userBlob.setting = _userSetting;
                            // userBlob[0].name = new byte[BS2Environment.BS2_USER_NAME_LEN];
                            // userBlob[0].name = Encoding.ASCII.GetBytes("60004285");
                            // userBlob[0].pin = new byte[BS2Environment.BS2_PIN_HASH_SIZE];
                            // userBlob[0].pin = Encoding.ASCII.GetBytes("1234");

                            // result = (BS2ErrorCode)API.BS2_EnrolUserEx(context, deviceId, userBlob, 1, 0);// userCount, overwrite
                            // if (result != BS2ErrorCode.BS_SDK_SUCCESS)
                            // {
                            //     litResult.Text += String.Format("BS2_EnrolUserEx failed. error : {0}", result) + setNewline();
                            // }
                            // else
                            // {
                            //     litResult.Text += "BS2_EnrolUserEx success." + setNewline();
                            // }

                            // get user datas
                            // result = (BS2ErrorCode)API.BS2_GetUserDatas(context, deviceId, uids, uidCount, out userBlob, 0x0001);// userMask
                            // if (result != BS2ErrorCode.BS_SDK_SUCCESS)
                            // {
                            //     litResult.Text += String.Format("BS2_GetUserDatas failed. error : {0}", result) + setNewline();
                            // }
                            // else
                            // {
                            //     litResult.Text += "BS2_GetUserDatas success." + setNewline();
                            // }
                            // ************************************* //
                            // // get config
                            // result = (BS2ErrorCode)API.BS2_GetConfig(context, deviceId, ref configs);
                            // if (result != BS2ErrorCode.BS_SDK_SUCCESS)
                            // {
                            //     litResult.Text += String.Format("BS2_GetConfig failed. error : {0}", result) + setNewline();
                            // }
                            // else
                            // {
                            //     litResult.Text += "BS2_GetConfig success." + setNewline();
                            // }

                            // get system config
                            // result = (BS2ErrorCode)API.BS2_GetSystemConfig(context, deviceId, out systemConfig);
                            // if (result != BS2ErrorCode.BS_SDK_SUCCESS)
                            // {
                            //     litResult.Text += String.Format("BS2_GetSystemConfig failed. error : {0}", result) + setNewline();
                            // }
                            // else
                            // {
                            //     litResult.Text += "BS2_GetSystemConfig success." + setNewline();
                            //     litResult.Text += String.Format("Time zone is : {0}", (systemConfig.timezone/(60*60)).ToString()) + setNewline(); // timezone in seconds
                            // }

                            // get GMT time
                            // result = (BS2ErrorCode)API.BS2_GetDeviceTime(context, deviceId, out gmtTime);
                            // if (result != BS2ErrorCode.BS_SDK_SUCCESS)
                            // {
                            //     litResult.Text += String.Format("BS2_GetDeviceTime failed. error : {0}", result) + setNewline();
                            // }
                            // else
                            // {
                            //     litResult.Text += "BS2_GetDeviceTime success." + setNewline();
                            //     DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);
                            //     DateTime currentTime = origin.AddSeconds(gmtTime);
                            //     DateTime currentTimeWT = origin.AddSeconds(gmtTime + systemConfig.timezone);
                            //     litResult.Text += String.Format("Device time without timezone : {0}", currentTime.ToString("yyyy-MM-dd HH:mm:ss")) + setNewline();
                            //     litResult.Text += String.Format("Device time with timezone : {0}", currentTimeWT.ToString("yyyy-MM-dd HH:mm:ss")) + setNewline();
                            // }
                            // ************************************* //

                            // release object
                            // API.BS2_ReleaseObject(uidsObjs);

                            // ****************************** end of process ******************************//
                        }
                        catch (Exception ex)
                        {
                            litResult.Text += ex.Message + setNewline();
                        }
                        finally
                        {
                            // disconnect device
                            result = (BS2ErrorCode)API.BS2_DisconnectDevice(context, deviceId);
                            if (result != BS2ErrorCode.BS_SDK_SUCCESS)
                            {
                                litResult.Text += String.Format("BS2_DisconnectDevice failed. error : {0}", result) + setNewline();
                            }
                            else
                            {
                                litResult.Text += "BS2_DisconnectDevice success." + setNewline();
                            }
                        }
                    }
                    // release context
                    API.BS2_ReleaseContext(context);
                }
                break;
                // case "cmdDisconnect":
                //     // context = API.BS2_AllocateContext();
                //     // result = (BS2ErrorCode)API.BS2_Initialize(context);
                //     // litResult.Text += "deviceId = " + deviceId.ToString() + setNewline();
                //     result = (BS2ErrorCode)API.BS2_DisconnectDevice(context, 540085574);
                //     if (result != BS2ErrorCode.BS_SDK_SUCCESS)
                //     {
                //         litResult.Text += String.Format("BS2_DisconnectDevice failed. error : {0}", result) + setNewline();
                //     }
                //     else
                //     {
                //         litResult.Text += "BS2_DisconnectDevice success." + setNewline();
                //     }
                //     API.BS2_ReleaseContext(context);
                //     break;
        }

        //string _template = String.Empty;
        //string _sub_templte = String.Empty;
        //string _query = String.Empty;
        //string _contents = String.Empty;
        //Method _method = new Method();
        //string _retData = String.Empty;

        //_template = ddlTemplate.SelectedValue;
        //_sub_templte = tbSubTemplate.Text.Trim();
        //_query = tbQuery.Text.Trim();

        //switch (cmdName)
        //{
        //    case "cmdGet":
        //        _method = Method.GET;
        //        break;
        //    case "cmdPost":
        //        _method = Method.POST;
        //        break;
        //    case "cmdDelete":
        //        _method = Method.DELETE;
        //        break;
        //    case "cmdPut":
        //        _method = Method.PUT;
        //        break;
        //}

        //_contents = _template + (_sub_templte != "" ? ("/" + _sub_templte) : "") + (_query != "" ? _query : "");

        ////litDebug.Text = _contents;

        //// write response data
        //try
        //{
        //    _retData = callApi(_method, _contents);

        //    // remove special string &#65279
        //    if (_retData.StartsWith("\ufeff"))
        //    {
        //        _retData = _retData.Substring(1);
        //    }

        //    litResult.Text = HttpUtility.HtmlEncode(_retData) + setNewline();
        //}
        //catch (Exception ex)
        //{
        //    litResult.Text += ex.Message + setNewline();
        //}

        //// convert json and write response status
        //try
        //{
        //    dynamic _results = JsonConvert.DeserializeObject<dynamic>(_retData);
        //    string _status = _results.Rsp.Status;

        //    litResult.Text += _status + setNewline();

        //    // check response status
        //    switch (_status)
        //    {
        //        case "Ok":
        //            // foreach (var item in _results.Rsp.Result)
        //            // {
        //            //     litResult.Text += item.Guid + setNewline();
        //            // }

        //            // DataTable dt = JsonConvert.DeserializeObject<DataTable>(JsonConvert.SerializeObject(_results.Rsp.Result));
        //            // Rsp[] dt = JsonConvert.DeserializeObject<Rsp[]>(JsonConvert.SerializeObject(_results.Rsp.Result));
        //            //  List<Rsp> dt = JsonConvert.DeserializeObject<List<Rsp>>(JsonConvert.SerializeObject(_results.Rsp.Result));

        //            Rsp _tempReturn = _funcTool.convertJsonToObject(typeof(Rsp), _results.ToString());
        //            // litResult.Text += _tempReturn + setNewline();

        //            switch (_template)
        //            {
        //                case "entity":
        //                    if (_sub_templte == String.Empty)
        //                    {
        //                        gvResult.DataSource = _tempReturn.Result;//[0].Entity;
        //                        gvResult.DataBind();
        //                    }
        //                    else
        //                    {
        //                        gvResult.DataSource = _tempReturn.Result[0].Entity;
        //                        gvResult.DataBind();
        //                    }
        //                    break;
        //                case "report":
        //                    gvResult.DataSource = _tempReturn.Result;
        //                    gvResult.DataBind();
        //                    break;
        //                case "customField":
        //                    gvResult.DataSource = _tempReturn.Result;
        //                    gvResult.DataBind();
        //                    break;
        //            }
        //            break;
        //        case "Fail":
        //            litResult.Text += (_results.Rsp.Result.SdkErrorCode != null ? (_results.Rsp.Result.SdkErrorCode + " : ") : "") + _results.Rsp.Result.Message + setNewline();
        //            gvResult.DataSource = null;
        //            gvResult.DataBind();
        //            break;
        //    }
        //}
        //catch (Exception ex)
        //{
        //    litResult.Text += ex.Message + setNewline();

        //    // clear GridView
        //    gvResult.DataSource = null;
        //    gvResult.DataBind();
        //}

        //// change response data to table
        //// try
        //// {
        ////     StringReader theReader = new StringReader(_retData);
        ////     DataSet theDataSet = new DataSet();
        ////     theDataSet.ReadXml(theReader);

        ////     gvResult.DataSource = theDataSet.Tables[0];
        ////     gvResult.DataBind();
        ////     setNewline();
        //// }
        //// catch (Exception ex)
        //// {
        ////     litResult.Text += ex.Message + setNewline();
        //// }

        //// get some value
        ////     try
        ////     {
        ////         XDocument doc;
        ////         doc = XDocument.Parse(_retData);
        ////         var q_status = (from x in doc.Elements("rsp")
        ////                         select x.Attribute("status").Value).First();

        ////         litResult.Text += q_status.ToString() + setNewline();

        ////         if (q_status == "ok")
        ////         {
        ////             foreach (XElement element in doc.Element("rsp").Element("QueryResult").Descendants())
        ////             {
        ////                 litResult.Text += element.Value + setNewline();
        ////             }
        ////         }
        ////         else if (q_status == "fail")
        ////         {
        ////             var q_status2 = (from x in doc.Element("rsp").Elements("error")
        ////                         select x.Attribute("code").Value).First();

        ////             XElement error_message = doc.Element("rsp").Element("error").Element("message");

        ////             litResult.Text += q_status2.ToString() + " : " + error_message.Value + setNewline();
        ////         }
        ////     }
        ////     catch (Exception ex)
        ////     {
        ////         litResult.Text += ex.Message + setNewline();
        ////     }
    }
    #endregion button command

    static public byte[] ToByteArray(object anyValue, int length)
    {
        if (length > 0)
        {
            int rawsize = Marshal.SizeOf(anyValue);
            IntPtr buffer = Marshal.AllocHGlobal(rawsize);
            Marshal.StructureToPtr(anyValue, buffer, false);
            byte[] rawdatas = new byte[rawsize * length];
            Marshal.Copy(buffer, rawdatas, (rawsize * (length - 1)), rawsize);
            Marshal.FreeHGlobal(buffer);
            return rawdatas;
        }
        return new byte[0];
    }

    #region events
    #endregion events

    #region reuse
    private string setNewline()
    {
        return "<br />----------<br />";
    }

    // private string callApi(Method methodType, string contents)
    // {
    //     var encodeContents = _funcTool.convEncodeUrl(contents);
    //     var client = new RestClient("http://" + Host + "/" + BaseUri + "/" + encodeContents);
    //     var request = new RestRequest(methodType);
    //     request.AddHeader("cache-control", "no-cache");
    //     request.AddHeader("Connection", "keep-alive");
    //     request.AddHeader("accept-encoding", "gzip, deflate");
    //     request.AddHeader("Host", Host);
    //     request.AddHeader("Cache-Control", "no-cache");
    //     request.AddHeader("Accept", "text/json");
    //     client.Authenticator = new HttpBasicAuthenticator(Username + ";" + ApplicationId, Password);
    //     IRestResponse response = client.Execute(request);

    //     return response.Content;
    // }
    #endregion reuse
}

[Serializable]
[XmlRoot("Rsp")]
public class Rsp
{
    [XmlElement("Status")]
    public string Status { get; set; }
    [XmlElement("Result")]
    public ResultDetail[] Result { get; set; }
    [XmlElement("Value")]
    public bool Value { get; set; }
    [XmlElement("ObsoletedMembers")]
    public ObsoletedMembersDetail[] ObsoletedMembers { get; set; }
}

[Serializable]
public class ResultDetail
{
    // report >> EntityConfiguration?q=EntityTypes@...
    [XmlElement("Guid")]
    public string Guid { get; set; }
    // report >> EntityConfiguration?q=EntityTypes@...

    // report >> DoorActivity?q=Doors@...
    [XmlElement("Timestamp")]
    public string Timestamp { get; set; }
    [XmlElement("EventType")]
    public int EventType { get; set; }
    [XmlElement("UnitGuid")]
    public string UnitGuid { get; set; }
    [XmlElement("DeviceGuid")]
    public string DeviceGuid { get; set; }
    [XmlElement("APGuid")]
    public string APGuid { get; set; }
    [XmlElement("SourceGuid")]
    public string SourceGuid { get; set; }
    [XmlElement("CredentialGuid")]
    public string CredentialGuid { get; set; }
    [XmlElement("CardholderGuid")]
    public string CardholderGuid { get; set; }
    [XmlElement("Credential2Guid")]
    public string Credential2Guid { get; set; }
    [XmlElement("TimeZone")]
    public string TimeZone { get; set; }
    [XmlElement("OccurrencePeriod")]
    public int OccurrencePeriod { get; set; }
    [XmlElement("AccessPointGroupGuid")]
    public string AccessPointGroupGuid { get; set; }
    // report >> DoorActivity?q=Doors@...

    // report >> activitytrails?q=Applications@SecurityDesk@ConfigTool
    [XmlElement("InitiatorEntityId")]
    public string InitiatorEntityId { get; set; }
    [XmlElement("InitiatorEntityName")]
    public string InitiatorEntityName { get; set; }
    [XmlElement("InitiatorEntityType")]
    public int InitiatorEntityType { get; set; }
    [XmlElement("InitiatorEntityTypeName")]
    public string InitiatorEntityTypeName { get; set; }
    [XmlElement("InitiatorEntityVersion")]
    public string InitiatorEntityVersion { get; set; }
    [XmlElement("ActivityType")]
    public int ActivityType { get; set; }
    [XmlElement("ActivityTypeName")]
    public string ActivityTypeName { get; set; }
    [XmlElement("ImpactedEntityId")]
    public string ImpactedEntityId { get; set; }
    [XmlElement("ImpactedEntityName")]
    public string ImpactedEntityName { get; set; }
    [XmlElement("ImpactedEntityType")]
    public int ImpactedEntityType { get; set; }
    [XmlElement("ImpactedEntityTypeName")]
    public string ImpactedEntityTypeName { get; set; }
    [XmlElement("InitiatorMachineName")]
    public string InitiatorMachineName { get; set; }
    [XmlElement("InitiatorApplicationType")]
    public int InitiatorApplicationType { get; set; }
    [XmlElement("InitiatorApplicationName")]
    public string InitiatorApplicationName { get; set; }
    [XmlElement("InitiatorApplicationVersion")]
    public string InitiatorApplicationVersion { get; set; }
    [XmlElement("EventTimestamp")]
    public string EventTimestamp { get; set; }
    // report >> activitytrails?q=Applications@SecurityDesk@ConfigTool

    // entity >> basic
    [XmlElement("Entity")]
    public EntityDetail[] Entity { get; set; }
    // entity >> basic

    // entity >> LogicalId(...,1)
    [XmlElement("ActivationDate")]
    public string ActivationDate { get; set; }
    [XmlElement("ActivationMode")]
    public ActivationModeDetail[] ActivationMode { get; set; }
    [XmlElement("ActiveDirectoryDomainName")]
    public string ActiveDirectoryDomainName { get; set; }
    [XmlElement("CanEscort")]
    public bool CanEscort { get; set; }
    [XmlElement("AntipassbackExemption")]
    public bool AntipassbackExemption { get; set; }
    [XmlElement("Credentials")]
    public string[] Credentials { get; set; }
    [XmlElement("EmailAddress")]
    public string EmailAddress { get; set; }
    [XmlElement("ExpirationDate")]
    public string ExpirationDate { get; set; }
    [XmlElement("ExpirationDuration")]
    public int ExpirationDuration { get; set; }
    [XmlElement("FirstName")]
    public string FirstName { get; set; }
    [XmlElement("Groups")]
    public string[] Groups { get; set; }
    [XmlElement("AccessPermissionLevel")]
    public int AccessPermissionLevel { get; set; }
    [XmlElement("AccessRules")]
    public string[] AccessRules { get; set; }
    [XmlElement("InheritAccessPermissionLevel")]
    public bool InheritAccessPermissionLevel { get; set; }
    [XmlElement("LastAccessPoint")]
    public LastAccessPointDetail[] LastAccessPoint { get; set; }
    [XmlElement("LastName")]
    public string LastName { get; set; }
    [XmlElement("Parents")]
    public string[] Parents { get; set; }
    [XmlElement("Picture")]
    public string Picture { get; set; }
    [XmlElement("PictureFileName")]
    public string PictureFileName { get; set; }
    [XmlElement("State")]
    public string State { get; set; }
    [XmlElement("Status")]
    public StatusDetail[] Status { get; set; }
    [XmlElement("UseExtendedGrantTime")]
    public bool UseExtendedGrantTime { get; set; }
    [XmlElement("Application")]
    public string Application { get; set; }
    [XmlElement("Behaviors")]
    public string[] Behaviors { get; set; }
    [XmlElement("EventToActions")]
    public string[] EventToActions { get; set; }
    [XmlElement("CreatedOn")]
    public string CreatedOn { get; set; }
    [XmlElement("CustomFields")]
    public CustomFieldsDetail[] CustomFields { get; set; }
    [XmlElement("Description")]
    public string Description { get; set; }
    [XmlElement("EntitySubType")]
    public int EntitySubType { get; set; }
    [XmlElement("EntityType")]
    public string EntityType { get; set; }
    [XmlElement("HiddenFromUI")]
    public bool HiddenFromUI { get; set; }
    [XmlElement("HierarchicalChildren")]
    public string[] HierarchicalChildren { get; set; }
    [XmlElement("HierarchicalParents")]
    public string[] HierarchicalParents { get; set; }
    [XmlElement("IsDisposed")]
    public bool IsDisposed { get; set; }
    [XmlElement("IsInMaintenance")]
    public bool IsInMaintenance { get; set; }
    [XmlElement("IsMaintenanceSupported")]
    public bool IsMaintenanceSupported { get; set; }
    [XmlElement("IsOnline")]
    public bool IsOnline { get; set; }
    [XmlElement("LinkedMaps")]
    public string[] LinkedMaps { get; set; }
    [XmlElement("LogicalId")]
    public string LogicalId { get; set; }
    [XmlElement("ModifiedOn")]
    public string ModifiedOn { get; set; }
    [XmlElement("MaintenanceEndTime")]
    public string MaintenanceEndTime { get; set; }
    [XmlElement("MaintenanceReason")]
    public string MaintenanceReason { get; set; }
    [XmlElement("Name")]
    public string Name { get; set; }
    [XmlElement("OwnerRole")]
    public string OwnerRole { get; set; }
    [XmlElement("OwnerRoleType")]
    public string OwnerRoleType { get; set; }
    [XmlElement("RunningState")]
    public string RunningState { get; set; }
    [XmlElement("SupportedCustomEvents")]
    public string[] SupportedCustomEvents { get; set; }
    [XmlElement("SupportedEvents")]
    public string[] SupportedEvents { get; set; }
    [XmlElement("Synchronised")]
    public bool Synchronised { get; set; }
    // entity >> LogicalId(...,1)
}

[Serializable]
public class EntityDetail
{
    [XmlElement("Application")]
    public string Application { get; set; }
    [XmlElement("Behaviors")]
    public string[] Behaviors { get; set; }
    [XmlElement("EventToActions")]
    public string[] EventToActions { get; set; }
    [XmlElement("CreatedOn")]
    public string CreatedOn { get; set; }
    [XmlElement("CustomFields")]
    public CustomFieldsDetail[] CustomFields { get; set; }
    [XmlElement("Description")]
    public string Description { get; set; }
    [XmlElement("EntitySubType")]
    public int EntitySubType { get; set; }
    [XmlElement("EntityType")]
    public string EntityType { get; set; }
    [XmlElement("Guid")]
    public string Guid { get; set; }
    [XmlElement("HiddenFromUI")]
    public bool HiddenFromUI { get; set; }
    [XmlElement("HierarchicalChildren")]
    public string[] HierarchicalChildren { get; set; }
    [XmlElement("HierarchicalParents")]
    public string[] HierarchicalParents { get; set; }
    [XmlElement("IsDisposed")]
    public bool IsDisposed { get; set; }
    [XmlElement("IsInMaintenance")]
    public bool IsInMaintenance { get; set; }
    [XmlElement("IsMaintenanceSupported")]
    public bool IsMaintenanceSupported { get; set; }
    [XmlElement("IsOnline")]
    public bool IsOnline { get; set; }
    [XmlElement("LinkedMaps")]
    public string[] LinkedMaps { get; set; }
    [XmlElement("LogicalId")]
    public string LogicalId { get; set; }
    [XmlElement("ModifiedOn")]
    public string ModifiedOn { get; set; }
    [XmlElement("MaintenanceEndTime")]
    public string MaintenanceEndTime { get; set; }
    [XmlElement("MaintenanceReason")]
    public string MaintenanceReason { get; set; }
    [XmlElement("Name")]
    public string Name { get; set; }
    [XmlElement("OwnerRole")]
    public string OwnerRole { get; set; }
    [XmlElement("OwnerRoleType")]
    public string OwnerRoleType { get; set; }
    [XmlElement("RunningState")]
    public string RunningState { get; set; }
    [XmlElement("SupportedCustomEvents")]
    public string[] SupportedCustomEvents { get; set; }
    [XmlElement("SupportedEvents")]
    public string[] SupportedEvents { get; set; }
    [XmlElement("Synchronised")]
    public bool Synchronised { get; set; }
}

[Serializable]
public class CustomFieldsDetail
{
    [XmlElement("CustomField")]
    public CustomFieldDetail[] CustomField { get; set; }
    [XmlElement("Name")]
    public string[] Name { get; set; }
    [XmlElement("ValueType")]
    public string[] ValueType { get; set; }
    [XmlElement("Value")]
    public string[] Value { get; set; }
}

[Serializable]
public class CustomFieldDetail
{
    [XmlElement("DefaultValue")]
    public string DefaultValue { get; set; }
    [XmlElement("CustomEntityTypeId")]
    public string CustomEntityTypeId { get; set; }
    [XmlElement("EntityType")]
    public string EntityType { get; set; }
    [XmlElement("GroupName")]
    public string GroupName { get; set; }
    [XmlElement("GroupPriority")]
    public int GroupPriority { get; set; }
    [XmlElement("Guid")]
    public string Guid { get; set; }
    [XmlElement("Mandatory")]
    public bool Mandatory { get; set; }
    [XmlElement("Name")]
    public string Name { get; set; }
    [XmlElement("Owner")]
    public string Owner { get; set; }
    [XmlElement("ShowInReports")]
    public bool ShowInReports { get; set; }
    [XmlElement("Unique")]
    public bool Unique { get; set; }
    [XmlElement("ValueType")]
    public string ValueType { get; set; }
    [XmlElement("CustomDataType")]
    public string CustomDataType { get; set; }
}

[Serializable]
public class ActivationModeDetail
{
    [XmlElement("TimeSpan")]
    public string TimeSpan { get; set; }
    [XmlElement("DateTime")]
    public string DateTime { get; set; }
}

[Serializable]
public class LastAccessPointDetail
{
    [XmlElement("AccessPointGuid")]
    public string AccessPointGuid { get; set; }
    [XmlElement("AccessPointDate")]
    public string AccessPointDate { get; set; }
    [XmlElement("AccessGranted")]
    public bool AccessGranted { get; set; }
}

[Serializable]
public class StatusDetail
{
    [XmlElement("ActivationDate")]
    public string ActivationDate { get; set; }
    [XmlElement("ActivationType")]
    public string ActivationType { get; set; }
    [XmlElement("ExpirationDate")]
    public string ExpirationDate { get; set; }
    [XmlElement("ExpirationType")]
    public string ExpirationType { get; set; }
    [XmlElement("ExpirationDuration")]
    public string ExpirationDuration { get; set; }
    [XmlElement("State")]
    public string State { get; set; }
}

[Serializable]
public class ObsoletedMembersDetail
{
    [XmlElement("Member")]
    public string Member { get; set; }
    [XmlElement("Message")]
    public string Message { get; set; }
}