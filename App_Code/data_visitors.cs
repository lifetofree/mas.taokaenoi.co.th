using System;
using System.Xml.Serialization;

[Serializable]
[XmlRoot ("data_visitors")]
public class data_visitors {
    [XmlElement ("return_code")]
    public int return_code { get; set; }

    [XmlElement ("return_msg")]
    public string return_msg { get; set; }

    [XmlElement ("return_idx")]
    public int return_idx { get; set; }

    [XmlElement ("return_node_idx")]
    public int return_node_idx { get; set; }

    [XmlElement ("return_actor_idx")]
    public int return_actor_idx { get; set; }

    [XmlElement ("vm_visitors_type_list_m0")]
    public vm_visitors_type_detail_m0[] vm_visitors_type_list_m0 { get; set; }

    [XmlElement ("search_visitor_type_list")]
    public search_visitor_type_detail[] search_visitor_type_list { get; set; }

    [XmlElement ("vm_vehicle_type_list_m0")]
    public vm_vehicle_type_detail_m0[] vm_vehicle_type_list_m0 { get; set; }

    [XmlElement ("vm_visitor_card_list_m0")]
    public vm_visitor_card_detail_m0[] vm_visitor_card_list_m0 { get; set; }

    [XmlElement ("vm_visitors_doc_list_u0")]
    public vm_visitors_doc_detail_u0[] vm_visitors_doc_list_u0 { get; set; }

    [XmlElement ("vm_visitors_doc_list_u1")]
    public vm_visitors_doc_detail_u1[] vm_visitors_doc_list_u1 { get; set; }

    [XmlElement ("vm_visitors_doc_list_u2")]
    public vm_visitors_doc_detail_u2[] vm_visitors_doc_list_u2 { get; set; }

    [XmlElement ("vm_visitors_doc_list_u3")]
    public vm_visitors_doc_detail_u3[] vm_visitors_doc_list_u3 { get; set; }

    [XmlElement ("vm_visitors_doc_list_l0")]
    public vm_visitors_doc_detail_l0[] vm_visitors_doc_list_l0 { get; set; }

    [XmlElement ("view_vm_visitors_doc_list_u3")]
    public view_vm_visitors_doc_detail_u3[] view_vm_visitors_doc_list_u3 { get; set; }

    [XmlElement ("vm_visitors_setperms_admin_list_m0")]
    public vm_visitors_setperms_admin_detail_m0[] vm_visitors_setperms_admin_list_m0 { get; set; }

    [XmlElement ("vm_visitors_menu_list_m0")]
    public vm_visitors_menu_detail_m0[] vm_visitors_menu_list_m0 { get; set; }

    [XmlElement ("vm_visitors_sendemail_list")]
    public vm_visitors_sendemail_detail[] vm_visitors_sendemail_list { get; set; }

    [XmlElement ("vm_visitor_vehicle_special_list_m0")]
    public vm_visitor_vehicle_special_detail_m0[] vm_visitor_vehicle_special_list_m0 { get; set; }

    [XmlElement ("vm_visitor_vehicle_special_list_l0")]
    public vm_visitor_vehicle_special_detail_l0[] vm_visitor_vehicle_special_list_l0 { get; set; }

    [XmlElement ("vm_visitors_security_list_m0")]
    public vm_visitors_security_detail_m0[] vm_visitors_security_list_m0 { get; set; }

    [XmlElement ("vm_visitors_board_list")]
    public vm_visitors_board_detail[] vm_visitors_board_list { get; set; }

    [XmlElement ("vm_visitors_report_search_list")]
    public vm_visitors_report_search_detail[] vm_visitors_report_search_list { get; set; }
}

#region master data
[Serializable]
public class vm_visitors_type_detail_m0 {
    [XmlElement ("m0_idx")]
    public int m0_idx { get; set; }

    [XmlElement ("visitor_type_name")]
    public string visitor_type_name { get; set; }

    [XmlElement ("flag_safety")]
    public int flag_safety { get; set; }

    [XmlElement ("flag_loading")]
    public int flag_loading { get; set; }

    [XmlElement ("type_status")]
    public int type_status { get; set; }

    [XmlElement ("create_date")]
    public string create_date { get; set; }

    [XmlElement ("update_date")]
    public string update_date { get; set; }
}

[Serializable]
public class search_visitor_type_detail {
    [XmlElement ("s_m0_idx")]
    public string s_m0_idx { get; set; }

    [XmlElement ("s_visitor_type_name")]
    public string s_visitor_type_name { get; set; }

    [XmlElement ("s_type_status")]
    public string s_type_status { get; set; }
}

[Serializable]
public class vm_vehicle_type_detail_m0 {
    [XmlElement ("m0_idx")]
    public int m0_idx { get; set; }

    [XmlElement ("vehicle_type_name")]
    public string vehicle_type_name { get; set; }

    [XmlElement ("type_status")]
    public int type_status { get; set; }

    [XmlElement ("create_date")]
    public string create_date { get; set; }

    [XmlElement ("update_date")]
    public string update_date { get; set; }
}

[Serializable]
public class vm_visitor_card_detail_m0 {
    [XmlElement ("m0_idx")]
    public int m0_idx { get; set; }

    [XmlElement ("visit_card_no")]
    public string visit_card_no { get; set; }

    [XmlElement ("visit_card_serial")]
    public string visit_card_serial { get; set; }

    [XmlElement ("visit_card_status")]
    public int visit_card_status { get; set; }

    [XmlElement ("create_date")]
    public string create_date { get; set; }

    [XmlElement ("update_date")]
    public string update_date { get; set; }
}

[Serializable]
public class vm_visitor_vehicle_special_detail_m0 {
    [XmlElement ("m0_idx")]
    public int m0_idx { get; set; }

    [XmlElement ("vehicle_plate_no")]
    public string vehicle_plate_no { get; set; }

    [XmlElement ("vehicle_plate_province")]
    public int vehicle_plate_province { get; set; }

    [XmlElement ("vehicle_state")]
    public int vehicle_state { get; set; }

    [XmlElement ("vehicle_status")]
    public int vehicle_status { get; set; }

    [XmlElement ("create_date")]
    public string create_date { get; set; }

    [XmlElement ("update_date")]
    public string update_date { get; set; }

    [XmlElement ("vehicle_plate_province_name")]
    public string vehicle_plate_province_name { get; set; }

    [XmlElement ("se_idx")]
    public int se_idx { get; set; }
}
#endregion master data

#region doc_u0
[Serializable]
public class vm_visitors_doc_detail_u0 {
    [XmlElement ("u0_idx")]
    public int u0_idx { get; set; }

    [XmlElement ("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement ("visitor_type_idx")]
    public int visitor_type_idx { get; set; }

    [XmlElement ("visit_date")]
    public string visit_date { get; set; }

    [XmlElement ("visit_emp_idx")]
    public int visit_emp_idx { get; set; }

    [XmlElement ("visit_title")]
    public string visit_title { get; set; }

    [XmlElement ("visit_detail")]
    public string visit_detail { get; set; }

    [XmlElement ("flag_safety")]
    public int flag_safety { get; set; }

    [XmlElement ("safety_comment")]
    public string safety_comment { get; set; }

    [XmlElement ("flag_vehicle")]
    public int flag_vehicle { get; set; }

    [XmlElement ("flag_walkin")]
    public int flag_walkin { get; set; }

    [XmlElement ("node_idx")]
    public int node_idx { get; set; }

    [XmlElement ("actor_idx")]
    public int actor_idx { get; set; }

    [XmlElement ("doc_status")]
    public int doc_status { get; set; }

    [XmlElement ("create_date")]
    public string create_date { get; set; }

    [XmlElement ("update_date")]
    public string update_date { get; set; }

    [XmlElement ("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement ("visit_emp_name_th")]
    public string visit_emp_name_th { get; set; }

    [XmlElement ("decision_idx")]
    public int decision_idx { get; set; }

    [XmlElement ("visitor_type_name")]
    public string visitor_type_name { get; set; }

    [XmlElement ("vehicle_type_name")]
    public string vehicle_type_name { get; set; }

    [XmlElement ("node_name")]
    public string node_name { get; set; }

    [XmlElement ("actor_name")]
    public string actor_name { get; set; }

    [XmlElement ("doc_status_name")]
    public string doc_status_name { get; set; }

    [XmlElement ("mail_list")]
    public string mail_list { get; set; }

    [XmlElement ("emp_idx_approve1")]
    public int emp_idx_approve1 { get; set; }

    [XmlElement ("emp_approve1")]
    public string emp_approve1 { get; set; }

    [XmlElement ("emp_idx_approve2")]
    public int emp_idx_approve2 { get; set; }

    [XmlElement ("emp_approve2")]
    public string emp_approve2 { get; set; }

    //start teppanop
    [XmlElement ("zmode")]
    public string zmode { get; set; }

    [XmlElement ("rpos_idx")]
    public int rpos_idx { get; set; }

    [XmlElement ("zcount")]
    public int zcount { get; set; }

    [XmlElement ("flag_loading")]
    public int flag_loading { get; set; }

    [XmlElement ("s_idcard")]
    public string s_idcard { get; set; }

    [XmlElement ("s_filter_keyword")]
    public string s_filter_keyword { get; set; }

    [XmlElement ("flag_arrived")]
    public int flag_arrived { get; set; }

    [XmlElement ("s_visit_name_th")]
    public string s_visit_name_th { get; set; }
    //end teppanop

    [XmlElement ("u1_idx")]
    public int u1_idx { get; set; }

    [XmlElement ("visit_prefix")]
    public string visit_prefix { get; set; }

    [XmlElement ("visit_firstname")]
    public string visit_firstname { get; set; }

    [XmlElement ("visit_lastname")]
    public string visit_lastname { get; set; }

    [XmlElement ("visit_identify_no")]
    public string visit_identify_no { get; set; }

    [XmlElement ("visit_phone_no")]
    public string visit_phone_no { get; set; }

    [XmlElement ("visit_card_no")]
    public string visit_card_no { get; set; }

    [XmlElement ("visit_card_ex_no")]
    public string visit_card_ex_no { get; set; }

    [XmlElement ("u2_idx")]
    public int u2_idx { get; set; }

    [XmlElement ("vehicle_type_idx")]
    public int vehicle_type_idx { get; set; }

    [XmlElement ("vehicle_plate_no")]
    public string vehicle_plate_no { get; set; }

    [XmlElement ("vehicle_plate_province")]
    public int vehicle_plate_province { get; set; }

    [XmlElement ("vehicle_plate_province_name")]
    public string vehicle_plate_province_name { get; set; }

    [XmlElement ("u3_idx")]
    public int u3_idx { get; set; }

    [XmlElement ("group_m0_idx")]
    public int group_m0_idx { get; set; }

    [XmlElement ("group_name")]
    public string group_name { get; set; }

    [XmlElement ("approve_status")]
    public int approve_status { get; set; }

    [XmlElement ("group_status")]
    public int group_status { get; set; }
}
#endregion doc_u0

#region  doc_u1
[Serializable]
public class vm_visitors_doc_detail_u1 {
    [XmlElement ("u1_idx")]
    public int u1_idx { get; set; }

    [XmlElement ("u0_idx")]
    public int u0_idx { get; set; }

    [XmlElement ("visit_prefix")]
    public string visit_prefix { get; set; }

    [XmlElement ("visit_firstname")]
    public string visit_firstname { get; set; }

    [XmlElement ("visit_lastname")]
    public string visit_lastname { get; set; }

    [XmlElement ("visit_prefix_en")]
    public string visit_prefix_en { get; set; }

    [XmlElement ("visit_firstname_en")]
    public string visit_firstname_en { get; set; }

    [XmlElement ("visit_lastname_en")]
    public string visit_lastname_en { get; set; }

    [XmlElement ("visit_house_no")]
    public string visit_house_no { get; set; }

    [XmlElement ("visit_village_no")]
    public string visit_village_no { get; set; }

    [XmlElement ("visit_lane")]
    public string visit_lane { get; set; }

    [XmlElement ("visit_road")]
    public string visit_road { get; set; }

    [XmlElement ("visit_district")]
    public string visit_district { get; set; }

    [XmlElement ("visit_amphure")]
    public string visit_amphure { get; set; }

    [XmlElement ("visit_province")]
    public string visit_province { get; set; }

    [XmlElement ("visit_identify_idx")]
    public int visit_identify_idx { get; set; }

    [XmlElement ("visit_identify_no")]
    public string visit_identify_no { get; set; }

    [XmlElement ("visit_phone_no")]
    public string visit_phone_no { get; set; }

    [XmlElement ("visit_card_no")]
    public string visit_card_no { get; set; }

    [XmlElement ("create_date")]
    public string create_date { get; set; }

    [XmlElement ("update_date")]
    public string update_date { get; set; }

    [XmlElement ("visit_card_ex_no")]
    public string visit_card_ex_no { get; set; }

    [XmlElement ("flag_photo")]
    public int flag_photo { get; set; }
}
#endregion doc_u1

#region  doc_u2
[Serializable]
public class vm_visitors_doc_detail_u2 {
    [XmlElement ("u2_idx")]
    public int u2_idx { get; set; }

    [XmlElement ("u0_idx")]
    public int u0_idx { get; set; }

    [XmlElement ("vehicle_type_idx")]
    public int vehicle_type_idx { get; set; }

    [XmlElement ("vehicle_plate_no")]
    public string vehicle_plate_no { get; set; }

    [XmlElement ("vehicle_plate_province")]
    public int vehicle_plate_province { get; set; }

    [XmlElement ("create_date")]
    public string create_date { get; set; }

    [XmlElement ("update_date")]
    public string update_date { get; set; }

    [XmlElement ("vehicle_plate_province_name")]
    public string vehicle_plate_province_name { get; set; }
}
#endregion doc_u2

#region  doc_u3
[Serializable]
public class vm_visitors_doc_detail_u3 {
    [XmlElement ("u3_idx")]
    public int u3_idx { get; set; }

    [XmlElement ("u0_idx")]
    public int u0_idx { get; set; }

    [XmlElement ("group_m0_idx")]
    public int group_m0_idx { get; set; }

    [XmlElement ("approve_status")]
    public int approve_status { get; set; }

    [XmlElement ("group_status")]
    public int group_status { get; set; }

    [XmlElement ("create_date")]
    public string create_date { get; set; }

    [XmlElement ("update_date")]
    public string update_date { get; set; }
}
#endregion doc_u3

#region doc_l0
[Serializable]
public class vm_visitors_doc_detail_l0 {
    [XmlElement ("l0_idx")]
    public int l0_idx { get; set; }

    [XmlElement ("u0_doc_idx")]
    public int u0_doc_idx { get; set; }

    [XmlElement ("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement ("u0_node_idx")]
    public int u0_node_idx { get; set; }

    [XmlElement ("u0_actor_idx")]
    public int u0_actor_idx { get; set; }

    [XmlElement ("m1_node_idx")]
    public int m1_node_idx { get; set; }

    [XmlElement ("remark")]
    public string remark { get; set; }

    [XmlElement ("create_date")]
    public string create_date { get; set; }

    [XmlElement ("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement ("node_name")]
    public string node_name { get; set; }

    [XmlElement ("actor_name")]
    public string actor_name { get; set; }

    [XmlElement ("decision_name")]
    public string decision_name { get; set; }
}
#endregion doc_l0

#region  view_doc_u3
[Serializable]
public class view_vm_visitors_doc_detail_u3 {
    [XmlElement ("u2_idx")]
    public int u2_idx { get; set; }

    [XmlElement ("u0_idx")]
    public int u0_idx { get; set; }

    [XmlElement ("group_m0_idx")]
    public int group_m0_idx { get; set; }

    [XmlElement ("approve_status")]
    public int approve_status { get; set; }

    [XmlElement ("group_status")]
    public int group_status { get; set; }

    [XmlElement ("rpos_idx")]
    public int rpos_idx { get; set; }
}
#endregion view_doc_u3

#region  vm_visitors_setperms_admin_m0
[Serializable]
public class vm_visitors_setperms_admin_detail_m0 {
    [XmlElement ("permission_idx")]
    public int permission_idx { get; set; }

    [XmlElement ("node_idx")]
    public int node_idx { get; set; }

    [XmlElement ("actor_idx")]
    public int actor_idx { get; set; }

    [XmlElement ("discrption")]
    public string discrption { get; set; }

    [XmlElement ("rpos_idx")]
    public int rpos_idx { get; set; }

    [XmlElement ("permission_status")]
    public int permission_status { get; set; }

    [XmlElement ("remark")]
    public string remark { get; set; }

    [XmlElement ("place_idx")]
    public int place_idx { get; set; }
}
#endregion vm_visitors_setperms_admin_m0

#region  vm_visitors_menu_m0
[Serializable]
public class vm_visitors_menu_detail_m0 {
    [XmlElement ("m0_idx")]
    public int m0_idx { get; set; }

    [XmlElement ("lb_menu")]
    public string lb_menu { get; set; }

    [XmlElement ("menu")]
    public string menu { get; set; }

    [XmlElement ("zcount")]
    public int zcount { get; set; }
}
#endregion vm_visitors_menu_m0

#region sendemail
[Serializable]
public class vm_visitors_sendemail_detail {
    [XmlElement ("operation_status_id")]
    public string operation_status_id { get; set; }

    [XmlElement ("u0_idx")]
    public int u0_idx { get; set; }

    [XmlElement ("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement ("visitor_type_idx")]
    public int visitor_type_idx { get; set; }

    [XmlElement ("visit_date")]
    public string visit_date { get; set; }

    [XmlElement ("visit_emp_idx")]
    public int visit_emp_idx { get; set; }

    [XmlElement ("flag_safety")]
    public int flag_safety { get; set; }

    [XmlElement ("safety_comment")]
    public string safety_comment { get; set; }

    [XmlElement ("flag_vehicle")]
    public int flag_vehicle { get; set; }

    [XmlElement ("node_idx")]
    public int node_idx { get; set; }

    [XmlElement ("actor_idx")]
    public int actor_idx { get; set; }

    [XmlElement ("doc_status")]
    public int doc_status { get; set; }

    [XmlElement ("create_date")]
    public string create_date { get; set; }

    [XmlElement ("update_date")]
    public string update_date { get; set; }

    [XmlElement ("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement ("visit_emp_name_th")]
    public string visit_emp_name_th { get; set; }

    [XmlElement ("decision_idx")]
    public int decision_idx { get; set; }

    [XmlElement ("visitor_type_name")]
    public string visitor_type_name { get; set; }

    [XmlElement ("vehicle_type_name")]
    public string vehicle_type_name { get; set; }

    [XmlElement ("node_name")]
    public string node_name { get; set; }

    [XmlElement ("actor_name")]
    public string actor_name { get; set; }

    [XmlElement ("doc_status_name")]
    public string doc_status_name { get; set; }

    [XmlElement ("mail_list")]
    public string mail_list { get; set; }

    [XmlElement ("emp_idx_approve1")]
    public int emp_idx_approve1 { get; set; }

    [XmlElement ("emp_approve1")]
    public string emp_approve1 { get; set; }

    [XmlElement ("emp_idx_approve2")]
    public int emp_idx_approve2 { get; set; }

    [XmlElement ("emp_approve2")]
    public string emp_approve2 { get; set; }

    //start teppanop

    [XmlElement ("zmode")]
    public string zmode { get; set; }

    [XmlElement ("rpos_idx")]
    public int rpos_idx { get; set; }

    [XmlElement ("emp_approve_email1")]
    public string emp_approve_email1 { get; set; }

    [XmlElement ("emp_approve_email2")]
    public string emp_approve_email2 { get; set; }

    [XmlElement ("zstatus")]
    public string zstatus { get; set; }

    //end teppanop

}
#endregion sendemail

#region vm_visitor_vehicle_special_l0
[Serializable]
public class vm_visitor_vehicle_special_detail_l0 {
    [XmlElement ("l0_idx")]
    public int l0_idx { get; set; }

    [XmlElement ("vehicle_m0_idx")]
    public int vehicle_m0_idx { get; set; }

    [XmlElement ("se_idx")]
    public int se_idx { get; set; }

    [XmlElement ("create_date")]
    public string create_date { get; set; }

    [XmlElement ("update_date")]
    public string update_date { get; set; }
}
#endregion vm_visitor_vehicle_special_l0

#region vm_visitors_security_detail_m0
[Serializable]
public class vm_visitors_security_detail_m0 {
    [XmlElement ("u0_idx")]
    public int u0_idx { get; set; }

    [XmlElement ("username")]
    public string username { get; set; }

    [XmlElement ("password")]
    public string password { get; set; }

    [XmlElement ("firstname")]
    public string firstname { get; set; }

    [XmlElement ("lastname")]
    public string lastname { get; set; }

    [XmlElement ("user_status")]
    public int user_status { get; set; }

    [XmlElement ("create_date")]
    public string create_date { get; set; }

    [XmlElement ("update_date")]
    public string update_date { get; set; }
}
#endregion vm_visitors_security_detail_m0

#region  board
[Serializable]
public class vm_visitors_board_detail {
    [XmlElement ("u2_idx")]
    public int u2_idx { get; set; }

    [XmlElement ("u0_idx")]
    public int u0_idx { get; set; }

    [XmlElement ("vehicle_plate_no")]
    public string vehicle_plate_no { get; set; }

    [XmlElement ("visit_date")]
    public string visit_date { get; set; }

    [XmlElement ("zvisit_date")]
    public string zvisit_date { get; set; }

    [XmlElement ("zvisit_time")]
    public string zvisit_time { get; set; }

    [XmlElement ("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement ("visit_title")]
    public string visit_title { get; set; }

    [XmlElement ("visitor_type_name")]
    public string visitor_type_name { get; set; }

    [XmlElement ("zstatus")]
    public string zstatus { get; set; }

    [XmlElement ("status_flag")]
    public string status_flag { get; set; }
}
#endregion board

#region report search
[Serializable]
public class vm_visitors_report_search_detail {
    [XmlElement ("s_date_type")]
    public string s_date_type { get; set; }

    [XmlElement ("s_report_type")]
    public string s_report_type { get; set; }

    [XmlElement ("s_start_date")]
    public string s_start_date { get; set; }

    [XmlElement ("s_end_date")]
    public string s_end_date { get; set; }

    [XmlElement ("s_report_org")]
    public string s_report_org { get; set; }

    [XmlElement ("s_report_dept")]
    public string s_report_dept { get; set; }

    [XmlElement ("s_report_sec")]
    public string s_report_sec { get; set; }

    [XmlElement ("s_report_emp_idx")]
    public string s_report_emp_idx { get; set; }

    [XmlElement ("s_visitor_type")]
    public string s_visitor_type { get; set; }

    [XmlElement ("s_group_idx")]
    public string s_group_idx { get; set; }

    [XmlElement ("s_group_by")]
    public string s_group_by { get; set; }

    [XmlElement ("s_order_by")]
    public string s_order_by { get; set; }
}
#endregion report search